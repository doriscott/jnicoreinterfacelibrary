/****************************************************************************/
/*
 * Copyright (C) 2000-2013 ZyXEL Communications, Corp.
 * All Rights Reserved.
 *
 * ZyXEL Confidential;
 * Protected as an unpublished work, treated as confidential,
 * and hold in trust and in strict confidence by receiving party.
 * Only the employees who need to know such ZyXEL confidential information
 * to carry out the purpose granted under NDA are allowed to access.
 *
 * The computer program listings, specifications and documentation
 * herein are the property of ZyXEL Communications, Corp. and shall
 * not be reproduced, copied, disclosed, or used in whole or in part
 * for any reason without the prior express written permission of
 * ZyXEL Communications, Corp.
 *
 */
/****************************************************************************/
//
//  tr181tree.h
//
//  Created by Kevin on 13/7/23.
//  Copyright (c) 2013年 pan star. All rights reserved.
//

#ifndef __OneConnectTree__
#define __OneConnectTree__

#include <iostream>
//#include "treeNode.h"
#include "tr181Tree.h"

class oneConnectTree : public tr181Tree{
public:
    oneConnectTree();
//    ~oneConnectTree();
    static treeNode * CreatHostWithIndex(int index);
    static treeNode * CreatRouteHopsWithIndex(int index);
    static treeNode * CreatIPv4ForwardingWithIndex(int index);
    static treeNode * CreatFWUpgradeWithIndex(int index);
    static treeNode * CreateWiFiAutoConfigApproveDataWithIndex(int index);
};
#endif /* defined(_tr181tree_) */
