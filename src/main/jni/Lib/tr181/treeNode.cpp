/****************************************************************************/
/*
* Copyright (C) 2000-2013 ZyXEL Communications, Corp.
* All Rights Reserved.
*
* ZyXEL Confidential;
* Protected as an unpublished work, treated as confidential,
* and hold in trust and in strict confidence by receiving party.
* Only the employees who need to know such ZyXEL confidential information
* to carry out the purpose granted under NDA are allowed to access.
*
* The computer program listings, specifications and documentation
* herein are the property of ZyXEL Communications, Corp. and shall
* not be reproduced, copied, disclosed, or used in whole or in part
* for any reason without the prior express written permission of
* ZyXEL Communications, Corp.
*
*/
/****************************************************************************/
//
//  treeNode.cpp
//
//  Created by Kevin on 13/7/23.
//  Copyright (c) 2013年 pan star. All rights reserved.
//

#include "treeNode.h"

treeNode::treeNode(){
    parent = NULL;
}
treeNode::~treeNode(){
    //cout<<"Destory:"<<key<<endl;
}
void treeNode::addChild(treeNode *child){
    child->parent = this;
    children.push_back(*child);
    
}

bool treeNode::hasChildren(){
    return !children.empty();
}

int treeNode::deep(){
    return parent==NULL ? 0 : parent->deep()+1;
}

int treeNode::childrenCount(){
    return children.empty() ? 0 : (int)children.size();
}

treeNode *treeNodeWithKey(string key, DATA_TYPE _type){
    treeNode *tmp = new treeNode;
    tmp->key = key;
    tmp->type = _type;
    return tmp;
}

treeNode *findNodeByKey(string _key, treeNode *node){
    //if (_key == node->key) {
    //int flag =_key.compare(node->key);
    if (_key == node->key) {
        
        return node;
    }else if (node->hasChildren()){
        for (int i = 0 ; i < node->children.size(); i++) {
            treeNode * a = findNodeByKey(_key, &node->children.at(i));
            if (a != NULL) {
                return a;
            }
        }
        
    }
    return NULL;
}
void removeNodeByKey(string key, treeNode *node){
    treeNode *temp = node;
    treeNode *tmp;
    tmp = temp;
    temp = findNodeByKey(key, tmp);
    while (temp!=NULL) {
        //temp->parent->children;
        for (int i =0; i < temp->parent->children.size(); i++) {
            if (temp->parent->children.at(i).key==key) {
                temp->parent->children.erase(temp->parent->children.begin()+i);
            }
        }
        temp = findNodeByKey(key, tmp);
    }
    //return node;
}
void getNodes(treeNode *root, vector<treeNode>array){
    
}
void nodesAddParent(treeNode *node){
    if (node->children.size()) {
        for (int i = 0; i < node->children.size(); i++) {
            node->children.at(i).parent = node;
            nodesAddParent(&node->children.at(i));
        }
    }else{
        return;
    }
}
void releaseNodes(treeNode *node){
    if (!node->children.empty()) {
        for (int i = 0; i < node->children.size(); i++) {
            //treeNode *t = node->children.at(i);
            releaseNodes(&node->children.at(i));
            //cout<<"Release node :" + node->key<<endl;
        }
        node->children.clear();
        node = NULL;
    }else{
        //cout<<"Release node :" + node->key<<endl;
        node = NULL;
        //delete node;
    }
}
