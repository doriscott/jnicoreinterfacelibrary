/****************************************************************************/
/*
 * Copyright (C) 2000-2013 ZyXEL Communications, Corp.
 * All Rights Reserved.
 *
 * ZyXEL Confidential;
 * Protected as an unpublished work, treated as confidential,
 * and hold in trust and in strict confidence by receiving party.
 * Only the employees who need to know such ZyXEL confidential information
 * to carry out the purpose granted under NDA are allowed to access.
 *
 * The computer program listings, specifications and documentation
 * herein are the property of ZyXEL Communications, Corp. and shall
 * not be reproduced, copied, disclosed, or used in whole or in part
 * for any reason without the prior express written permission of
 * ZyXEL Communications, Corp.
 *
 */
/****************************************************************************/
//
//  tr181tree.h
//
//  Created by Kevin on 13/7/23.
//  Copyright (c) 2013年 pan star. All rights reserved.
//

#ifndef __tr181tree__
#define __tr181tree__

// Device
#define DM_DEVICE					"Device"

// DeviceInfo
#define DM_DEVICEINFO				"DeviceInfo"
#define DM_MODELNAME				"ModelName"
#define DM_DEVICEMODE               "DeviceMode"
#define DM_WIFICONFIGFLAG           "WIFIConfigFlag"
#define DM_GATEWAY_AUTOCONFIG_ENABLE "GatewayAPautoconfigEnable"
#define DM_LANCONFIGSECURITY		"LANConfigSecurity"
#define DM_CONFIGPASSWORD			"ConfigPassword"
#define DM_SOFTWAREVERSION			"SoftwareVersion"
#define DM_SOFTWAREUPDATEOTA		"X_ZyXEL_SoftwareUpdateOTA"
#define DM_CHECKSOFTWARE			"CheckSoftwareVersion"
#define DM_LATESTSOFTWARE			"LatestSoftwareVersion"
#define DM_UPDATESOFTWARE			"UpdateSoftware"
#define DM_UPDATESOFTWARESTATUS		"UpdateSoftwareStatus"
#define DM_FWUPGRADESTATUS          "FWUpgradeState"
#define DM_HASNEWSOFTWARE           "HasNewSoftware"
#define DM_LATESTSOFTWAREDATE		"Newfirmwaredate"
//Kevin 2014/1/16
#define DM_SN          				"SerialNumber"     //OneConnect
//Kevin end

//Sharon add
#define DM_WANSTATUS                "X_ZyXEL_WanStatus"
#define DM_WANIPADDRESS             "X_ZyXEL_Wan_IPAddress"
#define DM_WANGATEWAY               "X_ZyXEL_Wan_Gateway"
#define DM_WANDNS                   "X_ZyXEL_Wan_DNS"
#define DM_WANPHYRATE               "X_ZyXEL_Wan_PhyRate"

//WiFi
#define DM_WIFI						"WiFi"
#define DM_WIFI_CHECK_BAND			"CheckFreqBand" //2.3
#define DM_ENABLE					"Enable"
#define DM_STATUS					"Status"

//Radio
#define DM_RADIONUM					"RadioNumberOfEntries"
#define DM_RADIO					"Radio"
#define DM_MAXBITRATE			    "MaxBitRate"
#define DM_FREQUENCYBAND			"OperatingFrequencyBand"
#define DM_STANDARDS				"OperatingStandards"
#define DM_AUTOCHSUPPORTED			"AutoChannelSupported"
#define DM_AUTOCHENABLE				"AutoChannelEnable"
#define DM_CHANNEL					"Channel"
#define DM_TRANSMITPWSUPPORTED		"TransmitPowerSupported"
#define DM_TRANSMITPW				"TransmitPower"

//SSID
#define DM_SSIDNUM					"SSIDNumberOfEntries"
#define DM_SSID						"SSID"

//AccessPoint
#define DM_APNUM					"AccessNumberOfEntries"
#define DM_AP						"AccessPoint"
#define DM_SECURITY					"Security"
#define DM_MODESUPPORTED			"ModesSupported"
#define DM_MODEENABLED				"ModeEnabled"
#define DM_PRESHAREKEY				"PreShareKey"
#define DM_ASSOCIATEDNUM			"AssociatedNumberOfEntries"
#define DM_ASSOCIATEDDEVICE			"AssociatedDevice"
#define DM_MAC						"MACAddress"

//GuestAP
#define DM_GUESTAP					"X_ZyXEL_GuestAP"

//Device.WiFi.X_ZyXEL_GuestAP.Timer
#define DM_TIMER            "Timer"
#define DM_TIMER_HOURS      "Hours"
#define DM_TIMER_MINUTES    "Minutes"

//Host
#define DM_HOSTS					"Hosts"
#define DM_HOST						"Host"
#define DM_HOSTNUM					"HostNumberOfEntries"
#define DM_PHYSADDRESS				"PhysAddress"
#define DM_HOSTNAME					"HostName"
#define DM_ACTIVE					"Active"
#define DM_HOSTTYPE					"X_ZyXEL_HostType"
#define DM_BLOCKING					"X_ZyXEL_Blocking"
#define DM_IPAddress						"IPAddress"
#define DM_CONTROLLABLEMASK			"X_ZyXEL_L2Device_ControllableMask"
#define DM_CONTROLMASK				"X_ZyXEL_L2Device_ControlMask"
#define DM_ADDRULE					"X_ZyXEL_AddPcpRule"
#define DM_ENDDEVCTR_BLOCK			"EndDevCtrl_Blocking"
#define DM_ENDDEVCTR_UNBLOCK		"EndDevCtrl_UnBlocking"

#define DM_L2CTR_REBOOT				"L2DevCtrl_Reboot"     //OneConnect
#define DM_L2CTR_WIFION				"L2DevCtrl_WiFiON"     //OneConnect
#define DM_L2CTR_WIFIOFF			"L2DevCtrl_WiFiOff"    //OneConnect
#define DM_L2STA_WIFI               "X_ZyXEL_WiFiStatus"   //OneConnect

#define DM_X_ZYXEL_MANUFACTURER     "X_ZyXEL_Manufacturer"
#define DM_X_ZYXEL_RSSI             "X_ZyXEL_RSSI"
#define DM_X_ZYXEL_BAND             "X_ZyXEL_Band"
#define DM_X_ZYXEL_WIFILINKRATE24G  "X_ZyXEL_WiFiLinkRate_24G"
#define DM_X_ZYXEL_WIFILINKRATE5G   "X_ZyXEL_WiFiLinkRate_5G"
#define DM_X_ZYXEL_CHANNEL24G       "X_ZyXEL_Channel_24G"
#define DM_X_ZYXEL_CHANNEL5G        "X_ZyXEL_Channel_5G"


//(value 5~1 ,(5,4) --->very  good ,(3,2)---->good ,(1)--->bad)
#define DM_WIFI_SIGNALSTRENGTH      "SignalStrength"       //OneConnect

//(0 : un-configure ,1:configured)
#define DM_L2STA_AUTOCONFIG         "X_ZyXEL_WiFiSyncStatus" //OneConnect
#define DM_L2ENA_AUTOCONFIG         "X_ZyXEL_AutoCfgEnable"  //OneConnect

//Sharon add
//L2Device,Client,(Alljoyn)
#define DM_CAPABILITYTYPE           "X_ZyXEL_CapabilityType"    //OneConnect
//WiFi,Others
#define DM_CONNECTIONTYPE           "X_ZyXEL_ConnectionType"    //OneConnect
//writable, a friendly device name like Powerline not PLA4231
#define DM_DEVICENAME_USER_DEFINED  "X_ZyXEL_DeviceName"        //OneConnect
#define DM_RHYRATE                  "X_ZyXEL_PhyRate"           //OneConnect
#define DM_SOFTWAREVERSION_HOST     "X_ZyXEL_SoftwareVersion"   //OneConnect
#define DM_NEIGHBOR                 "X_ZyXEL_Neighbor"          //OneConnect
#define DM_DL_Rate                 "X_ZyXEL_DL_Rate"          //OneConnect
#define DM_UL_Rate                 "X_ZyXEL_UL_Rate"          //OneConnect

#define DM_CONN_GUEST				"X_ZyXEL_Conn_Guest"

//X_ZyXEL_Ext
#define DM_ZYXELEXT					"X_ZyXEL_Ext"

//ParentalControl
#define DM_PARENTALCONTROL			"ParentalControl"
#define DM_PARENTALNUM				"ParentalControlNumberOfEntries"
#define DM_HOSTMAC					"HostMAC"
#define DM_PCPNAME					"PcpName"
#define DM_SCHEDULING				"Scheduling"
#define DM_DAYS						"Days"
#define DM_STARTHOUR				"TimeStartHour"
#define DM_STARTMIN					"TimeStartMin"
#define DM_STOPHOUR					"TimeStopHour"
#define DM_STOPMIN					"TimeStopMin"
#define DM_DELETE					"Delete"

//AppInfo
#define DM_SYSTEMINFO				"SystemInfo"
#define DM_SYSTEMNAME				"SystemName"
#define DM_APPINFO					"AppInfo"
#define DM_MAGICNUM					"MagicNum"
#define DM_APPVERSION				"AppVersion"
#define DM_FEATURELISTNUM			"FeatureListNumberOfEntries"
#define DM_FEATURELIST				"FeatureList"
//#define DM_DEVICEINFO               "DeviceInfo"
#define DM_GATEWAYINFO              "GatewayInfo"
#define DM_FLOWINFO                 "FlowInfo"
#define DM_LOGIN                    "Login"
#define DM_NETTOPOLOGY              "NetTopology"
#define DM_DEVICECTRL               "DeviceCtrl"
#define DM_L2DEVICECTRL             "L2DeviceCtrl"
#define DM_DEVICON                  "DevIcon"
#define DM_GUESTWIFI                "GuestWiFi"
//#define DM_WIFIAUTOCFG              "WiFiAutoCfg"
#define DM_WIFIONOFF                "WiFiOnOff"
#define DM_SPEEDTEST                "SpeedTest"
#define DM_DIAGONSTIC               "Diagnostic"
#define DM_CLOUDSERVICE             "CloudService"
#define DM_APPINFO_FWUPGRADE        "FwUpgrade"
#define DM_LEDONOFF                 "LEDOnOff"
#define DM_GUESTWIFIGROUP           "GuestWiFiGroup"

#define DM_NAME						"Name"
#define DM_VERSION					"Version"

//Battery
#define DM_BATTERYSTATUS			"BatteryStatus"
#define DM_CHARGESTATUS				"ChargeStat"
#define DM_TOTALCAPACITY			"TotalCapacity"
#define DM_REMAINCAPACITY			"RemainCapacity"

//3G
#define DM_ZYXEL3G					"X_ZyXEL_3GPP"
#define DM_INTERFACENUM				"InterfaceNumberOfEntries"
#define DM_INTERFACE				"Interface"
#define DM_PPPAUTH					"PPPAuthenticationProtocol"
#define DM_APNAME					"AccessPointName"
#define DM_USERNAME					"Username"
#define DM_PASSWORD					"Password"
#define DM_CELLULARMODE				"CellularMode"
#define DM_SIGNALSTRENGTH			"SignalStrength"
#define DM_SERVICEPROVIDER			"ServiceProvider"
#define DM_STATS					"Stats"
#define DM_BYTESRECEIVED			"BytesReceived"
#define DM_BYTESSENT				"BytesSent"
#define DM_DATAPLAN					"DataPlanManagement"
#define DM_RESETCOUNTER				"ResetCounter"
#define DM_LASTRESETDATE			"LastResetDate"
#define DM_MONTHLYRESETENABLE		"MonthlyResetEnable"
#define DM_MONTHLYRESETDAY			"MonthlyResetDay"
#define DM_MONTHLYLIMIT				"MonthlyLimit"

//Notify
#define DM_NOTIFY					"Notify"
#define DM_APS						"aps"
#define DM_TIMESTAMP				"timestamp"
#define DM_ALERT					"alert"

//Device.X_ZyXEL_Ext.WiFiAutoCfg                                OneConnect
#define DM_WIFI_AUTOCONFIG          "WiFiAutoCfg"

#define DM_WIFI_WIFICONFIGFLAG      "WIFIConfigFlag"

//Device.X_ZyXEL_Ext.WiFiAutoCfgByTime                          OneConnect
#define DM_WIFI_AUTOCONFIGBYTIME    "WiFiAutoCfgByTime"
#define DM_TIME                     "Time"

//Device.X_ZyXEL_Ext.L2DevCtrl_AutoCfg                          OneConnect
#define DM_L2DEV_CTR_AUTOCONFIG     "L2DevCtrl_AutoCfg"
#define DM_HOST_MAC                 "HostMAC"
#define DM_CTR_OP                   "ControlOption"
#define DM_CTR_FW_STATE             "NewFWUpgradeState"
#define DM_CTR_FW_SUCCESS           "NewFWUpgradeSuccess"

//Device.X_ZyXEL_Ext.L2DevCtrl_FWCfg                            OneConnect
#define DM_L2DEV_CTR_FWCONFIG       "L2DevCtrl_FWCfg"

//Device.X_ZyXEL_Ext.DevNameChange                              OneConnect
#define DM_CHANGE_DEV_NAME          "DevNameChange"

//Device.X_ZyXEL_Ext.HostTypeChange                             OneConnect
#define DM_CHANGE_HOST_TYPE         "HostTypeChange"
#define DM_HOST_TYPE                "HostType"

//Device.IP.Diagnostics.TraceRoute                              OneConnect
#define DM_IP                       "IP"
#define DM_DIAGNOSTICS              "Diagnostics"
#define DM_DIAGNOSTICS_STATE        "DiagnosticsState"

#define DM_TRACEROUTER              "TraceRoute"
#define DM_NUMOFTRIES               "NumberOfTries"
#define DM_TIMEOUT                  "Timeout"
#define DM_MAXHOPCOUNT              "MaxHopCount"
#define DM_NUMOFROUTERHOPS          "RouteHopsNumberOfEntries"

//Device.IP.Diagnostics.TraceRoute.RouteHops.i                  OneConnect
#define DM_ROUTERHOPS               "RouteHops"
#define DM_HOSTADDRESS              "HostAddress"
#define DM_RTTIMES                  "RTTimes"

//Device.X_ZyXEL_Ext.Router
#define DM_ROUTER                  "Router"
#define DM_NUMOFIPV4               "IPv4ForwardingNumberOfEntries"

//Device.X_ZyXEL_Ext.Router.IPv4Forwarding.i                    OneConnect
#define DM_IPV4_FORWARDING         "IPv4Forwarding"
#define DM_DES_IP                  "DestIPAddress"
#define DM_DES_SUBNETMASK          "DestSubnetMask"
#define DM_GATEWAY_IP              "GatewayIPAddress"
#define DM_ZYXEL_INTERFACE         "X_ZyXEL_Interface"

//Device.X_ZyXEL_Ext.DeviceInfo                                 OneConnect
#define DM_UP_TIME                  "UpTime"
#define DM_SYSTEM_TIME              "SystemTime"
//Device.X_ZyXEL_Ext.DeviceInfo.WanInfo                         OneConnect
#define DM_WANINFO                  "WanInfo"
#define DM_WAN_STATUS               "WanStatus"
#define DM_WAN_IP                   "Wan_IPAddress"
#define DM_WAN_PHYRATE              "Wan_PhyRate"
#define DM_WAN_MAC                  "Wan_Mac"
#define DM_WAN_RX                   "Wan_RX_Rate"
#define DM_WAN_TX                   "Wan_TX_Rate"
//Device.X_ZyXEL_Ext.DeviceInfo.ProcessStatus                   OneConnect
#define DM_PROCESS_STATUS           "ProcessStatus"
#define DM_CPU_USAGE                "CPUUsage"

//Device.X_ZyXEL_Ext.Reboot                                     OneConnect
#define DM_REBOOT                   "Reboot"

//Device.X_ZyXEL_Ext.FWUpgrade                                  OneConnect
#define DM_FWTALNUM                 "FWNumberOfEntries"
#define DM_FWUPGRADE                "FWUpgrade"
#define DM_FWUPGRADE_MODELNAME      "ModelName"
#define DM_FWUPGRADE_MAC            "MACAddress"
#define DM_FWUPGRADE_NEWFW          "Newfirmware"
#define DM_FWUPGRADE_NEWSTATE       "NewFWUpgradeState"
#define DM_FWUPGRADE_NEWVERSION     "NewFWUpgradeVersion"
#define DM_FWUPGRADE_NEWDATE        "Newfirmwaredate"

//Device.X_ZyXEL_Ext.SpeedTestInfo                              OneConnect
#define DM_SPEEDTESTINFO            "SpeedTestInfo"
#define DM_SP_URL                   "URL"
#define DM_SP_ACTION_TYPE           "ActionType"
#define DM_SP_END                   "End"
#define DM_SP_DL_RATE               "DownloadSpeed"
#define DM_SP_UL_RATE               "UploadSpeed"

//Device.X_ZyXEL_Ext.Description
#define DM_DESCRIPTION              "Description"
#define DM_GATEWAY_DESCRIPTION      "GatewayDescription"

//Device.X_ZyXEL_Ext.GwSpeedTestInfo
#define DM_GWSPEEDTESTINFO			"GwSpeedTestInfo"
#define DM_GWSPEED_SERVER_PORT		"ServerPort"
#define DM_GWSPEED_ENABLE			"Enable"

//Device.X_ZyXEL_Ext.WiFiconfigValue
#define DM_GATEWAY_RSSI				"WiFiconfigValue"
#define DM_RSSI						"RSSI"

//Device.X_ZyXEL_Ext.GatewayInfo
#define DM_GATEWAY_HOSTNAME         "GatewayHostname"
#define DM_GATEWAY_HOSTMAC          "GatewayHostMAC"
#define DM_GATEWAY_AUTOCONFIG       "GatewayAutoCfg"


//Device.X_ZyXEL_Ext.SmartDevCtrl
#define DM_SMARTDEV_CTR				"SmartDevCtrl"
#define DM_SMARTDEV_CTR_PROTOCOL	"Protocol"
#define DM_SMARTDEV_CTR_ID			"ID"
#define DM_SMARTDEV_CTR_ACTION		"Action"

#define DM_AUTO_CFG_APPROVE_NUM     "WiFiAutoCfgApproveNum"
#define DM_WIFI_AUTO_CFG_APPROVE    "X_ZyXEL_WIFI_Auto_Cfg_Approve"
#define DM_AUTO_CFG_APPROVE         "WiFiAutoCfgApprove"
#define DM_AUTO_CFG_APPROVE_DATA    "WiFiAutoCfgApproveData"
#define DM_AUTO_CFG_APPROVE_MAC     "MACAddress"
#define DM_OPTION                   "Option"

#include <iostream>
#include "treeNode.h"
/**
 *	@brief The LTERouter data structure.
 */
class tr181Tree : public treeNode{
public:
	tr181Tree();
	~tr181Tree();
	static treeNode * CreatHostWithIndex(int index);
	static treeNode * CreatInterfaceWithIndex(int index);
	static treeNode * CreatRadioWithIndex(int index);
	static treeNode * CreatAccessPointWithIndex(int index);
	static treeNode * CreatSSIDWithIndex(int index);

	//SCOTT TSENG ADD 2013-11-15
	static treeNode * CreatParentalControlWithIndex(int index);
	//SCOTT TSENG ADD END
};
#endif /* defined(_tr181tree_) */
