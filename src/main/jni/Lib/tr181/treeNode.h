/****************************************************************************/
/*
* Copyright (C) 2000-2013 ZyXEL Communications, Corp.
* All Rights Reserved.
*
* ZyXEL Confidential;
* Protected as an unpublished work, treated as confidential,
* and hold in trust and in strict confidence by receiving party.
* Only the employees who need to know such ZyXEL confidential information
* to carry out the purpose granted under NDA are allowed to access.
*
* The computer program listings, specifications and documentation
* herein are the property of ZyXEL Communications, Corp. and shall
* not be reproduced, copied, disclosed, or used in whole or in part
* for any reason without the prior express written permission of
* ZyXEL Communications, Corp.
*
*/
/****************************************************************************/
//
//  myNode.h
//
//  Created by Kevin on 13/7/23.
//  Copyright (c) 2013年 pan star. All rights reserved.
//

#ifndef __treeNode__
#define __treeNode__

#include <iostream>
#include <vector>
#include "../../ThirdParty/json/json.h"

using namespace std;

/**
 *	@brief Determine the data type such as int, string or object.
 */
enum DATA_TYPE{
    isInt=0,
    isString,
    isObject,
};
/**
 *	@brief Using the node can create data muti-tree.
 */
class treeNode{
public:
    string key;
    Json::Value data;
    DATA_TYPE type;
    bool expanded;
    treeNode *parent;
    vector<treeNode> children;
        
    /**
     *	@brief Return the node's degree in tree.
     */
    int deep();
    /**
     *	@brief Detemine the node has children or not.
     */
    bool hasChildren();
    /**
     *	@brief Add children node.
     *	@param[in] New children node.
     */
    void addChild(treeNode *children);
    /**
     *	@brief Count the total children of tree node.
     */
    int childrenCount();
    
    treeNode();
    ~treeNode();
    
};
/**
 *	@brief Creat tree node by key
 *	@param[in] The key word.
 *	@param[in] The data type of node.
 *  @return	The new node.
 */
treeNode *treeNodeWithKey(string key, DATA_TYPE _type);
/**
 *	@brief Find the tree node by key.
 *	@param[in] The key word.
 *  @param[in] The begin of node to search.
 *  @return The node you searched, if doesn't exist, return null.
 */
treeNode *findNodeByKey(string key, treeNode *node);
void removeNodeByKey(string key, treeNode *node);
void nodesAddParent(treeNode *node);
void releaseNodes(treeNode *node);
#endif /* defined(__treeNode__) */
