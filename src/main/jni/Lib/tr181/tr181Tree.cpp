/****************************************************************************/
/*
* Copyright (C) 2000-2013 ZyXEL Communications, Corp.
* All Rights Reserved.
*
* ZyXEL Confidential;
* Protected as an unpublished work, treated as confidential,
* and hold in trust and in strict confidence by receiving party.
* Only the employees who need to know such ZyXEL confidential information
* to carry out the purpose granted under NDA are allowed to access.
*
* The computer program listings, specifications and documentation
* herein are the property of ZyXEL Communications, Corp. and shall
* not be reproduced, copied, disclosed, or used in whole or in part
* for any reason without the prior express written permission of
* ZyXEL Communications, Corp.
*
*/
/****************************************************************************/
//
//  tr181tree.cpp
//
//  Created by Kevin on 13/7/23.
//  Copyright (c) 2013年 pan star. All rights reserved.
//

#include "tr181Tree.h"
//#include "Interface.h"
//#include "Host.h"
//#include "Radio.h"
//#include "AccessPoint.h"
#include <sstream>
//#include <android/log.h>

#define  LOG_TAG    "tr181Tree"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

tr181Tree::tr181Tree(){
    key = DM_DEVICE;
    parent = NULL;
    
    this->type = isObject;
    
    //string a = "X_ZyXEL_3GPP";
    addChild(treeNodeWithKey(DM_LANCONFIGSECURITY,isObject));
    addChild(treeNodeWithKey(DM_DEVICEINFO,isObject));
    addChild(treeNodeWithKey(DM_ZYXEL3G,isObject));
    addChild(treeNodeWithKey(DM_ZYXELEXT,isObject));
    addChild(treeNodeWithKey(DM_HOSTS,isObject));
    addChild(treeNodeWithKey(DM_WIFI,isObject));
//----------------LANConfigSecurity
    findNodeByKey(DM_LANCONFIGSECURITY, this)->addChild(treeNodeWithKey(DM_CONFIGPASSWORD,isString));
//----------------DeviceInfo
    findNodeByKey(DM_DEVICEINFO, this)->addChild(treeNodeWithKey(DM_MODELNAME,isString));

    // SCOTT TSENG ADD 2013-11-15
    findNodeByKey(DM_DEVICEINFO, this)->addChild(treeNodeWithKey(DM_SOFTWAREVERSION, isString));
    findNodeByKey(DM_DEVICEINFO, this)->addChild(treeNodeWithKey(DM_SOFTWAREUPDATEOTA, isObject));

    //findNodeByKey(DM_SOFTWAREUPDATEOTA, this)->addChild(treeNodeWithKey(DM_CHECKSOFTWARE, isString)); //Sharon modified
    findNodeByKey(DM_SOFTWAREUPDATEOTA, this)->addChild(treeNodeWithKey(DM_LATESTSOFTWARE, isString));
    findNodeByKey(DM_SOFTWAREUPDATEOTA, this)->addChild(treeNodeWithKey(DM_UPDATESOFTWARE, isString));
    findNodeByKey(DM_SOFTWAREUPDATEOTA, this)->addChild(treeNodeWithKey(DM_UPDATESOFTWARESTATUS, isString));
    // SCOTT TSENG ADD END
    findNodeByKey(DM_SOFTWAREUPDATEOTA, this)->addChild(treeNodeWithKey(DM_FWUPGRADESTATUS, isInt));
    findNodeByKey(DM_SOFTWAREUPDATEOTA, this)->addChild(treeNodeWithKey(DM_HASNEWSOFTWARE, isInt));
    findNodeByKey(DM_SOFTWAREUPDATEOTA, this)->addChild(treeNodeWithKey(DM_LATESTSOFTWAREDATE, isString));

//----------------X_ZyXEL_3GPP
    findNodeByKey(DM_ZYXEL3G, this)->addChild(treeNodeWithKey(DM_INTERFACENUM,isInt));
    findNodeByKey(DM_ZYXEL3G, this)->addChild(treeNodeWithKey(DM_INTERFACE,isObject));
    
    findNodeByKey(DM_INTERFACE, this)->addChild(CreatInterfaceWithIndex(1));
/*
//----------------X_ZyXEL_3GPP.Interface
    findNodeByKey("Interface", this)->addChild(treeNodeWithKey("i1",isObject));
    findNodeByKey("Interface", this)->addChild(treeNodeWithKey("i2",isObject));

//----------------X_ZyXEL_3GPP.Interface.i1
    findNodeByKey("i1", this)->addChild(treeNodeWithKey("Password",isString));
    findNodeByKey("i1", this)->addChild(treeNodeWithKey("Username",isString));
    findNodeByKey("i1", this)->addChild(treeNodeWithKey("PPPAuthenticationProtocol",isString));
    findNodeByKey("i1", this)->addChild(treeNodeWithKey("AccessPointName",isString));
    findNodeByKey("i1", this)->addChild(treeNodeWithKey("ServiceProvider",isString));
    findNodeByKey("i1", this)->addChild(treeNodeWithKey("SignalStrength",isInt));
    findNodeByKey("i1", this)->addChild(treeNodeWithKey("CellularMode",isString));
    findNodeByKey("i1", this)->addChild(treeNodeWithKey(Stats,isObject));
    //------------X_ZyXEL_3GPP.Interface.i2
    findNodeByKey("i2", this)->addChild(treeNodeWithKey("Password",isString));
    findNodeByKey("i2", this)->addChild(treeNodeWithKey("Username",isString));
    findNodeByKey("i2", this)->addChild(treeNodeWithKey("PPPAuthenticationProtocol",isString));
    findNodeByKey("i2", this)->addChild(treeNodeWithKey("AccessPointName",isString));
    findNodeByKey("i2", this)->addChild(treeNodeWithKey("ServiceProvider",isString));
    findNodeByKey("i2", this)->addChild(treeNodeWithKey("SingnalStrength",isInt));
    findNodeByKey("i2", this)->addChild(treeNodeWithKey("CellularMode",isString));
    findNodeByKey("i2", this)->addChild(treeNodeWithKey(Stats,isObject));
//----------------X_ZyXEL_3GPP.Interface.i1.Stats    
    findNodeByKey(Stats, this)->addChild(treeNodeWithKey("BytesReceived",isInt));
    findNodeByKey(Stats, this)->addChild(treeNodeWithKey("BytesSent",isInt));
    findNodeByKey(Stats, this)->addChild(treeNodeWithKey(DataPlan,isObject));
//----------------X_ZyXEL_3GPP.Interface.i1.Stats.DataPlanManagement
    findNodeByKey(DataPlan, this)->addChild(treeNodeWithKey("Enable",isString));
    findNodeByKey(DataPlan, this)->addChild(treeNodeWithKey("MonthlyLimit",isInt));
    findNodeByKey(DataPlan, this)->addChild(treeNodeWithKey("MonthlyResetDay",isInt));
    findNodeByKey(DataPlan, this)->addChild(treeNodeWithKey("MonthlyResetEnable",isString));
    findNodeByKey(DataPlan, this)->addChild(treeNodeWithKey("LastResetDate",isString));
    findNodeByKey(DataPlan, this)->addChild(treeNodeWithKey("ResetCounter",isInt));
*/
//----------------X_ZyXEL_Ext
    findNodeByKey(DM_ZYXELEXT, this)->addChild(treeNodeWithKey(DM_BATTERYSTATUS,isObject));
    findNodeByKey(DM_ZYXELEXT, this)->addChild(treeNodeWithKey(DM_SYSTEMINFO,isObject));
    findNodeByKey(DM_ZYXELEXT, this)->addChild(treeNodeWithKey(DM_APPINFO,isObject));
    findNodeByKey(DM_ZYXELEXT, this)->addChild(treeNodeWithKey(DM_NOTIFY,isObject));

    //SCOTT TSENG ADD 2013-11-15
    findNodeByKey(DM_ZYXELEXT, this)->addChild(treeNodeWithKey(DM_PARENTALNUM,isInt));
    findNodeByKey(DM_ZYXELEXT, this)->addChild(treeNodeWithKey(DM_PARENTALCONTROL,isObject));
    //SCOTT TSENG ADD END
    
    // SCOTT TSENG ADD 2014-01-09
    findNodeByKey(DM_ZYXELEXT, this)->addChild(treeNodeWithKey(DM_ENDDEVCTR_BLOCK,isObject));
    findNodeByKey(DM_ZYXELEXT, this)->addChild(treeNodeWithKey(DM_ENDDEVCTR_UNBLOCK,isObject));
    findNodeByKey(DM_ZYXELEXT, this)->addChild(treeNodeWithKey(DM_L2CTR_REBOOT,isObject));
    findNodeByKey(DM_ZYXELEXT, this)->addChild(treeNodeWithKey(DM_L2CTR_WIFION,isObject));
    findNodeByKey(DM_ZYXELEXT, this)->addChild(treeNodeWithKey(DM_L2CTR_WIFIOFF,isObject));

    findNodeByKey(DM_ENDDEVCTR_BLOCK, this)->addChild(treeNodeWithKey(DM_HOSTMAC, isString));
    findNodeByKey(DM_ENDDEVCTR_UNBLOCK, this)->addChild(treeNodeWithKey(DM_HOSTMAC, isString));
    findNodeByKey(DM_L2CTR_REBOOT, this)->addChild(treeNodeWithKey(DM_HOSTMAC, isString));
    findNodeByKey(DM_L2CTR_WIFION, this)->addChild(treeNodeWithKey(DM_HOSTMAC, isString));
    findNodeByKey(DM_L2CTR_WIFIOFF, this)->addChild(treeNodeWithKey(DM_HOSTMAC, isString));

//----------------X_ZyXEL_Ext.BatteryStatus
    findNodeByKey(DM_BATTERYSTATUS, this)->addChild(treeNodeWithKey(DM_CHARGESTATUS,isString));
    findNodeByKey(DM_BATTERYSTATUS, this)->addChild(treeNodeWithKey(DM_TOTALCAPACITY,isInt));
    findNodeByKey(DM_BATTERYSTATUS, this)->addChild(treeNodeWithKey(DM_REMAINCAPACITY,isInt));
//----------------X_ZyXEL_Ext.SystemInfo
    findNodeByKey(DM_SYSTEMINFO, this)->addChild(treeNodeWithKey(DM_SYSTEMNAME,isObject));
//----------------X_ZyXEL_Ext.AppInfo
//    findNodeByKey(DM_APPINFO, this)->addChild(treeNodeWithKey(DM_APPVERSION,isInt));
//    findNodeByKey(DM_APPINFO, this)->addChild(treeNodeWithKey(DM_MAGICNUM,isString));
//    findNodeByKey(DM_APPINFO, this)->addChild(treeNodeWithKey(DM_FEATURELISTNUM,isInt));
//    findNodeByKey(DM_APPINFO, this)->addChild(treeNodeWithKey(DM_FEATURELIST,isObject));

    //SCOTT TSENG ADD 2013-11-15
//----------------X_ZyXEL_Ext.ParentalControl
//    findNodeByKey(DM_PARENTALCONTROL, this)->addChild(CreatParentalControlWithIndex(1));

    //SCOTT TSENG ADD END
/*
//----------------X_ZyXEL_Ext.AppInfo.FeatureList
    findNodeByKey("FeatureList", this)->addChild(treeNodeWithKey("i1",isObject));
*/
//----------------X_ZyXEL_Ext.Notify
    findNodeByKey(DM_NOTIFY, this)->addChild(treeNodeWithKey(DM_APS,isObject));
//----------------X_ZyXEL_Ext.Notify.aps   
    findNodeByKey(DM_APS, this)->addChild(treeNodeWithKey(DM_TIMESTAMP,isString));
    findNodeByKey(DM_APS, this)->addChild(treeNodeWithKey(DM_ALERT,isInt));



//----------------Hosts
    findNodeByKey(DM_HOSTS, this)->addChild(treeNodeWithKey(DM_HOSTNUM,isInt));
    findNodeByKey(DM_HOSTS, this)->addChild(treeNodeWithKey(DM_HOST,isObject));
    
    //findNodeByKey(DM_HOST, this)->addChild(CreatHostWithIndex(1));
/*
//----------------Hosts.Host
    findNodeByKey(Hosts, this)->addChild(treeNodeWithKey("i1",isObject));
//----------------Hosts.Host.i1
    findNodeByKey("i1", this)->addChild(treeNodeWithKey("X_ZyXEL_HostType",isObject));
    findNodeByKey("i1", this)->addChild(treeNodeWithKey("HostName",isObject));
    findNodeByKey("i1", this)->addChild(treeNodeWithKey("PhysAddress",isObject));
*/
//----------------WiFi
    findNodeByKey(DM_WIFI, this)->addChild(treeNodeWithKey(DM_SSID,isObject));
    findNodeByKey(DM_WIFI, this)->addChild(treeNodeWithKey(DM_GUESTAP,isObject));
    findNodeByKey(DM_WIFI, this)->addChild(treeNodeWithKey(DM_AP,isObject));
    findNodeByKey(DM_WIFI, this)->addChild(treeNodeWithKey(DM_APNUM,isInt));
    findNodeByKey(DM_WIFI, this)->addChild(treeNodeWithKey(DM_SSIDNUM,isInt));
    findNodeByKey(DM_WIFI, this)->addChild(treeNodeWithKey(DM_RADIO,isObject));
    findNodeByKey(DM_WIFI, this)->addChild(treeNodeWithKey(DM_RADIONUM,isInt));

    //----------------WiFi.X_ZyXEL_GuestAP
    findNodeByKey(DM_GUESTAP, this)->addChild(treeNodeWithKey(DM_PRESHAREKEY,isString));
    findNodeByKey(DM_GUESTAP, this)->addChild(treeNodeWithKey(DM_SSID,isString));
    findNodeByKey(DM_GUESTAP, this)->addChild(treeNodeWithKey(DM_ENABLE,isInt));
    
    findNodeByKey(DM_GUESTAP, this)->addChild(treeNodeWithKey(DM_SECURITY,isObject));
    findNodeByKey(DM_SECURITY, this)->addChild(treeNodeWithKey(DM_MODEENABLED, isString));
    
//    //Device.WiFi.X_ZyXEL_GuestAP.Timer
//    findNodeByKey(DM_GUESTAP, this)->addChild(treeNodeWithKey(DM_TIMER,isObject));
//    findNodeByKey(DM_TIMER, this)->addChild(treeNodeWithKey(DM_TIMER_HOURS, isInt));
//    findNodeByKey(DM_TIMER, this)->addChild(treeNodeWithKey(DM_TIMER_MINUTES, isInt));
//    
//    //Device.X_ZyXEL_Ext.SpeedTestInfo
//    findNodeByKey(DM_ZYXELEXT, this)->addChild(treeNodeWithKey(DM_SPEEDTESTINFO,isObject));        
//    findNodeByKey(DM_SPEEDTESTINFO, this)->addChild(treeNodeWithKey(DM_SP_URL, isString));
//    findNodeByKey(DM_SPEEDTESTINFO, this)->addChild(treeNodeWithKey(DM_SP_ACTION_TYPE, isString));
//    findNodeByKey(DM_SPEEDTESTINFO, this)->addChild(treeNodeWithKey(DM_SP_END, isInt));
//    findNodeByKey(DM_SPEEDTESTINFO, this)->addChild(treeNodeWithKey(DM_SP_DL_RATE, isString));
//    findNodeByKey(DM_SPEEDTESTINFO, this)->addChild(treeNodeWithKey(DM_SP_UL_RATE, isString));

    //----------------
    //----------------WiFi.AccessPoint
    findNodeByKey(DM_AP, this)->addChild(CreatAccessPointWithIndex(1));
    findNodeByKey(DM_AP, this)->addChild(CreatAccessPointWithIndex(2));
    //----------------
    //----------------WiFi.SSID
    findNodeByKey(DM_SSID, findNodeByKey(DM_WIFI, this))->addChild(CreatSSIDWithIndex(1));
    findNodeByKey(DM_SSID, findNodeByKey(DM_WIFI, this))->addChild(CreatSSIDWithIndex(2));

    //findNodeByKey("SSID", tmp)->addChild(CreatSSIDWithIndex(1));
    //----------------
    //----------------WiFi.Radio
    findNodeByKey(DM_RADIO, this)->addChild(CreatRadioWithIndex(1));
    findNodeByKey(DM_RADIO, this)->addChild(CreatRadioWithIndex(2));
    //----------------

}
tr181Tree::~tr181Tree(){
    //releaseData(this);
    
}
treeNode * tr181Tree::CreatHostWithIndex(int index){
    treeNode *tmp = new treeNode;
    std::stringstream ss;
    ss << index;
    tmp->key = "i"+ ss.str();
    
    tmp->type = isObject;
    
    //----------------Hosts.Host.iX
    tmp->addChild(treeNodeWithKey(DM_HOSTTYPE,isString));
    tmp->addChild(treeNodeWithKey(DM_HOSTNAME,isString));
    tmp->addChild(treeNodeWithKey(DM_PHYSADDRESS,isString));
    
    //SCOTT TSENG ADD 2013-11-15
    tmp->addChild(treeNodeWithKey(DM_ACTIVE,isInt));
    tmp->addChild(treeNodeWithKey(DM_BLOCKING,isString));

    // SCOTT TSENG ADD 2013-12-25
    tmp->addChild(treeNodeWithKey(DM_IPAddress, isString));
//    tmp->addChild(treeNodeWithKey(DM_L2STA_WIFI, isInt));
    tmp->addChild(treeNodeWithKey(DM_ADDRULE, isString));
    
//    tmp->addChild(treeNodeWithKey(DM_CONTROLLABLEMASK, isInt));
    //SCOTT TSENG ADD END

    //2014-07-28
//    tmp->addChild(treeNodeWithKey(DM_WIFI_SIGNALSTRENGTH, isInt));
//    tmp->addChild(treeNodeWithKey(DM_L2STA_AUTOCONFIG, isInt));
    
    return tmp;
}
treeNode * tr181Tree::CreatInterfaceWithIndex(int index){
    treeNode *tmp = new treeNode;
    std::stringstream ss;
    ss << index;
    tmp->key = "i"+ ss.str();
    
    tmp->type = isObject;
    
    //----------------X_ZyXEL_3GPP.Interface.iX
    tmp->addChild(treeNodeWithKey(DM_PASSWORD,isString));
    tmp->addChild(treeNodeWithKey(DM_USERNAME,isString));
    tmp->addChild(treeNodeWithKey(DM_PPPAUTH,isString));
    tmp->addChild(treeNodeWithKey(DM_APNAME,isString));
    tmp->addChild(treeNodeWithKey(DM_SERVICEPROVIDER,isString));
    tmp->addChild(treeNodeWithKey(DM_SIGNALSTRENGTH,isInt));
    tmp->addChild(treeNodeWithKey(DM_CELLULARMODE,isString));
    tmp->addChild(treeNodeWithKey(DM_STATS,isObject));
    
    //----------------X_ZyXEL_3GPP.Interface.iX.Stats
    findNodeByKey(DM_STATS, tmp)->addChild(treeNodeWithKey(DM_BYTESRECEIVED,isInt));
    findNodeByKey(DM_STATS, tmp)->addChild(treeNodeWithKey(DM_BYTESSENT,isInt));
    findNodeByKey(DM_STATS, tmp)->addChild(treeNodeWithKey(DM_DATAPLAN,isObject));
    //----------------X_ZyXEL_3GPP.Interface.iX.Stats.DataPlanManagement
    findNodeByKey(DM_DATAPLAN, tmp)->addChild(treeNodeWithKey(DM_ENABLE,isString));
    findNodeByKey(DM_DATAPLAN, tmp)->addChild(treeNodeWithKey(DM_MONTHLYLIMIT,isInt));
    findNodeByKey(DM_DATAPLAN, tmp)->addChild(treeNodeWithKey(DM_MONTHLYRESETDAY,isInt));
    findNodeByKey(DM_DATAPLAN, tmp)->addChild(treeNodeWithKey(DM_MONTHLYRESETENABLE,isString));
    findNodeByKey(DM_DATAPLAN, tmp)->addChild(treeNodeWithKey(DM_LASTRESETDATE,isString));
    findNodeByKey(DM_DATAPLAN, tmp)->addChild(treeNodeWithKey(DM_RESETCOUNTER,isInt));
    
    return tmp;
}
treeNode * tr181Tree::CreatRadioWithIndex(int index){
    treeNode *tmp = new treeNode;
    std::stringstream ss;
    ss << index;
    tmp->key = "i"+ ss.str();
    
    tmp->type = isObject;
    
    //----------------WiFi.Radio.iX
    tmp->addChild(treeNodeWithKey(DM_AUTOCHSUPPORTED,isString));
    tmp->addChild(treeNodeWithKey(DM_CHANNEL,isInt));
    tmp->addChild(treeNodeWithKey(DM_TRANSMITPW,isInt));
    tmp->addChild(treeNodeWithKey(DM_AUTOCHENABLE,isInt));
    tmp->addChild(treeNodeWithKey(DM_TRANSMITPWSUPPORTED,isString));
    tmp->addChild(treeNodeWithKey(DM_SSID,isString));
    
    //SCOTT TSENG ADD 2013-11-15
    tmp->addChild(treeNodeWithKey(DM_ENABLE, isInt));
    tmp->addChild(treeNodeWithKey(DM_STATUS, isString));
    tmp->addChild(treeNodeWithKey(DM_MAXBITRATE, isString));
    tmp->addChild(treeNodeWithKey(DM_FREQUENCYBAND, isString));
    tmp->addChild(treeNodeWithKey(DM_STANDARDS, isString));
    //SCOTT TSENG ADD END

    return tmp;
}
treeNode * tr181Tree::CreatAccessPointWithIndex(int index){
    treeNode *tmp = new treeNode;
    std::stringstream ss;
    ss << index;
    tmp->key = "i"+ ss.str();
    
    tmp->type = isObject;
    
    //----------------WiFi.AccessPoint.iX
    tmp->addChild(treeNodeWithKey(DM_SECURITY,isObject));
    tmp->addChild(treeNodeWithKey(DM_ASSOCIATEDNUM,isInt));
    tmp->addChild(treeNodeWithKey(DM_ASSOCIATEDDEVICE,isObject));
    
    //----------------WiFi.AccessPoint.iX.Security
    findNodeByKey(DM_SECURITY, tmp)->addChild(treeNodeWithKey(DM_MODESUPPORTED,isString));
    findNodeByKey(DM_SECURITY, tmp)->addChild(treeNodeWithKey(DM_MODEENABLED,isString));
    findNodeByKey(DM_SECURITY, tmp)->addChild(treeNodeWithKey(DM_PRESHAREKEY,isString));
    //----------------WiFi.AccessPoint.iX.AssociatedDevice.iX
    findNodeByKey(DM_ASSOCIATEDDEVICE, tmp)->addChild(treeNodeWithKey("i1",isString));
    findNodeByKey("i1",findNodeByKey(DM_ASSOCIATEDDEVICE, tmp))->addChild(treeNodeWithKey(DM_MAC,isString));
    //
    //-------------------------------------------------------
    findNodeByKey(DM_SECURITY, tmp)->addChild(treeNodeWithKey(DM_WIFI_CHECK_BAND, isString));
    
    return tmp;
}
treeNode * tr181Tree::CreatSSIDWithIndex(int index){
    treeNode *tmp = new treeNode;
    std::stringstream ss;
    ss << index;
    tmp->key = "i"+ ss.str();
    
    tmp->type = isObject;
    
    tmp->addChild(treeNodeWithKey(DM_SSID,isString));

    //SCOTT TSENG ADD 2013-11-15
    tmp->addChild(treeNodeWithKey(DM_ENABLE, isInt));
    tmp->addChild(treeNodeWithKey(DM_STATUS, isString));
    tmp->addChild(treeNodeWithKey(DM_WIFI_CHECK_BAND, isString));
    //SCOTT TSENG ADD END

    return tmp;
}

// SCOTT TSENG ADD 2013-11-15
treeNode * tr181Tree::CreatParentalControlWithIndex(int index){
	treeNode *tmp = new treeNode;
	std::stringstream ss;
	ss << index;
	tmp->key = "i"+ss.str();
	//LOGI("ParentalControl Profile Index =%s", tmp->key.c_str());
	tmp->type = isObject;

	tmp->addChild(treeNodeWithKey(DM_HOSTMAC, isString));
	tmp->addChild(treeNodeWithKey(DM_ENABLE, isString));
	tmp->addChild(treeNodeWithKey(DM_DELETE, isString));
	tmp->addChild(treeNodeWithKey(DM_PCPNAME, isString));
	tmp->addChild(treeNodeWithKey(DM_SCHEDULING, isObject));


	//----------------X_ZyXEL_Ext.ParentControl.iX.Scheduling
	findNodeByKey(DM_SCHEDULING, tmp)->addChild(treeNodeWithKey(DM_DAYS,isString));
	findNodeByKey(DM_SCHEDULING, tmp)->addChild(treeNodeWithKey(DM_STARTHOUR,isString));
	findNodeByKey(DM_SCHEDULING, tmp)->addChild(treeNodeWithKey(DM_STARTMIN,isString));
	findNodeByKey(DM_SCHEDULING, tmp)->addChild(treeNodeWithKey(DM_STOPHOUR,isString));
	findNodeByKey(DM_SCHEDULING, tmp)->addChild(treeNodeWithKey(DM_STOPMIN,isString));

	return tmp;
}
// SCOTT TSENG ADD END

