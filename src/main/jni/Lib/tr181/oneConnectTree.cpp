//
//  oneConnectTree.cpp
//  NetworkManager
//
//  Created by Kevin on 2014/12/16.
//  Copyright (c) 2014年 ZyXEL. All rights reserved.
//

#include "oneConnectTree.h"
#include <sstream>
oneConnectTree::oneConnectTree(){

    treeNode *node;
    
    findNodeByKey(DM_DEVICEINFO, this)->addChild(treeNodeWithKey(DM_SN,isString));
    findNodeByKey(DM_DEVICEINFO, this)->addChild(treeNodeWithKey(DM_DEVICEMODE,isString));
    findNodeByKey(DM_DEVICEINFO, this)->addChild(treeNodeWithKey(DM_WIFICONFIGFLAG,isInt));
    
    //Device.IP.Diagnostics.TraceRoute
    node = findNodeByKey(DM_DEVICE, this);
    node->addChild(treeNodeWithKey(DM_IP,isObject));
    findNodeByKey(DM_IP, this)->addChild(treeNodeWithKey(DM_DIAGNOSTICS,isObject));
    findNodeByKey(DM_DIAGNOSTICS, this)->addChild(treeNodeWithKey(DM_TRACEROUTER,isObject));
    node = findNodeByKey(DM_TRACEROUTER, this);
    node->addChild(treeNodeWithKey(DM_DIAGNOSTICS_STATE,isString));
    node->addChild(treeNodeWithKey(DM_HOST,isString));
    node->addChild(treeNodeWithKey(DM_NUMOFTRIES,isInt));
    node->addChild(treeNodeWithKey(DM_TIMEOUT,isInt));
    //node->addChild(treeNodeWithKey(DM_MAXBITRATE,isInt));
    node->addChild(treeNodeWithKey(DM_NUMOFROUTERHOPS,isInt));
    node->addChild(treeNodeWithKey(DM_ROUTERHOPS,isInt));
    
    //Device.X_ZyXEL_Ext.DeviceInfo
    node = findNodeByKey(DM_ZYXELEXT, this);
    node->addChild(treeNodeWithKey(DM_DEVICEINFO,isObject));
    node->addChild(treeNodeWithKey(DM_ROUTER,isObject));
    node->addChild(treeNodeWithKey(DM_CHANGE_DEV_NAME,isObject));
    node->addChild(treeNodeWithKey(DM_CHANGE_HOST_TYPE,isObject));
    
    //Device.X_ZyXEL_Ext.WiFiAutoCfg
    node->addChild(treeNodeWithKey(DM_WIFI_AUTOCONFIG,isObject));
    node->addChild(treeNodeWithKey(DM_WIFI_AUTOCONFIGBYTIME,isObject));
    node->addChild(treeNodeWithKey(DM_L2DEV_CTR_AUTOCONFIG,isObject));
    node->addChild(treeNodeWithKey(DM_L2DEV_CTR_FWCONFIG,isObject));
    node = findNodeByKey(DM_WIFI_AUTOCONFIG, this);
    node->addChild(treeNodeWithKey(DM_CTR_OP,isInt));
    
    //Device.X_ZyXEL_Ext.WiFiAutoCfgByTime
    node = findNodeByKey(DM_WIFI_AUTOCONFIGBYTIME, this);
    node->addChild(treeNodeWithKey(DM_DEVICEMODE,isInt));
    node->addChild(treeNodeWithKey(DM_CTR_OP,isInt));
    node->addChild(treeNodeWithKey(DM_TIME,isInt));
    
    //Device.X_ZyXEL_Ext.L2DevCtrl_AutoCfg
    node = findNodeByKey(DM_L2DEV_CTR_AUTOCONFIG, this);
    node->addChild(treeNodeWithKey(DM_HOST_MAC,isString));
    node->addChild(treeNodeWithKey(DM_CTR_OP,isInt));    
    
    //Device.X_ZyXEL_Ext.L2DevCtrl_FWCfg
    node = findNodeByKey(DM_L2DEV_CTR_FWCONFIG, this);    
    node->addChild(treeNodeWithKey(DM_HOST_MAC, isString));
    node->addChild(treeNodeWithKey(DM_CTR_OP, isInt));
    node->addChild(treeNodeWithKey(DM_CTR_FW_STATE,isInt));
    node->addChild(treeNodeWithKey(DM_CTR_FW_SUCCESS,isInt));
    
    //Device.X_ZyXEL_Ext.Router
    node = findNodeByKey(DM_ROUTER, this);
    node->addChild(treeNodeWithKey(DM_IPV4_FORWARDING, isObject));
    node->addChild(treeNodeWithKey(DM_NUMOFIPV4, isInt));

    //Device.X_ZyXEL_Ext.DeviceInfo
    node = findNodeByKey(DM_DEVICEINFO, findNodeByKey(DM_ZYXELEXT, this));
    node->addChild(treeNodeWithKey(DM_UP_TIME, isInt));
    node->addChild(treeNodeWithKey(DM_SYSTEM_TIME, isString));
    node->addChild(treeNodeWithKey(DM_PROCESS_STATUS, isObject));
    node->addChild(treeNodeWithKey(DM_WANINFO, isObject));
    
    //Device.X_ZyXEL_Ext.DeviceInfo.WanInfo
    node = findNodeByKey(DM_WANINFO, node);
    node->addChild(treeNodeWithKey(DM_WAN_STATUS, isString));
    node->addChild(treeNodeWithKey(DM_WAN_IP, isString));
    node->addChild(treeNodeWithKey(DM_WAN_MAC, isString));
    node->addChild(treeNodeWithKey(DM_WAN_PHYRATE, isInt));
    node->addChild(treeNodeWithKey(DM_WAN_TX, isInt));
    node->addChild(treeNodeWithKey(DM_WAN_RX, isInt));
    
    //Device.X_ZyXEL_Ext.DeviceInfo.ProcessStatus
    node = findNodeByKey(DM_PROCESS_STATUS,findNodeByKey(DM_DEVICEINFO, findNodeByKey(DM_ZYXELEXT, this)));
    node->addChild(treeNodeWithKey(DM_CPU_USAGE, isInt));
    
    //Device.X_ZyXEL_Ext.DevNameChange
    node = findNodeByKey(DM_CHANGE_DEV_NAME,findNodeByKey(DM_ZYXELEXT, this));
    node->addChild(treeNodeWithKey(DM_HOST_MAC, isString));
    node->addChild(treeNodeWithKey(DM_NAME, isString));
    //Device.X_ZyXEL_Ext.HostTypeChange
    node = findNodeByKey(DM_CHANGE_HOST_TYPE,findNodeByKey(DM_ZYXELEXT, this));
    node->addChild(treeNodeWithKey(DM_HOST_MAC, isString));
    node->addChild(treeNodeWithKey(DM_HOST_TYPE, isInt));
    
    //Device.X_ZyXEL_Ext.Reboot
    node = findNodeByKey(DM_ZYXELEXT, this);
    node->addChild(treeNodeWithKey(DM_REBOOT, isInt));
    
    //Device.X_ZyXEL_Ext.FWNumberOfEntries
    node = findNodeByKey(DM_ZYXELEXT, this);
    node->addChild(treeNodeWithKey(DM_FWTALNUM, isInt));
    
    //Device.X_ZyXEL_Ext.FWUpgrade.iX
    node = findNodeByKey(DM_ZYXELEXT, this);
    node->addChild(treeNodeWithKey(DM_FWUPGRADE, isObject));
    node->addChild(CreatFWUpgradeWithIndex(1));
    
    //Device.X_ZyXEL_Ext.Description
    node = findNodeByKey(DM_ZYXELEXT, this);
    node->addChild(treeNodeWithKey(DM_DESCRIPTION, isObject));
    node = findNodeByKey(DM_DESCRIPTION,findNodeByKey(DM_ZYXELEXT, this));
    node->addChild(treeNodeWithKey(DM_GATEWAY_DESCRIPTION, isInt));
    
    //Device.X_ZyXEL_Ext.GwSpeedTestInfo
    findNodeByKey(DM_ZYXELEXT, this)->addChild(treeNodeWithKey(DM_GWSPEEDTESTINFO, isObject));
    findNodeByKey(DM_GWSPEEDTESTINFO, this)->addChild(treeNodeWithKey(DM_GWSPEED_SERVER_PORT, isInt));
    findNodeByKey(DM_GWSPEEDTESTINFO, this)->addChild(treeNodeWithKey(DM_GWSPEED_ENABLE, isInt));
    
    //Device.X_ZyXEL_Ext.WiFiconfigValue
    findNodeByKey(DM_ZYXELEXT, this)->addChild(treeNodeWithKey(DM_GATEWAY_RSSI, isObject));
    findNodeByKey(DM_GATEWAY_RSSI, this)->addChild(treeNodeWithKey(DM_HOST_MAC, isString));
    findNodeByKey(DM_GATEWAY_RSSI, this)->addChild(treeNodeWithKey(DM_RSSI, isInt));


    //Device.X_ZyXEL_Ext.GatewayInfo.GatewayHostname
    findNodeByKey(DM_ZYXELEXT, this)->addChild(treeNodeWithKey(DM_GATEWAYINFO,isObject));
    findNodeByKey(DM_GATEWAYINFO, this)->addChild(treeNodeWithKey(DM_GATEWAY_HOSTNAME,isString));

    //Device.X_ZyXEL_Ext.GatewayInfo.GatewayHostMAC
    findNodeByKey(DM_GATEWAYINFO, this)->addChild(treeNodeWithKey(DM_GATEWAY_HOSTMAC,isString));

    //Device.X_ZyXEL_Ext.GatewayInfo.GatewayAutoCfg
    findNodeByKey(DM_GATEWAYINFO, this)->addChild(treeNodeWithKey(DM_GATEWAY_AUTOCONFIG,isInt));

    //Device.X_ZyXEL_Ext.WiFiAutoCfgApprove.Enable
    findNodeByKey(DM_ZYXELEXT, this)->addChild(treeNodeWithKey(DM_AUTO_CFG_APPROVE,isObject));
    findNodeByKey(DM_AUTO_CFG_APPROVE, this)->addChild(treeNodeWithKey(DM_ENABLE,isInt));
    findNodeByKey(DM_AUTO_CFG_APPROVE, this)->addChild(treeNodeWithKey(DM_AUTO_CFG_APPROVE_NUM,isInt));

    //Device.X_ZyXEL_Ext.WiFiAutoCfgApproveData
    findNodeByKey(DM_ZYXELEXT, this)->addChild(treeNodeWithKey(DM_AUTO_CFG_APPROVE_DATA,isObject));

    /*
    //Device.X_ZyXEL_Ext.AppInfo
    node = findNodeByKey(DM_APPINFO, findNodeByKey(DM_ZYXELEXT, this));
    node->addChild(treeNodeWithKey(DM_APPVERSION, isString));
    node = findNodeByKey(DM_FEATURELIST, node);
    node->addChild(treeNodeWithKey(DM_DEVICEINFO, isInt));
    node->addChild(treeNodeWithKey(DM_GATEWAYINFO, isInt));
    node->addChild(treeNodeWithKey(DM_FLOWINFO, isInt));
    node->addChild(treeNodeWithKey(DM_LOGIN, isInt));
    node->addChild(treeNodeWithKey(DM_NETTOPOLOGY, isInt));
    node->addChild(treeNodeWithKey(DM_DEVICECTRL, isInt));
    node->addChild(treeNodeWithKey(DM_L2DEVICECTRL, isInt));
    node->addChild(treeNodeWithKey(DM_DEVICON, isInt));
    node->addChild(treeNodeWithKey(DM_GUESTWIFI, isInt));
    node->addChild(treeNodeWithKey(DM_WIFI_AUTOCONFIG, isInt));
    node->addChild(treeNodeWithKey(DM_WIFIONOFF, isInt));
    node->addChild(treeNodeWithKey(DM_SPEEDTEST, isInt));
    node->addChild(treeNodeWithKey(DM_DIAGONSTIC, isInt));
    node->addChild(treeNodeWithKey(DM_CLOUDSERVICE, isInt));
    node->addChild(treeNodeWithKey(DM_APPINFO_FWUPGRADE, isInt));
    node->addChild(treeNodeWithKey(DM_LEDONOFF, isInt));    
    */
    
    /* Move to tr181Tree class
    //Device.WiFi.X_ZyXEL_GuestAP.Timer
    node = findNodeByKey(DM_DEVICE, this);
    node->addChild(treeNodeWithKey(DM_WIFI,isObject));
    findNodeByKey(DM_GUESTAP, this)->addChild(treeNodeWithKey(DM_TIMER,isObject));
    node->addChild(treeNodeWithKey(DM_TIMER_HOURS, isInt));
    node->addChild(treeNodeWithKey(DM_TIMER_MINUTES, isInt));
    
    //Device.X_ZyXEL_Ext.SpeedTestInfo
    node = findNodeByKey(DM_ZYXELEXT, this);
    node->addChild(treeNodeWithKey(DM_SPEEDTESTINFO,isObject));
    node->addChild(treeNodeWithKey(DM_SP_URL, isString));
    node->addChild(treeNodeWithKey(DM_SP_ACTION_TYPE, isString));
    node->addChild(treeNodeWithKey(DM_SP_END, isInt));
    node->addChild(treeNodeWithKey(DM_SP_DL_RATE, isString));
    node->addChild(treeNodeWithKey(DM_SP_UL_RATE, isString));
    */
    //Device.WiFi.X_ZyXEL_GuestAP.Timer
    findNodeByKey(DM_GUESTAP, this)->addChild(treeNodeWithKey(DM_TIMER,isObject));
    findNodeByKey(DM_TIMER, this)->addChild(treeNodeWithKey(DM_TIMER_HOURS, isInt));
    findNodeByKey(DM_TIMER, this)->addChild(treeNodeWithKey(DM_TIMER_MINUTES, isInt));
    
    //Device.X_ZyXEL_Ext.SpeedTestInfo
    findNodeByKey(DM_ZYXELEXT, this)->addChild(treeNodeWithKey(DM_SPEEDTESTINFO,isObject));
    findNodeByKey(DM_SPEEDTESTINFO, this)->addChild(treeNodeWithKey(DM_SP_URL, isString));
    findNodeByKey(DM_SPEEDTESTINFO, this)->addChild(treeNodeWithKey(DM_SP_ACTION_TYPE, isString));
    findNodeByKey(DM_SPEEDTESTINFO, this)->addChild(treeNodeWithKey(DM_SP_END, isInt));
    findNodeByKey(DM_SPEEDTESTINFO, this)->addChild(treeNodeWithKey(DM_SP_DL_RATE, isString));
    findNodeByKey(DM_SPEEDTESTINFO, this)->addChild(treeNodeWithKey(DM_SP_UL_RATE, isString));

    //Device.X_ZyXEL_Ext.SmartDevCtrl
    findNodeByKey(DM_ZYXELEXT, this)->addChild(treeNodeWithKey(DM_SMARTDEV_CTR, isObject));
    findNodeByKey(DM_SMARTDEV_CTR, this)->addChild(treeNodeWithKey(DM_SMARTDEV_CTR_PROTOCOL, isString));
    findNodeByKey(DM_SMARTDEV_CTR, this)->addChild(treeNodeWithKey(DM_SMARTDEV_CTR_ID, isString));
    findNodeByKey(DM_SMARTDEV_CTR, this)->addChild(treeNodeWithKey(DM_SMARTDEV_CTR_ACTION, isString));

}
treeNode * oneConnectTree::CreatHostWithIndex(int index){
    treeNode *tmp = new treeNode;
    std::stringstream ss;
    ss << index;
    tmp->key = "i"+ ss.str();
    
    tmp->type = isObject;
    
    //----------------Hosts.Host.iX
    tmp->addChild(treeNodeWithKey(DM_HOSTTYPE,isInt));
    tmp->addChild(treeNodeWithKey(DM_HOSTNAME,isString));
    tmp->addChild(treeNodeWithKey(DM_PHYSADDRESS,isString));
    
    //SCOTT TSENG ADD 2013-11-15
    tmp->addChild(treeNodeWithKey(DM_ACTIVE,isInt));
    tmp->addChild(treeNodeWithKey(DM_BLOCKING,isString));
    
    // SCOTT TSENG ADD 2013-12-25
    tmp->addChild(treeNodeWithKey(DM_IPAddress, isString));
    tmp->addChild(treeNodeWithKey(DM_L2STA_WIFI, isInt));
    tmp->addChild(treeNodeWithKey(DM_ADDRULE, isString));
    
    tmp->addChild(treeNodeWithKey(DM_CONTROLLABLEMASK, isInt));
    //SCOTT TSENG ADD END
    
    //2014-07-28
    tmp->addChild(treeNodeWithKey(DM_WIFI_SIGNALSTRENGTH, isInt));
    tmp->addChild(treeNodeWithKey(DM_L2STA_AUTOCONFIG, isInt));
    //2015-02-02
    tmp->addChild(treeNodeWithKey(DM_L2ENA_AUTOCONFIG, isInt));
    
    //Sharon add
    tmp->addChild(treeNodeWithKey(DM_CAPABILITYTYPE, isString));
    tmp->addChild(treeNodeWithKey(DM_CONNECTIONTYPE, isString));
    tmp->addChild(treeNodeWithKey(DM_DEVICENAME_USER_DEFINED, isString));
    tmp->addChild(treeNodeWithKey(DM_RHYRATE, isInt));
    tmp->addChild(treeNodeWithKey(DM_SOFTWAREVERSION_HOST, isString));
    tmp->addChild(treeNodeWithKey(DM_NEIGHBOR, isString));
    tmp->addChild(treeNodeWithKey(DM_DL_Rate, isInt));
    tmp->addChild(treeNodeWithKey(DM_UL_Rate, isInt));
    tmp->addChild(treeNodeWithKey(DM_CONN_GUEST, isInt));

    //SCOTT add
    tmp->addChild(treeNodeWithKey(DM_WIFI_AUTO_CFG_APPROVE, isInt));

    tmp->addChild(treeNodeWithKey(DM_X_ZYXEL_MANUFACTURER, isString));
    tmp->addChild(treeNodeWithKey(DM_X_ZYXEL_RSSI, isInt));
    tmp->addChild(treeNodeWithKey(DM_X_ZYXEL_BAND, isInt));
    tmp->addChild(treeNodeWithKey(DM_X_ZYXEL_WIFILINKRATE24G, isInt));
    tmp->addChild(treeNodeWithKey(DM_X_ZYXEL_WIFILINKRATE5G, isInt));
    tmp->addChild(treeNodeWithKey(DM_X_ZYXEL_CHANNEL24G, isInt));
    tmp->addChild(treeNodeWithKey(DM_X_ZYXEL_CHANNEL5G, isInt));



    return tmp;
}
treeNode * oneConnectTree::CreatRouteHopsWithIndex(int index){
    treeNode *tmp = new treeNode;
    std::stringstream ss;
    ss << index;
    tmp->key = "i"+ ss.str();
    tmp->type = isObject;
    
    tmp->addChild(treeNodeWithKey(DM_HOST,isString));
    tmp->addChild(treeNodeWithKey(DM_HOSTADDRESS,isString));
    tmp->addChild(treeNodeWithKey(DM_RTTIMES,isString));
    
    return tmp;
}
treeNode * oneConnectTree::CreatIPv4ForwardingWithIndex(int index){
    treeNode *tmp = new treeNode;
    std::stringstream ss;
    ss << index;
    tmp->key = "i"+ ss.str();
    tmp->type = isObject;

    tmp->addChild(treeNodeWithKey(DM_DES_IP,isString));
    tmp->addChild(treeNodeWithKey(DM_DES_SUBNETMASK,isString));
    tmp->addChild(treeNodeWithKey(DM_GATEWAY_IP,isString));
    tmp->addChild(treeNodeWithKey(DM_ZYXEL_INTERFACE,isString));
    
    return tmp;
}
treeNode * oneConnectTree::CreatFWUpgradeWithIndex(int index){
    treeNode *tmp = new treeNode;
    std::stringstream ss;
    ss << index;
    tmp->key = "i"+ ss.str();
    tmp->type = isObject;
    
    tmp->addChild(treeNodeWithKey(DM_FWUPGRADE_MODELNAME,isString));
    tmp->addChild(treeNodeWithKey(DM_FWUPGRADE_MAC,isString));
    tmp->addChild(treeNodeWithKey(DM_FWUPGRADE_NEWFW,isInt));
    tmp->addChild(treeNodeWithKey(DM_FWUPGRADE_NEWSTATE,isInt));
    tmp->addChild(treeNodeWithKey(DM_FWUPGRADE_NEWVERSION,isString));
    tmp->addChild(treeNodeWithKey(DM_FWUPGRADE_NEWDATE,isString));    
    
    return tmp;
}

treeNode * oneConnectTree::CreateWiFiAutoConfigApproveDataWithIndex(int index){
    treeNode *tmp = new treeNode;
    std::stringstream ss;
    ss << index;
    tmp->key = "i"+ ss.str();
    tmp->type = isObject;

    tmp->addChild(treeNodeWithKey(DM_AUTO_CFG_APPROVE_MAC,isString));
    tmp->addChild(treeNodeWithKey(DM_OPTION,isInt));

    return tmp;
}

