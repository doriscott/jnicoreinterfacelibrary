/****************************************************************************/
/*
* Copyright (C) 2000-2013 ZyXEL Communications, Corp.
* All Rights Reserved.
*
* ZyXEL Confidential;
* Protected as an unpublished work, treated as confidential,
* and hold in trust and in strict confidence by receiving party.
* Only the employees who need to know such ZyXEL confidential information
* to carry out the purpose granted under NDA are allowed to access.
*
* The computer program listings, specifications and documentation
* herein are the property of ZyXEL Communications, Corp. and shall
* not be reproduced, copied, disclosed, or used in whole or in part
* for any reason without the prior express written permission of
* ZyXEL Communications, Corp.
*
*/
/****************************************************************************/
//
//  MyStream.h
//
//  Created by Kevin on 2013/10/25.
//  Copyright (c) 2013年 pan star. All rights reserved.
//

#ifndef __CSocket__
#define __CSocket__
#include <memory>
#include <iostream>
#include <string>

//#include "ISingleton.h"
using namespace std;
#define TIMEOUT_TIME 10
#define TIMEOUT_SEND 20
#define TIMEOUT_RECV 60
#define MAXDATASIZE 4096

class CSocket{
public:
    char _encodeData[MAXDATASIZE];
    int sockfd;
    string serverIP;
    int serverPort;
    int timeout_send;
    int timeout_recv;
    
    void SetSendTimout(int time);
    void SetRecvTimout(int time);
    int ConnectToHostWithPort(string ip, int port);
    //void openSocket();
    void CloseSocket();
    int SendData(char* senData ,int sendbytes);
    int RecvData(char *recvData, int *recvbytes);
    //sharedStream;
    void SetServerIP(string ip);
    void SetServerPort(int port);

    static CSocket * sharedSocket()
    {
        if( 0== _instance.get())
        {
            _instance.reset( new CSocket);
        }
        return _instance.get();
    }
//protected:
    CSocket(void)
    {
        cout <<"Create CSocket"<<endl;
        timeout_send = 0;
        timeout_recv = 0;
    }
    virtual ~CSocket(void)
    {
        cout << "Destroy CSocket"<<endl;
    }
    friend class auto_ptr<CSocket>;
    static auto_ptr<CSocket> _instance;
};

#endif /* defined(__CSocket__) */
