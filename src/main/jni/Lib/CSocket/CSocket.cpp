/****************************************************************************/
/*
* Copyright (C) 2000-2013 ZyXEL Communications, Corp.
* All Rights Reserved.
*
* ZyXEL Confidential;
* Protected as an unpublished work, treated as confidential,
* and hold in trust and in strict confidence by receiving party.
* Only the employees who need to know such ZyXEL confidential information
* to carry out the purpose granted under NDA are allowed to access.
*
* The computer program listings, specifications and documentation
* herein are the property of ZyXEL Communications, Corp. and shall
* not be reproduced, copied, disclosed, or used in whole or in part
* for any reason without the prior express written permission of
* ZyXEL Communications, Corp.
*
*/
/****************************************************************************/
//
//  CSocket.cpp
//
//  Created by Kevin on 2013/10/25.
//  Copyright (c) 2013年 pan star. All rights reserved.
//
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>
#include <string.h>
#include "CSocket.h"
#include "../../APP_Agent/CSecurity.h"
#include <arpa/inet.h>
//#include <android/log.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <iostream>
#include <algorithm>
#include "../../APP_Agent/ErrorCode.h"

#define  LOG_TAG    "CSocket"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)

auto_ptr<CSocket> CSocket::_instance;
//int CSocket::sockfd;
//CSocket* CSocket::_instance;

/*
CSocket &CSocket::sharedSocket(){
    static CSocket pInst;
    return pInst;
}
 */

void CSocket::SetSendTimout(int time){
    timeout_send = time;
}
void CSocket::SetRecvTimout(int time){
    timeout_recv = time;
}

void CSocket::SetServerIP(string ip){
	serverIP = ip;
}
void CSocket::SetServerPort(int port){
	serverPort = port;
}
int CSocket::ConnectToHostWithPort(string ip, int port){
    serverIP = ip;
    serverPort = port;

    //struct hostent *host;
                                      /* struct hostent
                                       * {
                                       * char *h_name; // general hostname
                                       * char **h_aliases; // hostname's alias
                                       * int h_addrtype; // AF_INET
                                       * int h_length;
                                       * char **h_addr_list;
                                       * };
                                       */
    struct sockaddr_in server_addr;
    
    
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
    	printf("Can't creat socket!");
        return ERR_SOCK_CANT_CREATE_SOCK;
    }
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(port);
    server_addr.sin_addr.s_addr = inet_addr(ip.c_str());
    //inet_pton(AF_INET, argv[1], &server_addr.sin_addr);
    memset(&(server_addr.sin_zero), 0, 8);
    
    fd_set writeset;
    struct timeval timeout;
    unsigned long ul = 1;
    int error = -1, len = sizeof(int);
    int bTimeoutFlag = false;
    int ret;
    
    /*Setting socket to non-blocking mode */
    ioctl(sockfd, FIONBIO, &ul);//将socket设置成非阻塞模式
    
    /* create the connection by socket
     * means that connect "sockfd" to "server_addr"
     * 同步非阻塞模式
     */
    if (connect(sockfd, (struct sockaddr *)&server_addr, sizeof(struct sockaddr)) == -1)
    {
        timeout.tv_sec  = TIMEOUT_TIME;
        timeout.tv_usec = 0;
        FD_ZERO(&writeset);
        FD_SET(sockfd, &writeset);
        
        
        ret = select(sockfd+1, NULL, &writeset, NULL, &timeout);
        if (ret == 0)              //返回0，代表在描述词状态改变已超过timeout时间
        {
            getsockopt(sockfd, SOL_SOCKET, SO_ERROR, &error, (socklen_t *)&len);
            if (error == 0)          // 超时，可以做更进一步的处理，如重试等
            {
                bTimeoutFlag = true;
                printf("Connect timeout!\n");
            }
            else
            {
            	printf("Cann't connect to server!\n");
            }
            return ERR_SOCK_CONNECT_TIMEOUT;
        }
        else if ( ret == -1)      // 返回-1， 有错误发生，错误原因存在于errno
        {
        	printf("Cann't connect to server!\n");
            return ERR_SOCK_CONNECT_TIMEOUT;
        }
        else                      // 成功，返回描述词状态已改变的个数
        {
        	printf("Connect success!\n");
        }
    }
    else
    {
    	printf("Connect success!\n");
        ret = true;
    }
    
    ul = 0;
    ioctl(sockfd, FIONBIO, &ul); //重新将socket设置成阻塞模式

    return true;
}
void CSocket::CloseSocket(){
    if (sockfd) {
        close(sockfd);
    }
    
}
int CSocket::SendData(char* sendData ,int length){
    
    int result,error;
    struct timeval timeout;
    socklen_t tlen = sizeof(timeout);
    
    if (timeout_send==0) {
        timeout.tv_sec = TIMEOUT_SEND;
    }else{
        timeout.tv_sec = timeout_send;
    }
    timeout.tv_usec= 0;
    result = setsockopt(sockfd, SOL_SOCKET, SO_SNDTIMEO, (char*)&timeout.tv_sec, tlen);
    if (result < 0)
    {
    	perror("setsockopt:in SendData()");
        //exit(1);
        return ERR_SOCK_SETSOCKOPT;
    }
//    for(int i = 0; i < 3; i++)
//    {
        if (send(sockfd, sendData, length, 0) == -1)
        {
            getsockopt(sockfd, SOL_SOCKET, SO_ERROR, &error, &tlen);
            if (error == 0)
            {
//            	printf("Send timeout!(%d time)\n",i+1);
                return ERR_SOCK_SEND_TIMEOUT;
//                continue;
            }
        }
//        else
//        {
//            break;
//        }
//    }
    
    return true;
}
int CSocket::RecvData(char *recvData, int *recvbytes){
	
//    int recvbytes;
    char rcv_buf[MAXDATASIZE];
    memset(rcv_buf, 0, sizeof(rcv_buf));
    
    int result,error;
    struct timeval timeout;
    socklen_t tlen = sizeof(timeout);
    
    if (timeout_recv==0) {
        timeout.tv_sec = TIMEOUT_RECV;
    }else{
        timeout.tv_sec = timeout_recv;
    }

    timeout.tv_usec= 0;
    result = setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout.tv_sec, tlen);
    if (result < 0)
    {
    	perror("setsockopt:(in ReceiveData)");
        return ERR_SOCK_SETSOCKOPT;
    }
    
    
    if ((*recvbytes = (int)recv(sockfd, rcv_buf, MAXDATASIZE, 0)) == -1)
    {
        getsockopt(sockfd, SOL_SOCKET, SO_ERROR, &error, &tlen);
        if (error == 0)
        {
            printf("Recv timeout!\n");
            return ERR_SOCK_RECV_TIMEOUT;
        }
    }
//    else{
//        printf("----errr----\n");
//    }
    
    memcpy(recvData, rcv_buf, sizeof(rcv_buf));
    //recvData = rcv_buf;
    //rcv_buf[recvbytes] = '\0';
    //printf("recv:%s\n", rcv_buf);
    
    return true;
}
