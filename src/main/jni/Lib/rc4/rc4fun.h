#ifndef __ZyXEL_Cipher_RC4fun__
#define __ZyXEL_Cipher_RC4fun__

typedef unsigned long ULONG;//4 bytes
typedef unsigned char u_char;//one byte
typedef unsigned short WORD;//two bytes
typedef unsigned int  DWORD; //4 bytes
#define ENCRPT_LEN 4096
struct rc4_state 
{
	u_char	perm[ENCRPT_LEN];
	u_char	index1;
	u_char	index2;
};

struct socketFormat 
{
	u_char	ucVersion;
	u_char	ucType;
	WORD    wPayloadSize;
};

void swap_bytes(u_char *upSwapA, u_char *upSwapB);
void rc4_init(struct rc4_state *state,u_char *key, int keylen);
void rc4_crypt(struct rc4_state *state,u_char *inbuf, u_char *outbuf, int buflen);
void processData(char *pszSrc,char *pszTarget,int nLen,char*pwdData,int pwdLen);

#endif