#include <stdio.h>
#include <string.h>
#include "rc4fun.h"

void swap_bytes(u_char *upSwapA, u_char *upSwapB)
{
	u_char temp;

	temp = *upSwapA;
	*upSwapA = *upSwapB;
	*upSwapB = temp;
}

void processData(char *pszSrc,char *pszTarget,int nLen,char*pwdData,int pwdLen)
{
    struct rc4_state rc4_box;
    char szKey[ENCRPT_LEN];
    
    memset(szKey,0,sizeof(szKey));
    strcpy(szKey,pwdData);
    
    memset(&rc4_box,0,sizeof(rc4_box));
    rc4_init(&rc4_box,(u_char*)szKey,(int)strlen(szKey));
    
    rc4_crypt(&rc4_box,(u_char*)pszSrc,(u_char*)pszTarget,nLen);
}

void rc4_init(struct rc4_state *state,u_char *key, int keylen)
{
	u_char uData;
	int nLoop;

	/* Initialize state with identity permutation */
	for (nLoop = 0; nLoop < ENCRPT_LEN; nLoop++)
		state->perm[nLoop] = (u_char)nLoop; 
	state->index1 = 0;
	state->index2 = 0;
  
	/* Randomize the permutation using key data */
	for (uData = nLoop = 0; nLoop < ENCRPT_LEN; nLoop++)
	{
		uData += state->perm[nLoop] + key[nLoop % keylen]; 
		swap_bytes(&state->perm[nLoop], &state->perm[uData]);
	}
}

void rc4_crypt(struct rc4_state *state,
			   u_char *inbuf, u_char *outbuf, int buflen)
{
	int nLoop;
	u_char uData;

	for (nLoop = 0; nLoop < buflen; nLoop++) 
	{
		/* Update modification indicies */
		state->index1++;
		state->index2 += state->perm[state->index1];

		/* Modify permutation */
		swap_bytes(&state->perm[state->index1],&state->perm[state->index2]);

		/* Encrypt/decrypt next byte */
		uData = state->perm[state->index1] + state->perm[state->index2];
		outbuf[nLoop] = inbuf[nLoop] ^ state->perm[uData];
	}
}
