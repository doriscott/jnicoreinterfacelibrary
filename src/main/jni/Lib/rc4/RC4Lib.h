//
//  ZyXELAPPLib.h
//  ZyXELAPPLib
//
//  Created by cheng-heng hsu on 12/7/9.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

//#import <Foundation/Foundation.h>
#ifndef __ZyXEL_Cipher_RC4Lib__
#define __ZyXEL_Cipher_RC4Lib__

#define fineStatus                  0
#define nullPointer                 0x1
#define typeError                   0x2
#define datEmpty                    0x3
#define passwordEmpty               0x4

//#define msgType_DiscoverReq         0x1
//#define msgType_DiscoverResp        0x2
//#define msgType_SystemQueryReq      0x3
//#define msgType_SystemQueryResp     0x4
//#define msgType_ParameterGetReq     0x5
//#define msgType_ParameterGetResp    0x6
//#define msgType_ParameterSetReq     0x7
//#define msgType_ParameterSetResp    0x8
//#define msgType_DeviceNotifyReq     0x9
//#define msgType_DeviceNotifyResp    0xa
//#define msgType_ObjectAddReq		0xc
//#define msgType_ObjectAddResp		0xd
//#define msgType_ObjectDeleteReq		0xe
//#define msgType_ObjectDeleteResp	0xf

#include "rc4fun.h"

int msgEncode(int reqType ,char *msg ,int msgLen ,char *pwdData ,int pwdLen);
int msgDecode(int *reqType ,char * msg ,int msgLen ,char* pwdData ,int pwdLen);

#endif
