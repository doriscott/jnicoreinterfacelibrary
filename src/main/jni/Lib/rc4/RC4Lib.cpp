//
//  ZyXELAPPLib.m
//  ZyXELAPPLib
//
//  Created by cheng-heng hsu on 12/7/9.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "rc4fun.h"
#include "RC4Lib.h"
#include "../../APP_Agent/MessageType.h"



int msgEncode(int reqType ,char *msg ,int msgLen ,char *pwdData ,int pwdLen)
{
    int nRet;
    struct socketFormat *sockFormat;
    char *pszTmp,*pszmsg;
    
    if(reqType<msgType_DiscoverReq||reqType>msgType_DeviceNotifyResp)
        return typeError;
    sockFormat=(struct socketFormat *)msg;
    sockFormat->ucVersion=0x1;
    sockFormat->ucType=(u_char)reqType;
    sockFormat->wPayloadSize=msgLen-sizeof(struct socketFormat);
    if(sockFormat->wPayloadSize==0)
        return datEmpty;
    
    if(pwdLen==0)
        return passwordEmpty;
    pszmsg=(char*)malloc(sockFormat->wPayloadSize);
    pszTmp=msg+sizeof(struct socketFormat);
    if(reqType>msgType_DiscoverResp)
    {
        processData(pszTmp,pszmsg,sockFormat->wPayloadSize,pwdData,pwdLen);
        memcpy(pszTmp,pszmsg,sockFormat->wPayloadSize);
    }
    nRet=fineStatus;
    free(pszmsg);
    return nRet;
//    int nRet;
//    char *pszmsg;
//    
//    if(reqType<msgType_DiscoverReq||reqType>msgType_DeviceNotifyResp)
//        return typeError;
//
//    if(msgLen==0)
//        return datEmpty;
//
//    if(pwdLen==0)
//        return passwordEmpty;
//    pszmsg=(char*)malloc(msgLen);
//
//    if(reqType>msgType_DiscoverResp)
//    {
//        processData(msg,pszmsg,msgLen,pwdData,pwdLen);
//        memcpy(msg,pszmsg,msgLen);
//    }
//    nRet=fineStatus;
//    free(pszmsg);
//    return nRet;
}

int msgDecode(int *reqType ,char * msg ,int msgLen ,char* pwdData ,int pwdLen)
{
    int errorCode=0;
	memcpy(&errorCode,msg+2,1);
    
	errorCode = errorCode - 256 -100;
    
	if (msgLen<=5 && errorCode<0) {
		printf("Fail to received data!\n");
		printf("Error Code : %d\n",errorCode);
		//errorHandle->handleErrorOccur(errorCode-100);
		return errorCode;
	}
    
    int nRet;
    struct socketFormat *sockFormat;
    char *pszTmp,*pszmsg;
    
    if(reqType==NULL)
        return nullPointer;
    sockFormat=(struct socketFormat *)msg;
    *reqType=sockFormat->ucType;
    sockFormat->wPayloadSize=msgLen-sizeof(struct socketFormat);
    if(sockFormat->wPayloadSize==0)
        return datEmpty;
    
    if(pwdLen==0)
        return passwordEmpty;
    pszmsg=(char*)malloc(sockFormat->wPayloadSize);
    pszTmp=msg+sizeof(struct socketFormat);
    if(*reqType>msgType_DiscoverResp)
    {
        processData(pszTmp,pszmsg,sockFormat->wPayloadSize,pwdData,pwdLen);
        memcpy(pszTmp,pszmsg,sockFormat->wPayloadSize);
    }
    
    nRet=fineStatus;
    free(pszmsg);
    
    return nRet;
    
//    int nRet;
//    char *pszmsg;
//    
//    if(msgLen==0)
//        return datEmpty;
//
//    if(pwdLen==0)
//        return passwordEmpty;
//    pszmsg=(char*)malloc(msgLen);
//
//    if(reqType>msgType_DiscoverResp)
//    {
//        processData(msg,pszmsg,msgLen,pwdData,pwdLen);
//        memcpy(msg,pszmsg,msgLen);
//    }
//
//    nRet=fineStatus;
//    free(pszmsg);
//    
//    return nRet;
}

