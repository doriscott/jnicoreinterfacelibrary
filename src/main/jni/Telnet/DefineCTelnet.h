/****************************************************************************/
/*
 * Copyright (C) 2000-2015 ZyXEL Communications, Corp.
 * All Rights Reserved.
 *
 * ZyXEL Confidential;
 * Protected as an unpublished work, treated as confidential,
 * and hold in trust and in strict confidence by receiving party.
 * Only the employees who need to know such ZyXEL confidential information
 * to carry out the purpose granted under NDA are allowed to access.
 *
 * The computer program listings, specifications and documentation
 * herein are the property of ZyXEL Communications, Corp. and shall
 * not be reproduced, copied, disclosed, or used in whole or in part
 * for any reason without the prior express written permission of
 * ZyXEL Communications, Corp.
 *
 */
/****************************************************************************/
//
//  DefineCTelnet.h
//
//  Created by Sharon Yang on 2014/2/7.
//  Copyright (c) 2014 ZyXEL. All rights reserved.
//


#define CMD_LOGIN_DEFAULT_PORT 23
#define CMD_LOGIN_DEFAULT_USERNAME "admin"
#define CMD_LOGIN_DEFAULT_PASSWORD "1234"

#define CMD_MODELNAME "sys atsh"

#define PRODUCT_MODEL_SERIOUS_AMG "AMG"
#define PRODUCT_MODEL_1 "P-1202"
#define PRODUCT_MODEL_2 "P-1302" //  P-1302

#define PRODUCT_MODEL_SERIOUS_VMG "VMG"
#define PRODUCT_MODEL_SERIOUS_VMG2 "DSL"
#define PRODUCT_MODEL_VMG3312 "VMG3312"
#define PRODUCT_MODEL_SERIOUS_NBG "NBG"

// AMG DSL Command
#define CMD_WAN_LINK_STATUS_A "cat /proc/tc3162/adsl_stats | grep 'ADSL link status'"
#define CMD_WAN_LINK_STATUS1_A "wan adsl statu"
#define CMD_WAN_LINK_STATUS2_A "dmesg |grep 'ADSL link status' > /tmp/tttsg; cat /tmp/tttsg"
#define CMD_WAN_RESET_A "wan adsl reset"
#define CMD_WAN_LINK_RATE_A "wan adsl chandata"
#define CMD_CHECK_LED_STATUS_A "cat /proc/tc3162/led_internet"
#define CMD_PPP_AUTH_STATUS_A "cat /tmp/pppd.fail"
#define CMD_AUTOHUNT_STOP1_A "atmcmd vchunt active no"
#define CMD_AUTOHUNT_STOP2_A "echo 1 > /proc/AUTO_PVC_STOP"

#define CMD_AUTOHUNT_START_A "atmcmd vchunt active yes"
#define CMD_GET_WAN_IP_A "tcapi get DeviceInfo_PVC0 WanIP"
#define CMD_RENEW_WAN_IP_A "tcapi commit Wan_PVC0"
#define CMD_GET_DNS_IP_A "tcapi get DeviceInfo_PVC0 DNSIP"
#define CMD_RENEW_DNS_IP_A "tcapi commit Wan_PVC0"
#define CMD_PING_A "ping -c 3 "
#define CMD_SYSAUTHMSTC_A "sys auth MSTC;sys auth ZyXEL"
#define CMD_SET_VPI_A "tcapi set Wan_PVC0 VPI "
#define CMD_SET_VCI_A "tcapi set Wan_PVC0 VCI "
#define CMD_SET_USERNAME_A "tcapi set Wan_PVC0 USERNAME "
#define CMD_SET_PASSWORD_A "tcapi set Wan_PVC0 PASSWORD "
#define CMD_SET_IPOE_ISP_A "tcapi set Wan_PVC0 ISP 0"
#define CMD_SET_IPOE_ENCAP_A "tcapi set Wan_PVC0 ENCAP \"1483 Bridged IP LLC\""
#define CMD_SET_IPOE_MTU_A "tcapi set Wan_PVC0 MTU 1500"
#define CMD_SET_PPPOE_ISP_A "tcapi set Wan_PVC0 ISP 2"
#define CMD_SET_PPPOE_ENCAP_A "tcapi set Wan_PVC0 ENCAP \"PPPoE LLC\""
#define CMD_SET_PPPOE_MTU_A "tcapi set Wan_PVC0 MTU 1492"
#define CMD_SAFE_A "tcapi save"
#define CMD_COMMIT_A "tcapi commit Wan"
#define CMD_LOGOUT_A "exit"

//AMG EtherWAN Command

//set ETH IPoE mode
#define CMD_SET_ETH_ISP "tcapi set EtherWan_Entry0 ISP 0"
#define CMD_SET_ETH_MTU "tcapi set EtherWan_Entry0 MTU 1500"
#define CMD_SET_ETH_ENCAP "tcapi set EtherWan_Entry0 ENCAP '1483 Bridged IP VC-Mux'"
//set ETH PPPoE Command
#define CMD_SET_ETH_ISP2 "tcapi set EtherWan_Entry0 ISP 2"
#define CMD_SET_ETH_MTU2 "tcapi set EtherWan_Entry0 MTU 1492"
#define CMD_SET_ETH_ENCAP2 "tcapi set EtherWan_Entry0 ENCAP 'PPPoE VC-Mux'"

//change mode to EtherWAN
#define CMD_SET_ETH "tcapi set Wan_Common TransMode ETH"
#define CMD_COMMIT_ETH "tcapi commit Wan_Common"
#define CMD_REBOOT "reboot"
#define CMD_SET_USERNAME_ETH "tcapi set EtherWan_Entry0 USERNAME "
#define CMD_SET_PASSWORD_ETH "tcapi set EtherWan_Entry0 PASSWORD "
#define CMD_COMMIT_ETH2 "tcapi commit EtherWan_Entry0"
#define CMD_GET_MODE "tcapi get Wan_Common TransMode"

//VMG DSL Command
#define CMD_AUTOHUNT_STOP_B "vcautohunt stop zyad1234"
#define CMD_WAN_SHOW_B "wan show"
#define CMD_WAN_SHOWDEFAULT_B "wan showdefault"
#define CMD_WAN_EDIT_B "wan editintf servicename "
#define CMD_VPI_VCI_B " --vpivci "
#define CMD_PROTOCOL_IPOE_B " --protocol ipoe "
#define CMD_PROTOCOL_PPPOE_B " --protocol pppoe --username "
#define CMD_PROTOCOL_PPPOA_B " --protocol pppoa --username "
#define CMD_PASSWORD_B " --password "
#define CMD_WAN_LINK_STATUS_B "xdslctl info"
#define CMD_PPP_AUTH_STATUS_B "pppoectl status"
#define CMD_AUTOHUNT_START_B "vcautohunt send zyad1234"
#define CMD_WAN_RELEASE_B "wan release servicename "
#define CMD_WAN_RENEW_B "wan renew servicename "
#define CMD_DNS_STATUS "dns showruntime"
#define CMD_SET_ACTIVE "vcautohunt set active 1 zyad1234"
#define CMD_SAVE_ACTIVE "vcautohunt save zyad1234"
#define CMD_SHOW_ACTIVE "vcautohunt show zyad1234"

//NBG ROUTER Command
#define CMD_FW_CHECK "get_online_info"
#define CMD_FW_SET_DOWNLOAD "get_online_fw `cat /tmp/fw_online_info | grep '.bin'`"
#define CMD_FW_SAVE_LOG_STEP1 "[ -f /tmp/rootfs ] && {"
#define CMD_FW_SAVE_LOG_STEP2 "sleep 1 && echo 3 > /proc/sys/vm/drop_caches; cp /sbin/reboot /tmp; fw_upgrade fw_check 1> /tmp/logfile 2> /dev/null;"
#define CMD_FW_SAVE_LOG_STEP3 "}"
#define CMD_FW_GET_LOG_DOWNLOADSTATUS_STEP1 "result=$(cat /tmp/logfile | grep \"Upgrade check success\")"
#define CMD_FW_GET_LOG_DOWNLOADSTATUS_STEP2 "echo $result"
#define CMD_FW_SET_UPGRADE_STEP1 "{"
#define CMD_FW_SET_UPGRADE_STEP2 "/etc/init.d/dlna stop; /etc/init.d/samba stop; killall -9 proftpd; sleep 1 && echo 1 > /proc/sys/vm/drop_caches; echo 'Firmware upgrading, Please wait !' > /dev/console; fw_upgrade exec_mtd && /tmp/reboot &"
#define CMD_FW_SET_UPGRADE_STEP3 "}"

#define TOKEN_SPACE " "

#define LOGIN_INCORRECT "Login incorrect"
#define LOGIN_CORRECT "\r\n# "
#define LOGIN_CORRECT2 "\r\n> "
#define DATA_TOKEN_LINE "\r\n"
#define DATA_TOKEN_TRIM " #\r\n\t"
#define DATA_TOKEN_EN "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
#define DATA_TOKEN_MODELNAME "Product Model"
#define DATA_TOKEN_LINK_STATUS "ADSL link status"
#define DATA_TOKEN_LINK_DOWN "ADSL link status: down"
#define DATA_TOKEN_LINK_UP "ADSL link status: up"
#define DATA_TOKEN_LINKRATE_DOWN "near-end interleaved channel bit rate"
#define DATA_TOKEN_LINKRATE_UP "far-end interleaved channel bit rate"
#define DATA_TOKEN_PPP_AUTH_FAIL "authentication failed"
#define DATA_TOKEN_PPP_AUTH_FAIL2 "PPP Auth failed"
#define DATA_TOKEN_GET_WAN_IP_FAIL1 "no attribute information"
#define DATA_TOKEN_GET_WAN_IP_FAIL2 "0.0.0.0"
#define DATA_TOKEN_GET_WAN_IP_FAIL3 "(null)"
#define DATA_TOKEN_PING_FAIL1 "0 packets received"
#define DATA_TOKEN_PING_FAIL2 "unreachable"
#define DATA_TOKEN_PING_FAIL3 "Unknown host"
#define DATA_TOKEN_PING_SUCCESS "bytes from"
#define DATA_TOKEN_IDLE "Idle"
#define DATA_TOKEN_STATUS "Status"
#define DATA_TOKEN_SHOWTIME "Showtime"
#define DATA_TOKEN_CONNECTED "Connected"
#define DATA_TOKEN_NAMESERVER "nameserver"
#define DATA_TOKEN_LINKRATE_DOWN2 "Downstream rate = "
#define DATA_TOKEN_LINKRATE_UP2 "Upstream rate = "
#define DATA_TOKEN_DNS_IP "10.10.100.1"
#define DATA_TOKEN_ATM "ATM"
#define DATA_TOKEN_PTM "PTM"
#define DATA_TOKEN_ETH "ETH"
#define DATA_TOKEN_ADDRESS "address"
#define DATA_TOKEN_FW_SUCCESS "Upgrade check success"


enum EN_TELNET_TYPE{
    EN_TELNET_TYPE_A = 1, //AMG,P-1202,P-1302
    EN_TELNET_TYPE_B = 2, //VMG
    EN_TELNET_TYPE_C = 3, //NBG
    EN_TELNET_TYPE_D = 4  //ex:DEVICE_F
};


//Error code should be join to "ErrorCode.h"
#define ERR_TELNET_DATA_NONE -202
#define ERR_TELNET_DATA_OVERFLOW -203
#define ERR_TELNET_DATA_WRONG -204
#define ERR_TELNET_MODEL_EMPTY -205
#define ERR_TELNET_MODEL_NOT_SUPPORT -206


