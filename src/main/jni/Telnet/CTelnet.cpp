/****************************************************************************/
/*
 * Copyright (C) 2000-2015 ZyXEL Communications, Corp.
 * All Rights Reserved.
 *
 * ZyXEL Confidential;
 * Protected as an unpublished work, treated as confidential,
 * and hold in trust and in strict confidence by receiving party.
 * Only the employees who need to know such ZyXEL confidential information
 * to carry out the purpose granted under NDA are allowed to access.
 *
 * The computer program listings, specifications and documentation
 * herein are the property of ZyXEL Communications, Corp. and shall
 * not be reproduced, copied, disclosed, or used in whole or in part
 * for any reason without the prior express written permission of
 * ZyXEL Communications, Corp.
 *
 */
/****************************************************************************/
//
//  CTelnet.cpp
//
//  Created by Sharon Yang on 2014/1/27.
//  Copyright (c) 2014 ZyXEL. All rights reserved.
//

#include "CTelnet.h"

//#include <android/log.h>
//#define  LOG_TAG    "CTelnet"
//#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)

CTelnet::CTelnet() {
	//LOGI(" CTelnet %d", 0);
}
CTelnet::~CTelnet() {
	//LOGI(" ~CTelnet %d", 0);
}

int CTelnet::Login(string ip, int port, string t_username, string t_password) {
	bool blRet = false;
	char RecvData[MAXDATASIZE];
	memset(RecvData, 0, sizeof(RecvData));

	CSocket *mySocket = CSocket::sharedSocket();

	mySocket->ConnectToHostWithPort(ip, port);
	mySocket->SetSendTimout(TIMEOUT_LOGIN_SEND_TELNET);
	mySocket->SetRecvTimout(TIMEOUT_LOGIN_RECV_TELNET);

	recvStreamData(RecvData);
	sendStreamData(t_username);
	recvStreamData(RecvData);
	sendStreamData(t_password);
	recvStreamData(RecvData);

	//LOGI("Login RecvData=%s", RecvData);

	if (strlen(RecvData) > 0) {
		if (strstr(RecvData, LOGIN_INCORRECT) == NULL) {
			blRet = 1;
			//LOGI("Already Login...%d", 0);
		} else {
			blRet = 0;
			//LOGI("Can not Login...%d", 0);
		}
	} else {
		blRet = -1;
		//LOGI("Can not Login...%d", 1);
	}

	return blRet;
}

int CTelnet::Login(string ip, int port, string t_password) {
	int blRet = 0;
	char RecvData[MAXDATASIZE];
	memset(RecvData, 0, sizeof(RecvData));

	CSocket *mySocket = CSocket::sharedSocket();

	mySocket->ConnectToHostWithPort(ip, port);
	mySocket->SetSendTimout(TIMEOUT_LOGIN_SEND_TELNET);
	mySocket->SetRecvTimout(TIMEOUT_LOGIN_RECV_TELNET);

	recvStreamData(RecvData);
	sendStreamData(CMD_LOGIN_DEFAULT_USERNAME);
	recvStreamData(RecvData);
	sendStreamData(t_password);
	recvStreamData(RecvData);

	if (strlen(RecvData) > 0) {
		if (strstr(RecvData, LOGIN_INCORRECT) == NULL
				&& strstr(RecvData, LOGIN_CORRECT) != NULL) {
			blRet = true;
			printf("Already Login...\n");
		} else {
			blRet = false;
		}
	} else {
		blRet = false;
	}

	return blRet;
}

int CTelnet::Login(string ip, int port) {
	bool blRet = false;
	char RecvData[MAXDATASIZE];
	memset(RecvData, 0, sizeof(RecvData));

	CSocket *mySocket = CSocket::sharedSocket();

	mySocket->ConnectToHostWithPort(ip, port);
	mySocket->SetSendTimout(TIMEOUT_LOGIN_SEND_TELNET);
	mySocket->SetRecvTimout(TIMEOUT_LOGIN_RECV_TELNET);

	recvStreamData(RecvData);
	sendStreamData(CMD_LOGIN_DEFAULT_USERNAME);
	recvStreamData(RecvData);
	sendStreamData(CMD_LOGIN_DEFAULT_PASSWORD);
	recvStreamData(RecvData);

	if (strlen(RecvData) > 0) {
		if (strstr(RecvData, LOGIN_INCORRECT) == NULL
				&& strstr(RecvData, LOGIN_CORRECT) != NULL) {
			blRet = true;
			printf("Already Login...\n");
		} else {
			blRet = false;
		}
	} else {
		blRet = false;
	}

	return blRet;
}

int CTelnet::Login(string ip, string t_password) {
	bool blRet = false;
	char RecvData[MAXDATASIZE];
	memset(RecvData, 0, sizeof(RecvData));

	CSocket *mySocket = CSocket::sharedSocket();

	mySocket->ConnectToHostWithPort(ip, CMD_LOGIN_DEFAULT_PORT);
	mySocket->SetSendTimout(TIMEOUT_LOGIN_SEND_TELNET);
	mySocket->SetRecvTimout(TIMEOUT_LOGIN_RECV_TELNET);

	recvStreamData(RecvData);
	sendStreamData(CMD_LOGIN_DEFAULT_USERNAME);
	recvStreamData(RecvData);
	sendStreamData(t_password);
	recvStreamData(RecvData);

	if (strlen(RecvData) > 0) {
		if (strstr(RecvData, LOGIN_INCORRECT) == NULL) {
			blRet = 1;
			//LOGI("Already Login...%d", 0);
		} else {
			blRet = 0;
			//LOGI("Can not Login...%d", 0);
		}
	} else {
		blRet = -1;
		//LOGI("Can not Login...%d", 1);
	}

	return blRet;
}

int CTelnet::Logout() {
	int Ret = 0;
	char RecvData[MAXDATASIZE];
	memset(RecvData, 0, sizeof(RecvData));

	switch (modelindex) {
	case EN_TELNET_TYPE_A:
	case EN_TELNET_TYPE_B:
		Ret = sendCustomCommandToHost(CMD_LOGOUT_A, RecvData);
		break;

	default:
		Ret = ERR_TELNET_MODEL_EMPTY;
		break;
	}

	CSocket *mySocket = CSocket::sharedSocket();

	mySocket->CloseSocket();
	return Ret;
}

int CTelnet::GetModelName(string& name) {
	int Ret = true;
	char recdata[MAXDATASIZE];
	char* location;
	char seps[] = "\r\n:";
	char* token = NULL;
	memset(recdata, 0, sizeof(recdata));

	Ret = sendCustomCommandToHost(CMD_MODELNAME, recdata);

	if (Ret > 0) {
		location = strstr(recdata, DATA_TOKEN_MODELNAME);

		////LOGI(" GetModelName %s", recdata);

		if (location != NULL) {
			token = strtok(location, seps);
			int count = 0;
			while (token != NULL) {
				if (count == 1) {
					location = token;
					break;
				}
				token = strtok(NULL, seps);
				count++;
			}
			strncpy(recdata, location + 1, strlen(location) - 1);
			recdata[strlen(location) - 1] = '\0';
			name = recdata;
			modelname = recdata;

			//Define nTypeIndex
			if (strstr(recdata, PRODUCT_MODEL_SERIOUS_AMG)
					|| strstr(recdata, PRODUCT_MODEL_1)
					|| strstr(recdata, PRODUCT_MODEL_2) != NULL) {
				modelindex = EN_TELNET_TYPE_A;
			} else if (strstr(recdata, PRODUCT_MODEL_SERIOUS_VMG) != NULL
					|| strstr(recdata, PRODUCT_MODEL_SERIOUS_VMG2) != NULL) {
				modelindex = EN_TELNET_TYPE_B;
            } else if (strstr(recdata, PRODUCT_MODEL_SERIOUS_NBG) != NULL) {
                modelindex = EN_TELNET_TYPE_C;
                
			} else {
				modelindex = 0;
				return ERR_TELNET_MODEL_NOT_SUPPORT;
			}
			return true;

		} else {
			return false;
		}
	}

	return Ret;
}
int CTelnet::ConnectIPoE_VDSL() {

	int Ret = false;
	char RecvData[MAXDATASIZE];
	memset(RecvData, 0, sizeof(RecvData));
	string strtemp, strvpi, strvci;
	stringstream ss;
	stringstream ss2;

	char *location = NULL;
	char *location1 = NULL;

	char space[] = " ";
	char* token = NULL;
	char *sn = NULL;

	switch (modelindex) {
	case EN_TELNET_TYPE_B:

		service_name = "";

        //3312 -> wan show, others -> wan showdefault
        if (strstr(modelname.c_str(), PRODUCT_MODEL_VMG3312) != NULL) {
            Ret = sendCustomCommandToHost(CMD_WAN_SHOW_B, RecvData);
        }
        else Ret = sendCustomCommandToHost(CMD_WAN_SHOWDEFAULT_B, RecvData);
		if (Ret != true)
			return Ret;

		location = strstr(RecvData, DATA_TOKEN_ADDRESS);
		location1 = strstr(RecvData, DATA_TOKEN_PTM);

		if (location != NULL) {

			token = strtok(location, space);

			//LOGI("=========== token 1=%s", token);

			if (location1 != NULL) {
				while (token != NULL) {
                    sn = token;
					token = strtok(NULL, space);
					if (token != NULL){
						/*location_atm = strstr(token, DATA_TOKEN_ATM);
                         if (location_atm != NULL)
                         break;
                         */
                        if(strncmp(token, DATA_TOKEN_PTM, sizeof(DATA_TOKEN_PTM))==0)
                            break;
                    }
                    else return false;
					//sn = token;
				}

			} else {
				//LOGI(" no PTM interface = %d", 0);
				return false;
			}

			//LOGI("xxx service name = %s", sn);
			//string servicename(sn);
            
			service_name = sn;

			strtemp = CMD_WAN_EDIT_B + service_name + CMD_PROTOCOL_IPOE_B;

			Ret = sendCustomCommandToHost(strtemp, RecvData);
		} else
			Ret = false;

		break;

	default:
		Ret = ERR_TELNET_MODEL_EMPTY;
		break;
	}

	return Ret;

}

int CTelnet::ConnectIPoE(int vpi, int vci) {
	int Ret = false;
	char RecvData[MAXDATASIZE];
	memset(RecvData, 0, sizeof(RecvData));
	string strtemp, strvpi, strvci;
	stringstream ss;
	stringstream ss2;

	char *location = NULL;
	char *location2 = NULL;

	char space[] = " ";
	char* token = NULL;
	char *sn = NULL;

	switch (modelindex) {
	case EN_TELNET_TYPE_A:
		Ret = sendCustomCommandToHost(CMD_AUTOHUNT_STOP1_A, RecvData);
		if (Ret != true)
			return Ret;
		Ret = sendCustomCommandToHost(CMD_AUTOHUNT_STOP2_A, RecvData);
		if (Ret != true)
			return Ret;
		Ret = sendCustomCommandToHost(CMD_SYSAUTHMSTC_A, RecvData);
		if (Ret != true)
			return Ret;
		ss << vpi;
		ss >> strvpi;
		strtemp = CMD_SET_VPI_A + strvpi;
		Ret = sendCustomCommandToHost(strtemp, RecvData);
		if (Ret != true)
			return Ret;
		ss.str("");
		ss.clear();
		ss << vci;
		ss >> strvci;
		strtemp = CMD_SET_VCI_A + strvci;
		Ret = sendCustomCommandToHost(strtemp, RecvData);
		if (Ret != true)
			return Ret;
		Ret = sendCustomCommandToHost(CMD_SET_IPOE_ISP_A, RecvData);
		if (Ret != true)
			return Ret;
		Ret = sendCustomCommandToHost(CMD_SET_IPOE_ENCAP_A, RecvData);
		if (Ret != true)
			return Ret;
		Ret = sendCustomCommandToHost(CMD_SET_IPOE_MTU_A, RecvData);
		if (Ret != true)
			return Ret;
		Ret = sendCustomCommandToHost(CMD_SAFE_A, RecvData);
		if (Ret != true)
			return Ret;
		Ret = sendCustomCommandToHost(CMD_COMMIT_A, RecvData);
		if (Ret != true)
			return Ret;
		break;
	case EN_TELNET_TYPE_B:

		service_name = "";

		Ret = sendCustomCommandToHost(CMD_AUTOHUNT_STOP_B, RecvData);
		if (Ret != true)
			return Ret;

        //3312 -> wan show, others -> wan showdefault
        if (strstr(modelname.c_str(), PRODUCT_MODEL_VMG3312) != NULL) {
            Ret = sendCustomCommandToHost(CMD_WAN_SHOW_B, RecvData);
        }
        else Ret = sendCustomCommandToHost(CMD_WAN_SHOWDEFAULT_B, RecvData);
		if (Ret != true)
			return Ret;

		location = strstr(RecvData, DATA_TOKEN_ADDRESS);
		location2 = strstr(RecvData, DATA_TOKEN_ATM);

		if (location != NULL) {

			token = strtok(location, space);

			//LOGI("=========== token 1=%s", token);

			if (location2 != NULL) {
				while (token != NULL) {
                    sn = token;
					token = strtok(NULL, space);
					if (token != NULL){
						/*location_atm = strstr(token, DATA_TOKEN_ATM);
                         if (location_atm != NULL)
                         break;
                         */
                        if(strncmp(token, DATA_TOKEN_ATM, sizeof(DATA_TOKEN_ATM))==0)
                            break;
                    }
                    else return false;
					//sn = token;
				}

			} else {
				//LOGI(" no interface = %d", 0);
				return false;
			}

			//LOGI("xxx service name = %s", sn);
			//string servicename(sn);
            printf("sn=%s\n", sn);
			service_name = sn;

			ss << vpi;
			ss >> strvpi;
			ss2 << vci;
			ss2 >> strvci;
			strtemp = CMD_WAN_EDIT_B + service_name + CMD_VPI_VCI_B + strvpi
					+ TOKEN_SPACE + strvci + CMD_PROTOCOL_IPOE_B;
			Ret = sendCustomCommandToHost(strtemp, RecvData);
			if (Ret != true)
				return Ret;
		} else
			Ret = false;

		break;

	default:
		Ret = ERR_TELNET_MODEL_EMPTY;
		break;
	}

	return Ret;
}

int CTelnet::ConnectPPPoE_VDSL(string c_username, string c_password) {

	int Ret = false;
	char RecvData[MAXDATASIZE];
	memset(RecvData, 0, sizeof(RecvData));
	string strtemp, strvpi, strvci;
	stringstream ss;
	stringstream ss2;

	char *location = NULL;
	char *location1 = NULL;

	char space[] = " ";
	char* token = NULL;
	char *sn = NULL;

	switch (modelindex) {
	case EN_TELNET_TYPE_B:

		service_name = "";

        //3312 -> wan show, others -> wan showdefault
        if (strstr(modelname.c_str(), PRODUCT_MODEL_VMG3312) != NULL) {
            Ret = sendCustomCommandToHost(CMD_WAN_SHOW_B, RecvData);
        }
        else Ret = sendCustomCommandToHost(CMD_WAN_SHOWDEFAULT_B, RecvData);
		if (Ret != true)
			return Ret;

		location = strstr(RecvData, DATA_TOKEN_ADDRESS);
		location1 = strstr(RecvData, DATA_TOKEN_PTM);

		if (location != NULL) {

			token = strtok(location, space);

			//LOGI("=========== token 1=%s", token);

			if (location1 != NULL) {
				while (token != NULL) {
                    sn = token;
					token = strtok(NULL, space);
					if (token != NULL){
						/*location_atm = strstr(token, DATA_TOKEN_ATM);
                         if (location_atm != NULL)
                         break;
                         */
                        if(strncmp(token, DATA_TOKEN_PTM, sizeof(DATA_TOKEN_PTM))==0)
                            break;
                    }
                    else return false;
					//sn = token;
				}

			} else {
				//LOGI(" no PTM interface = %d", 0);
				return false;
			}

			//LOGI("xxx service name = %s", sn);
			//string servicename(sn);

			service_name = sn;

			strtemp = CMD_WAN_EDIT_B + service_name +
			CMD_PROTOCOL_PPPOE_B + c_username + CMD_PASSWORD_B + c_password;

			Ret = sendCustomCommandToHost(strtemp, RecvData);

		} else
			Ret = false;

		break;

	default:
		Ret = ERR_TELNET_MODEL_EMPTY;
		break;
	}

	return Ret;

}
int CTelnet::ConnectPPPoE(int vpi, int vci, string c_username,
		string c_password) {
	int Ret = false;
	char RecvData[MAXDATASIZE];
	memset(RecvData, 0, sizeof(RecvData));
	string strtemp, strvpi, strvci;
	stringstream ss;
	stringstream ss2;

	char *location = NULL;
	char *location2 = NULL;

	char space[] = " ";
	char* token = NULL;
	char *sn = NULL;

	switch (modelindex) {
	case EN_TELNET_TYPE_A:
		Ret = sendCustomCommandToHost(CMD_AUTOHUNT_STOP1_A, RecvData);
		if (Ret != true)
			return Ret;
		Ret = sendCustomCommandToHost(CMD_AUTOHUNT_STOP2_A, RecvData);
		if (Ret != true)
			return Ret;
		Ret = sendCustomCommandToHost(CMD_SYSAUTHMSTC_A, RecvData);
		if (Ret != true)
			return Ret;
		ss << vpi;
		ss >> strvpi;
		strtemp = CMD_SET_VPI_A + strvpi;
		Ret = sendCustomCommandToHost(strtemp, RecvData);
		if (Ret != true)
			return Ret;
		ss.str("");
		ss.clear();
		ss << vci;
		ss >> strvci;
		strtemp = CMD_SET_VCI_A + strvci;
		Ret = sendCustomCommandToHost(strtemp, RecvData);
		if (Ret != true)
			return Ret;
		strtemp = CMD_SET_USERNAME_A + c_username;
		Ret = sendCustomCommandToHost(strtemp, RecvData);
		if (Ret != true)
			return Ret;
		strtemp = CMD_SET_PASSWORD_A + c_password;
		Ret = sendCustomCommandToHost(strtemp, RecvData);
		if (Ret != true)
			return Ret;
		Ret = sendCustomCommandToHost(CMD_SET_PPPOE_ISP_A, RecvData);
		if (Ret != true)
			return Ret;
		Ret = sendCustomCommandToHost(CMD_SET_PPPOE_ENCAP_A, RecvData);
		if (Ret != true)
			return Ret;
		Ret = sendCustomCommandToHost(CMD_SET_PPPOE_MTU_A, RecvData);
		if (Ret != true)
			return Ret;
		Ret = sendCustomCommandToHost(CMD_SAFE_A, RecvData);
		if (Ret != true)
			return Ret;
		Ret = sendCustomCommandToHost(CMD_COMMIT_A, RecvData);
		if (Ret != true)
			return Ret;
		break;
	case EN_TELNET_TYPE_B:

		service_name = "";

		Ret = sendCustomCommandToHost(CMD_AUTOHUNT_STOP_B, RecvData);
		if (Ret != true)
			return Ret;
        //3312 -> wan show, others -> wan showdefault
        if (strstr(modelname.c_str(), PRODUCT_MODEL_VMG3312) != NULL) {
            Ret = sendCustomCommandToHost(CMD_WAN_SHOW_B, RecvData);
        }
        else Ret = sendCustomCommandToHost(CMD_WAN_SHOWDEFAULT_B, RecvData);
		if (Ret != true)
			return Ret;

		location = strstr(RecvData, DATA_TOKEN_ADDRESS);
		location2 = strstr(RecvData, DATA_TOKEN_ATM);

		if (location != NULL) {
            
			token = strtok(location, space);
            //printf(">>%s\n",token);
			
			if (location2 != NULL) {
				
				while (token != NULL) {
                    sn = token;
					token = strtok(NULL, space);
					if (token != NULL){
						/*location_atm = strstr(token, DATA_TOKEN_ATM);
                          if (location_atm != NULL)
                            break;
                         */
                        if(strncmp(token, DATA_TOKEN_ATM, sizeof(DATA_TOKEN_ATM))==0)
                            break;
                    }
                    else return false;
					//sn = token;
				}
			} else {
				//LOGI(" no interface = %d", 0);
				return false;
			}

			//LOGI("xxx service name = %s", sn);
            printf("sn=%s\n",sn);
			service_name = sn;

			ss << vpi;
			ss >> strvpi;
			ss2 << vci;
			ss2 >> strvci;
			strtemp = CMD_WAN_EDIT_B + service_name + CMD_VPI_VCI_B + strvpi
					+ TOKEN_SPACE + strvci +
					CMD_PROTOCOL_PPPOE_B + c_username + CMD_PASSWORD_B
					+ c_password;

			Ret = sendCustomCommandToHost(strtemp, RecvData);
		} else
			Ret = false;

		break;

	default:
		Ret = ERR_TELNET_MODEL_EMPTY;
		break;
	}

	return Ret;
}
int CTelnet::IsEthMode() {
	int Ret = false;
	char RecvData[MAXDATASIZE];
	memset(RecvData, 0, sizeof(RecvData));
	char *location = NULL;
	switch (modelindex) {
	case EN_TELNET_TYPE_B:
		Ret = sendCustomCommandToHost(CMD_GET_MODE, RecvData);
		if (Ret != true)
			return -1;
		location = strstr(RecvData, DATA_TOKEN_ETH);
		if (location != NULL)
			return 1;
		else
			return 0;

		break;
	}
	return Ret;

}
int CTelnet::ConnectEthWAN(string c_username, string c_password) {
	int Ret = false;
	char RecvData[MAXDATASIZE];
	memset(RecvData, 0, sizeof(RecvData));
	string strtemp;

	char *location = NULL;
	char *location_ptm = NULL;

	char space[] = " ";
	char* token = NULL;
	char *sn = NULL;

	switch (modelindex) {
	case EN_TELNET_TYPE_A:
		Ret = sendCustomCommandToHost(CMD_SYSAUTHMSTC_A, RecvData);
		if (Ret != true)
			return Ret;
		strtemp = CMD_SET_ETH_ISP2;
				Ret = sendCustomCommandToHost(strtemp, RecvData);
				if (Ret != true)
					return Ret;
				strtemp = CMD_SET_ETH_MTU2;
						Ret = sendCustomCommandToHost(strtemp, RecvData);
						if (Ret != true)
							return Ret;
						strtemp = CMD_SET_ETH_ENCAP2;
								Ret = sendCustomCommandToHost(strtemp, RecvData);
								if (Ret != true)
									return Ret;


		strtemp = CMD_SET_USERNAME_ETH + c_username;
		Ret = sendCustomCommandToHost(strtemp, RecvData);
		if (Ret != true)
			return Ret;
		strtemp = CMD_SET_PASSWORD_ETH + c_password;
		Ret = sendCustomCommandToHost(strtemp, RecvData);
		if (Ret != true)
			return Ret;
		strtemp = CMD_COMMIT_ETH2;
		Ret = sendCustomCommandToHost(strtemp, RecvData);
		if (Ret != true)
			return Ret;
		strtemp = CMD_SAFE_A;
		Ret = sendCustomCommandToHost(strtemp, RecvData);
		if (Ret != true)
			return Ret;
		break;
	case EN_TELNET_TYPE_B:
		service_name = "";

        //3312 -> wan show, others -> wan showdefault
        if (strstr(modelname.c_str(), PRODUCT_MODEL_VMG3312) != NULL) {
            Ret = sendCustomCommandToHost(CMD_WAN_SHOW_B, RecvData);
        }
        else Ret = sendCustomCommandToHost(CMD_WAN_SHOWDEFAULT_B, RecvData);
		if (Ret != true)
			return Ret;

		location = strstr(RecvData, DATA_TOKEN_ADDRESS);
		if (location != NULL) {
			//LOGI(" found address  = %d", 0);
			token = strtok(location, space);

			//LOGI("=========== token 1=%s", token);

			while (token != NULL) {

				token = strtok(NULL, space);

				if (token == NULL) {
					//LOGI("xx token = NULL, no ETH interface %d", 0);
					return false;
				}

				location_ptm = strstr(token, DATA_TOKEN_ETH);
				if (location_ptm != NULL)
					break;

				sn = token;

			}
			//LOGI("xxx service name = %s", sn);
			//string servicename(sn);
			service_name = sn;

			strtemp = CMD_WAN_EDIT_B + service_name + CMD_PROTOCOL_PPPOE_B
					+ c_username + CMD_PASSWORD_B + c_password;
			Ret = sendCustomCommandToHost(strtemp, RecvData);
			if (Ret != true)
				return Ret;
		} else
			Ret = false;
		break;

	default:
		Ret = ERR_TELNET_MODEL_EMPTY;
		break;
	}

	return Ret;
}

int CTelnet::ConnectPPPoA(int vpi, int vci, string c_username,
		string c_password) {
	int Ret = false;
	char RecvData[MAXDATASIZE];
	memset(RecvData, 0, sizeof(RecvData));
	string strtemp, strvpi, strvci;
	stringstream ss, ss2;
    
    char *location = NULL;
	char *location2 = NULL;
    
    char space[] = " ";
	char* token = NULL;
	char *sn = NULL;

	switch (modelindex) {
	case EN_TELNET_TYPE_A:
		Ret = sendCustomCommandToHost(CMD_AUTOHUNT_STOP1_A, RecvData);
		if (Ret != true)
			return Ret;
		Ret = sendCustomCommandToHost(CMD_AUTOHUNT_STOP2_A, RecvData);
		if (Ret != true)
			return Ret;
		Ret = sendCustomCommandToHost(CMD_SYSAUTHMSTC_A, RecvData);
		if (Ret != true)
			return Ret;
		ss << vpi;
		ss >> strvpi;
		strtemp = CMD_SET_VPI_A + strvpi;
		sendCustomCommandToHost(strtemp, RecvData);
		if (Ret != true)
			return Ret;
		ss.str("");
		ss.clear();
		ss << vci;
		ss >> strvci;
		strtemp = CMD_SET_VCI_A + strvci;
		Ret = sendCustomCommandToHost(strtemp, RecvData);
		if (Ret != true)
			return Ret;
		strtemp = CMD_SET_USERNAME_A + c_username;
		Ret = sendCustomCommandToHost(strtemp, RecvData);
		if (Ret != true)
			return Ret;
		strtemp = CMD_SET_PASSWORD_A + c_password;
		Ret = sendCustomCommandToHost(strtemp, RecvData);
		if (Ret != true)
			return Ret;
		Ret = sendCustomCommandToHost(CMD_SAFE_A, RecvData);
		if (Ret != true)
			return Ret;
		Ret = sendCustomCommandToHost(CMD_COMMIT_A, RecvData);
		if (Ret != true)
			return Ret;
		break;
    
        case EN_TELNET_TYPE_B:
            
            service_name = "";
            
            Ret = sendCustomCommandToHost(CMD_AUTOHUNT_STOP_B, RecvData);
            if (Ret != true)
                return Ret;
            //3312 -> wan show, others -> wan showdefault
            if (strstr(modelname.c_str(), PRODUCT_MODEL_VMG3312) != NULL) {
                Ret = sendCustomCommandToHost(CMD_WAN_SHOW_B, RecvData);
            }
            else Ret = sendCustomCommandToHost(CMD_WAN_SHOWDEFAULT_B, RecvData);
            if (Ret != true)
                return Ret;
            
            location = strstr(RecvData, DATA_TOKEN_ADDRESS);
            location2 = strstr(RecvData, DATA_TOKEN_ATM);
            
            if (location != NULL) {
                
                token = strtok(location, space);
                //printf(">>%s\n",token);
                
                if (location2 != NULL) {
                    
                    while (token != NULL) {
                        sn = token;
                        token = strtok(NULL, space);
                        if (token != NULL){
                            /*location_atm = strstr(token, DATA_TOKEN_ATM);
                             if (location_atm != NULL)
                             break;
                             */
                            if(strncmp(token, DATA_TOKEN_ATM, sizeof(DATA_TOKEN_ATM))==0)
                                break;
                        }
                        else return false;
                        //sn = token;
                    }
                } else {
                    //LOGI(" no interface = %d", 0);
                    return false;
                }
                
                //LOGI("xxx service name = %s", sn);
                printf("sn=%s\n",sn);
                service_name = sn;
                
                ss << vpi;
                ss >> strvpi;
                ss2 << vci;
                ss2 >> strvci;
                strtemp = CMD_WAN_EDIT_B + service_name + CMD_VPI_VCI_B + strvpi
                + TOKEN_SPACE + strvci +
                CMD_PROTOCOL_PPPOA_B + c_username + CMD_PASSWORD_B
                + c_password;
                
                Ret = sendCustomCommandToHost(strtemp, RecvData);
            } else
                Ret = false;
            
            break;

	default:
		Ret = ERR_TELNET_MODEL_EMPTY;
		break;
	}

	return Ret;
}

int CTelnet::GetWanLinkStatus() {
	int Ret = false;
	char recdata[MAXDATASIZE];
	char *location_status = NULL;
	char *location_status2 = NULL;
	char *location_idle = NULL;
//int ret_up=0, ret_down=0;
	char *ret_up = NULL;
	char *ret_down = NULL;

	download_speed = 0;
	upload_speed = 0;

	char space[] = " ";
	char* location;
	char *location_up;
	char* token = NULL;

	stringstream ss;

	memset(recdata, 0, sizeof(recdata));

	switch (modelindex) {
	case EN_TELNET_TYPE_A:
		Ret = sendCustomCommandToHost(CMD_WAN_LINK_STATUS_A, recdata);
		if (Ret != true)
			return Ret;
		if (Ret > 0) {

			//LOGI(" GetWanLinkStatus= %s", recdata);

			location_status = strstr(recdata, DATA_TOKEN_LINK_STATUS);

			ret_up = strstr(recdata, DATA_TOKEN_LINK_UP);
			ret_down = strstr(recdata, DATA_TOKEN_LINK_DOWN);

			//LOGI(" ret_up= %s", ret_up);
			//LOGI(" ret_down= %s", ret_down);

			if (location_status != NULL) {
				if (ret_up != NULL) {
					return 1;
				} else if (ret_down != NULL) {
					return 0;
				} else {
					//LOGI(" GetWanLinkStatus error= %d", 0);
					Ret = 3; //device busy
				}
			} else {
				//LOGI(" GetWanLinkStatus error = %d", 1);
				Ret = 3; //device busy
			}
		}else
			return -1;//connection fail
		break;
	case EN_TELNET_TYPE_B:
		Ret = sendCustomCommandToHost(CMD_WAN_LINK_STATUS_B, recdata);
		//if (Ret != true)
		//return Ret;

		if (Ret > 0) {
			location_status2 = strstr(recdata, DATA_TOKEN_STATUS);
			if (location_status2 != NULL) {

				location_idle = strstr(recdata, DATA_TOKEN_IDLE);
				if (location_idle != NULL)
					return 0; //status is idle

				location_status = strstr(recdata, DATA_TOKEN_SHOWTIME);

				if (location_status != NULL) {

					//download_speed = 0;
					//upload_speed = 0;
					location = strstr(recdata, DATA_TOKEN_LINKRATE_DOWN2);
					if (location != NULL) {
						token = strtok(location, space);
						//LOGI("=========== token 1=%s", token);
						token = strtok(NULL, space);

						//LOGI("=========== token 2=%s", token);
						token = strtok(NULL, space);

						//LOGI("=========== token 3=%s", token);
						token = strtok(NULL, space);

						//LOGI("=========== token 4=%s", token);

						ss << token;
						ss >> download_speed;

						//LOGI("download_speed>>%d", download_speed);

						ss.str("");
						ss.clear();
					}
					location_up = strstr(recdata, DATA_TOKEN_LINKRATE_UP2);
					if (location_up != NULL) {
						token = strtok(location_up, space);
						//LOGI("=========== token 1=%s", token);
						token = strtok(NULL, space);

						//LOGI("=========== token 2=%s", token);
						token = strtok(NULL, space);

						//LOGI("=========== token 3=%s", token);
						token = strtok(NULL, space);

						//LOGI("=========== token 4=%s", token);

						ss << token;
						ss >> upload_speed;

						//LOGI("upload_speed>>%d", upload_speed);

					}

					return 1; //status = showtime

				} else
					return 2; //status = training , started, channel analysis...

			} else {
				return 3; //device is busy, wait 20 sec and try again

			}

		} else {
			return -1; //error
		}

		break;

	default:
		Ret = ERR_TELNET_MODEL_EMPTY;
		break;

	}

	return Ret;
}

int CTelnet::ResetWan() {
	int Ret = false;
	char RecvData[MAXDATASIZE];
	memset(RecvData, 0, sizeof(RecvData));

	switch (modelindex) {
	case EN_TELNET_TYPE_A:
		Ret = sendCustomCommandToHost(CMD_WAN_RESET_A, RecvData);
		break;

	default:
		Ret = ERR_TELNET_MODEL_EMPTY;
		break;
	}

	return Ret;
}

int CTelnet::GetWanLinkRate(int& upload, int& download) {
	int Ret = false;
	char recdata[MAXDATASIZE];
	char downdata[MAXDATASIZE / 4];
	char updata[MAXDATASIZE / 4];
	char *location_down = NULL, *location_up = NULL;
	char seps[] = "\r\n:";
	char *token = NULL;
	memset(recdata, 0, sizeof(recdata));
	memset(downdata, 0, sizeof(downdata));
	memset(updata, 0, sizeof(updata));
	stringstream ss;
	download = 0;
	upload = 0;

	switch (modelindex) {
	case EN_TELNET_TYPE_A:
		Ret = sendCustomCommandToHost(CMD_WAN_LINK_RATE_A, recdata);
		if (Ret > 0) {
			location_down = strstr(recdata, DATA_TOKEN_LINKRATE_DOWN);
			location_up = strstr(recdata, DATA_TOKEN_LINKRATE_UP);
			if (location_down && location_up != NULL) {

				//get download rate
				token = strtok(location_down, seps);
				int count = 0;
				while (token != NULL) {
					if (count == 1) {
						location_down = token;
						break;
					}
					token = strtok(NULL, seps);
					count++;
				}
				strncpy(downdata, location_down + 1,
						strlen(location_down) - 1 - 5);
				downdata[strlen(location_down) - 1 - 5] = '\0';
				ss << downdata;
				ss >> download;
				//download = atoi(recdata);

				ss.str("");
				ss.clear();

				//get upload rate
				token = strtok(location_up, seps);
				count = 0;
				while (token != NULL) {
					if (count == 1) {
						location_up = token;
						break;
					}
					token = strtok(NULL, seps);
					count++;
				}
				strncpy(updata, location_up + 1, strlen(location_up) - 1 - 5);
				updata[strlen(location_up) - 1 - 5] = '\0';
				ss << updata;
				ss >> upload;
				//upload = atoi(recdata);

				return true;
			} else {
				Ret = ERR_TELNET_DATA_NONE;
			}
		}
		break;
	case EN_TELNET_TYPE_B:
		upload = upload_speed;
		download = download_speed;
		return true;
		break;
	default:
		Ret = ERR_TELNET_MODEL_EMPTY;
		break;
	}

	return Ret;
}

int CTelnet::GetPPPAuthenticationStatus() {
	int Ret = true;
	char RecvData[MAXDATASIZE];
	memset(RecvData, 0, sizeof(RecvData));

	switch (modelindex) {
	case EN_TELNET_TYPE_A:
		Ret = sendCustomCommandToHost(CMD_PPP_AUTH_STATUS_A, RecvData);
		if (Ret > 0) {
			if (strstr(RecvData, DATA_TOKEN_PPP_AUTH_FAIL) != NULL) {
				return 0;
			} else {
				return 1;
			}
		} else
			return -1;
		break;
	case EN_TELNET_TYPE_B:
		Ret = sendCustomCommandToHost(CMD_PPP_AUTH_STATUS_B, RecvData);

		if (Ret > 0) {
			if (strstr(RecvData, DATA_TOKEN_PPP_AUTH_FAIL2) != NULL) {
				return 0;
			} else {
				return 1;
			}
		} else
			return -1;
		break;

	default:
		Ret = ERR_TELNET_MODEL_EMPTY;
		break;
	}

	return Ret;
}
int CTelnet::SetEthWANMode() {
	int Ret = false;
	char RecvData[MAXDATASIZE];
	memset(RecvData, 0, sizeof(RecvData));

	switch (modelindex) {
	case EN_TELNET_TYPE_A:
		Ret = sendCustomCommandToHost(CMD_SYSAUTHMSTC_A, RecvData);
		if (Ret != true)
			return Ret;
		Ret = sendCustomCommandToHost(CMD_SET_ETH_ISP, RecvData);
		if (Ret != true)
			return Ret;
		Ret = sendCustomCommandToHost(CMD_SET_ETH_MTU, RecvData);
		if (Ret != true)
			return Ret;
		Ret = sendCustomCommandToHost(CMD_SET_ETH_ENCAP, RecvData);
		if (Ret != true)
			return Ret;
		Ret = sendCustomCommandToHost(CMD_COMMIT_ETH2, RecvData);
		if (Ret != true)
			return Ret;
		Ret = sendCustomCommandToHost(CMD_SAFE_A, RecvData);
		if (Ret != true)
			return Ret;

		Ret = sendCustomCommandToHost(CMD_SET_ETH, RecvData);
		if (Ret != true)
			return Ret;
		Ret = sendCustomCommandToHost(CMD_COMMIT_ETH, RecvData);
		if (Ret != true)
			return Ret;
		Ret = sendCustomCommandToHost(CMD_SAFE_A, RecvData);
		if (Ret != true)
			return Ret;
		sendCustomCommandToHost(CMD_REBOOT, RecvData);
		//if (Ret != true)
		//return Ret;

		break;

	default:
		Ret = ERR_TELNET_MODEL_EMPTY;
		break;
	}

	return Ret;
}
int CTelnet::SetAutoHunt(bool enable) {
	int Ret = false;
	char RecvData[MAXDATASIZE];
	memset(RecvData, 0, sizeof(RecvData));

	switch (modelindex) {
	case EN_TELNET_TYPE_A:
		if (enable == true) {
			Ret = sendCustomCommandToHost(CMD_AUTOHUNT_START_A, RecvData);
		} else {
			Ret = sendCustomCommandToHost(CMD_AUTOHUNT_STOP1_A, RecvData);
			if (Ret != true)
				return Ret;
			Ret = sendCustomCommandToHost(CMD_AUTOHUNT_STOP2_A, RecvData);
			if (Ret != true)
				return Ret;
		}
		break;
	case EN_TELNET_TYPE_B:
		if (enable == true) {
//			Ret = sendCustomCommandToHost(CMD_SHOW_ACTIVE, RecvData);
//						if (Ret != true)
//										return Ret;

//			Ret = sendCustomCommandToHost(CMD_SET_ACTIVE, RecvData);
//			if (Ret != true)
//							return Ret;
//						Ret = sendCustomCommandToHost(CMD_SAVE_ACTIVE, RecvData);
//						if (Ret != true)
//							return Ret;

			Ret = sendCustomCommandToHost(CMD_AUTOHUNT_START_B, RecvData);
		} else {
			Ret = sendCustomCommandToHost(CMD_AUTOHUNT_STOP_B, RecvData);
		}
		break;

	default:
		Ret = ERR_TELNET_MODEL_EMPTY;
		break;
	}

	return Ret;
}

int CTelnet::GetWanIP(string& ip) {
	int Ret = false;
	char RecvData[MAXDATASIZE];
	memset(RecvData, 0, sizeof(RecvData));
	string strtemp;
	char *location_fail = NULL;
	char *location_connected = NULL;
	char space[] = " ";

	char* token = NULL;

	switch (modelindex) {
	case EN_TELNET_TYPE_A:
		Ret = sendCustomCommandToHost(CMD_GET_WAN_IP_A, RecvData);
		if (Ret > 0) {
			location_fail = strstr(RecvData, DATA_TOKEN_GET_WAN_IP_FAIL1);

			if (location_fail != NULL) {
//                if(strstr(RecvData, DATA_TOKEN_GET_WAN_IP_FAIL1) != NULL ||
//                   strstr(RecvData, DATA_TOKEN_GET_WAN_IP_FAIL2) != NULL){
				ip = "";
				Ret = false;
			} else {

				if (strchr(RecvData, '.') != NULL) {
					strtemp = RecvData;
					strtemp = strtemp.substr(
							strtemp.find_first_not_of(DATA_TOKEN_EN));
					strtemp = strtemp.substr(
							strtemp.find_first_not_of(DATA_TOKEN_TRIM));
					strtemp = strtemp.substr(0,
							strtemp.find_last_not_of(DATA_TOKEN_TRIM) + 1);
					ip = strtemp;
					if (ip.empty()) {
						return false;
					} else if (ip.compare(DATA_TOKEN_GET_WAN_IP_FAIL2) == 0) {
						return false;
					}
				} else {
					return false;
				}
				return true;
			}
		} else {
			ip = "ERROR";
			return false;
		}
		break;
	case EN_TELNET_TYPE_B:
        //3312 -> wan show, others -> wan showdefault
        if (strstr(modelname.c_str(), PRODUCT_MODEL_VMG3312) != NULL) {
            Ret = sendCustomCommandToHost(CMD_WAN_SHOW_B, RecvData);
        }
		else Ret = sendCustomCommandToHost(CMD_WAN_SHOWDEFAULT_B, RecvData);
            
		if (Ret > 0) {
			location_connected = strstr(RecvData, DATA_TOKEN_CONNECTED);
			if (location_connected != NULL) {
				token = strtok(location_connected, space);
				//LOGI("=========== token 1=%s", token);
				token = strtok(NULL, space);
				//LOGI("=========== token 2=%s", token);
				token = strtok(NULL, space);
				//LOGI("=========== token 3=%s", token);
                
                if (strchr(token, '.') != NULL) {					
					ip = token;
					if (ip.empty()) {
						return false;
					} else if (ip.compare(DATA_TOKEN_GET_WAN_IP_FAIL2) == 0) {
						return false;
					}
                    else if (ip.compare(DATA_TOKEN_GET_WAN_IP_FAIL3) == 0) {
						return false;
					}
				} else {
					return false;
				}
                
                //ip = token;

				return true;
			} else {
				ip = "";
				return false;
			}

		} else {
			ip = "ERROR";
			return false;
		}
		break;
	default:
		Ret = ERR_TELNET_MODEL_EMPTY;
		break;
	}

	return Ret;
}

int CTelnet::RenewWanIP() {
	int Ret = false;
	char RecvData[MAXDATASIZE];
	memset(RecvData, 0, sizeof(RecvData));

	string strtmp;

	switch (modelindex) {
	case EN_TELNET_TYPE_A:
		Ret = sendCustomCommandToHost(CMD_RENEW_WAN_IP_A, RecvData);
		break;
	case EN_TELNET_TYPE_B:

		strtmp = CMD_WAN_RELEASE_B + service_name;

		Ret = sendCustomCommandToHost(strtmp, RecvData);
		if (Ret != true)
			return Ret;

		strtmp = CMD_WAN_RENEW_B + service_name;

		Ret = sendCustomCommandToHost(strtmp, RecvData);
		break;

	default:
		Ret = ERR_TELNET_MODEL_EMPTY;
		break;
	}

	return Ret;
}

int CTelnet::GetDNSStatus(string& ip) {
	int Ret = false;
	char RecvData[MAXDATASIZE];
	memset(RecvData, 0, sizeof(RecvData));
	string strtemp;
	char *location_fail = NULL;
	char *location_nameserver = NULL;
	char space[] = " ";
	char* token = NULL;

	switch (modelindex) {
	case EN_TELNET_TYPE_A:
		Ret = sendCustomCommandToHost(CMD_GET_DNS_IP_A, RecvData);
		if (Ret > 0) {
			location_fail = strstr(RecvData, DATA_TOKEN_GET_WAN_IP_FAIL1);

			if (location_fail != NULL) {
//                if(strstr(RecvData, DATA_TOKEN_GET_WAN_IP_FAIL1) != NULL ||
//                   strstr(RecvData, DATA_TOKEN_GET_WAN_IP_FAIL2) != NULL){
				ip = "";
				Ret = false;
			} else {

				if (strchr(RecvData, '.') != NULL) {
					strtemp = RecvData;
					strtemp = strtemp.substr(
							strtemp.find_first_not_of(DATA_TOKEN_EN));
					strtemp = strtemp.substr(
							strtemp.find_first_not_of(DATA_TOKEN_TRIM));
					strtemp = strtemp.substr(0,
							strtemp.find_last_not_of(DATA_TOKEN_TRIM) + 1);
					ip = strtemp;
					if (strtemp.empty()) {
						return false;
					} else if (strstr(strtemp.c_str(), DATA_TOKEN_GET_WAN_IP_FAIL2) != NULL) {
						return false;
					}
				} else {
                    ip = "";
					return false;
				}
				return true;
			}
		}
		break;
	case EN_TELNET_TYPE_B:
		Ret = sendCustomCommandToHost(CMD_DNS_STATUS, RecvData);
		if (Ret > 0) {
			location_nameserver = strstr(RecvData, DATA_TOKEN_NAMESERVER);
			if (location_nameserver != NULL) {
				token = strtok(location_nameserver, space);
				//LOGI("=========== token 1=%s", token);
				token = strtok(NULL, space);
				//LOGI("=========== token 2=%s", token);
                
                if (strchr(token, '.') != NULL) {
                    strtemp = token;
                    ip = strtemp;
                    if (strtemp.empty()) {
						return false;
					} else if (strstr(strtemp.c_str(), DATA_TOKEN_GET_WAN_IP_FAIL2) != NULL) {
						return false;
					}
                } else {
                    ip = "";
                    return false;
                }
				return true;
			} else {
				ip = "";
				return false;

			}
		}
		break;
	default:
		Ret = ERR_TELNET_MODEL_EMPTY;
		break;
	}

	return Ret;
}

int CTelnet::RenewDNS() {
	int Ret = false;
	char RecvData[MAXDATASIZE];
	memset(RecvData, 0, sizeof(RecvData));

	string strtmp;

	switch (modelindex) {
	case EN_TELNET_TYPE_A:
		Ret = sendCustomCommandToHost(CMD_RENEW_DNS_IP_A, RecvData);
		break;
	case EN_TELNET_TYPE_B:
		strtmp = CMD_WAN_RELEASE_B + service_name;

		Ret = sendCustomCommandToHost(strtmp, RecvData);
		if (Ret != true)
			return Ret;

		strtmp = CMD_WAN_RENEW_B + service_name;
		Ret = sendCustomCommandToHost(strtmp, RecvData);
		break;

	default:
		Ret = ERR_TELNET_MODEL_EMPTY;
		break;
	}

	return Ret;
}

int CTelnet::Ping(string host) {
	int Ret = false;
	char RecvData[MAXDATASIZE];
	memset(RecvData, 0, sizeof(RecvData));

	switch (modelindex) {
	case EN_TELNET_TYPE_A:
	case EN_TELNET_TYPE_B:
		Ret = sendCustomCommandToHost((CMD_PING_A + host), RecvData);
		if (Ret > 0) {
			if (strstr(RecvData, DATA_TOKEN_PING_SUCCESS) != NULL) {
				return 1;
			} else if (strstr(RecvData, DATA_TOKEN_PING_FAIL1)
					|| strstr(RecvData, DATA_TOKEN_PING_FAIL2)
					|| strstr(RecvData, DATA_TOKEN_PING_FAIL3) != NULL) {
				return 0;
			} else { //timeout
				return 0;
			}
		} else
			return -1;
		break;

	default:
		Ret = ERR_TELNET_MODEL_EMPTY;
		break;
	}

	return Ret;
}

int CTelnet::SetDownloadFW(){
    int Ret = false;
    char RecvData[MAXDATASIZE];
    memset(RecvData, 0, sizeof(RecvData));
   
    switch (modelindex) {
        case EN_TELNET_TYPE_C:
            sendStreamData(CMD_FW_CHECK);
            //sendStreamData(CMD_FW_SET_DOWNLOAD);
            Ret = sendCustomCommandToHost(CMD_FW_SET_DOWNLOAD, RecvData, TIMEOUT_FW_SEND_TELNET, TIMEOUT_FW_RECV_TELNET);
            return 1;
            break;
            
        default:
            Ret = ERR_TELNET_MODEL_EMPTY;
            break;
    }
    
    return Ret;
}

int CTelnet::GetDownloadFWStatus(){
    int Ret = false;
    char RecvData[MAXDATASIZE];
    memset(RecvData, 0, sizeof(RecvData));
    
    switch (modelindex) {
        case EN_TELNET_TYPE_C:
            sendStreamData(CMD_FW_SAVE_LOG_STEP1);
            sendStreamData(CMD_FW_SAVE_LOG_STEP2);
            sendStreamData(CMD_FW_SAVE_LOG_STEP3);
            Ret = sendCustomCommandToHost(CMD_FW_GET_LOG_DOWNLOADSTATUS_STEP1, RecvData, TIMEOUT_FW_SEND_TELNET, TIMEOUT_FW_RECV_TELNET);
            Ret = sendCustomCommandToHost(CMD_FW_GET_LOG_DOWNLOADSTATUS_STEP2, RecvData, TIMEOUT_FW_SEND_TELNET, TIMEOUT_FW_RECV_TELNET);
            if (Ret > 0) {
                if (strstr(RecvData, DATA_TOKEN_FW_SUCCESS) != NULL) {
                    return 1;
                } else {
                    return 0;
                }
            } else //timeout
                return -1;
            break;
            
        default:
            Ret = ERR_TELNET_MODEL_EMPTY;
            break;
    }
    return Ret;    
}

int CTelnet::SetUpgradeFW(){
    int Ret = false;
    char RecvData[MAXDATASIZE];
    memset(RecvData, 0, sizeof(RecvData));
    
    switch (modelindex) {
        case EN_TELNET_TYPE_C:
            sendStreamData(CMD_FW_SET_UPGRADE_STEP1);
            sendStreamData(CMD_FW_SET_UPGRADE_STEP2);
            //sendStreamData(CMD_FW_SET_UPGRADE_STEP3);
            Ret = sendCustomCommandToHost(CMD_FW_SET_UPGRADE_STEP3, RecvData);
            return 1;
            break;
            
        default:
            Ret = ERR_TELNET_MODEL_EMPTY;
            break;
    }
    return Ret;
}

void CTelnet::sendStreamData(string senddata) {
    //string strtemp(senddata);
	senddata = senddata + "\n";
	int len = (int) strlen(senddata.c_str());
	CSocket *mySocket = CSocket::sharedSocket();
	mySocket->SendData((char*) senddata.c_str(), len);
}

int CTelnet::recvStreamData(char* recvdata) {
	int Ret = true;
	int revbyte = 0;
	int tempIndex = 0;
	int count = 0;
	char chData[MAXDATASIZE];
	char chDATAALL[MAXDATASIZE];
	memset(chData, 0, sizeof(chData));
	memset(chDATAALL, 1, sizeof(chDATAALL));

	CSocket *mySocket = CSocket::sharedSocket();

	while (1) {
		Ret = mySocket->RecvData(chData, &revbyte);

		////LOGI("Ret====== = %d\n", Ret);

		tempIndex = tempIndex + (int)strlen(chData);
		if (Ret <= 0) {
			if (Ret == ERR_SOCK_RECV_TIMEOUT && tempIndex != 0) {
				Ret = true;
			} else if (Ret == ERR_SOCK_RECV_TIMEOUT && tempIndex == 0) {
				Ret = ERR_TELNET_DATA_NONE;
				//LOGI("error empty. %d\n", 0);
			}
			break;
		} else if ((tempIndex > MAXDATASIZE)
				|| (tempIndex + strlen(chData) > MAXDATASIZE)) {
			Ret = ERR_TELNET_DATA_OVERFLOW;
			//LOGI("error overflow. %d\n", 0);
			break;
		} else if (count > MAXDATASIZE / 4) {
			//LOGI("error count>MAXDATASIZE %d\n", 0);
			Ret = false;
			break;
		}

		strncpy((chDATAALL + tempIndex - strlen(chData)), chData,
				strlen(chData));
		chDATAALL[tempIndex] = '\0';
		chData[revbyte] = '\0';
		//LOGI("Recv:%s\n", chData);
		count++;
	}
	if (Ret > 0)
		memcpy(recvdata, chDATAALL, sizeof(chDATAALL));
    //printf("RecvInfo:%s\n", recvdata);
    CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
    string aa;
    aa.assign(recvdata);
    errorHandle->handleDebugMessage("\nRecvInfo:"+aa+"\n");
	return Ret;
}

int CTelnet::sendCustomCommandToHost(string sendcommand, char* receivedata) {
	int Ret = true;
	char RecvData[MAXDATASIZE];
	int len = 0;

	//LOGI("sendCustomCommandToHost=%s", sendcommand.c_str());

	CSocket *mySocket = CSocket::sharedSocket();

    //string strtemp(sendcommand);
	sendcommand = sendcommand + DATA_TOKEN_LINE;
	len = (int) strlen(sendcommand.c_str());

	mySocket->SetSendTimout(TIMEOUT_SEND_TELNET);
	mySocket->SetRecvTimout(TIMEOUT_RECV_TELNET);

	sendStreamData(sendcommand);
	Ret = recvStreamData(RecvData);

	if ((Ret > 0) && (strlen(RecvData) > 0)) {

		//LOGI("**RecvData=%s", RecvData);
		////LOGI("**sendcommand=%s",sendcommand.c_str());

		if (strstr(RecvData, sendcommand.c_str()) != NULL) {
			memmove(RecvData, (RecvData + len), (strlen(RecvData) - len));
			RecvData[strlen(RecvData) - len] = '\0';
			memcpy(receivedata, RecvData, sizeof(RecvData));
		} else {
			Ret = false;
		}
	} else {
		//LOGI("sendcommand return false;");
		return false;
	}
    //Remove end of singal at the final location of charater.
	/*
	 char rcv_buf[MAXDATASIZE];
	 memset(rcv_buf, 1, sizeof(rcv_buf));//must be reset '1', otherwise rcv_buf is not continously.
	 char rcv_buf_temp[MAXDATASIZE];
	 memset(rcv_buf_temp, 1, sizeof(rcv_buf_temp));
	 if(strchr(rcv_buf_temp +(strlen(rcv_buf_temp)-2), '#') != NULL){
	 rcv_buf_temp[strlen(rcv_buf_temp)-2] = '\0';
	 }
	 return rcv_buf_temp;
	 */

	return Ret;
}

int CTelnet::sendCustomCommandToHost(string sendcommand, char* receivedata, int sendsecond, int recvsecond) {
    int Ret = true;
    char RecvData[MAXDATASIZE];
    int len = 0;
    
    //LOGI("sendCustomCommandToHost=%s", sendcommand.c_str());
    
    CSocket *mySocket = CSocket::sharedSocket();
    
    //string strtemp(sendcommand);
    sendcommand = sendcommand + DATA_TOKEN_LINE;
    len = (int) strlen(sendcommand.c_str());
    
    mySocket->SetSendTimout(sendsecond);
    mySocket->SetRecvTimout(recvsecond);
    
    sendStreamData(sendcommand);
    Ret = recvStreamData(RecvData);
    
    if ((Ret > 0) && (strlen(RecvData) > 0)) {
        
        //LOGI("**RecvData=%s", RecvData);
        ////LOGI("**sendcommand=%s",sendcommand.c_str());
        
        if (strstr(RecvData, sendcommand.c_str()) != NULL) {
            memmove(RecvData, (RecvData + len), (strlen(RecvData) - len));
            RecvData[strlen(RecvData) - len] = '\0';
            memcpy(receivedata, RecvData, sizeof(RecvData));
        } else {
            Ret = false;
        }
    } else {
        //LOGI("sendcommand return false;");
        return false;
    }
    //Remove end of singal at the final location of charater.
    /*
     char rcv_buf[MAXDATASIZE];
     memset(rcv_buf, 1, sizeof(rcv_buf));//must be reset '1', otherwise rcv_buf is not continously.
     char rcv_buf_temp[MAXDATASIZE];
     memset(rcv_buf_temp, 1, sizeof(rcv_buf_temp));
     if(strchr(rcv_buf_temp +(strlen(rcv_buf_temp)-2), '#') != NULL){
     rcv_buf_temp[strlen(rcv_buf_temp)-2] = '\0';
     }
     return rcv_buf_temp;
     */
    
    return Ret;
}

