/****************************************************************************/
/*
 * Copyright (C) 2000-2015 ZyXEL Communications, Corp.
 * All Rights Reserved.
 *
 * ZyXEL Confidential;
 * Protected as an unpublished work, treated as confidential,
 * and hold in trust and in strict confidence by receiving party.
 * Only the employees who need to know such ZyXEL confidential information
 * to carry out the purpose granted under NDA are allowed to access.
 *
 * The computer program listings, specifications and documentation
 * herein are the property of ZyXEL Communications, Corp. and shall
 * not be reproduced, copied, disclosed, or used in whole or in part
 * for any reason without the prior express written permission of
 * ZyXEL Communications, Corp.
 *
 */
/****************************************************************************/
//
//  CTelnet.h
//
//  Created by Sharon Yang on 2014/1/27.
//  Copyright (c) 2014 ZyXEL. All rights reserved.
//

#pragma once
#include <stdio.h>
#include <iostream>
#include <string>
#include <sstream>
#include "Lib/CSocket/CSocket.h"
#include "APP_Agent/ErrorCode.h"
#include "Telnet/DefineCTelnet.h"
#include "APP_Agent/CErrorHandle.h"

using namespace std;

#define TIMEOUT_LOGIN_SEND_TELNET 2
#define TIMEOUT_LOGIN_RECV_TELNET 4
#define TIMEOUT_SEND_TELNET 5
#define TIMEOUT_RECV_TELNET 5
#define TIMEOUT_FW_SEND_TELNET 2
#define TIMEOUT_FW_RECV_TELNET 3
#define MAXDATASIZE 4096


class CTelnet
{
public:
	CTelnet();
	~CTelnet();

	string t_username;
	string t_password;
	string modelname;
	//CSocket mySocket;

	int Login(string ip, int port, string t_username, string t_password);
	int Login(string ip, int port, string t_password);
	int Login(string ip, int port);
	int Login(string ip, string t_password);
	int Logout();

	int GetModelName(string& modelname);

	int ConnectIPoE(int vpi, int vci);
	int ConnectPPPoE(int vpi, int vci, string c_username, string c_password);
	int ConnectPPPoA(int vpi, int vci, string c_username, string c_password);

	int ConnectIPoE_VDSL();
	int ConnectPPPoE_VDSL(string c_username, string c_password);

	int GetWanLinkStatus(); //Return 1:Up 0:Down
	int ResetWan();
	int GetWanLinkRate(int& upload, int& download); //kbps
	int GetPPPAuthenticationStatus();   // Return 1:Success 0:Fail
	int SetAutoHunt(bool enable);
	int GetWanIP(string& ip);
	int RenewWanIP();
	int GetDNSStatus(string& ip);
	int RenewDNS();
	int Ping(string host); //[IN]ip or hostname
	//int GetServiceName(string& servicename);

	int SetEthWANMode();
	int ConnectEthWAN(string c_username, string c_password);
	int IsEthMode();

	int SetDownloadFW();
	int GetDownloadFWStatus();
	int SetUpgradeFW();

	int sendCustomCommandToHost(string sendcommand, char* receivedata);
	int sendCustomCommandToHost(string sendcommand, char* receivedata, int sendsecond, int recvsecond);

private:
	int modelindex = 0;
	int upload_speed;
	int download_speed;
	string service_name;

	void sendStreamData(string senddata);
	int recvStreamData(char* recvdata);

};


