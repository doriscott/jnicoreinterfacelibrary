#include <jni.h>
#include <android/log.h>

#include <sstream>
#include "APP_Agent/CWiFi.h"

#define  LOG_TAG    "libgl2jni"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

typedef unsigned char byte;
#define BUFFER_OFFSET 0x4
typedef void (*CALLBACK)(int);
static JavaVM *jvm;
static int isRunning;
static JNIEnv *env;

CWiFi myGuestWiFi = *new CWiFi(GUID_GUEST_WIFI);

extern "C" {

JNIEXPORT jstring JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGuestWiFiLib_getGuestWiFiEnable(
		JNIEnv * env, jobject obj);

JNIEXPORT jstring JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGuestWiFiLib_getGuestWiFiSSID(
		JNIEnv * env, jobject obj);

JNIEXPORT jstring JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGuestWiFiLib_getGuestWiFiSecurity(
		JNIEnv * env, jobject obj);

JNIEXPORT jstring JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGuestWiFiLib_getGuestWiFiPassword(
		JNIEnv * env, jobject obj);

JNIEXPORT jobject JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGuestWiFiLib_getGuestWiFiTimer(
		JNIEnv * env, jobject obj);

JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGuestWiFiLib_addGuestWiFiEnableToBuffer(
		JNIEnv * env, jobject obj, jstring status);

JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGuestWiFiLib_addGuestWiFiSSIDToBuffer(
		JNIEnv * env, jobject obj, jstring ssid);

JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGuestWiFiLib_addGuestWiFiSecurityToBuffer(
		JNIEnv * env, jobject obj, jstring security);

JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGuestWiFiLib_addGuestWiFiPasswordToBuffer(
		JNIEnv * env, jobject obj, jstring password);

JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGuestWiFiLib_addGuestWiFiTimerToBuffer(
		JNIEnv * env, jobject obj, jobject profile);

};

JNIEXPORT jstring JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGuestWiFiLib_getGuestWiFiEnable(
		JNIEnv * env, jobject obj) {
	string ret_str = "";
	bool isEnable;
    isEnable = myGuestWiFi.IsEnable();
    if (isEnable)
        ret_str = "true";
    else
    	ret_str = "false";
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGuestWiFiLib_getGuestWiFiSSID(
		JNIEnv * env, jobject obj) {
    string ret_str = "";
    ret_str = myGuestWiFi.GetNetworkName();
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGuestWiFiLib_getGuestWiFiSecurity(
		JNIEnv * env, jobject obj) {
    string ret_str = "";
    EN_WIFI_SECURITY mode;
    mode = myGuestWiFi.GetSecurity();
    if (mode == WPA) {
        ret_str = "WPA";
    } else if (mode == WPA2) {
    	ret_str = "WPA2";
    } else if (mode == WPAPSK) {
    	ret_str = "WPAPSK";
    } else if (mode == WPA2PSK) {
    	ret_str = "WPA2PSK";
    } else if (mode == WEP64) {
    	ret_str = "WEP64Bits";
    } else if (mode == WEP128) {
    	ret_str = "WEP128Bits";
    } else if (mode == NA) {
    	ret_str = "N/A";
    } else {
    	ret_str = "none";
    }
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGuestWiFiLib_getGuestWiFiPassword(
		JNIEnv * env, jobject obj) {
    string ret_str = "";
    ret_str = myGuestWiFi.GetPassword();
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jobject JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGuestWiFiLib_getGuestWiFiTimer(
		JNIEnv * env, jobject obj) {
        jclass guestWifiTimerProfileClass =env->FindClass("com/zyxel/android/jni/model/GuestWiFiTimerProfile");
		jmethodID constructor = env->GetMethodID(guestWifiTimerProfileClass,"<init>", "()V");
		jobject guestWifiTimerProfileObject =  env->NewObject(guestWifiTimerProfileClass, constructor);

		// field in class GuestWiFiProfile
		jfieldID mHours = env->GetFieldID(guestWifiTimerProfileClass, "hours","Ljava/lang/String;");
		jfieldID mMinutes = env->GetFieldID(guestWifiTimerProfileClass, "minutes","Ljava/lang/String;");

		ST_WIFI_TIMER wifi_timer = myGuestWiFi.GetTimer();
		int tmpHours_int = wifi_timer.Hours;
		int tmpMinutes_int = wifi_timer.Minutes;
		stringstream hours_string;
		stringstream minutes_string;
		hours_string << tmpHours_int;
		minutes_string << tmpMinutes_int;

		string retHours_str = hours_string.str();
		string retMinutes_str = minutes_string.str();
//		LOGI("Guest WiFi Timer Hours = %d", wifi_timer.Hours);
//		LOGI("Guest WiFi Timer Minutes = %d", wifi_timer.Minutes);
//		LOGI("Guest WiFi Timer Hours = %s", retHours_str.c_str());
//		LOGI("Guest WiFi Timer Minutes = %s", retMinutes_str.c_str());

		env->SetObjectField(guestWifiTimerProfileObject, mHours,env->NewStringUTF(retHours_str.c_str()));
		env->SetObjectField(guestWifiTimerProfileObject, mMinutes,env->NewStringUTF(retMinutes_str.c_str()));

		return guestWifiTimerProfileObject;
}

JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGuestWiFiLib_addGuestWiFiEnableToBuffer(
		JNIEnv * env, jobject obj, jstring status) {
	bool tmp_bool;
    const char *tmp = env->GetStringUTFChars(status, 0);
	string input_string(tmp);
    if(input_string.compare("TRUE") == 0)
		tmp_bool = true;
	else
		tmp_bool = false;
	myGuestWiFi.SetEnableToBuffer(tmp_bool);
}

JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGuestWiFiLib_addGuestWiFiSSIDToBuffer(
		JNIEnv * env, jobject obj, jstring ssid) {
    const char *tmp = env->GetStringUTFChars(ssid, 0);
	string input_string(tmp);
	myGuestWiFi.SetNetworkNameToBuffer(input_string);
}

JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGuestWiFiLib_addGuestWiFiSecurityToBuffer(
		JNIEnv * env, jobject obj, jstring security) {
    EN_WIFI_SECURITY tmp_security;
    const char *tmp = env->GetStringUTFChars(security, 0);
	string input_string(tmp);
    if(input_string.compare("WPA2PSK") == 0)
        tmp_security = WPA2PSK;
    else
    	tmp_security = NONE;
	myGuestWiFi.SetSecurityToBuffer(tmp_security);
}

JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGuestWiFiLib_addGuestWiFiPasswordToBuffer(
		JNIEnv * env, jobject obj, jstring password) {
    const char *tmp = env->GetStringUTFChars(password, 0);
	string input_string(tmp);
	myGuestWiFi.SetPasswordToBuffer(input_string);

}

JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGuestWiFiLib_addGuestWiFiTimerToBuffer(
		JNIEnv * env, jobject obj, jobject profile) {
vector<ST_WIFI_TIMER> pf_array;
	// class ArrayList
	jclass cls_arraylist = env->GetObjectClass(profile);
	// method in class ArrayList
	jmethodID arraylist_get = env->GetMethodID(cls_arraylist, "get", "(I)Ljava/lang/Object;");
	jmethodID arraylist_size = env->GetMethodID(cls_arraylist, "size", "()I");
	jint profile_size = env->CallIntMethod(profile, arraylist_size);
	LOGI("ArrayList Size =%d", profile_size);

	for(int i = 0; i < profile_size; i++) {
		jobject guestWifiTimerProfileObject = env->CallObjectMethod(profile, arraylist_get, i);

		// class GuestWiFiTimerProfile
		jclass guestWifiTimerProfileClass = env->GetObjectClass(guestWifiTimerProfileObject);

		// method in class GuestWiFiTimerProfile
		jmethodID getHours = env->GetMethodID(guestWifiTimerProfileClass, "getHours", "()Ljava/lang/String;");
		jmethodID getMinutes = env->GetMethodID(guestWifiTimerProfileClass, "getMinutes", "()Ljava/lang/String;");

//		// invoke method in object GuestWiFiTimerProfile
		jstring hours = (jstring)env->CallObjectMethod(guestWifiTimerProfileObject, getHours);
		jstring minutes = (jstring)env->CallObjectMethod(guestWifiTimerProfileObject, getMinutes);

		// converter jstring to char to string
		const char *tmp_hours = env->GetStringUTFChars(hours, 0);
		const char *tmp_minutes = env->GetStringUTFChars(minutes, 0);

		string hours_string(tmp_hours);
		string minutes_string(tmp_minutes);

//		LOGI("hours = %s", hours_string.c_str());
//		LOGI("minutes = %s", minutes_string.c_str());

		int hours_int = atoi(hours_string.c_str());
		int minutes_int = atoi(minutes_string.c_str());

//		LOGI("hours = %d", hours_int);
//		LOGI("minutes = %d", minutes_int);

		ST_WIFI_TIMER st_wifi_timer;
		st_wifi_timer.Hours = hours_int;
		st_wifi_timer.Minutes = minutes_int;

		myGuestWiFi.SetTimerToBuffer(st_wifi_timer);
	}
}







