#include <jni.h>
#include <android/log.h>

#include <sstream>
#include "APP_Agent/CEndDevicePool.h"

typedef unsigned char byte;
#define BUFFER_OFFSET 0x4
typedef void (*CALLBACK)(int);
static JavaVM *jvm;
static int isRunning;
static JNIEnv *env;

vector<ST_EndDevice> end_device_list;
CEndDevicePool myEndDevicePool = *new CEndDevicePool();


extern "C" {

JNIEXPORT jobject JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIEndDevicePoolLib_getTotalEndDevice(
		JNIEnv * env, jobject obj);

JNIEXPORT jobject JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIEndDevicePoolLib_getL2DeviceList(
		JNIEnv * env, jobject obj);

JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIEndDevicePoolLib_addL2ControlWiFiOnToBuffer(
		JNIEnv * env, jobject obj, jstring mac);

JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIEndDevicePoolLib_addL2ControlWiFiOffToBuffer(
		JNIEnv * env, jobject obj, jstring mac);

JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIEndDevicePoolLib_addL2ControlRebootToBuffer(
		JNIEnv * env, jobject obj, jstring mac);

JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIEndDevicePoolLib_addBlockDeviceToBuffer(
		JNIEnv * env, jobject obj, jstring mac);

JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIEndDevicePoolLib_addNonBlockDeviceToBuffer(
		JNIEnv * env, jobject obj, jstring mac);

JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIEndDevicePoolLib_addNewDeviceNameToBuffer(
		JNIEnv * env, jobject obj, jstring mac, jstring name);

JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIEndDevicePoolLib_addNewDeviceTypeToBuffer(
		JNIEnv * env, jobject obj, jstring mac, jint type);
};

JNIEXPORT jobject JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIEndDevicePoolLib_getTotalEndDevice(
		JNIEnv * env, jobject obj){
	//ArrayList Object
    	jclass cls_ArrayList = env->FindClass("java/util/ArrayList");
    	jmethodID construct = env->GetMethodID(cls_ArrayList, "<init>", "()V");
    	jobject obj_ArrayList = env->NewObject(cls_ArrayList, construct, "");
    	jmethodID arrayList_add = env->GetMethodID(cls_ArrayList, "add",
    											   "(Ljava/lang/Object;)Z");

    		jclass EndDeviceProfileClass =env->FindClass("com/zyxel/android/jni/model/EndDeviceProfile");
    		jmethodID constructor = env->GetMethodID(EndDeviceProfileClass,"<init>", "()V");

    		// field in class EndDeviceProfile
    		jfieldID mMAC = env->GetFieldID(EndDeviceProfileClass, "MAC","Ljava/lang/String;");
    		jfieldID mName = env->GetFieldID(EndDeviceProfileClass, "Name","Ljava/lang/String;");
    		jfieldID mUserDefineName = env->GetFieldID(EndDeviceProfileClass, "UserDefineName","Ljava/lang/String;");
    		jfieldID mActive = env->GetFieldID(EndDeviceProfileClass, "Active","Ljava/lang/String;");
    		jfieldID mBlocking = env->GetFieldID(EndDeviceProfileClass, "Blocking","Ljava/lang/String;");
    		jfieldID mIP = env->GetFieldID(EndDeviceProfileClass, "IPAddress","Ljava/lang/String;");
    		jfieldID mConnectionType = env->GetFieldID(EndDeviceProfileClass, "ConnectionType","Ljava/lang/String;");
    		jfieldID mCapabilityType = env->GetFieldID(EndDeviceProfileClass, "CapabilityType","Ljava/lang/String;");
    		jfieldID mSoftwareVersion = env->GetFieldID(EndDeviceProfileClass,"SoftwareVersion", "Ljava/lang/String;");
    		jfieldID mHostType = env->GetFieldID(EndDeviceProfileClass, "HostType","I");
    		jfieldID mL2AutoConfigEnable = env->GetFieldID(EndDeviceProfileClass, "L2AutoConfigEnable","I");
    		jfieldID mL2WifiStatus = env->GetFieldID(EndDeviceProfileClass, "L2WifiStatus","I");
    		jfieldID mL2WifiSyncStatus = env->GetFieldID(EndDeviceProfileClass, "L2WifiSyncStatus","I");
    		jfieldID mL2ControllableMask = env->GetFieldID(EndDeviceProfileClass, "L2ControllableMask","I");
    		jfieldID mSignalStrength = env->GetFieldID(EndDeviceProfileClass, "SignalStrength","I");
    		jfieldID mPhyRate = env->GetFieldID(EndDeviceProfileClass, "PhyRate","I");
    		jfieldID mNeighbor = env->GetFieldID(EndDeviceProfileClass, "Neighbor","Ljava/lang/String;");
    		jfieldID mDLRate = env->GetFieldID(EndDeviceProfileClass, "DLRate","I");
    		jfieldID mULRate = env->GetFieldID(EndDeviceProfileClass, "ULRate","I");
    		jfieldID mGuestGroup = env->GetFieldID(EndDeviceProfileClass, "GuestGroup","I");
    		jfieldID mWiFiAutoConfigApprove = env->GetFieldID(EndDeviceProfileClass, "WiFiAutoConfigApprove","I");
    		jfieldID mManufacturer = env->GetFieldID(EndDeviceProfileClass, "Manufacturer","Ljava/lang/String;");
    		jfieldID mRssi = env->GetFieldID(EndDeviceProfileClass, "Rssi","I");
    		jfieldID mBand = env->GetFieldID(EndDeviceProfileClass, "Band","I");
    		jfieldID mLinkRate24G = env->GetFieldID(EndDeviceProfileClass, "LinkRate24G","I");
    		jfieldID mLinkRate5G = env->GetFieldID(EndDeviceProfileClass, "LinkRate5G","I");
    		jfieldID mChannel24G = env->GetFieldID(EndDeviceProfileClass, "Channel24G","I");
    		jfieldID mChannel5G = env->GetFieldID(EndDeviceProfileClass, "Channel5G","I");

    		end_device_list.clear();
    		end_device_list = myEndDevicePool.GetEndDeviceList();
    		int device_size = end_device_list.size();
    		string name_str = "";
    		string mac_str = "";
    		string active_str = "";
    		string hosttype_str = "";
    		bool blocking = false;
    		string blocking_str = "";
    		string ipaddress_str = "";
    		//		string controlmask_str = "";

    		if (end_device_list.size() > 0) {
    			for (int i = 0; i < end_device_list.size(); i++) {
    				jobject EndDeviceProfileObject = env->NewObject(
    						EndDeviceProfileClass, constructor);
    				ST_EndDevice client = end_device_list.at(i);
    				//		LOGI("Device = %s", client.IPAddress.c_str());
    				name_str = client.Name;
    				mac_str = client.MAC;
    				//				active_str = client.OnlineStatus;
    				hosttype_str = client.ConnectInterface;
    				blocking = client.Block;
    				if (blocking) {
    					blocking_str = "Blocking";
    				} else {
    					blocking_str = "Non-Blocking";
    				}
    				ipaddress_str = client.IPAddress;
    				//				controlmask_str = client.ControlMask;

    //							LOGI("client list=%s", name_str.c_str());
    //							LOGI("client mac=%s", mac_str.c_str());
    //							LOGI("client active=%s", active_str.c_str());
    //							LOGI("client hosttype=%s", hosttype_str.c_str());
    //							LOGI("client blocking=%s", blocking_str.c_str());
    //							LOGI("client ipaddress=%s", ipaddress_str.c_str());
    //				//				LOGI("client controlmask=%s", controlmask_str.c_str());
    //				LOGI("client l2 auto config enable = %d", client.L2AutoConfigEnable);
    //				LOGI("client software version = %s", client.SoftwareVersion.c_str());

    				env->SetObjectField(EndDeviceProfileObject, mName,env->NewStringUTF(name_str.c_str()));
    				env->SetObjectField(EndDeviceProfileObject, mUserDefineName,env->NewStringUTF(client.UserDefineName.c_str()));
    				env->SetObjectField(EndDeviceProfileObject, mMAC,env->NewStringUTF(mac_str.c_str()));
    				env->SetObjectField(EndDeviceProfileObject, mBlocking,env->NewStringUTF(blocking_str.c_str()));
    				env->SetObjectField(EndDeviceProfileObject, mIP,env->NewStringUTF(ipaddress_str.c_str()));
    				env->SetObjectField(EndDeviceProfileObject, mConnectionType,env->NewStringUTF(client.ConnectionType.c_str()));
    				env->SetObjectField(EndDeviceProfileObject, mCapabilityType,env->NewStringUTF(client.CapabilityType.c_str()));
    				env->SetObjectField(EndDeviceProfileObject, mSoftwareVersion,env->NewStringUTF(client.SoftwareVersion.c_str()));
    				env->SetIntField(EndDeviceProfileObject, mHostType, client.hostType);
    				env->SetIntField(EndDeviceProfileObject, mL2AutoConfigEnable, client.L2AutoConfigEnable);
    				env->SetIntField(EndDeviceProfileObject, mL2WifiStatus, client.L2WifiStatus);
    				env->SetIntField(EndDeviceProfileObject, mL2WifiSyncStatus, client.L2AutoConfigStatus);
    				env->SetIntField(EndDeviceProfileObject, mL2ControllableMask, client.ControlMask);
    				env->SetIntField(EndDeviceProfileObject, mSignalStrength, client.SignalStrength);
    				env->SetIntField(EndDeviceProfileObject, mPhyRate, client.PhyRate);
    				env->SetObjectField(EndDeviceProfileObject, mNeighbor,env->NewStringUTF(client.Neighbor.c_str()));
    				env->SetIntField(EndDeviceProfileObject, mDLRate, client.DLRate);
    				env->SetIntField(EndDeviceProfileObject, mULRate, client.ULRate);
    				env->SetIntField(EndDeviceProfileObject, mGuestGroup, client.GuestWiFiGroup);
    				env->SetIntField(EndDeviceProfileObject, mWiFiAutoConfigApprove, client.AutoConfigApprove);
    				env->SetObjectField(EndDeviceProfileObject, mManufacturer, env->NewStringUTF(client.Manufacturer.c_str()));
    				env->SetIntField(EndDeviceProfileObject, mRssi, client.Rssi);
    				env->SetIntField(EndDeviceProfileObject, mBand, client.Band);
    				env->SetIntField(EndDeviceProfileObject, mLinkRate24G, client.LinkRate24G);
    				env->SetIntField(EndDeviceProfileObject, mLinkRate5G, client.LinkRate5G);
    				env->SetIntField(EndDeviceProfileObject, mChannel24G, client.Channel24G);
    				env->SetIntField(EndDeviceProfileObject, mChannel5G, client.Channel5G);
    				env->CallBooleanMethod(obj_ArrayList, arrayList_add,EndDeviceProfileObject);
    			}
    		}
    	return obj_ArrayList;
}

JNIEXPORT jobject JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIEndDevicePoolLib_getL2DeviceList(
		JNIEnv * env, jobject obj) {
	//ArrayList Object
	jclass cls_ArrayList = env->FindClass("java/util/ArrayList");
	jmethodID construct = env->GetMethodID(cls_ArrayList, "<init>", "()V");
	jobject obj_ArrayList = env->NewObject(cls_ArrayList, construct, "");
	jmethodID arrayList_add = env->GetMethodID(cls_ArrayList, "add",
											   "(Ljava/lang/Object;)Z");
		vector<ST_L2FWEndDevice> l2DeviceList = myEndDevicePool.GetL2FWEndDeviceList();
		int dd = l2DeviceList.size();
//		LOGI("GET L2 DEVICE LIST !!!!!!!!!!!!!!%d", dd);
		for (int i = 0; i < l2DeviceList.size(); i++) {
			ST_L2FWEndDevice client = l2DeviceList.at(i);
//			LOGI("Device = %s", client.ModelName.c_str());
//			LOGI("Device = %s", client.MAC.c_str());
//			LOGI("Device = %s", client.NewFWUpgradeVersion.c_str());
//			LOGI("Device = %s", client.NewFWUpgradeDate.c_str());
//			LOGI("Device = %d", client.FWUpgradeState);

			jclass l2FWEndDeviceProfileClass =env->FindClass("com/zyxel/android/jni/model/L2FWEndDeviceProfile");
			jmethodID constructor = env->GetMethodID(l2FWEndDeviceProfileClass,"<init>", "()V");
			jobject l2FWEndDeviceProfileObject =  env->NewObject(l2FWEndDeviceProfileClass, constructor);

			jfieldID mac = env->GetFieldID(l2FWEndDeviceProfileClass, "MAC","Ljava/lang/String;");
			jfieldID modelName = env->GetFieldID(l2FWEndDeviceProfileClass, "ModelName","Ljava/lang/String;");
			jfieldID newFWVersion = env->GetFieldID(l2FWEndDeviceProfileClass, "NewFWVersion","Ljava/lang/String;");
			jfieldID newFWReleaseDate = env->GetFieldID(l2FWEndDeviceProfileClass, "NewFWReleaseDate","Ljava/lang/String;");
			jfieldID fwUpgradeState = env->GetFieldID(l2FWEndDeviceProfileClass, "FWUpgradeState","I");

			env->SetObjectField(l2FWEndDeviceProfileObject, mac,env->NewStringUTF(l2DeviceList.at(i).MAC.c_str()));
			env->SetObjectField(l2FWEndDeviceProfileObject, modelName,env->NewStringUTF(l2DeviceList.at(i).ModelName.c_str()));
			env->SetObjectField(l2FWEndDeviceProfileObject, newFWVersion,env->NewStringUTF(l2DeviceList.at(i).NewFWUpgradeVersion.c_str()));
			env->SetObjectField(l2FWEndDeviceProfileObject, newFWReleaseDate,env->NewStringUTF(l2DeviceList.at(i).NewFWUpgradeDate.c_str()));
			env->SetIntField(l2FWEndDeviceProfileObject, fwUpgradeState, l2DeviceList.at(i).FWUpgradeState);
			env->CallBooleanMethod(obj_ArrayList, arrayList_add,l2FWEndDeviceProfileObject);
		}
	return obj_ArrayList;

}

JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIEndDevicePoolLib_addL2ControlWiFiOnToBuffer(
		JNIEnv * env, jobject obj, jstring mac) {
	const char *tmp = env->GetStringUTFChars(mac, 0);
	string input_string(tmp);
    myEndDevicePool.SetL2DevWiFiOnToBuffer(input_string);
}
JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIEndDevicePoolLib_addL2ControlWiFiOffToBuffer(
		JNIEnv * env, jobject obj, jstring mac){
	const char *tmp = env->GetStringUTFChars(mac, 0);
	string input_string(tmp);
    myEndDevicePool.SetL2DevWiFiOffToBuffer(input_string);
}

JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIEndDevicePoolLib_addL2ControlRebootToBuffer(
		JNIEnv * env, jobject obj, jstring mac){
	const char *tmp = env->GetStringUTFChars(mac, 0);
	string input_string(tmp);
    myEndDevicePool.SetL2DevRebootToBuffer(input_string);
}

JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIEndDevicePoolLib_addBlockDeviceToBuffer(
		JNIEnv * env, jobject obj, jstring mac){
	const char *tmp = env->GetStringUTFChars(mac, 0);
	string input_string(tmp);
    myEndDevicePool.SetBlockToBuffer(input_string);
}

JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIEndDevicePoolLib_addNonBlockDeviceToBuffer(
		JNIEnv * env, jobject obj, jstring mac){
	const char *tmp = env->GetStringUTFChars(mac, 0);
	string input_string(tmp);
    myEndDevicePool.SetNonBlockToBuffer(input_string);
}

JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIEndDevicePoolLib_addNewDeviceNameToBuffer(
		JNIEnv * env, jobject obj, jstring mac, jstring name){
	const char *tmp1 = env->GetStringUTFChars(mac, 0);
	string input_string1(tmp1);
	const char *tmp2 = env->GetStringUTFChars(name, 0);
	string input_string2(tmp2);
		myEndDevicePool.SetNameToBuffer(input_string1,input_string2);
}
JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIEndDevicePoolLib_addNewDeviceTypeToBuffer(
		JNIEnv * env, jobject obj, jstring mac, jint type){
	const char *tmp = env->GetStringUTFChars(mac, 0);
	string input_string(tmp);
    myEndDevicePool.SetTypeToBuffer(input_string, type);
}


