#include <string.h>
#include <jni.h>
#include <android/log.h>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sstream>
#include "APP_Agent/CWiFiRadio.h"

typedef unsigned char byte;
#define BUFFER_OFFSET 0x4
typedef void (*CALLBACK)(int);
static JavaVM *jvm;
static int isRunning;
static JNIEnv *env;
CWiFiRadio myWiFiRadio = *new CWiFiRadio();

extern "C" {

JNIEXPORT jint JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIWiFiRadioLib_getTotalRadioNumber(
		JNIEnv * env, jobject obj);

JNIEXPORT jobject JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIWiFiRadioLib_getWiFiRadioProfile(
		JNIEnv * env, jobject obj);
};

JNIEXPORT jint JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIWiFiRadioLib_getTotalRadioNumber(
		JNIEnv * env, jobject obj) {

	return myWiFiRadio.GetTotalRadioNumber();
}

JNIEXPORT jobject JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIWiFiRadioLib_getWiFiRadioProfile(
		JNIEnv * env, jobject obj) {
	//ArrayList Object
	jclass cls_ArrayList = env->FindClass("java/util/ArrayList");
	jmethodID construct = env->GetMethodID(cls_ArrayList, "<init>", "()V");
	jobject obj_ArrayList = env->NewObject(cls_ArrayList, construct, "");
	jmethodID arrayList_add = env->GetMethodID(cls_ArrayList, "add",
											   "(Ljava/lang/Object;)Z");
	ST_WiFiRadio wifiRadioProfileST = myWiFiRadio.GetWiFiRadioStatus();

	jclass wifiRadioProfile = env->FindClass("com/zyxel/android/jni/model/WiFiRadioProfile");
	jmethodID constructor = env->GetMethodID(wifiRadioProfile,"<init>", "()V");
	jobject wifiRadioProfileObject =  env->NewObject(wifiRadioProfile, constructor);

	jfieldID _24gStatus = env->GetFieldID(wifiRadioProfile, "_24gStatus","I");
	jfieldID _5gStatus = env->GetFieldID(wifiRadioProfile, "_5gStatus","I");
	jfieldID _24gChannel = env->GetFieldID(wifiRadioProfile, "_24gChannel","I");
	jfieldID _5gChannel = env->GetFieldID(wifiRadioProfile, "_5gChannel","I");

	env->SetIntField(wifiRadioProfileObject, _24gStatus, wifiRadioProfileST._24gStatus);
	env->SetIntField(wifiRadioProfileObject, _5gStatus, wifiRadioProfileST._5gStatus);
	env->SetIntField(wifiRadioProfileObject, _24gChannel, wifiRadioProfileST._channel24G);
	env->SetIntField(wifiRadioProfileObject, _5gChannel, wifiRadioProfileST._channel5G);

	env->CallBooleanMethod(obj_ArrayList, arrayList_add,wifiRadioProfileObject);
	return obj_ArrayList;
}






