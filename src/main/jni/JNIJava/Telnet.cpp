#include <string.h>
#include <jni.h>
#include <android/log.h>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sstream>
#include "Telnet/CTelnet.h"

typedef unsigned char byte;
#define BUFFER_OFFSET 0x4
typedef void (*CALLBACK)(int);
static JavaVM *jvm;
static int isRunning;
static JNIEnv *env;
CTelnet mTelnet;

extern "C" {
JNIEXPORT jint JNICALL Java_com_zyxel_android_jni_corelibinterface_JNITelnetLib_loginWithTelnet(
		JNIEnv * env, jobject obj, jstring ip,jint port,jstring username,jstring password);
JNIEXPORT jboolean JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNITelnetLib_logoutWithTelnet(
		JNIEnv * env, jobject obj);

JNIEXPORT jint JNICALL Java_com_zyxel_android_jni_corelibinterface_JNITelnetLib_getModelNameWithTelent(
		JNIEnv * env, jobject obj, jstring modelname);

JNIEXPORT jint JNICALL Java_com_zyxel_android_jni_corelibinterface_JNITelnetLib_setNewFirmwareDownloadWithTelnet(
		JNIEnv * env, jobject obj);

JNIEXPORT jint JNICALL Java_com_zyxel_android_jni_corelibinterface_JNITelnetLib_getNewFirmwareDownloadStatusWithTelnet(
		JNIEnv * env, jobject obj);

JNIEXPORT jint JNICALL Java_com_zyxel_android_jni_corelibinterface_JNITelnetLib_setNewFirmwareUpgradeWithTelnet(
		JNIEnv * env, jobject obj);
};

JNIEXPORT jint JNICALL Java_com_zyxel_android_jni_corelibinterface_JNITelnetLib_loginWithTelnet(
		JNIEnv * env, jobject obj, jstring ip, jint port, jstring username,
		jstring password) {

//	CTelnet mTelnet;
	const char *tmp = env->GetStringUTFChars(ip, 0);
	string t_ip(tmp);

	const char *tmp2 = env->GetStringUTFChars(username, 0);
	string t_username(tmp2);

	const char *tmp3 = env->GetStringUTFChars(password, 0);
	string t_password(tmp3);

//	LOGI(
//			"Java_com_zyxel_android_jni_corelibinterface_JNITelnetLib_login=%s,%s",
//			tmp2, tmp3);

	return mTelnet.Login(t_ip, port, t_username, t_password);

}
JNIEXPORT jboolean JNICALL Java_com_zyxel_android_jni_corelibinterface_JNITelnetLib_logoutWithTelnet(
		JNIEnv * env, jobject obj) {

	LOGI("Java_com_zyxel_android_jni_corelibinterface_JNITelnetLib_logout %d", 0);

	return true;
}

JNIEXPORT jint JNICALL Java_com_zyxel_android_jni_corelibinterface_JNITelnetLib_getModelNameWithTelent(
		JNIEnv * env, jobject obj, jstring modelname){
	const char *tmp = env->GetStringUTFChars(modelname, 0);
	string t_modelName(tmp);
	return mTelnet.GetModelName(t_modelName);
}

JNIEXPORT jint JNICALL Java_com_zyxel_android_jni_corelibinterface_JNITelnetLib_setNewFirmwareDownloadWithTelnet(
		JNIEnv * env, jobject obj){

	return mTelnet.SetDownloadFW();
}

JNIEXPORT jint JNICALL Java_com_zyxel_android_jni_corelibinterface_JNITelnetLib_getNewFirmwareDownloadStatusWithTelnet(
		JNIEnv * env, jobject obj){
	return mTelnet.GetDownloadFWStatus();
}

JNIEXPORT jint JNICALL Java_com_zyxel_android_jni_corelibinterface_JNITelnetLib_setNewFirmwareUpgradeWithTelnet(
		JNIEnv * env, jobject obj){
	return mTelnet.SetUpgradeFW();
}






