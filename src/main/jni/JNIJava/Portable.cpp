#include <jni.h>
#include <android/log.h>

#include <sstream>
#include "APP_Agent/CBatteryInfo.h"
#include "APP_Agent/CMobile.h"
#include "APP_Agent/CMobileMonitor.h"

typedef unsigned char byte;
#define BUFFER_OFFSET 0x4
typedef void (*CALLBACK)(int);
static JavaVM *jvm;
static int isRunning;
static JNIEnv *env;

CMobile myMobile = *new CMobile();
CMobileMonitor myMobileMonitor = *new CMobileMonitor();
CBatteryInfo myBatteryInfo = *new CBatteryInfo();

extern "C" {

JNIEXPORT jstring JNICALL
        Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getAPN(
        JNIEnv *env, jobject obj);

JNIEXPORT jstring JNICALL
        Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getAuthentication(
        JNIEnv *env, jobject obj);

JNIEXPORT jstring JNICALL
        Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getUserName(
        JNIEnv *env, jobject obj);

JNIEXPORT jstring JNICALL
        Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_get3GPassword(
        JNIEnv *env, jobject obj);

JNIEXPORT jstring JNICALL
        Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getSignalStrength(
        JNIEnv *env, jobject obj);

JNIEXPORT jstring JNICALL
        Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getCellularMode(
        JNIEnv *env, jobject obj);

JNIEXPORT jstring JNICALL
        Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getServiceProvider(
        JNIEnv *env, jobject obj);

JNIEXPORT jstring JNICALL
        Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getAutoReset(
        JNIEnv *env, jobject obj);

JNIEXPORT jstring JNICALL
        Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getResetCount(
        JNIEnv *env, jobject obj);

JNIEXPORT jstring JNICALL
        Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getResetDay(
        JNIEnv *env, jobject obj);

JNIEXPORT jstring JNICALL
        Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getLastResetDate(
        JNIEnv *env, jobject obj);

JNIEXPORT jstring JNICALL
        Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getQuota(
        JNIEnv *env, jobject obj);

JNIEXPORT jstring JNICALL
        Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getCurrentUsage(
        JNIEnv *env, jobject obj);

JNIEXPORT jstring JNICALL
        Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getByteSent(
        JNIEnv *env, jobject obj);

JNIEXPORT jstring JNICALL
        Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getByteReceived(
        JNIEnv *env, jobject obj);

JNIEXPORT jstring JNICALL
        Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getUsageAdvice(
        JNIEnv *env, jobject obj);

JNIEXPORT jstring JNICALL
        Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getDataPlanManagementEnable(
        JNIEnv *env, jobject obj);

JNIEXPORT jstring JNICALL
        Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getBatteryRemainCapacity(
        JNIEnv *env, jobject obj);

JNIEXPORT jstring JNICALL
        Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getBatteryTotalCapacity(
        JNIEnv *env, jobject obj);

JNIEXPORT jstring JNICALL
        Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getBatteryChargeStatus(
        JNIEnv *env, jobject obj);

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_addAPNToBuffer(
        JNIEnv *env, jobject obj, jstring apn);

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_addAuthenticationToBuffer(
        JNIEnv *env, jobject obj, jstring authentication);

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_addUserNameToBuffer(
        JNIEnv *env, jobject obj, jstring username);

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_add3GPasswordToBuffer(
        JNIEnv *env, jobject obj, jstring password);

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_addQuotaToBuffer(
        JNIEnv *env, jobject obj, jstring quota);

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_addResetDayToBuffer(
        JNIEnv *env, jobject obj, jstring day);

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_addAutoResetToBuffer(
        JNIEnv *env, jobject obj, jstring status);

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_addDataPlanManagementEnableToBuffer(
        JNIEnv *env, jobject obj, jstring status);
};

JNIEXPORT jstring JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getAPN(
        JNIEnv *env, jobject obj) {
    string ret_str = "";
    ret_str = myMobile.GetAPN();
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getAuthentication(
        JNIEnv *env, jobject obj) {
    string ret_str = "";
    EN_AUTH auth;
    auth = myMobile.GetAuthenication();
    if (auth == PAP)
        ret_str = "PAP";
    else if (auth == CHAR)
        ret_str = "CHAR";
    else
        ret_str = "NONE";
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getUserName(
        JNIEnv *env, jobject obj) {
    string ret_str = "";
    ret_str = myMobile.GetUsername();
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_get3GPassword(
        JNIEnv *env, jobject obj) {
    string ret_str = "";
    ret_str = myMobile.GetPassword();
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getSignalStrength(
        JNIEnv *env, jobject obj) {
    string ret_str = "";
    string tmp;
    stringstream ss(tmp);
    int tmp_int;
    tmp_int = myMobile.GetSignalStength();
    ss << tmp_int;
    ret_str = ss.str();
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getCellularMode(
        JNIEnv *env, jobject obj) {
    string ret_str = "";
    ret_str = myMobile.GetCellularMode();
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getServiceProvider(
        JNIEnv *env, jobject obj) {
    string ret_str = "";
    ret_str = myMobile.GetServiceProvider();
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getAutoReset(
        JNIEnv *env, jobject obj) {
    string ret_str = "";
    bool isEnable;
    isEnable = myMobileMonitor.GetAutoReset();
    if (isEnable)
        ret_str = "true";
    else
        ret_str = "false";
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getResetCount(
        JNIEnv *env, jobject obj) {
    string ret_str = "";
    string tmp;
    stringstream ss(tmp);
    int tmp_int;
    tmp_int = myMobileMonitor.GetResetCounter();
    ss << tmp_int;
    ret_str = ss.str();
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getResetDay(
        JNIEnv *env, jobject obj) {
    string ret_str = "";
    string tmp;
    stringstream ss(tmp);
    int tmp_int;
    tmp_int = myMobileMonitor.GetCycleResetDate();
    ss << tmp_int;
    ret_str = ss.str();
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getLastResetDate(
        JNIEnv *env, jobject obj) {
    string ret_str = "";
    ret_str = myMobileMonitor.GetLastResetDate();
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getQuota(
        JNIEnv *env, jobject obj) {
    string ret_str = "";
    string tmp;
    stringstream ss(tmp);
    int tmp_int;
    tmp_int = myMobileMonitor.GetQuota();
    ss << tmp_int;
    ret_str = ss.str();
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getCurrentUsage(
        JNIEnv *env, jobject obj) {
    string ret_str = "";
    string tmp;
    stringstream ss(tmp);
    float tmp_float;
    tmp_float = myMobileMonitor.GetCurrentUsage();
    ss << tmp_float;
    ret_str = ss.str();
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getByteSent(
        JNIEnv *env, jobject obj) {
    string ret_str = "";
    string tmp;
    stringstream ss(tmp);
    float tmp_float;
    tmp_float = myMobileMonitor.GetDataSend();
    ss << tmp_float;
    ret_str = ss.str();
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getByteReceived(
        JNIEnv *env, jobject obj) {
    string ret_str = "";
    string tmp;
    stringstream ss(tmp);
    float tmp_float;
    tmp_float = myMobileMonitor.GetDataReceived();
    ss << tmp_float;
    ret_str = ss.str();
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getUsageAdvice(
        JNIEnv *env, jobject obj) {
    string ret_str = "";
    string tmp;
    stringstream ss(tmp);
    float tmp_float;
    tmp_float = myMobileMonitor.GetDailyUsageAdvice();
    ss << tmp_float;
    ret_str = ss.str();
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getDataPlanManagementEnable(
        JNIEnv *env, jobject obj) {
    string ret_str = "";
    bool isEnable;
    isEnable = myMobileMonitor.IsEnable();
    if (isEnable)
        ret_str = "true";
    else
        ret_str = "false";
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getBatteryRemainCapacity(
        JNIEnv *env, jobject obj) {
    string ret_str = "";
    string tmp;
    stringstream ss(tmp);
    int tmp_int;
    tmp_int = myBatteryInfo.GetRemainCapacity();
    ss << tmp_int;
    ret_str = ss.str();
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getBatteryTotalCapacity(
        JNIEnv *env, jobject obj) {
    string ret_str = "";
    string tmp;
    stringstream ss(tmp);
    int tmp_int;
    tmp_int = myBatteryInfo.GetTotalCapacity();
    ss << tmp_int;
    ret_str = ss.str();
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_getBatteryChargeStatus(
        JNIEnv *env, jobject obj) {
    string ret_str = "";
    ret_str = myBatteryInfo.GetChargeState();
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_addAPNToBuffer(
        JNIEnv *env, jobject obj, jstring apn) {
    const char *tmp = env->GetStringUTFChars(apn, 0);
    string input_string(tmp);
    myMobile.SetAPNToBuffer(input_string);
}

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_addAuthenticationToBuffer(
        JNIEnv *env, jobject obj, jstring authentication) {
    const char *tmp = env->GetStringUTFChars(authentication, 0);
    string input_string(tmp);
    EN_AUTH tmp_auth;
    if(input_string.compare("PAP") == 0) {
        tmp_auth = PAP;
    } else if(input_string.compare("CHAP") == 0) {
        tmp_auth = CHAR;
    } else
        tmp_auth = _NONE;
    myMobile.SetAuthenicationToBuffer(tmp_auth);
}

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_addUserNameToBuffer(
        JNIEnv *env, jobject obj, jstring username) {
    const char *tmp = env->GetStringUTFChars(username, 0);
    string input_string(tmp);
    myMobile.SetUsernameToBuffer(input_string);
}

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_add3GPasswordToBuffer(
        JNIEnv *env, jobject obj, jstring password) {
    const char *tmp = env->GetStringUTFChars(password, 0);
    string input_string(tmp);
}

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_addQuotaToBuffer(
        JNIEnv *env, jobject obj, jstring quota) {
    const char *tmp = env->GetStringUTFChars(quota, 0);
    string input_string(tmp);
    myMobile.SetPasswordToBuffer(input_string);
}

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_addResetDayToBuffer(
        JNIEnv *env, jobject obj, jstring day) {
    const char *tmp = env->GetStringUTFChars(day, 0);
    string input_string(tmp);
    int tmp_int;
    tmp_int = atoi(input_string.c_str());
    myMobileMonitor.SetCycleResetDateToBuffer(tmp_int);
}

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_addAutoResetToBuffer(
        JNIEnv *env, jobject obj, jstring status) {
    const char *tmp = env->GetStringUTFChars(status, 0);
    string input_string(tmp);
    bool tmp_bool;
    if(input_string.compare("TRUE") == 0)
        tmp_bool = true;
    else
        tmp_bool = false;
    myMobileMonitor.SetAutoResetToBuffer(tmp_bool);
}

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIPortableLib_addDataPlanManagementEnableToBuffer(
        JNIEnv *env, jobject obj, jstring status) {
    const char *tmp = env->GetStringUTFChars(status, 0);
    string input_string(tmp);
    bool tmp_bool;
    if(input_string.compare("TRUE") == 0)
        tmp_bool = true;
    else
        tmp_bool = false;
    myMobileMonitor.SetEnableToBuffer(tmp_bool);
}







