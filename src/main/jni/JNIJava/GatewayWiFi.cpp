#include <string.h>
#include <jni.h>
#include <android/log.h>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sstream>
#include "APP_Agent/CDataMessage.h"
#include "APP_Agent/CErrorHandle.h"
#include "APP_Agent/CTemporary.h"
#include "APP_Agent/CWiFi.h"

#define  LOG_TAG    "libgl2jni"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

typedef unsigned char byte;
#define BUFFER_OFFSET 0x4
typedef void (*CALLBACK)(int);
static JavaVM *jvm;
static int isRunning;
static JNIEnv *env;

CWiFi myWiFi = *new CWiFi(GUID_WIFI);

extern "C" {

JNIEXPORT jstring JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_getWiFiEnable(
		JNIEnv * env, jobject obj);

JNIEXPORT jstring JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_getWiFiSSID(
		JNIEnv * env, jobject obj);

JNIEXPORT jstring JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_getWiFiSecurity(
		JNIEnv * env, jobject obj);

JNIEXPORT jstring JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_getWiFiPassword(
		JNIEnv * env, jobject obj);

JNIEXPORT jstring JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_getWiFiEnableByBand(
		JNIEnv * env, jobject obj, int band);

JNIEXPORT jstring JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_getWiFiSSIDByBand(
		JNIEnv * env, jobject obj, int band);

JNIEXPORT jstring JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_getWiFiSecurityByBand(
		JNIEnv * env, jobject obj, int band);

JNIEXPORT jstring JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_getWiFiPasswordByBand(
		JNIEnv * env, jobject obj, int band);

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_addWiFiEnableToBuffer(
		JNIEnv * env, jobject obj, jstring status);

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_addWiFiSSIDToBuffer(
		JNIEnv * env, jobject obj, jstring ssid);

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_addWiFiSecurityToBuffer(
		JNIEnv * env, jobject obj, jstring security);

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_addWiFiPasswordToBuffer(
		JNIEnv * env, jobject obj, jstring password);

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_addWiFiEnableByBandToBuffer(
		JNIEnv * env, jobject obj, int band, jstring status);

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_addWiFiSSIDByBandToBuffer(
		JNIEnv * env, jobject obj, int band, jstring ssid);

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_addWiFiSecurityByBandToBuffer(
		JNIEnv * env, jobject obj, int band, jstring security);

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_addWiFiPasswordByBandToBuffer(
		JNIEnv * env, jobject obj, int band, jstring password);
};

JNIEXPORT jstring JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_getWiFiEnable(
		JNIEnv * env, jobject obj) {
	string ret_str = "";
	bool isEnable;
	isEnable = myWiFi.IsEnable();
	if (isEnable)
		ret_str = "true";
	else
		ret_str = "false";
	return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_getWiFiSSID(
		JNIEnv * env, jobject obj) {
	string ret_str = "";
	ret_str = myWiFi.GetNetworkName();
	return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_getWiFiSecurity(
		JNIEnv * env, jobject obj) {
	string ret_str = "";
	EN_WIFI_SECURITY mode;
	mode = myWiFi.GetSecurity();
	if (mode == WPA) {
		ret_str = "WPA";
	} else if (mode == WPA2) {
		ret_str = "WPA2";
	} else if (mode == WPAPSK) {
		ret_str = "WPAPSK";
	} else if (mode == WPA2PSK) {
		ret_str = "WPA2PSK";
	} else if (mode == WEP64) {
		ret_str = "WEP64Bits";
	} else if (mode == WEP128) {
		ret_str = "WEP128Bits";
	} else if (mode == NA) {
		ret_str = "N/A";
	} else {
		ret_str = "none";
	}
	return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_getWiFiPassword(
		JNIEnv * env, jobject obj) {
	string ret_str = "";
	ret_str = myWiFi.GetPassword();
	return env->NewStringUTF(ret_str.c_str());
}


JNIEXPORT jstring JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_getWiFiEnableByBand(
		JNIEnv * env, jobject obj, int band) {
	string ret_str = "";
	bool isEnable;
	switch (band) {
		case 0:
			isEnable = myWiFi.GetSSIDEnable(EN_WIFI_BAND_DUAL);
			break;
		case 1:
			isEnable = myWiFi.GetSSIDEnable(EN_WIFI_BAND_24G);
			break;
		case 2:
			isEnable = myWiFi.GetSSIDEnable(EN_WIFI_BAND_5G);
			break;
	}
	if (isEnable)
		ret_str = "true";
	else
		ret_str = "false";
	return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_getWiFiSSIDByBand(
		JNIEnv * env, jobject obj, int band) {
	string ret_str = "";
	switch (band) {
		case 0:
			ret_str = myWiFi.GetNetworkName(EN_WIFI_BAND_DUAL);
			break;
		case 1:
			ret_str = myWiFi.GetNetworkName(EN_WIFI_BAND_24G);
			break;
		case 2:
			ret_str = myWiFi.GetNetworkName(EN_WIFI_BAND_5G);
			break;
	}
	return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_getWiFiSecurityByBand(
		JNIEnv * env, jobject obj, int band) {
	string ret_str = "";
	EN_WIFI_SECURITY mode;
	switch (band) {
		case 0:
			mode = myWiFi.GetSecurity(EN_WIFI_BAND_DUAL);
			break;
		case 1:
			mode = myWiFi.GetSecurity(EN_WIFI_BAND_24G);
			break;
		case 2:
			mode = myWiFi.GetSecurity(EN_WIFI_BAND_5G);
			break;
	}
	if (mode == WPA) {
		ret_str = "WPA";
	} else if (mode == WPA2) {
		ret_str = "WPA2";
	} else if (mode == WPAPSK) {
		ret_str = "WPAPSK";
	} else if (mode == WPA2PSK) {
		ret_str = "WPA2PSK";
	} else if (mode == WEP64) {
		ret_str = "WEP64Bits";
	} else if (mode == WEP128) {
		ret_str = "WEP128Bits";
	} else if (mode == NA) {
		ret_str = "N/A";
	} else {
		ret_str = "none";
	}
	return env->NewStringUTF(ret_str.c_str());

}

JNIEXPORT jstring JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_getWiFiPasswordByBand(
		JNIEnv * env, jobject obj, int band) {
	string ret_str = "";
	switch (band) {
		case 0:
			ret_str = myWiFi.GetPassword(EN_WIFI_BAND_DUAL);
			break;
		case 1:
			ret_str = myWiFi.GetPassword(EN_WIFI_BAND_24G);
			break;
		case 2:
			ret_str = myWiFi.GetPassword(EN_WIFI_BAND_5G);
			break;
	}
	return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_addWiFiEnableToBuffer(
		JNIEnv * env, jobject obj, jstring status) {
	bool tmp_bool;
	const char *tmp = env->GetStringUTFChars(status, 0);
	string input_string(tmp);
	if(input_string.compare("TRUE") == 0)
		tmp_bool = true;
	else
		tmp_bool = false;
	myWiFi.SetEnableToBuffer(tmp_bool);
}

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_addWiFiSSIDToBuffer(
		JNIEnv * env, jobject obj, jstring ssid) {
	const char *tmp = env->GetStringUTFChars(ssid, 0);
	string input_string(tmp);
	myWiFi.SetNetworkNameToBuffer(input_string);
}

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_addWiFiSecurityToBuffer(
		JNIEnv * env, jobject obj, jstring security) {
	EN_WIFI_SECURITY tmp_security;
	const char *tmp = env->GetStringUTFChars(security, 0);
	string input_string(tmp);
	if(input_string.compare("WPA2PSK") == 0)
		tmp_security = WPA2PSK;
	else
		tmp_security = WPA2PSK;
	myWiFi.SetSecurityToBuffer(tmp_security);
}

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_addWiFiPasswordToBuffer(
		JNIEnv * env, jobject obj, jstring password) {
	const char *tmp = env->GetStringUTFChars(password, 0);
	string input_string(tmp);
	myWiFi.SetPasswordToBuffer(input_string);

}

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_addWiFiEnableByBandToBuffer(
		JNIEnv * env, jobject obj, int band, jstring status) {
	bool tmp_bool;
	const char *tmp = env->GetStringUTFChars(status, 0);
	string input_string(tmp);
	if(input_string.compare("TRUE") == 0)
		tmp_bool = true;
	else
		tmp_bool = false;

	switch (band) {
		case 0:
			myWiFi.SetEnableToBuffer(EN_WIFI_BAND_DUAL, tmp_bool);
			break;
		case 1:
			myWiFi.SetEnableToBuffer(EN_WIFI_BAND_24G, tmp_bool);
			break;
		case 2:
			myWiFi.SetEnableToBuffer(EN_WIFI_BAND_5G, tmp_bool);
		    break;
	}
}

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_addWiFiSSIDByBandToBuffer(
		JNIEnv * env, jobject obj, int band, jstring ssid) {
	const char *tmp = env->GetStringUTFChars(ssid, 0);
	string input_string(tmp);
	switch (band) {
		case 0:
			myWiFi.SetNetworkNameToBuffer(EN_WIFI_BAND_DUAL, input_string);
			break;
		case 1:
			myWiFi.SetNetworkNameToBuffer(EN_WIFI_BAND_24G, input_string);
			break;
		case 2:
			myWiFi.SetNetworkNameToBuffer(EN_WIFI_BAND_5G, input_string);
		break;
	}
}

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_addWiFiSecurityByBandToBuffer(
		JNIEnv * env, jobject obj, int band, jstring security) {
	EN_WIFI_SECURITY tmp_security;
	const char *tmp = env->GetStringUTFChars(security, 0);
	string input_string(tmp);
	if(input_string.compare("WPA2PSK") == 0)
		tmp_security = WPA2PSK;
	else
		tmp_security = NONE;
	switch (band) {
		case 0:
			myWiFi.SetSecurityToBuffer(tmp_security);
			break;
		case 1:
			myWiFi.SetSecurityToBuffer(tmp_security);
			break;
		case 2:
			myWiFi.SetSecurityToBuffer(tmp_security);
		break;
	}
}

JNIEXPORT void JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIGatewayWiFiLib_addWiFiPasswordByBandToBuffer(
		JNIEnv * env, jobject obj, int band, jstring password) {
	const char *tmp = env->GetStringUTFChars(password, 0);
	string input_string(tmp);
	switch (band) {
		case 0:
			myWiFi.SetPasswordToBuffer(EN_WIFI_BAND_DUAL, input_string);
			break;
		case 1:
			myWiFi.SetPasswordToBuffer(EN_WIFI_BAND_24G, input_string);
			break;
		case 2:
			myWiFi.SetPasswordToBuffer(EN_WIFI_BAND_5G, input_string);
		break;
	}
}







