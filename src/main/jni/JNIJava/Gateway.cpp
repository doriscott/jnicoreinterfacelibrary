#include <jni.h>
#include <android/log.h>

#include <sstream>
#include "APP_Agent/CDevice.h"
#include "APP_Agent/DevicePool.h"
#include "APP_Agent/CUDPSocket.h"
#include "APP_Agent/DeviceDiscover.h"
#include "APP_Agent/CErrorHandle.h"
#define  LOG_TAG    "libgl2jni"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

typedef unsigned char byte;
#define BUFFER_OFFSET 0x4
typedef void (*CALLBACK)(int);
static JavaVM *jvm;
static int isRunning;
static JNIEnv *env;

CDevice myDevice = *new CDevice();
vector<ST_DEVICE> devicelist;
vector<ST_DEVICE> filter_devicelist;

CErrorHandle *myErrorHandleGateway;

void jniagentcallbackGateway(int id) {
	int status = jvm->AttachCurrentThread(&env, NULL);
	if (status != 0) {

	} else {
		LOGI("Receive Error Code From Gateway.cpp --> %d\n", id);
		jclass JNIEventHandlerClass =
				env->FindClass(
						"com/zyxel/android/jni/listener/JNIEventHandler");
		jmethodID constructor = env->GetMethodID(JNIEventHandlerClass, "<init>",
												 "()V");
		jobject JNIEventHandlerObject = env->NewObject(JNIEventHandlerClass,
													   constructor, "(I)V");
		jmethodID mEventFromJNI = env->GetMethodID(JNIEventHandlerClass,
												   "eventFromJNI", "(I)V");
		env->CallVoidMethod(JNIEventHandlerObject, mEventFromJNI, id);
	}

}

extern "C" {

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_registerErrorCodeBackFunction(
		JNIEnv * env, jobject obj);

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_search(
		JNIEnv * env, jobject obj);

JNIEXPORT jobjectArray JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_queryDeviceList(
		JNIEnv * env, jobject obj, jstring model);

JNIEXPORT jobject JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_queryGatewayList(
		JNIEnv * env, jobject obj);

JNIEXPORT jboolean JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_loginDevice(
		JNIEnv * env, jobject obj, jint index, jstring password);

JNIEXPORT jobject JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getAppFeatureList(
		JNIEnv * env, jobject obj);

JNIEXPORT jstring JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_isNewGatewayFirmwareAvailable(
		JNIEnv * env, jobject obj);

JNIEXPORT jstring JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getNewGatewayFirmwareVersion(
		JNIEnv * env, jobject obj);

JNIEXPORT jstring JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getNewGatewayFirmwareReleaseDate(
		JNIEnv * env, jobject obj);

JNIEXPORT jobject JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getNewGatewayFirmwareInfo(
		JNIEnv * env, jobject obj);

JNIEXPORT jobject JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getWanInfo(
		JNIEnv * env, jobject obj);

JNIEXPORT jobject JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getDeviceInfo(
		JNIEnv * env, jobject obj);

JNIEXPORT jstring JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getGatewayLanIP(
		JNIEnv * env, jobject obj);

JNIEXPORT jstring JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getCurrentFirmwareVersion(
		JNIEnv * env, jobject obj);

JNIEXPORT jstring JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getWiFiAutoConfigEnable(
		JNIEnv * env, jobject obj);

JNIEXPORT jstring JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getDevicePassword(
		JNIEnv * env, jobject obj);

JNIEXPORT jobject JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getTraceRouterResult(
		JNIEnv * env, jobject obj);

JNIEXPORT jobject JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getIPV4Forwarding(
		JNIEnv * env, jobject obj);

JNIEXPORT jint JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getL2FWUpgradingStatus(
		JNIEnv * env, jobject obj, jstring mac);

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_addL2WiFiAutoConfigEnableBuffer(
		JNIEnv * env, jobject obj, jstring mac, jstring operation);

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_addDevicePasswordToBuffer(
		JNIEnv * env, jobject obj, jstring password);

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_setGatewayFirmwareUpgrade(
		JNIEnv * env, jobject obj);

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_setL2ControlFirmwareConfig(
		JNIEnv * env, jobject obj, jstring mac, jint operation);

JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIEndDevicePoolLib_addL2WiFiAutoConfigEnableBuffer(
		JNIEnv * env, jobject obj, jstring mac, jstring operation);

JNIEXPORT jstring JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getNewGatewayFirmwareUpdateStatus(
		JNIEnv * env, jobject obj);

JNIEXPORT jint JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getClient2GatewaySpeedTestPort(
		JNIEnv * env, jobject obj);

JNIEXPORT jint JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_setClient2GatewaySpeedTestEnable(
		JNIEnv * env, jobject obj, jint option);

JNIEXPORT jint JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getGateway2ClientRSSI(
		JNIEnv * env, jobject obj, jstring mobielMAC);

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_setGateway2InternetSpeedTest(
		JNIEnv * env, jobject obj, jstring url, jint operation);

JNIEXPORT jstring JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getGateway2InternetDownload(
		JNIEnv * env, jobject obj);

JNIEXPORT jstring JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getGateway2InternetUpload(
		JNIEnv * env, jobject obj);

JNIEXPORT jstring JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_isGateway2InternetSpeedTestEnd(
		JNIEnv * env, jobject obj);

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_addRebootToBuffer(
		JNIEnv * env, jobject obj);

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_addWiFiAutoConfigEnableToBuffer(
		JNIEnv * env, jobject obj, jstring option);

JNIEXPORT jstring JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getGatewayHostName(
		JNIEnv * env, jobject obj);

JNIEXPORT jstring JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getGatewayHostMAC(
		JNIEnv * env, jobject obj);

JNIEXPORT jint JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getGatewayAutoConfig(
		JNIEnv * env, jobject obj);

JNIEXPORT jobject JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getGatewayHostInfo(
		JNIEnv * env, jobject obj);

JNIEXPORT jint JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_setAPWiFiAutoConfig(
		JNIEnv * env, jobject obj);

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_addSmartHomePowerPlugBuffer(
		JNIEnv * env, jobject obj, jstring protocol, jstring id, jstring op);

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_reloadSmartHomeDaemon(
		JNIEnv * env, jobject obj, jstring protocol);

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_setWiFiAutoConfigApproveProfileSize(
		JNIEnv * env, jobject obj, jint size);

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_addAutoConfigApproveProfileListToBuffer(
		JNIEnv * env, jobject obj, jobject profile);

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_setAutoConfigApproveToBuffer(
		JNIEnv * env, jobject obj, jint option);

JNIEXPORT bool JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getAutoConfigApprove(
		JNIEnv * env, jobject obj);
};

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_registerErrorCodeBackFunction(
		JNIEnv * env, jobject obj){
		    myErrorHandleGateway = CErrorHandle::sharedInstance();
        	if(myErrorHandleGateway) {
        		myErrorHandleGateway->setCallbackFun(jniagentcallbackGateway);
        	} else {
        	}
}

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_search(
		JNIEnv * env, jobject obj) {
	int status = env->GetJavaVM(&jvm);
	if(status != 0) {
		LOGI("Fail to get JVM!!!!!!!!");
	} else {
		LOGI("get JVM!!!!!!!!");
	}
	Broadcast();
}

JNIEXPORT jobject JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_queryGatewayList(
		JNIEnv * env, jobject obj){
	//ArrayList Object
	jclass cls_ArrayList = env->FindClass("java/util/ArrayList");
	jmethodID construct = env->GetMethodID(cls_ArrayList, "<init>", "()V");
	jobject obj_ArrayList = env->NewObject(cls_ArrayList, construct, "");
	jmethodID arrayList_add = env->GetMethodID(cls_ArrayList, "add",
											   "(Ljava/lang/Object;)Z");
	devicelist.clear();
	filter_devicelist.clear();
	devicelist = DevicePool::getDeviceList();
	filter_devicelist = devicelist;
	for (int i = 0; i < filter_devicelist.size(); i++) {


		jclass gatewayProfile =env->FindClass("com/zyxel/android/jni/model/GatewayProfile");
		jmethodID constructor = env->GetMethodID(gatewayProfile,"<init>", "()V");
		jobject gatewayProfileObject =  env->NewObject(gatewayProfile, constructor);

		jfieldID modelName = env->GetFieldID(gatewayProfile, "modelName","Ljava/lang/String;");
		jfieldID IP = env->GetFieldID(gatewayProfile, "IP","Ljava/lang/String;");
		jfieldID firmwareVersion = env->GetFieldID(gatewayProfile, "firmwareVersion","Ljava/lang/String;");
		jfieldID serial = env->GetFieldID(gatewayProfile, "serial","Ljava/lang/String;");
//		jfieldID modelType = env->GetFieldID(gatewayProfile, "type", "Ljava/lang/String;");
		jfieldID modelType = env->GetFieldID(gatewayProfile, "type", "I");

		env->SetObjectField(gatewayProfileObject, modelName,env->NewStringUTF(filter_devicelist.at(i).ModelName.c_str()));
		env->SetObjectField(gatewayProfileObject, IP,env->NewStringUTF(filter_devicelist.at(i).IP.c_str()));
		env->SetObjectField(gatewayProfileObject, firmwareVersion,env->NewStringUTF(filter_devicelist.at(i).SoftwareVersion.c_str()));
		env->SetObjectField(gatewayProfileObject, serial,env->NewStringUTF(filter_devicelist.at(i).SerialNumber.c_str()));
//		env->SetObjectField(gatewayProfileObject, modelType,env->NewStringUTF(filter_devicelist.at(i).DeviceMode.c_str()));
		env->SetIntField(gatewayProfileObject, modelType, filter_devicelist.at(i).DeviceMode);
		env->CallBooleanMethod(obj_ArrayList, arrayList_add,gatewayProfileObject);
	}
	return obj_ArrayList;
}

JNIEXPORT jobjectArray JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_queryDeviceList(
		JNIEnv * env, jobject obj, jstring model) {
	const char *tmp = env->GetStringUTFChars(model, 0);
	string model_string(tmp);
	LOGI("Allow Model Name = %s", model_string.c_str());
	devicelist.clear();
	filter_devicelist.clear();

	jobjectArray ret;
	int index = 0;

	devicelist = DevicePool::getDeviceList();
	if(model_string=="All"){
		LOGI("Allow all device !!!!!!!!!!!!");
		filter_devicelist = devicelist;
	}else{
		int device_size = devicelist.size();
		for (int i = 0; i < device_size; i++) {
			if (devicelist.at(i).ModelName.find(model_string) != std::string::npos) {
				LOGI("Find NBG Router !!!!!!!!!!!!");
				LOGI("Home Router Model Name = %s",
					 devicelist.at(i).ModelName.c_str());
				filter_devicelist.push_back(devicelist.at(i));
			} else {
				LOGI("Find CPE Router !!!!!!!!!!!");
				LOGI("CPE Router Model Name = %s",
					 devicelist.at(i).ModelName.c_str());
//				filter_devicelist.push_back(devicelist.at(i));
			}
		}
	}
	ret = (jobjectArray) env->NewObjectArray(filter_devicelist.size(),
											 env->FindClass("java/lang/String"), env->NewStringUTF(""));
	for (int i = 0; i < filter_devicelist.size(); i++) {
		env->SetObjectArrayElement(ret, i,
								   env->NewStringUTF(filter_devicelist.at(i).ModelName.c_str()));
	}
	return (ret);
}

JNIEXPORT jboolean JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_loginDevice(
		JNIEnv * env, jobject obj, jint index, jstring password) {
	const char *tmp = env->GetStringUTFChars(password, 0);
	string password_string(tmp);
	int login_status = Login(filter_devicelist.at(index), password_string);
	//int login_status = Login(index, password_string);
	if (login_status == 1) {
//		LOGI("Login Successful !!!!!!!!!!!!");
		return true;
	} else {
//		LOGI("Login Fail !!!!!!!!!!!!");
		return false;
	}
	//	return login_status;

}

JNIEXPORT jstring JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_isNewGatewayFirmwareAvailable(
		JNIEnv * env, jobject obj) {
	string ret_str = "";
	bool isEnable;
	isEnable = myDevice.GetHasAvailableVersion();
	if(isEnable)
		ret_str = "true";
	else
		ret_str = "false";
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getNewGatewayFirmwareVersion(
		JNIEnv * env, jobject obj) {
	string ret_str = "";
    ret_str = myDevice.GetAvailableVersion();
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getNewGatewayFirmwareReleaseDate(
		JNIEnv * env, jobject obj) {
	string ret_str = "";
    ret_str = myDevice.GetFWReleaseDate();
    return env->NewStringUTF(ret_str.c_str());
}


JNIEXPORT jobject JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getAppFeatureList(
		JNIEnv * env, jobject obj) {

	//ArrayList Object
	jclass cls_ArrayList = env->FindClass("java/util/ArrayList");
	jmethodID construct = env->GetMethodID(cls_ArrayList, "<init>", "()V");
	jobject obj_ArrayList = env->NewObject(cls_ArrayList, construct, "");
	jmethodID arrayList_add = env->GetMethodID(cls_ArrayList, "add",
											   "(Ljava/lang/Object;)Z");
	ST_AppFeatureInfo appFeatureInfo = DevicePool::getAppFeature();
//	LOGI("app feature AppVersion %s", appFeatureInfo.AppVersion.c_str());
//	LOGI("app feature DeviceInfo %d", appFeatureInfo.DeviceInfo);
//	LOGI("app feature GatewayInfo %d", appFeatureInfo.GatewayInfo);
//	LOGI("app feature FlowInfo %d", appFeatureInfo.FlowInfo);
//	LOGI("app feature Login %d", appFeatureInfo.Login);
//	LOGI("app feature NetTopology %d", appFeatureInfo.NetTopology);
//	LOGI("app feature DeviceCtrl %d", appFeatureInfo.DeviceCtrl);
//	LOGI("app feature L2DeviceCtrl %d", appFeatureInfo.L2DeviceCtrl);
//	LOGI("app feature DevIcon %d", appFeatureInfo.DevIcon);
//	LOGI("app feature GuestWiFi %d", appFeatureInfo.GuestWiFi);
//	LOGI("app feature WiFiAutoCfg %d", appFeatureInfo.WiFiAutoCfg);
//	LOGI("app feature WiFiOnOff %d", appFeatureInfo.WiFiOnOff);
//	LOGI("app feature SpeedTest %d", appFeatureInfo.SpeedTest);
//	LOGI("app feature Diagnostic %d", appFeatureInfo.Diagnostic);
//	LOGI("app feature CloudService %d", appFeatureInfo.CloudService);
//	LOGI("app feature FwUpgrade %d", appFeatureInfo.FwUpgrade);
//	LOGI("app feature LEDOnOff %d", appFeatureInfo.LEDOnOff);
//	LOGI("app feature GuestWiFiGroup %d", appFeatureInfo.GuestWiFiGroup);

	jclass appFeatureInfoProfileClass =env->FindClass("com/zyxel/android/jni/model/AppFeatureInfoProfile");
	jmethodID constructor = env->GetMethodID(appFeatureInfoProfileClass,"<init>", "()V");
	jobject appFeatureInfoObject =  env->NewObject(appFeatureInfoProfileClass, constructor);

	jfieldID AppVersion = env->GetFieldID(appFeatureInfoProfileClass, "AppVersion","Ljava/lang/String;");
	jfieldID DeviceInfo = env->GetFieldID(appFeatureInfoProfileClass, "DeviceInfo","I");
	jfieldID GatewayInfo = env->GetFieldID(appFeatureInfoProfileClass, "GatewayInfo","I");
	jfieldID FlowInfo = env->GetFieldID(appFeatureInfoProfileClass, "FlowInfo","I");
	jfieldID Login = env->GetFieldID(appFeatureInfoProfileClass, "Login","I");
	jfieldID NetTopology = env->GetFieldID(appFeatureInfoProfileClass, "NetTopology","I");
	jfieldID DeviceCtrl = env->GetFieldID(appFeatureInfoProfileClass, "DeviceCtrl","I");
	jfieldID L2DeviceCtrl = env->GetFieldID(appFeatureInfoProfileClass, "L2DeviceCtrl","I");
	jfieldID DevIcon = env->GetFieldID(appFeatureInfoProfileClass, "DevIcon","I");
	jfieldID GuestWiFi = env->GetFieldID(appFeatureInfoProfileClass, "GuestWiFi","I");
	jfieldID WiFiAutoCfg = env->GetFieldID(appFeatureInfoProfileClass, "WiFiAutoCfg","I");
	jfieldID WiFiOnOff = env->GetFieldID(appFeatureInfoProfileClass, "WiFiOnOff","I");
	jfieldID SpeedTest = env->GetFieldID(appFeatureInfoProfileClass, "SpeedTest","I");
	jfieldID Diagnostic = env->GetFieldID(appFeatureInfoProfileClass, "Diagnostic","I");
	jfieldID CloudService = env->GetFieldID(appFeatureInfoProfileClass, "CloudService","I");
	jfieldID FwUpgrade = env->GetFieldID(appFeatureInfoProfileClass, "FwUpgrade","I");
	jfieldID LEDOnOff = env->GetFieldID(appFeatureInfoProfileClass, "LEDOnOff","I");
	jfieldID GuestWiFiGroup = env->GetFieldID(appFeatureInfoProfileClass, "GuestWiFiGroup","I");

	env->SetObjectField(appFeatureInfoObject, AppVersion,env->NewStringUTF(appFeatureInfo.AppVersion.c_str()));
	env->SetIntField(appFeatureInfoObject, DeviceInfo, appFeatureInfo.DeviceInfo);
	env->SetIntField(appFeatureInfoObject, GatewayInfo, appFeatureInfo.GatewayInfo);
	env->SetIntField(appFeatureInfoObject, FlowInfo, appFeatureInfo.FlowInfo);
	env->SetIntField(appFeatureInfoObject, Login, appFeatureInfo.Login);
	env->SetIntField(appFeatureInfoObject, NetTopology, appFeatureInfo.NetTopology);
	env->SetIntField(appFeatureInfoObject, DeviceCtrl, appFeatureInfo.DeviceCtrl);
	env->SetIntField(appFeatureInfoObject, L2DeviceCtrl, appFeatureInfo.L2DeviceCtrl);
	env->SetIntField(appFeatureInfoObject, DevIcon, appFeatureInfo.DevIcon);
	env->SetIntField(appFeatureInfoObject, GuestWiFi, appFeatureInfo.GuestWiFi);
	env->SetIntField(appFeatureInfoObject, WiFiAutoCfg, appFeatureInfo.WiFiAutoCfg);
	env->SetIntField(appFeatureInfoObject, WiFiOnOff, appFeatureInfo.WiFiOnOff);
	env->SetIntField(appFeatureInfoObject, SpeedTest, appFeatureInfo.SpeedTest);
	env->SetIntField(appFeatureInfoObject, Diagnostic, appFeatureInfo.Diagnostic);
	env->SetIntField(appFeatureInfoObject, CloudService, appFeatureInfo.CloudService);
	env->SetIntField(appFeatureInfoObject, FwUpgrade, appFeatureInfo.FwUpgrade);
	env->SetIntField(appFeatureInfoObject, LEDOnOff, appFeatureInfo.LEDOnOff);
	env->SetIntField(appFeatureInfoObject, GuestWiFiGroup, appFeatureInfo.GuestWiFiGroup);

	env->CallBooleanMethod(obj_ArrayList, arrayList_add,appFeatureInfoObject);
	return obj_ArrayList;
}

JNIEXPORT jobject JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getWanInfo(
		JNIEnv * env, jobject obj) {
		ST_WanInfo wanInfo = myDevice.GetWanInfo();

		jclass WanInfoProfileClass =
				env->FindClass("com/zyxel/android/jni/model/WanInfoProfile");
		jmethodID constructor = env->GetMethodID(WanInfoProfileClass,"<init>", "()V");
		jobject wanInfoObject =  env->NewObject(WanInfoProfileClass, constructor);

		jfieldID wanStatus = env->GetFieldID(WanInfoProfileClass, "WanStatus","Ljava/lang/String;");
		jfieldID wanIP = env->GetFieldID(WanInfoProfileClass, "WanIP","Ljava/lang/String;");
		jfieldID wanMAC = env->GetFieldID(WanInfoProfileClass, "WanMAC","Ljava/lang/String;");
		jfieldID wanPhyRate = env->GetFieldID(WanInfoProfileClass, "WanPhyRate","I");
		jfieldID wanTX = env->GetFieldID(WanInfoProfileClass, "WanTX","I");
		jfieldID wanRX = env->GetFieldID(WanInfoProfileClass, "WanRX","I");

		env->SetObjectField(wanInfoObject, wanStatus,env->NewStringUTF(wanInfo.WanStatus.c_str()));
		env->SetObjectField(wanInfoObject, wanIP,env->NewStringUTF(wanInfo.WanIP.c_str()));
		env->SetObjectField(wanInfoObject, wanMAC,env->NewStringUTF(wanInfo.WanMAC.c_str()));
		env->SetIntField(wanInfoObject, wanPhyRate, wanInfo.WanPhyRate);
		env->SetIntField(wanInfoObject, wanTX, wanInfo.WanTX);
		env->SetIntField(wanInfoObject, wanRX, wanInfo.WanRX);
		return wanInfoObject;
}

JNIEXPORT jobject JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getDeviceInfo(
		JNIEnv * env, jobject obj) {
        ST_DeviceInfo deviceInfo = myDevice.GetDeviceInfo();

		jclass DeviceInfoProfileClass =
				env->FindClass("com/zyxel/android/jni/model/DeviceInfoProfile");
		jmethodID constructor = env->GetMethodID(DeviceInfoProfileClass,"<init>", "()V");
		jobject deviceInfoObject =  env->NewObject(DeviceInfoProfileClass, constructor);

		jfieldID systemTime = env->GetFieldID(DeviceInfoProfileClass, "SystemTime","Ljava/lang/String;");
		jfieldID cpuUsage = env->GetFieldID(DeviceInfoProfileClass, "CPU_Usage","I");
		jfieldID upTime = env->GetFieldID(DeviceInfoProfileClass, "UpTime","I");

		env->SetObjectField(deviceInfoObject, systemTime,env->NewStringUTF(deviceInfo.SystemTime.c_str()));
		env->SetIntField(deviceInfoObject, cpuUsage, deviceInfo.CPU_Usage);
		env->SetIntField(deviceInfoObject, upTime, deviceInfo.UpTime);
		return deviceInfoObject;
}

JNIEXPORT jstring JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getGatewayLanIP(
		JNIEnv * env, jobject obj) {
	string ret_str = "";
    ret_str = myDevice.getGatewayLANIP();
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getCurrentFirmwareVersion(
		JNIEnv * env, jobject obj) {
	string ret_str = "";
    ret_str = myDevice.GetCurrentVersion();
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getWiFiAutoConfigEnable(
		JNIEnv * env, jobject obj) {
	string ret_str = "";
	bool isEnable;
	isEnable = myDevice.GetWiFiAutoConfig();
	if (isEnable)
		ret_str = "true";
	else
		ret_str = "false";
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getDevicePassword(
		JNIEnv * env, jobject obj){
	string ret_str = "";
	ret_str = myDevice.GetConfigPassword();
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jobject JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getTraceRouterResult(
		JNIEnv * env, jobject obj) {
	//ArrayList Object
	jclass cls_ArrayList = env->FindClass("java/util/ArrayList");
	jmethodID construct = env->GetMethodID(cls_ArrayList, "<init>", "()V");
	jobject obj_ArrayList = env->NewObject(cls_ArrayList, construct, "");
	jmethodID arrayList_add = env->GetMethodID(cls_ArrayList, "add",
											   "(Ljava/lang/Object;)Z");
    vector<ST_RouteHop> routehops = myDevice.TraceRouter();

		for(int i = 0 ; i < routehops.size() ; i++ ){
			jclass routeHopProfileClass =env->FindClass("com/zyxel/android/jni/model/RouteHopProfile");
			jmethodID constructor = env->GetMethodID(routeHopProfileClass,"<init>", "()V");
			jobject routeHopObject =  env->NewObject(routeHopProfileClass, constructor);

			jfieldID host = env->GetFieldID(routeHopProfileClass, "Host","Ljava/lang/String;");
			jfieldID hostAddress = env->GetFieldID(routeHopProfileClass, "HostAddress","Ljava/lang/String;");
			jfieldID rttTime = env->GetFieldID(routeHopProfileClass, "RTTimes","Ljava/lang/String;");
			//		jfieldID wanTX = env->GetFieldID(WanInfoProfileClass, "WanTX","I");

			env->SetObjectField(routeHopObject, host,env->NewStringUTF(routehops.at(i).Host.c_str()));
			env->SetObjectField(routeHopObject, hostAddress,env->NewStringUTF(routehops.at(i).HostAddress.c_str()));
			env->SetObjectField(routeHopObject, rttTime,env->NewStringUTF(routehops.at(i).RTTimes.c_str()));
			//		env->SetIntField(wanInfoObject, wanPhyRate, wanInfo.WanPhyRate);
			env->CallBooleanMethod(obj_ArrayList, arrayList_add,routeHopObject);
		}
	return obj_ArrayList;
}

JNIEXPORT jobject JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getIPV4Forwarding(
		JNIEnv * env, jobject obj) {
	//ArrayList Object
	jclass cls_ArrayList = env->FindClass("java/util/ArrayList");
	jmethodID construct = env->GetMethodID(cls_ArrayList, "<init>", "()V");
	jobject obj_ArrayList = env->NewObject(cls_ArrayList, construct, "");
	jmethodID arrayList_add = env->GetMethodID(cls_ArrayList, "add",
											   "(Ljava/lang/Object;)Z");
		vector<ST_IPV4Forwarding> ipv4Forwarding = myDevice.GetIPV4Forwarding();

		for(int i = 0 ; i < ipv4Forwarding.size() ; i++ ){
			jclass ipv4ForwardingProfileClass =env->FindClass("com/zyxel/android/jni/model/IPV4ForwardingProfile");
			jmethodID constructor = env->GetMethodID(ipv4ForwardingProfileClass,"<init>", "()V");
			jobject ipv4ForwardingObject =  env->NewObject(ipv4ForwardingProfileClass, constructor);

			jfieldID destIPAddress = env->GetFieldID(ipv4ForwardingProfileClass, "DestIPAddress","Ljava/lang/String;");
			jfieldID destSubnetMask = env->GetFieldID(ipv4ForwardingProfileClass, "DestSubnetMask","Ljava/lang/String;");
			jfieldID gatewayIPAddress = env->GetFieldID(ipv4ForwardingProfileClass, "GatewayIPAddress","Ljava/lang/String;");
			jfieldID interface = env->GetFieldID(ipv4ForwardingProfileClass, "Interface","Ljava/lang/String;");

			env->SetObjectField(ipv4ForwardingObject, destIPAddress,env->NewStringUTF(ipv4Forwarding.at(i).DestIPAddress.c_str()));
			env->SetObjectField(ipv4ForwardingObject, destSubnetMask,env->NewStringUTF(ipv4Forwarding.at(i).DestSubnetMask.c_str()));
			env->SetObjectField(ipv4ForwardingObject, gatewayIPAddress,env->NewStringUTF(ipv4Forwarding.at(i).GatewayIPAddress.c_str()));
			env->SetObjectField(ipv4ForwardingObject, interface,env->NewStringUTF(ipv4Forwarding.at(i).Interface.c_str()));

			env->CallBooleanMethod(obj_ArrayList, arrayList_add,ipv4ForwardingObject);
		}
	return obj_ArrayList;
}

JNIEXPORT jint JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getL2FWUpgradingStatus(
		JNIEnv * env, jobject obj, jstring mac) {
	const char *tmp = env->GetStringUTFChars(mac, 0);
	string mac_string(tmp);
//	LOGI("UPGRADE L2 DEVICE MAC = %s", mac_string.c_str());
	return myDevice.GetL2DevCtrlFWState(mac_string);
}

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_addL2WiFiAutoConfigEnableBuffer(
		JNIEnv * env, jobject obj, jstring mac, jstring operation) {
	const char *tmp1 = env->GetStringUTFChars(mac, 0);
	string input_string1(tmp1);
	const char *tmp2 = env->GetStringUTFChars(operation, 0);
	string input_string2(tmp2);
			if(input_string2.compare("TRUE") == 0){
				myDevice.SetL2DevCtrlAutoConfigToBuffer(tmp1, true);
			}else{
				myDevice.SetL2DevCtrlAutoConfigToBuffer(tmp1, false);
			}
}

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_addDevicePasswordToBuffer(
		JNIEnv * env, jobject obj, jstring password) {
	const char *tmp = env->GetStringUTFChars(password, 0);
	string input_string(tmp);
    myDevice.SetConfigPasswordToBuffer(input_string.c_str());
}

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_addFirmwareUpgradeToBuffer(
		JNIEnv * env, jobject obj, jstring operation) {
	const char *tmp = env->GetStringUTFChars(operation, 0);
	bool tmp_bool;
	string input_string(tmp);
	if(input_string.compare("TRUE") == 0)
		tmp_bool = true;
	else
		tmp_bool = false;
	myDevice.UpdateSoftware();
}

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_setGatewayFirmwareUpgrade(
		JNIEnv * env, jobject obj) {
    myDevice.UpdateSoftware();
}

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_setL2ControlFirmwareConfig(
		JNIEnv * env, jobject obj, jstring mac, jint operation) {
	const char *tmp = env->GetStringUTFChars(mac, 0);
	string input_string(tmp);
			if(operation == 0) {
//				LOGI("SET L2 CONTROL FW CONFIG = FALSE");
				myDevice.SetL2DevCtrlFWConfig(input_string, 0);
			} else {
//				LOGI("SET L2 CONTROL FW CONFIG = TRUE");
				myDevice.SetL2DevCtrlFWConfig(input_string, 1);
			}
}

JNIEXPORT jstring JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getNewGatewayFirmwareUpdateStatus(
		JNIEnv * env, jobject obj) {
        string ret_str = "";
        int tmp_int;
        string tmp;
        stringstream ss(tmp);
		tmp_int = myDevice.GetFWUpdateState();
		ss << tmp_int;
		ret_str = ss.str();
		return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jint JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getClient2GatewaySpeedTestPort(
		JNIEnv * env, jobject obj){
	return myDevice.GetGwSpeedTestPort();
}

JNIEXPORT jint JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_setClient2GatewaySpeedTestEnable(
		JNIEnv * env, jobject obj, jint option){
	if(option == 1) {
		return myDevice.SetGwSpeedTestEnable(true);
	} else {
		return myDevice.SetGwSpeedTestEnable(false);
	}
}

JNIEXPORT jint JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getGateway2ClientRSSI(
		JNIEnv * env, jobject obj, jstring mobielMAC) {
	const char *tmp = env->GetStringUTFChars(mobielMAC, 0);
	string t_mobileMAC(tmp);
//	LOGI("RSSI JNI = %d", myDevice.GetGateway2ClientRssi(t_mobileMAC));
	return myDevice.GetGateway2ClientRssi(t_mobileMAC);
}

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_setGateway2InternetSpeedTest(
		JNIEnv * env, jobject obj, jstring url, jint operation) {
		const char *tmp = env->GetStringUTFChars(url, 0);
    	string input_string(tmp);
			if (operation == 0) {
//				LOGI("download");
				myDevice.SetSpeedTest(input_string, false);
			} else {
//				LOGI("upload");
				myDevice.SetSpeedTest(input_string, true);
			}
}

JNIEXPORT jstring JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getGateway2InternetDownload(
		JNIEnv * env, jobject obj) {
	string ret_str = "";
    ret_str = myDevice.GetSpeedTestDownload();
	return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getGateway2InternetUpload(
		JNIEnv * env, jobject obj) {
	string ret_str = "";
    ret_str = myDevice.GetSpeedTestUpload();
	return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_isGateway2InternetSpeedTestEnd(
		JNIEnv * env, jobject obj) {
	string ret_str = "";
	bool isEnable;
	isEnable = myDevice.GetSpeedTestEnd();
	if (isEnable)
		ret_str = "true";
	else
		ret_str = "false";
    return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_addRebootToBuffer(
		JNIEnv * env, jobject obj) {
    myDevice.Reboot();
}

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_addWiFiAutoConfigEnableToBuffer(
		JNIEnv * env, jobject obj, jstring option) {

	const char *tmp = env->GetStringUTFChars(option, 0);
	bool tmp_bool;
	string input_string(tmp);
	if(input_string.compare("TRUE") == 0)
		tmp_bool = true;
	else
		tmp_bool = false;
	myDevice.SetWiFiAutoConfigToBuffer(tmp_bool);
}

JNIEXPORT jstring JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getGatewayHostName(
		JNIEnv * env, jobject obj) {
	string ret_str = "";
	ret_str = myDevice.GetGatewayHostName();
	return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jstring JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getGatewayHostMAC(
		JNIEnv * env, jobject obj) {
	string ret_str = "";
	ret_str = myDevice.GetGatewayHostMAC();
	return env->NewStringUTF(ret_str.c_str());
}

JNIEXPORT jint JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getGatewayAutoConfig(
		JNIEnv * env, jobject obj) {
	return myDevice.GetGatewayAutoConfig();
}

JNIEXPORT jobject JNICALL
Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getGatewayHostInfo(
		JNIEnv * env, jobject obj) {
//ArrayList Object
	jclass cls_ArrayList = env->FindClass("java/util/ArrayList");
	jmethodID construct = env->GetMethodID(cls_ArrayList, "<init>", "()V");
	jobject obj_ArrayList = env->NewObject(cls_ArrayList, construct, "");
	jmethodID arrayList_add = env->GetMethodID(cls_ArrayList, "add",
											   "(Ljava/lang/Object;)Z");
	ST_GatewayHostInfo gatewayHostInfo = myDevice.GetGatewayHostInfo();

	jclass gatewayHostInfoProfileClass =env->FindClass("com/zyxel/android/jni/model/GatewayHostInfoProfile");
	jmethodID constructor = env->GetMethodID(gatewayHostInfoProfileClass,"<init>", "()V");
	jobject gatewayHostInfoObject =  env->NewObject(gatewayHostInfoProfileClass, constructor);

	jfieldID hostName = env->GetFieldID(gatewayHostInfoProfileClass, "hostName","Ljava/lang/String;");
	jfieldID hostMAC = env->GetFieldID(gatewayHostInfoProfileClass, "hostMAC","Ljava/lang/String;");
	jfieldID autoConfigEnable = env->GetFieldID(gatewayHostInfoProfileClass, "autoConfigEnable","I");

	env->SetObjectField(gatewayHostInfoObject, hostName,env->NewStringUTF(gatewayHostInfo.HostName.c_str()));
	env->SetObjectField(gatewayHostInfoObject, hostMAC,env->NewStringUTF(gatewayHostInfo.HostMAC.c_str()));
	env->SetIntField(gatewayHostInfoObject, autoConfigEnable, gatewayHostInfo.AutoConfig);

	env->CallBooleanMethod(obj_ArrayList, arrayList_add,gatewayHostInfoObject);
	return obj_ArrayList;
}

JNIEXPORT jint JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_setAPWiFiAutoConfig(
		JNIEnv * env, jobject obj) {
	return myDevice.SetAPWiFiAutoConfig(30);
}

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_addSmartHomePowerPlugBuffer(
		JNIEnv * env, jobject obj, jstring protocol, jstring id, jstring op) {
		LOGI("Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_addSmartHomePowerPlugBuffer");
	const char *tmp1 = env->GetStringUTFChars(protocol, 0);
	string input_string1(tmp1);
	const char *tmp2 = env->GetStringUTFChars(id, 0);
	string input_string2(tmp2);
	const char *tmp3 = env->GetStringUTFChars(op, 0);
	string input_string3(tmp3);
	if(input_string3.compare("TRUE") == 0) {
	LOGI("SetSmartDevCtrlPowerToBuffer on");
		myDevice.SetSmartDevCtrlPowerToBuffer(tmp1, tmp2, true);
	}else{
	LOGI("SetSmartDevCtrlPowerToBuffer off");
		myDevice.SetSmartDevCtrlPowerToBuffer(tmp1, tmp2, false);
	}
}

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_reloadSmartHomeDaemon(
		JNIEnv * env, jobject obj, jstring protocol) {
	const char *tmp = env->GetStringUTFChars(protocol, 0);
	string input_string1(tmp);
    myDevice.SetSmartDevCtrlDaemon(tmp);
}

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_addAutoConfigApproveProfileListToBuffer(
		JNIEnv * env, jobject obj, jobject profile) {
	// class ArrayList
	jclass cls_arraylist = env->GetObjectClass(profile);
	// method in class ArrayList
	jmethodID arraylist_get = env->GetMethodID(cls_arraylist, "get", "(I)Ljava/lang/Object;");
	jmethodID arraylist_size = env->GetMethodID(cls_arraylist, "size", "()I");
	jint profile_size = env->CallIntMethod(profile, arraylist_size);
	//	LOGI("ArrayList Size =%d", profile_size);

    vector<ST_WiFiAutoConfigApproveInfo> pf_array;
	for(int i = 0; i < profile_size; i++) {

		jobject obj_profile = env->CallObjectMethod(profile, arraylist_get, i);

		// class WiFiAutoConfigApproveProfile
		jclass cls = env->GetObjectClass(obj_profile);

		// method in class WiFiAutoConfigApproveProfile
		jmethodID getMAC = env->GetMethodID(cls, "getMac", "()Ljava/lang/String;");
		jmethodID getStatus = env->GetMethodID(cls, "getAction", "()Ljava/lang/String;");

		// invoke method in object ParentalControlProfile
		jstring mac = (jstring)env->CallObjectMethod(obj_profile, getMAC);
		jstring status = (jstring)env->CallObjectMethod(obj_profile, getStatus);

		// converter jstring to char to string
		const char *tmp_mac = env->GetStringUTFChars(mac, 0);
		const char *tmp_status = env->GetStringUTFChars(status, 0);

		string mac_string(tmp_mac);
		string status_string(tmp_status);

				LOGI("Add WiFi Auto Config Approve JNI MAC =%s", mac_string.c_str());
				LOGI("Add WiFi Auto Config Approve JNI Status =%s", status_string.c_str());
		ST_WiFiAutoConfigApproveInfo pf;
		pf.HostMAC = mac_string;
		if(status_string == "0") {
		    pf.AutoConfig = 0;
		} else if(status_string == "1") {
		    pf.AutoConfig = 1;
		}

		pf_array.push_back(pf);
	}
		myDevice.SetWiFiAutoConfigApproveProfileList(pf_array);
}

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_setAutoConfigApproveToBuffer(
		JNIEnv * env, jobject obj, jint option) {
	if(option == 1) {
		myDevice.SetWiFiAutoConfigApprove(true);
	} else {
		myDevice.SetWiFiAutoConfigApprove(false);
	}
}

JNIEXPORT bool JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIGatewayLib_getAutoConfigApprove(
		JNIEnv * env, jobject obj) {
	return myDevice.GetWiFiAutoConfigApprove();
}
