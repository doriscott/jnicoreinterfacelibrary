#include <string.h>
#include <jni.h>
#include <android/log.h>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sstream>
#include "APP_Agent/CDataMessage.h"
#include "APP_Agent/CEndDevicePool.h"
#include "APP_Agent/CErrorHandle.h"
#include "APP_Agent/CTemporary.h"
#include "APP_Agent/CParentalControl.h"

typedef unsigned char byte;
#define BUFFER_OFFSET 0x4
typedef void (*CALLBACK)(int);
static JavaVM *jvm;
static int isRunning;
static JNIEnv *env;

CParentalControl myParentalControl = *new CParentalControl();

extern "C" {

JNIEXPORT jobject JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIParentalControlLib_getParentalControlProfile(
		JNIEnv * env, jobject obj);

JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIParentalControlLib_addEliminateParentalControlRuleToBuffer(
		JNIEnv * env, jobject obj, jobject profile);

JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIParentalControlLib_addModifyParentalControlRuleToBuffer(
		JNIEnv * env, jobject obj, jobject profile);

JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIParentalControlLib_addNewParentalControlRuleToBuffer(
		JNIEnv * env, jobject obj, jobject profile);
};

JNIEXPORT jobject JNICALL Java_com_zyxel_android_jni_corelibinterface_JNIParentalControlLib_getParentalControlProfile(
		JNIEnv * env, jobject obj){
	//ArrayList Object
	jclass cls_ArrayList = env->FindClass("java/util/ArrayList");
	jmethodID construct = env->GetMethodID(cls_ArrayList, "<init>", "()V");
	jobject obj_ArrayList = env->NewObject(cls_ArrayList, construct, "");
	jmethodID arrayList_add = env->GetMethodID(cls_ArrayList, "add",
											   "(Ljava/lang/Object;)Z");
        jclass ParentalControlProfileClass =
				env->FindClass(
						"com/zyxel/android/jni/model/ParentalControlProfile");
		jmethodID constructor = env->GetMethodID(ParentalControlProfileClass,
												 "<init>", "()V");

		// field in class EndDeviceProfile
		jfieldID mID = env->GetFieldID(ParentalControlProfileClass, "ID",
									   "Ljava/lang/String;");
		jfieldID mMAC = env->GetFieldID(ParentalControlProfileClass,
										"EndDeviceMAC", "Ljava/lang/String;");
		jfieldID mEnable = env->GetFieldID(ParentalControlProfileClass,
										   "Enable", "Ljava/lang/String;");
		jfieldID mProfileName = env->GetFieldID(ParentalControlProfileClass,
												"ProfileName", "Ljava/lang/String;");
		jfieldID mSchedulingDays = env->GetFieldID(ParentalControlProfileClass,
												   "SchedulingDays", "Ljava/lang/String;");
		jfieldID mStartHour = env->GetFieldID(ParentalControlProfileClass,
											  "SchedulingTimeStartHour", "Ljava/lang/String;");
		jfieldID mStartMin = env->GetFieldID(ParentalControlProfileClass,
											 "SchedulingTimeStartMin", "Ljava/lang/String;");
		jfieldID mStopHour = env->GetFieldID(ParentalControlProfileClass,
											 "SchedulingTimeStopHour", "Ljava/lang/String;");
		jfieldID mStopMin = env->GetFieldID(ParentalControlProfileClass,
											"SchedulingTimeStopMin", "Ljava/lang/String;");

		vector<ST_ParentalControl> parental_controlprofile_list;

		parental_controlprofile_list = myParentalControl.GetProfileList();
		//		LOGI("!!!!!!!!!!!!!! JNI ALL PROFILE Size = %d", parental_controlprofile_list.size());
		int profile_size = parental_controlprofile_list.size();
		string id_str = "";
		string mac_str = "";
		string enable_str = "";
		string profileName_str = "";
		string schedulingDays_str = "";
		string startHour_str = "";
		string startMin_str = "";
		string stopHour_str = "";
		string stopMin_str = "";

		if (profile_size == 0) {
			//			LOGI("Profile Size = 0 !!!!!!!!!!!!!!");
			jobject ParentalControlProfileObject = env->NewObject(
					ParentalControlProfileClass, constructor);
		} else {
			for (int i = 0; i < parental_controlprofile_list.size(); i++) {
				jobject ParentalControlProfileObject = env->NewObject(
						ParentalControlProfileClass, constructor);
				ST_ParentalControl profile = parental_controlprofile_list.at(i);
				id_str = profile.ID;
				mac_str = profile.EndDeviceMAC;
				enable_str = profile.Enable;
				profileName_str = profile.ProfileName;
				schedulingDays_str = profile.SchedulingDays;
				startHour_str = profile.SchedulingTimeStartHour;
				startMin_str = profile.SchedulingTimeStartMin;
				stopHour_str = profile.SchedulingTimeStopHour;
				stopMin_str = profile.SchedulingTimeStopMin;

				//			LOGI("profile id=%s", id_str.c_str());
				//			LOGI("profile mac=%s", mac_str.c_str());
				//			LOGI("profile enable=%s", enable_str.c_str());
				//			LOGI("profile profile name=%s", profileName_str.c_str());
				//			LOGI("profile days=%s", schedulingDays_str.c_str());
				//			LOGI("profile start hour=%s", startHour_str.c_str());
				//			LOGI("profile start min=%s", startMin_str.c_str());
				//			LOGI("profile stop hour=%s", stopHour_str.c_str());
				//			LOGI("profile stop min=%s", stopMin_str.c_str());

				env->SetObjectField(ParentalControlProfileObject, mID,
									env->NewStringUTF(id_str.c_str()));
				env->SetObjectField(ParentalControlProfileObject, mMAC,
									env->NewStringUTF(mac_str.c_str()));
				env->SetObjectField(ParentalControlProfileObject, mEnable,
									env->NewStringUTF(enable_str.c_str()));
				env->SetObjectField(ParentalControlProfileObject, mProfileName,
									env->NewStringUTF(profileName_str.c_str()));
				env->SetObjectField(ParentalControlProfileObject,
									mSchedulingDays,
									env->NewStringUTF(schedulingDays_str.c_str()));
				env->SetObjectField(ParentalControlProfileObject, mStartHour,
									env->NewStringUTF(startHour_str.c_str()));
				env->SetObjectField(ParentalControlProfileObject, mStartMin,
									env->NewStringUTF(startMin_str.c_str()));
				env->SetObjectField(ParentalControlProfileObject, mStopHour,
									env->NewStringUTF(stopHour_str.c_str()));
				env->SetObjectField(ParentalControlProfileObject, mStopMin,
									env->NewStringUTF(stopMin_str.c_str()));
				env->CallBooleanMethod(obj_ArrayList, arrayList_add,
									   ParentalControlProfileObject);
			}
		}
		return obj_ArrayList;
}

JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIParentalControlLib_addEliminateParentalControlRuleToBuffer(
		JNIEnv * env, jobject obj, jobject profile) {
vector<ST_ParentalControl> pf_array;
	// class ArrayList
	jclass cls_arraylist = env->GetObjectClass(profile);
	// method in class ArrayList
	jmethodID arraylist_get = env->GetMethodID(cls_arraylist, "get", "(I)Ljava/lang/Object;");
	jmethodID arraylist_size = env->GetMethodID(cls_arraylist, "size", "()I");
	jint profile_size = env->CallIntMethod(profile, arraylist_size);
	//	LOGI("ArrayList Size =%d", profile_size);

	for(int i = 0; i < profile_size; i++) {

		jobject obj_profile = env->CallObjectMethod(profile, arraylist_get, i);

		// class ParentalControlProfile
		jclass cls_parentalcontrolprofile = env->GetObjectClass(obj_profile);

		// method in class ParentalControlProfile
		jmethodID getID = env->GetMethodID(cls_parentalcontrolprofile, "getID", "()Ljava/lang/String;");
		jmethodID getMAC = env->GetMethodID(cls_parentalcontrolprofile, "getMAC", "()Ljava/lang/String;");
		jmethodID getStatus = env->GetMethodID(cls_parentalcontrolprofile, "getStatus", "()Ljava/lang/String;");
		jmethodID getProfileName = env->GetMethodID(cls_parentalcontrolprofile, "getProfileName", "()Ljava/lang/String;");
		jmethodID getSchedulingDays = env->GetMethodID(cls_parentalcontrolprofile, "getSchedulingDays", "()Ljava/lang/String;");
		jmethodID getSchedulingTimeStartHour = env->GetMethodID(cls_parentalcontrolprofile, "getSchedulingTimeStartHour", "()Ljava/lang/String;");
		jmethodID SchedulingTimeStartMin = env->GetMethodID(cls_parentalcontrolprofile, "getSchedulingTimeStartMin", "()Ljava/lang/String;");
		jmethodID SchedulingTimeStopHour = env->GetMethodID(cls_parentalcontrolprofile, "getSchedulingTimeStopHour", "()Ljava/lang/String;");
		jmethodID SchedulingTimeStopMin = env->GetMethodID(cls_parentalcontrolprofile, "getSchedulingTimeStopMin", "()Ljava/lang/String;");

		// invoke method in object ParentalControlProfile
		jstring id = (jstring)env->CallObjectMethod(obj_profile, getID);
		jstring mac = (jstring)env->CallObjectMethod(obj_profile, getMAC);
		jstring status = (jstring)env->CallObjectMethod(obj_profile, getStatus);
		jstring name = (jstring)env->CallObjectMethod(obj_profile, getProfileName);
		jstring days = (jstring)env->CallObjectMethod(obj_profile, getSchedulingDays);
		jstring startH = (jstring)env->CallObjectMethod(obj_profile, getSchedulingTimeStartHour);
		jstring startM = (jstring)env->CallObjectMethod(obj_profile, SchedulingTimeStartMin);
		jstring stopH = (jstring)env->CallObjectMethod(obj_profile, SchedulingTimeStopHour);
		jstring stopM = (jstring)env->CallObjectMethod(obj_profile, SchedulingTimeStopMin);

		// converter jstring to char to string
		const char *tmp_id = env->GetStringUTFChars(id, 0);
		const char *tmp_mac = env->GetStringUTFChars(mac, 0);
		const char *tmp_status = env->GetStringUTFChars(status, 0);
		const char *tmp_name = env->GetStringUTFChars(name, 0);
		const char *tmp_days = env->GetStringUTFChars(days, 0);
		const char *tmp_startH = env->GetStringUTFChars(startH, 0);
		const char *tmp_startM = env->GetStringUTFChars(startM, 0);
		const char *tmp_stopH = env->GetStringUTFChars(stopH, 0);
		const char *tmp_stopM = env->GetStringUTFChars(stopM, 0);

		string id_string(tmp_id);
		string mac_string(tmp_mac);
		string status_string(tmp_status);
		string name_string(tmp_name);
		string days_string(tmp_days);
		string startH_string(tmp_startH);
		string startM_string(tmp_startM);
		string stopH_string(tmp_stopH);
		string stopM_string(tmp_stopM);

//		LOGI("Object Delete Request JNI ID =%s", id_string.c_str());
//		LOGI("Object Delete Request JNI MAC =%s", mac_string.c_str());
//		LOGI("Object Delete Request JNI Status =%s", status_string.c_str());
//		LOGI("Object Delete Request JNI Name =%s", name_string.c_str());
//		LOGI("Object Delete Request JNI Days =%s", days_string.c_str());
//		LOGI("Object Delete Request JNI Start Hour =%s", startH_string.c_str());
//		LOGI("Object Delete Request JNI Start Min =%s", startM_string.c_str());
//		LOGI("Object Delete Request JNI Stop Hour =%s", stopH_string.c_str());
//		LOGI("Object Delete Request JNI Stop Min =%s", stopM_string.c_str());

		ST_ParentalControl pf;
		pf.ID = id_string;
		pf.EndDeviceMAC = mac_string;
		pf.Enable = status_string;
		pf.ProfileName = name_string;
		pf.SchedulingDays = days_string;
		pf.SchedulingTimeStartHour = startH_string;
		pf.SchedulingTimeStartMin = startM_string;
		pf.SchedulingTimeStopHour = stopH_string;
		pf.SchedulingTimeStopMin = stopM_string;
		pf_array.push_back(pf);
	}

	myParentalControl.DeleteProfiles(pf_array);
}

JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIParentalControlLib_addModifyParentalControlRuleToBuffer(
		JNIEnv * env, jobject obj, jobject profile) {
	vector <ST_ParentalControl> pf_array;
	// class ArrayList
	jclass cls_arraylist = env->GetObjectClass(profile);
	// method in class ArrayList
	jmethodID arraylist_get = env->GetMethodID(cls_arraylist, "get", "(I)Ljava/lang/Object;");
	jmethodID arraylist_size = env->GetMethodID(cls_arraylist, "size", "()I");
	jint profile_size = env->CallIntMethod(profile, arraylist_size);
	//	LOGI("ArrayList Size =%d", profile_size);
	for (int i = 0; i < profile_size; i++) {
		jobject obj_profile = env->CallObjectMethod(profile, arraylist_get, i);

		// class ParentalControlProfile
		jclass cls = env->GetObjectClass(obj_profile);

		// method in class ParentalControlProfile
		jmethodID getID = env->GetMethodID(cls, "getID", "()Ljava/lang/String;");
		jmethodID getMAC = env->GetMethodID(cls, "getMAC", "()Ljava/lang/String;");
		jmethodID getStatus = env->GetMethodID(cls, "getStatus", "()Ljava/lang/String;");
		jmethodID getProfileName = env->GetMethodID(cls, "getProfileName",
													"()Ljava/lang/String;");
		jmethodID getSchedulingDays = env->GetMethodID(cls, "getSchedulingDays",
													   "()Ljava/lang/String;");
		jmethodID getSchedulingTimeStartHour = env->GetMethodID(cls,
																"getSchedulingTimeStartHour",
																"()Ljava/lang/String;");
		jmethodID getSchedulingTimeStartMin = env->GetMethodID(cls, "getSchedulingTimeStartMin",
															   "()Ljava/lang/String;");
		jmethodID getSchedulingTimeStopHour = env->GetMethodID(cls, "getSchedulingTimeStopHour",
															   "()Ljava/lang/String;");
		jmethodID getSchedulingTimeStopMin = env->GetMethodID(cls, "getSchedulingTimeStopMin",
															  "()Ljava/lang/String;");

		// invoke method in object ParentalControlProfile
		jstring id = (jstring) env->CallObjectMethod(obj_profile, getID);
		jstring mac = (jstring) env->CallObjectMethod(obj_profile, getMAC);
		jstring status = (jstring) env->CallObjectMethod(obj_profile, getStatus);
		jstring name = (jstring) env->CallObjectMethod(obj_profile, getProfileName);
		jstring days = (jstring) env->CallObjectMethod(obj_profile, getSchedulingDays);
		jstring startH = (jstring) env->CallObjectMethod(obj_profile,
														 getSchedulingTimeStartHour);
		jstring startM = (jstring) env->CallObjectMethod(obj_profile,
														 getSchedulingTimeStartMin);
		jstring stopH = (jstring) env->CallObjectMethod(obj_profile, getSchedulingTimeStopHour);
		jstring stopM = (jstring) env->CallObjectMethod(obj_profile, getSchedulingTimeStopMin);

		// converter jstring to char to string
		const char *tmp_id = env->GetStringUTFChars(id, 0);
		const char *tmp_mac = env->GetStringUTFChars(mac, 0);
		const char *tmp_status = env->GetStringUTFChars(status, 0);
		const char *tmp_name = env->GetStringUTFChars(name, 0);
		const char *tmp_days = env->GetStringUTFChars(days, 0);
		const char *tmp_startH = env->GetStringUTFChars(startH, 0);
		const char *tmp_startM = env->GetStringUTFChars(startM, 0);
		const char *tmp_stopH = env->GetStringUTFChars(stopH, 0);
		const char *tmp_stopM = env->GetStringUTFChars(stopM, 0);

		string id_string(tmp_id);
		string mac_string(tmp_mac);
		string status_string(tmp_status);
		string name_string(tmp_name);
		string days_string(tmp_days);
		string startH_string(tmp_startH);
		string startM_string(tmp_startM);
		string stopH_string(tmp_stopH);
		string stopM_string(tmp_stopM);

//		LOGI("Object Edit Request JNI ID =%s", id_string.c_str());
//		LOGI("Object Edit Request JNI MAC =%s", mac_string.c_str());
//		LOGI("Object Edit Request JNI Status =%s", status_string.c_str());
//		LOGI("Object Edit Request JNI Name =%s", name_string.c_str());
//		LOGI("Object Edit Request JNI Days =%s", days_string.c_str());
//		LOGI("Object Edit Request JNI Start Hour =%s", startH_string.c_str());
//		LOGI("Object Edit Request JNI Start Min =%s", startM_string.c_str());
//		LOGI("Object Edit Request JNI Stop Hour =%s", stopH_string.c_str());
//		LOGI("Object Edit Request JNI Stop Min =%s", stopM_string.c_str());

		ST_ParentalControl pf;
		pf.ID = id_string;
		pf.EndDeviceMAC = mac_string;
		pf.Enable = status_string;
		pf.ProfileName = name_string;
		pf.SchedulingDays = days_string;
		pf.SchedulingTimeStartHour = startH_string;
		pf.SchedulingTimeStartMin = startM_string;
		pf.SchedulingTimeStopHour = stopH_string;
		pf.SchedulingTimeStopMin = stopM_string;
		pf_array.push_back(pf);

	}
	myParentalControl.EditProfile(pf_array.at(0));
}

JNIEXPORT void JNICALL
		Java_com_zyxel_android_jni_corelibinterface_JNIParentalControlLib_addNewParentalControlRuleToBuffer(
		JNIEnv * env, jobject obj, jobject profile){
		CEndDevicePool myEndDevicePool = *new CEndDevicePool();
	vector<ST_ParentalControl> pf_array;
	// class ArrayList
	jclass cls_arraylist = env->GetObjectClass(profile);
	// method in class ArrayList
	jmethodID arraylist_get = env->GetMethodID(cls_arraylist, "get", "(I)Ljava/lang/Object;");
	jmethodID arraylist_size = env->GetMethodID(cls_arraylist, "size", "()I");
	jint profile_size = env->CallIntMethod(profile, arraylist_size);
	//	LOGI("ArrayList Size =%d", profile_size);

	for(int i = 0; i < profile_size; i++) {

		jobject obj_profile = env->CallObjectMethod(profile, arraylist_get, i);

		// class ParentalControlProfile
		jclass cls = env->GetObjectClass(obj_profile);

		// method in class ParentalControlProfile
		jmethodID getID = env->GetMethodID(cls, "getID", "()Ljava/lang/String;");
		jmethodID getMAC = env->GetMethodID(cls, "getMAC", "()Ljava/lang/String;");
		jmethodID getStatus = env->GetMethodID(cls, "getStatus", "()Ljava/lang/String;");
		jmethodID getProfileName = env->GetMethodID(cls, "getProfileName", "()Ljava/lang/String;");
		jmethodID getSchedulingDays = env->GetMethodID(cls, "getSchedulingDays", "()Ljava/lang/String;");
		jmethodID getSchedulingTimeStartHour = env->GetMethodID(cls, "getSchedulingTimeStartHour", "()Ljava/lang/String;");
		jmethodID getSchedulingTimeStartMin = env->GetMethodID(cls, "getSchedulingTimeStartMin", "()Ljava/lang/String;");
		jmethodID getSchedulingTimeStopHour = env->GetMethodID(cls, "getSchedulingTimeStopHour", "()Ljava/lang/String;");
		jmethodID getSchedulingTimeStopMin = env->GetMethodID(cls, "getSchedulingTimeStopMin", "()Ljava/lang/String;");

		// invoke method in object ParentalControlProfile
		jstring id = (jstring)env->CallObjectMethod(obj_profile, getID);
		jstring mac = (jstring)env->CallObjectMethod(obj_profile, getMAC);
		jstring status = (jstring)env->CallObjectMethod(obj_profile, getStatus);
		jstring name = (jstring)env->CallObjectMethod(obj_profile, getProfileName);
		jstring days = (jstring)env->CallObjectMethod(obj_profile, getSchedulingDays);
		jstring startH = (jstring)env->CallObjectMethod(obj_profile, getSchedulingTimeStartHour);
		jstring startM = (jstring)env->CallObjectMethod(obj_profile, getSchedulingTimeStartMin);
		jstring stopH = (jstring)env->CallObjectMethod(obj_profile, getSchedulingTimeStopHour);
		jstring stopM = (jstring)env->CallObjectMethod(obj_profile, getSchedulingTimeStopMin);

		// converter jstring to char to string
		const char *tmp_id = env->GetStringUTFChars(id, 0);
		const char *tmp_mac = env->GetStringUTFChars(mac, 0);
		const char *tmp_status = env->GetStringUTFChars(status, 0);
		const char *tmp_name = env->GetStringUTFChars(name, 0);
		const char *tmp_days = env->GetStringUTFChars(days, 0);
		const char *tmp_startH = env->GetStringUTFChars(startH, 0);
		const char *tmp_startM = env->GetStringUTFChars(startM, 0);
		const char *tmp_stopH = env->GetStringUTFChars(stopH, 0);
		const char *tmp_stopM = env->GetStringUTFChars(stopM, 0);

		string id_string(tmp_id);
		string mac_string(tmp_mac);
		string status_string(tmp_status);
		string name_string(tmp_name);
		string days_string(tmp_days);
		string startH_string(tmp_startH);
		string startM_string(tmp_startM);
		string stopH_string(tmp_stopH);
		string stopM_string(tmp_stopM);

//				LOGI("Object Add Request JNI ID =%s", id_string.c_str());
//				LOGI("Object Add Request JNI MAC =%s", mac_string.c_str());
//				LOGI("Object Add Request JNI Status =%s", status_string.c_str());
//				LOGI("Object Add Request JNI Name =%s", name_string.c_str());
//				LOGI("Object Add Request JNI Days =%s", days_string.c_str());
//				LOGI("Object Add Request JNI Start Hour =%s", startH_string.c_str());
//				LOGI("Object Add Request JNI Start Min =%s", startM_string.c_str());
//				LOGI("Object Add Request JNI Stop Hour =%s", stopH_string.c_str());
//				LOGI("Object Add Request JNI Stop Min =%s", stopM_string.c_str());

		ST_ParentalControl pf;
		pf.ID = id_string;
		pf.EndDeviceMAC = mac_string;
		pf.Enable = status_string;
		pf.ProfileName = name_string;
		pf.SchedulingDays = days_string;
		pf.SchedulingTimeStartHour = startH_string;
		pf.SchedulingTimeStartMin = startM_string;
		pf.SchedulingTimeStopHour = stopH_string;
		pf.SchedulingTimeStopMin = stopM_string;
		pf_array.push_back(pf);
	}
	//	LOGI("Profile MAC = %s\n", pf_array.at(0).EndDeviceMAC.c_str());
	myEndDevicePool.AddPcpRule(pf_array.at(0).EndDeviceMAC);
	CTemporary::SendBuffer();
	myParentalControl.AddProfile(pf_array.at(0));
}




