/****************************************************************************/
/*
 * Copyright (C) 2000-2013 ZyXEL Communications, Corp.
 * All Rights Reserved.
 *
 * ZyXEL Confidential;
 * Protected as an unpublished work, treated as confidential,
 * and hold in trust and in strict confidence by receiving party.
 * Only the employees who need to know such ZyXEL confidential information
 * to carry out the purpose granted under NDA are allowed to access.
 *
 * The computer program listings, specifications and documentation
 * herein are the property of ZyXEL Communications, Corp. and shall
 * not be reproduced, copied, disclosed, or used in whole or in part
 * for any reason without the prior express written permission of
 * ZyXEL Communications, Corp.
 *
 */
/****************************************************************************/
/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// OpenGL ES 2.0 code
#include <string.h>
#include <jni.h>
#include <android/log.h>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sstream>
#include "APP_Agent/CDataMessage.h"
#include "APP_Agent/UID_Define.h"
#include "APP_Agent/CUDPSocket.h"
#include "APP_Agent/CErrorHandle.h"
#include "APP_Agent/CTemporary.h"
#include "Lib/CSocket/CSocket.h"
#include "APP_Agent/DeviceDiscover.h"


#define  LOG_TAG    "libgl2jni"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
typedef unsigned char byte;
#define BUFFER_OFFSET 0x4
typedef void (*CALLBACK)(int);
static JavaVM *jvm;
static int isRunning;
static JNIEnv *env;

CErrorHandle *myErrorHandle;
void jniagentcallback(int id) {
	int status = jvm->AttachCurrentThread(&env, NULL);
	if (status != 0) {

	} else {
		LOGI("Receive Error Code From C --> %d\n", id);
		jclass JNIEventHandlerClass =
				env->FindClass(
						"com/zyxel/android/jni/listener/JNIEventHandler");
		jmethodID constructor = env->GetMethodID(JNIEventHandlerClass, "<init>",
												 "()V");
		jobject JNIEventHandlerObject = env->NewObject(JNIEventHandlerClass,
													   constructor, "(I)V");
		jmethodID mEventFromJNI = env->GetMethodID(JNIEventHandlerClass,
												   "eventFromJNI", "(I)V");
		env->CallVoidMethod(JNIEventHandlerObject, mEventFromJNI, id);
	}

}

extern "C" {

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNILib_init(
		JNIEnv * env, jobject obj);

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNILib_sendBuffer(
		JNIEnv * env, jobject obj);

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNILib_closeSocket(
		JNIEnv * env, jobject obj);
};

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNILib_init(
		JNIEnv * env, jobject obj) {
    myErrorHandle = CErrorHandle::sharedInstance();
	if(myErrorHandle) {
		myErrorHandle->setCallbackFun(jniagentcallback);
	} else {
	}
}

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNILib_sendBuffer(
		JNIEnv * env, jobject obj) {
	CTemporary::SendBuffer();
}

JNIEXPORT void JNICALL Java_com_zyxel_android_jni_corelibinterface_JNILib_closeSocket(
		JNIEnv * env, jobject obj) {
	Logout();
}






