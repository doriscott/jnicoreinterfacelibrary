//
//  CFile.h
//  ConnectAPP
//
//  Created by Kevin on 2014/2/19.
//  Copyright (c) 2014年 ZyXEL. All rights reserved.
//

#ifndef __ConnectAPP__CFile__
#define __ConnectAPP__CFile__

#include <iostream>
#include <fstream>
#include "CEndDevicePool.h"

#define CLIENT_LIST_FILE_NAME "clientlist.txt"

using namespace std;

class CFile{
public:
    CFile();
    ~CFile();
    
    bool setPath(string path);
    string getPath();
    
    bool saveNetworkDevices(vector<ST_EndDevice> deviceList);
    vector<ST_EndDevice> loadNetworkDevices();
    vector<ST_EndDevice> loadNetworkDevices(vector<ST_EndDevice> deviceList);
    //ST_DEVICE getNetworkDevicesWithFile();
    
private:
    
    string filePath;
    
};

#endif /* defined(__ConnectAPP__CFile__) */
