/**
 *  @brief		It's the data controller for portable router.
 *  @details    Setting or Getting Wi-Fi SSID, password, security type ...etc.
 *  @author
 - Kevin Wu--v2.0.0--2015.8.20
 *  @copyright
 .
 * Copyright (C) 2000-2015 ZyXEL Communications, Corp.\n
 * All Rights Reserved.\n
 *
 * ZyXEL Confidential;\n
 * Protected as an unpublished work, treated as confidential,\n
 * and hold in trust and in strict confidence by receiving party.\n
 * Only the employees who need to know such ZyXEL confidential information\n
 * to carry out the purpose granted under NDA are allowed to access.\n
 *\n
 * The computer program listings, specifications and documentation\n
 * herein are the property of ZyXEL Communications, Corp. and shall\n
 * not be reproduced, copied, disclosed, or used in whole or in part\n
 * for any reason without the prior express written permission of\n
 * ZyXEL Communications, Corp.\n
 * @file		CMobile.h
 */

#ifndef __CMobile__
#define __CMobile__

#include <iostream>
#include "Property.h"

using namespace std;

enum EN_AUTH{
	_NONE =0,
	PAP,
	CHAR
};
enum EN_SIGNAL{
    Lv0 =0,
    Lv1,
    Lv2,
    Lv3,
    Lv4
};
/// It's the data controller for ZyXEL portable router.
class CMobile
{
public:
    Property<string, CMobile, READ_WRITE> APN;
	Property<EN_AUTH, CMobile, READ_WRITE> Authentication;
	Property<string, CMobile, READ_WRITE> Username;
	Property<string, CMobile, READ_WRITE> Password;
    
    Property<string, CMobile, READ_ONLY> CellularMode;
	Property<EN_SIGNAL, CMobile, READ_ONLY> SignalStrength;
	Property<string, CMobile, READ_ONLY> ServiceProvider;
    
	CMobile();
	//~CMobile();
    CMobile(int GUID);
	//~CWiFi();
	int GetGUID();
    
    /**
     *  Getting the APN of cellular.
     *
     *  @return The APN of cellular.
     */
	string GetAPN();
    /**
     *  Setting the APN of cellular.
     *
     *  @param string The APN you want to be setted.
     *
     *  @return Setting successful or failture.
     */
	bool SetAPN(string);
    /**
     *  Setting the APN of cellular to buffer.
     *
     *  @param name string The name you want to be setted.
     *
     *  @return Setting successful or failture.
     */
    bool SetAPNToBuffer(string name);
    
    /**
     *  Getting the authenication.
     
     *  @return Setting authenication type.
     */
	EN_AUTH GetAuthenication();
	bool SetAuthenication(EN_AUTH op);
    bool SetAuthenicationToBuffer(EN_AUTH op);
    
	string GetUsername();
	bool SetUsername(string name);
    bool SetUsernameToBuffer(string name);
    
	string GetPassword();
	bool SetPassword(string pwd);
    bool SetPasswordToBuffer(string pwd);
    
	EN_SIGNAL GetSignalStength();
	string GetCellularMode();
    string GetServiceProvider();
	
    bool SendBuffer();
    
protected:
	//static const char *data;
    int GUID;
private:
	string _APN;
	EN_AUTH _Authentication;
	string _Username;
	string _Password;
	EN_SIGNAL _SignalStrength;
    string _Mode;
	string _ServiceProvider;
    
};
#endif /* defined(__CMobile__) */
