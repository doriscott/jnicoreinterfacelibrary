/****************************************************************************/
/*
 * Copyright (C) 2000-2013 ZyXEL Communications, Corp.
 * All Rights Reserved.
 *
 * ZyXEL Confidential;
 * Protected as an unpublished work, treated as confidential,
 * and hold in trust and in strict confidence by receiving party.
 * Only the employees who need to know such ZyXEL confidential information
 * to carry out the purpose granted under NDA are allowed to access.
 *
 * The computer program listings, specifications and documentation
 * herein are the property of ZyXEL Communications, Corp. and shall
 * not be reproduced, copied, disclosed, or used in whole or in part
 * for any reason without the prior express written permission of
 * ZyXEL Communications, Corp.
 *
 */
/****************************************************************************/
//
//  CParentalControl.cpp
//
//  Created by Scott Tseng on 13/11/20.
//  Copyright (c) 2013�~ pan star. All rights reserved.
//
#include "CParentalControl.h"
#include <string>
#include <sstream>
#include "UID_Define.h"
//#include <android/log.h>
#include "MessageType.h"
#include "CJSON.h"
#include "CDataMessage.h"
#include "CErrorHandle.h"
#include "CTemporary.h"

vector<ST_ParentalControl> CParentalControl:: ST_ParentalControlList;
int CParentalControl::current_profile_size;
#define  LOG_TAG    "CParentalControl"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

//static TCPStream *stream;

CParentalControl::CParentalControl(int id) {
	GUID = id;
	//	iDataMessage = new CDataMessage;
}
CParentalControl::CParentalControl() {
	//	iDataMessage = new CDataMessage;
	GUID = GUID_WIFI;
}
int CParentalControl::GetGUID() {
	return GUID;
}

int CParentalControl::GetTotalProfile() {
	try {
		string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
		temp = DM.CreatParameterToString(UUID_parentalcontrol_num_of_entries, GUID_NONE);

		string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);

		DM.ParserData(receive);
		temp = DM.GetParameter(UUID_parentalcontrol_num_of_entries, GUID_NONE);
		current_profile_size = (int) atoi(temp.c_str());
		return (int) atoi(temp.c_str());
	} catch (int errorCode) {
		CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
		errorHandle->handleErrorOccur(errorCode);
        return 0;
	}
}

vector<ST_ParentalControl> CParentalControl::GetProfileList() {
	try {
		string temp;
		int totalParentalProfiles;
		totalParentalProfiles = GetTotalProfile();
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        CJSON json = *new CJSON(CTemporary::HEAD);
        
		//	printf("TotalParentalProfile: %d\n",totalParentalProfiles);
		//  LOGI("TotalParentalProfile: %d\n",totalParentalProfiles);

		if(totalParentalProfiles == 0) {
			ST_ParentalControlList.clear();
			return ST_ParentalControlList;
		}
		
		for(int i = 0; i < totalParentalProfiles; i++) {
			treeNode *tempT = new treeNode;
			temp = DM.CreatParameterToString(UUID_all_profile, GUID_NONE, i+1);
//					temp = DM.CreatParameterToString(UUID_all_profile, GUID_NONE);
            
			json.creatTreeFromJSON(tempT, temp);
			nodesAddParent(tempT);
			removeNodeByKey(DM_DELETE, tempT);
			temp =json.CJSON::genJsonFromTreeWithData(tempT);
//			LOGI("PCP = %s", temp.c_str());
			string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
			DM.ParserData(receive);
		}
        ST_ParentalControlList.clear();
		ST_ParentalControl t1;
		for (int i = 1; i <= totalParentalProfiles; i++) {
			std::stringstream indexStr;
			indexStr << i;
			t1.ID = indexStr.str();
			t1.EndDeviceMAC = DM.GetParameter(UUID_parentalcontrol_hostmac, GUID_NONE, i);
			t1.Enable = DM.GetParameter(UUID_parentalcontrol_enable, GUID_NONE, i);
			t1.ProfileName = DM.GetParameter(UUID_parentalcontrol_pcpname, GUID_NONE, i);
			t1.SchedulingDays = DM.GetParameter(UUID_parentalcontrol_schedule_days, GUID_NONE, i);
			t1.SchedulingTimeStartHour = DM.GetParameter(UUID_parentalcontrol_schedule_time_start_hour, GUID_NONE, i);
			t1.SchedulingTimeStartMin = DM.GetParameter(UUID_parentalcontrol_schedule_time_start_min, GUID_NONE, i);
			t1.SchedulingTimeStopHour = DM.GetParameter(UUID_parentalcontrol_schedule_time_stop_hour, GUID_NONE, i);
			t1.SchedulingTimeStopMin = DM.GetParameter(UUID_parentalcontrol_schedule_time_stop_min, GUID_NONE, i);
			ST_ParentalControlList.push_back(t1);
		}
		
	} catch (int errorCode) {
		//LOGI("GetProfileList Error Code = %d", errorCode);
		CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
		errorHandle->handleErrorOccur(errorCode);
	}
    return ST_ParentalControlList;
}

ST_ParentalControl CParentalControl::GetProfile(int index) {
	try {
		vector<ST_ParentalControl> parentalControlList;
		parentalControlList = GetProfileList();
		return parentalControlList.at(index - 1);
	} catch (int errorCode) {
		CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
		errorHandle->handleErrorOccur(errorCode);
        ST_ParentalControl t;
        return t;
	}
}

void CParentalControl::EditProfile(ST_ParentalControl pf) {

	try {
		int index;
//		ST_ParentalControlList.clear();
//		GetProfileList();

		for(int i = 0; i < ST_ParentalControlList.size(); i++) {
			ST_ParentalControl tmp_current = ST_ParentalControlList.at(i);
			ST_ParentalControl tmp_modify = pf;
			if((tmp_current.EndDeviceMAC == tmp_modify.EndDeviceMAC) &&
					(tmp_current.ID == tmp_modify.ID)) {
				index = i + 1;
			}
		}
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
		//LOGI("Find Modify Profile Index = %d", index);
		DM.SetParameterToBuffer(UUID_parentalcontrol_hostmac,
				pf.EndDeviceMAC.c_str(),  CTemporary::BUFF, index);
		DM.SetParameterToBuffer(UUID_parentalcontrol_enable,
				pf.Enable.c_str(),  CTemporary::BUFF, index);
		DM.SetParameterToBuffer(UUID_parentalcontrol_pcpname,
				pf.ProfileName.c_str(),  CTemporary::BUFF, index);
		DM.SetParameterToBuffer(UUID_parentalcontrol_schedule_days,
				pf.SchedulingDays.c_str(),  CTemporary::BUFF, index);
		DM.SetParameterToBuffer(UUID_parentalcontrol_schedule_time_start_hour,
				pf.SchedulingTimeStartHour.c_str(),  CTemporary::BUFF, index);
		DM.SetParameterToBuffer(UUID_parentalcontrol_schedule_time_start_min,
				pf.SchedulingTimeStartMin.c_str(),  CTemporary::BUFF, index);
		DM.SetParameterToBuffer(UUID_parentalcontrol_schedule_time_stop_hour,
				pf.SchedulingTimeStopHour.c_str(),  CTemporary::BUFF, index);
		DM.SetParameterToBuffer(UUID_parentalcontrol_schedule_time_stop_min,
				pf.SchedulingTimeStopMin.c_str(),  CTemporary::BUFF, index);
	} catch (int errorCode) {
		CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
		errorHandle->handleErrorOccur(errorCode);
	}
}

void CParentalControl::AddProfile(ST_ParentalControl pf) {
	try {
//		ST_ParentalControlList.clear();
		int current_profile_size = GetTotalProfile();
        int new_profile_index = current_profile_size;
		//int new_profile_index = current_profile_size + 1;

		treeNode *nodeTT = findNodeByKey(DM_PARENTALCONTROL,CTemporary::HEAD);
		nodeTT->addChild(tr181Tree::CreatParentalControlWithIndex(new_profile_index));
		//	LOGI("Current Profile Size = %d\n", current_profile_size);
		//	LOGI("New Add Profile Index = %d\n", new_profile_index);

		treeNode *nodeT = findNodeByKey(DM_PARENTALCONTROL,CTemporary::HEAD);
		//	LOGI("Profile MAC =%s", pf.EndDeviceMAC.c_str());
		//	LOGI("Profile Status =%s", pf.Enable.c_str());
		//	LOGI("Profile Name =%s", pf.ProfileName.c_str());
		//	LOGI("Profile Days =%s", pf.SchedulingDays.c_str());
		//	LOGI("Profile Start Hour =%s", pf.SchedulingTimeStartHour.c_str());
		//	LOGI("Profile Start Min =%s", pf.SchedulingTimeStartMin.c_str());
		//	LOGI("Profile Stop Hour =%s", pf.SchedulingTimeStopHour.c_str());
		//	LOGI("Profile Stop Min =%s", pf.SchedulingTimeStopMin.c_str());

		//	LOGI("Find Node Index =%d\n", new_profile_index);
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
		DM.SetParameterToBuffer(UUID_parentalcontrol_hostmac,
				pf.EndDeviceMAC.c_str(),  CTemporary::BUFF, new_profile_index);
		DM.SetParameterToBuffer(UUID_parentalcontrol_enable,
				pf.Enable.c_str(),  CTemporary::BUFF, new_profile_index);
		DM.SetParameterToBuffer(UUID_parentalcontrol_pcpname,
				pf.ProfileName.c_str(),  CTemporary::BUFF, new_profile_index);
		DM.SetParameterToBuffer(UUID_parentalcontrol_schedule_days,
				pf.SchedulingDays.c_str(),  CTemporary::BUFF, new_profile_index);
		DM.SetParameterToBuffer(UUID_parentalcontrol_schedule_time_start_hour,
				pf.SchedulingTimeStartHour.c_str(),  CTemporary::BUFF, new_profile_index);
		DM.SetParameterToBuffer(UUID_parentalcontrol_schedule_time_start_min,
				pf.SchedulingTimeStartMin.c_str(),  CTemporary::BUFF, new_profile_index);
		DM.SetParameterToBuffer(UUID_parentalcontrol_schedule_time_stop_hour,
				pf.SchedulingTimeStopHour.c_str(),  CTemporary::BUFF, new_profile_index);
		DM.SetParameterToBuffer(UUID_parentalcontrol_schedule_time_stop_min,
				pf.SchedulingTimeStopMin.c_str(),  CTemporary::BUFF, new_profile_index);
	} catch (int errorCode) {
		CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
		errorHandle->handleErrorOccur(errorCode);
	}
}

//void CParentalControl::AddProfiles(vector<ST_ParentalControl> pf_array) {
//
//	LOGI("Add Profile Size =%d", pf_array.size());
//	int new_profile_size = pf_array.size();
//	treeNode *nodeT = findNodeByKey("ParentalControl",CJSON::HEAD);
//	for(int i = 0; i < new_profile_size; i++) {
//		int current_profile_size = i+1;
//		nodeT->addChild(tr181Tree::CreatParentalControlWithIndex(i+1));
//	}
//	//	treeNode *node = NULL;
//	//	node = findNodeByKey("HostMAC",
//	//						findNodeByKey("i4",
//	//								findNodeByKey("ParentalControl",
//	//										findNodeByKey("X_ZyXEL_Ext", CJSON::HEAD))));
//	//	string json = CJSON::genJsonFromTree(node);
//	//	LOGI("Profile 2 JSON String =%s", json.c_str());
//
//	for(int i = 3; i < new_profile_size; i++) {
//		int current_profile_size = i+1;
//
//		ST_ParentalControl tmp_pf;
//		tmp_pf = pf_array.at(i);
//		LOGI("Profile MAC =%s", tmp_pf.EndDeviceMAC.c_str());
//		LOGI("Profile Status =%s", tmp_pf.Enable.c_str());
//		LOGI("Profile Name =%s", tmp_pf.ProfileName.c_str());
//		LOGI("Profile Days =%s", tmp_pf.SchedulingDays.c_str());
//		LOGI("Profile Start Hour =%s", tmp_pf.SchedulingTimeStartHour.c_str());
//		LOGI("Profile Start Min =%s", tmp_pf.SchedulingTimeStartMin.c_str());
//		LOGI("Profile Stop Hour =%s", tmp_pf.SchedulingTimeStopHour.c_str());
//		LOGI("Profile Stop Min =%s", tmp_pf.SchedulingTimeStopMin.c_str());
//
//		string tmp_id;
//		string id;
//		stringstream ss;
//		ss << i+1;
//		tmp_id = ss.str();
//		//		id = id.append("i").append(tmp_id);
//		id = "i3";
//		//		LOGI("Profile Index =%s", id.c_str());
//		DM.SetParameterToBuffer(UUID_parentalcontrol_hostmac,
//				tmp_pf.EndDeviceMAC.c_str(),  CTemporary::BUFF, id);
//		DM.SetParameterToBuffer(UUID_parentalcontrol_enable,
//				tmp_pf.Enable.c_str(),  CTemporary::BUFF, id);
//		DM.SetParameterToBuffer(UUID_parentalcontrol_pcpname,
//				tmp_pf.ProfileName.c_str(),  CTemporary::BUFF, id);
//		DM.SetParameterToBuffer(UUID_parentalcontrol_schedule_days,
//				tmp_pf.SchedulingDays.c_str(),  CTemporary::BUFF, id);
//		DM.SetParameterToBuffer(UUID_parentalcontrol_schedule_time_start_hour,
//				tmp_pf.SchedulingTimeStartHour.c_str(),  CTemporary::BUFF, id);
//		DM.SetParameterToBuffer(UUID_parentalcontrol_schedule_time_start_min,
//				tmp_pf.SchedulingTimeStartMin.c_str(),  CTemporary::BUFF, id);
//		DM.SetParameterToBuffer(UUID_parentalcontrol_schedule_time_stop_hour,
//				tmp_pf.SchedulingTimeStopHour.c_str(),  CTemporary::BUFF, id);
//		DM.SetParameterToBuffer(UUID_parentalcontrol_schedule_time_stop_min,
//				tmp_pf.SchedulingTimeStopMin.c_str(),  CTemporary::BUFF, id);
//
//	}
//}

void CParentalControl::DeleteProfiles(vector<ST_ParentalControl> pf_array) {
	try {
		ST_ParentalControlList.clear();
		GetProfileList();
		int tmp_index;
		vector<int> indexArray;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        
		for(int i = 0; i < ST_ParentalControlList.size(); i++) {
			ST_ParentalControl tmp_current = ST_ParentalControlList.at(i);
			for(int j = 0; j < pf_array.size(); j++) {
				ST_ParentalControl tmp_delete = pf_array.at(j);
				if((tmp_current.EndDeviceMAC == tmp_delete.EndDeviceMAC) &&
						(tmp_current.ProfileName == tmp_delete.ProfileName) &&
						(tmp_current.SchedulingDays == tmp_delete.SchedulingDays) &&
						(tmp_current.SchedulingTimeStartHour == tmp_delete.SchedulingTimeStartHour) &&
						(tmp_current.SchedulingTimeStartMin == tmp_delete.SchedulingTimeStartMin) &&
						(tmp_current.SchedulingTimeStopHour == tmp_delete.SchedulingTimeStopHour) &&
                    (tmp_current.SchedulingTimeStopMin == tmp_delete.SchedulingTimeStopMin) &&
                   (tmp_current.ID == tmp_delete.ID)) {
					//				LOGI("Current MAC = %s --> Delete MAC = %s", tmp_current.EndDeviceMAC.c_str(), tmp_delete.EndDeviceMAC.c_str());
					//				LOGI("Current Name = %s --> Delete Name = %s", tmp_current.ProfileName.c_str(), tmp_delete.ProfileName.c_str());
					tmp_index = i + 1;
					//				LOGI("Find Delete PC Index = %d\n", tmp_index);
					indexArray.push_back(tmp_index);
				}

			}
		}

		int size = indexArray.size();

		for(int i = (size - 1); i >= 0; i--) {
			//	for(int i = 0; i < size; i++) {
			//treeNode * = node;
			//		LOGI("Find Parental Control Profile Index = %d\n", indexArray.at(i));
			DM.SetParameterToBuffer(UUID_modify_profile, "1",  CTemporary::BUFF, indexArray.at(i));

		}
	} catch (int errorCode) {
		CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
		errorHandle->handleErrorOccur(errorCode);
	}
}
bool CParentalControl::SendBuffer(){
    try {
        CTemporary::SendBuffer();
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return true;
}

string CParentalControl::SendBufferAndRecvReq(){
    string temp = CTemporary::SendBufferAndRecvReq();
    return temp;
}
