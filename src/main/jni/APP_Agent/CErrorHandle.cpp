/****************************************************************************/
/*
 * Copyright (C) 2000-2013 ZyXEL Communications, Corp.
 * All Rights Reserved.
 *
 * ZyXEL Confidential;
 * Protected as an unpublished work, treated as confidential,
 * and hold in trust and in strict confidence by receiving party.
 * Only the employees who need to know such ZyXEL confidential information
 * to carry out the purpose granted under NDA are allowed to access.
 *
 * The computer program listings, specifications and documentation
 * herein are the property of ZyXEL Communications, Corp. and shall
 * not be reproduced, copied, disclosed, or used in whole or in part
 * for any reason without the prior express written permission of
 * ZyXEL Communications, Corp.
 *
 */
/****************************************************************************/

#include "CErrorHandle.h"
#include <sstream>
#include <vector>
//#include <android/log.h>
#define  LOG_TAG    "CErrorHandler"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

#define DebugMsg(f_,...) printf((f_), __VA_ARGS__);\
string temp;\
Format(temp,(f_), __VA_ARGS__);\
CErrorHandle *errorHandle = CErrorHandle::sharedInstance();\
errorHandle->handleErrorOccur(temp);\

auto_ptr<CErrorHandle> CErrorHandle::_instance;
//CALLBACK callback = defaultErrorHandle;
void defaultErrorHandle(int errorCode){
    cout<<"In defaultErrorHandle"<<endl;
    cout<<"Error:"<<errorCode<<endl;
}
void CErrorHandle::setCallbackFun(CALLBACK temp){
    callback = temp;
}
void CErrorHandle::setCallbackDebugFun(CALLBACK_MSG temp){
    callbackMSG = temp;
}

void CErrorHandle::handleErrorOccur(int errorCode){
	//LOGI("CError Handle handle Error Occur = %d\n", errorCode);
    
    if (callback==nullptr) {
        defaultErrorHandle(errorCode);
    }else{
        callback(errorCode);
    }
    
    stringstream ss;
    ss << errorCode;
    handleDebugMessage("Error:"+ss.str());
}
void CErrorHandle::handleDebugMessage(string message){
    if (callbackMSG==nullptr) {
        printf("%s",message.c_str());
    }else{
        callbackMSG(message);
    }
    
}

std::string Format(const char* lpszFormat, ...)
{
    // Warning : "vsnprintf" crashes with an access violation
    // exception if lpszFormat is not a "const char*" (for example, const string&)
    
    size_t  nSize     = 1024;
    char    *lpBuffer = (char*)malloc(nSize);
    
    va_list lpParams;
    
    while (true)
    {
        va_start(lpParams, lpszFormat);
        
        int nResult = vsnprintf(
                                lpBuffer,
                                nSize,
                                lpszFormat,
                                lpParams
                                );
        
        va_end(lpParams);
        
        if ((nResult >= 0) && (nResult < (int)nSize) )
        {
            // Success
            
            lpBuffer[nResult] = '\0';
            std::string sResult(lpBuffer);
            
            free (lpBuffer);
            
            return sResult;
        }
        else
        {
            // Increase buffer
            
            nSize =
            (nResult < 0)
            ? nSize *= 2
            : (nResult + 1)
            ;
            
            lpBuffer = (char *)realloc(lpBuffer, nSize);
        }
    }
}
