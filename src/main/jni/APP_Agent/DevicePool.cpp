/****************************************************************************/
/*
* Copyright (C) 2000-2013 ZyXEL Communications, Corp.
* All Rights Reserved.
*
* ZyXEL Confidential;
* Protected as an unpublished work, treated as confidential,
* and hold in trust and in strict confidence by receiving party.
* Only the employees who need to know such ZyXEL confidential information
* to carry out the purpose granted under NDA are allowed to access.
*
* The computer program listings, specifications and documentation
* herein are the property of ZyXEL Communications, Corp. and shall
* not be reproduced, copied, disclosed, or used in whole or in part
* for any reason without the prior express written permission of
* ZyXEL Communications, Corp.
*
*/
/****************************************************************************/
#include "DevicePool.h"
//#include <android/log.h>
#include "CSecurity.h"
#include "CEndDevicePool.h"
#include "CDataMessage.h"
#include "CErrorHandle.h"
#define  LOG_TAG    "DevicePool.cpp"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

ST_DEVICE DevicePool::currentDevice;
vector<ST_DEVICE> DevicePool::deviceList;
int DevicePool::currentDeviceIndex = -1;
ST_AppFeatureInfo DevicePool::appFeatureInfo;


void DevicePool::setAppFeature(ST_AppFeatureInfo appFeature){
    appFeatureInfo = appFeature;
}
ST_AppFeatureInfo DevicePool::getAppFeature(){
    return appFeatureInfo;
}

void DevicePool::clearDevice(){
    deviceList.clear();
}

void DevicePool::addDevice(ST_DEVICE device){

	if (device.ModelName == Device_WAH7130) {
		device.Port = 263;
	} else if(device.ModelName == Device_NBG6716) {
		device.Port = 263;
	} else if(device.ModelName == Device_AMG1312) {
		device.Port = 263;
	} else if(device.ModelName == Device_AMG1202) {
	 	device.Port = 263;
	}else{
        device.Port = 263;
    }
	deviceList.push_back(device);

	printf("DevicePool::addDevice=%s\n",deviceList.at(0).ModelName.c_str());

}
vector<ST_DEVICE> DevicePool::getDeviceList(){
	return deviceList;
}
ST_DEVICE DevicePool::getDevice(int i){
	return deviceList.at(i);
}
int DevicePool::getCurrentDeviceIndex(){
	return currentDeviceIndex;
}
void DevicePool::setCurrentDeviceIndex(int idx){
	currentDeviceIndex = idx;
}
void DevicePool::setCurrentDevice(ST_DEVICE temp){
    currentDevice = temp;
}
ST_DEVICE DevicePool::getCurrentDevice(){
    if (currentDeviceIndex == -1) {
        return currentDevice;
    }else{
        return getDevice(getCurrentDeviceIndex());
    }
}
int DevicePool::getEncryptType(){
	string modelname;
	modelname = getCurrentDevice().ModelName;

	string str1(Device_WAH7130);
	string str2(Device_NBG6716);
	string str3(Device_AMG1312);
	string str4(Device_AMG1202);

	if(modelname.compare(str1)==0)
		return RC4;
	if(modelname.compare(str2)==0)
		return RC4;
	if(modelname.compare(str3)==0)
		return RC4;
	if(modelname.compare(str4)==0)
		return RC4;
	else
		return RC4;
}

EN_DEVICE_MODEL_TYPE DevicePool::checkDeviceModelType(string modelName){
    
//    if (modelName.find("AMG1202-T10B")!=string::npos ||
//        modelName.find("P-1202-T10B")!=string::npos ||
//        modelName.find("AMG1302-T10B")!=string::npos ||
//        modelName.find("P-1302-T10B")!=string::npos ||
//        modelName.find("AMG1312-T10B")!=string::npos ||
//        modelName.find("VMG3312-B10A")!=string::npos ||
//        modelName.find("VMG1312-B10A")!=string::npos
//        ){
//        return DEVICE_CPE_ROUTER;
//    }else{
//        return DEVICE_HOME_ROUTER;
//    }
    if (modelName.find(DEVICE_HOME_ROUTER) != std::string::npos) {
        return DEVICE_HOME_ROUTER;
    }else{
        return DEVICE_CPE_ROUTER;
    }
}
