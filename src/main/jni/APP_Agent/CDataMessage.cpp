/****************************************************************************/
/*
 * Copyright (C) 2000-2013 ZyXEL Communications, Corp.
 * All Rights Reserved.
 *
 * ZyXEL Confidential;
 * Protected as an unpublished work, treated as confidential,
 * and hold in trust and in strict confidence by receiving party.
 * Only the employees who need to know such ZyXEL confidential information
 * to carry out the purpose granted under NDA are allowed to access.
 *
 * The computer program listings, specifications and documentation
 * herein are the property of ZyXEL Communications, Corp. and shall
 * not be reproduced, copied, disclosed, or used in whole or in part
 * for any reason without the prior express written permission of
 * ZyXEL Communications, Corp.
 *
 */
/****************************************************************************/
//
//  CDataMessage.cpp
//
//  Created by Kevin on 13/10/7.
//  Copyright (c) 2013年 pan star. All rights reserved.
//
#include <sstream>
#include "CDataMessage.h"
#include <sstream>
#include <android/log.h>
#include "DevicePool.h"
#include "UID_Define.h"
#include "CSecurity.h"
#include "../Lib/tr181/tr181Tree.h"
#include <algorithm>
#include "CErrorHandle.h"
#include "ErrorCode.h"
#include "MessageType.h"
#include "CTemporary.h"

#define  LOG_TAG    "CDataMessage"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

#define DebugMsg(f_,...) printf((f_), __VA_ARGS__);\
CErrorHandle *errorHandle = CErrorHandle::sharedInstance();\
errorHandle->handleDebugMessage((string)Format((f_), __VA_ARGS__));\

//#define DebugMsg(f) CErrorHandle *errorHandle = CErrorHandle::sharedInstance();\
//errorHandle->handleErrorOccur(f);\


ST_DEVICE ZyXELDevice;

string sendAndRecvReq(string temp, int msgType) {
	char _encodeData[MAXDATASIZE];
	char sendData[MAXDATASIZE];
	char recvData[MAXDATASIZE];
	int recvbytes;
    
    //CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
    //errorHandle->handleDebugMessage(temp);
    
	strcpy(sendData, temp.c_str());
//    if (msgType == msgType_ParameterSetReq) {
//        printf("-->Send(SET):%s\n",sendData);
//        //DebugMsg("-->Send(SET):%s\n",sendData);
//    }else if(msgType == msgType_ParameterGetReq){
//        printf("-->Send(GET):%s\n",sendData);
//        //DebugMsg("-->Send(SET):%s\n",sendData);
//    }else{
//        printf("-->S:%s\n",sendData);
//    }
	
	LOGI("-->S:%s\n", sendData);

	string str = string(sendData);
	//*std::remove(sendData, sendData+strlen(sendData), '\n') = ' ';
	//str.erase(remove(str.begin(), str.end(), ' '), str.end());
	str.erase(remove(str.begin(), str.end(), '\n'), str.end());
    
    CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
    errorHandle->handleDebugMessage("\nSend:"+str+"\n");
    
	int len;
	len = (int)str.length();
	//printf("len:%d\n",len);

	memset(_encodeData,0,sizeof(_encodeData));
	memcpy(_encodeData+4,str.c_str(),len);


	CSecurity *mySecurity = new CSecurity(CTemporary::Key);
	mySecurity->SetEncryptMethod(RC4);
	mySecurity->EncrypData(msgType, _encodeData,len+4);

//    printf("======================================\n");
//    printf("%s",_encodeData);
//    for (int i = 0; i < len+4; i++) {
//        printf("%c:",(unsigned)_encodeData[i]);
//        printf("0x%d ",_encodeData[i]);
//    }
//    printf("\n======================================\n");
	//--------------------------------------------------
	CSocket *test = CSocket::sharedSocket();

	int error;
	//CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
	error = test->SendData(_encodeData,len+4);
	if (error<=0) {
		printf("CSocket can't send data\n");
		throw error;
	}
	error = test->RecvData(recvData,&recvbytes);
	if (error<=0) {
		printf("CSocket can't recv data\n");
		throw error;
	}
    
    if (recvbytes==0) {
        throw ERR_SOCK_SEND_TIMEOUT;
    }
	//--------------------------------------------------Start Decrpy

//	int errorCode=0;
//	memcpy(&errorCode,recvData+2,1);
//    if (errorCode==0 || error<=0) {
//        //errorHandle->handleErrorOccur(-1);
//        return "Error:0";
//    }
//	errorCode = errorCode - 256 -100;
//
//	if (recvbytes<=5 && errorCode<0) {
//		printf("Fail to received data!\n");
//		printf("Error Code : %d\n",errorCode);
//		//errorHandle->handleErrorOccur(errorCode-100);
//		throw errorCode;
//	}

	memset(_encodeData,0,sizeof(_encodeData));
	memcpy(_encodeData,recvData,recvbytes);
    
    int type = 0;
	int errorCode = mySecurity->DecrypData(type, _encodeData, recvbytes);
    
    if (errorCode <= -1) {
        throw errorCode;
    }
	//printf("-->Receive:%s , len:%zu\n",_encodeData+4,strlen(_encodeData));
    string recv(_encodeData+4);
    errorHandle->handleDebugMessage("\nRecv:"+recv+"\n");
    
    //DebugMsg("-->Receive:%s , len:%zu\n",_encodeData+4,strlen(_encodeData));
    
	LOGI("-->R:%s , len:%zu\n",_encodeData+4,strlen(_encodeData));
	//string recv(_encodeData+4);
	return _encodeData+4;

	/*
	 CSocket testS;
	 testS.StartClient("192.168.1.1", 263);
	 testS.sendData((char*)temp.c_str(), msgType);
	 const char *receive = testS.receiveData();
	 testS.closeSocket();
	 return (char*)receive;
	 */
	/*
	 TCPStream *stream = [TCPStream sharedStream];
	 [stream sendData:(char*)temp.c_str() withType:msgType];
	 const char *receive =[stream receiveData];
	 return (char*)receive;
	 */
}
CDataMessage::CDataMessage(treeNode* tree, EN_FORMAT formatType) {
	ptreeNode = tree;
    nodesAddParent(ptreeNode);
	FormatType = formatType;
}

ST_DEVICE CDataMessage::DeviceDiscoverResp(char *tmp) {
	//tmp = "{ \"Device\": { \"DeviceInfo\": { \"ModelName\": \"WAH7130\" }, \"X_ZyXEL_Ext\": { \"SystemInfo\": { \"SystemName\": \"WAH7130\" } } } }";
    /*
    Ex for ONE Connect 2.3:
    {
        "Device": {
            "DeviceInfo": {
                "DeviceMode": "Router",
                "WIFIConfigFlag": "0",
                "GatewayAPautoconfigEnable": "0",
                "LocalAPautoconfigEnable": "1",
                "ModelName": "NBG6816",
                "SoftwareVersion": "V1.00(AAWB.4)b3_",
                "SerialNumber": "S150Y04110076"
            },
            "X_ZyXEL_Ext": {
                "SystemInfo": {
                    "SystemName": "NBG6816"
                }
            }
        }
    }
    */
    
	string input;
	input.assign(tmp);
    
	printf("Response from app agent=%s", input.c_str());
    
    CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
    errorHandle->handleDebugMessage("Response from app agent:"+input+"\n");
    
	ST_DEVICE temp;
    
	Json::Reader reader;
	Json::Value root;
	if (reader.parse(input, root)) {
		Json::Value Device = root[DM_DEVICE];
		Json::Value DeviceInfo = Device[DM_DEVICEINFO];
		Json::Value ModelName = DeviceInfo[DM_MODELNAME];
		cout << "ModelName:" << ModelName.asString() << endl;
		temp.ModelName = ModelName.asString();
        
        Json::Value SoftwareVersion = DeviceInfo[DM_SOFTWAREVERSION];
        cout << "SoftwareVersion:" << SoftwareVersion.asString() << endl;
        temp.SoftwareVersion = SoftwareVersion.asString();
        Json::Value SerialNumber = DeviceInfo[DM_SN];
        cout << "SerialNumber:" << SerialNumber.asString() << endl;
        temp.SerialNumber = SerialNumber.asString();
        Json::Value DeviceMode = DeviceInfo[DM_DEVICEMODE];
        cout << "DeviceMode:" << DeviceMode.asString() << endl;
        if (DeviceMode.asString() == "Router") {
            temp.DeviceMode = EN_DEVICE_TYPE_ROUTER;
        }
        else if (DeviceMode.asString() == "AP") {
            temp.DeviceMode = EN_DEVICE_TYPE_AP;
        }
        else if (DeviceMode.asString() == "Repeater") {
            temp.DeviceMode = EN_DEVICE_TYPE_REPEATER;
        }
        else{
            temp.DeviceMode = EN_DEVICE_TYPE_NOT_SUPPORT;
        }
        Json::Value WIFIConfigFlag = DeviceInfo[DM_WIFICONFIGFLAG];
        cout << "WIFIConfigFlag:" << WIFIConfigFlag.asInt() << endl;
        if (WIFIConfigFlag.asInt() == 0) {
            temp.WIFIConfigFlag = EN_WIFICONFIG_FLAG_NOT_CHANGE;
        }
        else if (WIFIConfigFlag.asInt() == 1){
            temp.WIFIConfigFlag = EN_WIFICONFIG_FLAG_CHANGE;
//            temp.WIFIConfigFlag = EN_WIFICONFIG_FLAG_NOT_CHANGE;
        }
        Json::Value gatewayAutoConfigEnable = DeviceInfo[DM_GATEWAY_AUTOCONFIG_ENABLE];
        cout << "GatewayAutoConfigEnable:" << gatewayAutoConfigEnable.asInt() << endl;
        //LOGI("Gateway Auto Config Enable:%d", gatewayAutoConfigEnable.asInt());
        temp.GatewayAutoConfigEnable = gatewayAutoConfigEnable.asInt();
        Json::Value X_ZyXEL_Ext = Device[DM_ZYXELEXT];
		Json::Value SystemInfo = X_ZyXEL_Ext[DM_SYSTEMINFO];
		Json::Value SystemName = SystemInfo[DM_SYSTEMNAME];
		cout << "SystemName:" << SystemName.asString() << endl;
		temp.SystemName = SystemName.asString();
        
	} else {
		cout << "Can't Paser the \"DeviceDiscoverResp\" !" << endl;
		return temp;
	}
	ZyXELDevice = temp;
	return temp;
}

ST_AppFeatureInfo CDataMessage::DeviceSystemResp(char* tmp){
    //Send: "Device":{"X_ZyXEL_Ext":{"AppInfo":{"MagicNum":"Z3704","AppVersion":1}}}
    /*
     Recv: "Device": { "X_ZyXEL_Ext": { "AppInfo": { "AppVersion": "2.0", "FeatureList": { "DeviceInfo": 102, "GatewayInfo": 202, "FlowInfo": 302, "Login": 402, "NetTopology": 502, "DeviveCtrl": 602, "L2DeviveCtrl": 701, "DevIcon": 801, "GuestWiFi": 902, "WiFiAutoCfg": 1001, "WiFiOnOff": 1101, "SpeedTest": 1202, "Diagnostic": 1301, "CloudService": 1401, "FwUpgrade": 1501, "LEDOnOff": 0 }, "MagicNum": "Z3704" } } }
    */
    string input;
    input.assign(tmp);
    
    ST_AppFeatureInfo temp;
    Json::Reader reader;
    Json::Value root;
    if (reader.parse(input, root)) {
        Json::Value Device = root[DM_DEVICE];
        Json::Value X_ZyXEL_Ext = Device[DM_ZYXELEXT];
        Json::Value AppInfo = X_ZyXEL_Ext[DM_APPINFO];
        Json::Value AppVersion = AppInfo[DM_APPVERSION];
        cout << "AppVersion:" << AppVersion.asString() << endl;
        temp.AppVersion = AppVersion.asString();
        
        Json::Value FeatureList = AppInfo[DM_FEATURELIST];
        Json::Value DeviceInfo = FeatureList[DM_DEVICEINFO];
        cout << "DeviceInfo:" << DeviceInfo.asInt() << endl;
        temp.DeviceInfo = DeviceInfo.asInt();
        Json::Value GatewayInfo = FeatureList[DM_GATEWAYINFO];
        cout << "GatewayInfo:" << GatewayInfo.asInt() << endl;
        temp.GatewayInfo = GatewayInfo.asInt();
        Json::Value FlowInfo = FeatureList[DM_FLOWINFO];
        cout << "FlowInfo:" << FlowInfo.asInt() << endl;
        temp.FlowInfo = FlowInfo.asInt();
        Json::Value Login = FeatureList[DM_LOGIN];
        cout << "Login:" << Login.asInt() << endl;
        temp.Login = Login.asInt();
        Json::Value NetTopology = FeatureList[DM_NETTOPOLOGY];
        cout << "NetTopology:" << NetTopology.asInt() << endl;
        temp.NetTopology = NetTopology.asInt();
        Json::Value DeviceCtrl = FeatureList[DM_DEVICECTRL];
        cout << "DeviceCtrl:" << DeviceCtrl.asInt() << endl;
        temp.DeviceCtrl = DeviceCtrl.asInt();
        Json::Value L2DeviceCtrl = FeatureList[DM_L2DEVICECTRL];
        cout << "L2DeviceCtrl:" << L2DeviceCtrl.asInt() << endl;
        temp.L2DeviceCtrl = L2DeviceCtrl.asInt();
        Json::Value DevIcon = FeatureList[DM_DEVICON];
        cout << "DevIcon:" << DevIcon.asInt() << endl;
        temp.DevIcon = DevIcon.asInt();
        Json::Value GuestWiFi = FeatureList[DM_GUESTWIFI];
        cout << "GuestWiFi:" << GuestWiFi.asInt() << endl;
        temp.GuestWiFi = GuestWiFi.asInt();
        Json::Value WiFiAutoCfg = FeatureList[DM_WIFI_AUTOCONFIG];
        cout << "WiFiAutoCfg:" << WiFiAutoCfg.asInt() << endl;
        temp.WiFiAutoCfg = WiFiAutoCfg.asInt();
        Json::Value WiFiOnOff = FeatureList[DM_WIFIONOFF];
        cout << "WiFiOnOff:" << WiFiOnOff.asInt() << endl;
        temp.WiFiOnOff = WiFiOnOff.asInt();
        Json::Value SpeedTest = FeatureList[DM_SPEEDTEST];
        cout << "SpeedTest:" << SpeedTest.asInt() << endl;
        temp.SpeedTest = SpeedTest.asInt();
        Json::Value Diagnostic = FeatureList[DM_DIAGONSTIC];
        cout << "Diagnostic:" << Diagnostic.asInt() << endl;
        temp.Diagnostic = Diagnostic.asInt();
        Json::Value CloudService = FeatureList[DM_CLOUDSERVICE];
        cout << "CloudService:" << CloudService.asInt() << endl;
        temp.CloudService = CloudService.asInt();
        Json::Value FwUpgrade = FeatureList[DM_APPINFO_FWUPGRADE];
        cout << "FwUpgrade:" << FwUpgrade.asInt() << endl;
        temp.FwUpgrade = FwUpgrade.asInt();
        Json::Value LEDOnOff = FeatureList[DM_LEDONOFF];
        cout << "LEDOnOff:" << LEDOnOff.asInt() << endl;
        temp.LEDOnOff = LEDOnOff.asInt();

        Json::Value GuestWiFiGroup = FeatureList[DM_GUESTWIFIGROUP];
        cout << "GuestWiFiGroup:" << GuestWiFiGroup.asInt() << endl;
        temp.GuestWiFiGroup = GuestWiFiGroup.asInt();
        
    } else {
        cout << "Can't Paser the \"DeviceSystemResp\" !" << endl;
        return temp;
    }
    return temp;
}


string CDataMessage::GetInterfaceIndex(int UUID, int GUID) {
	string modelName = ZyXELDevice.ModelName;
	//LOGI("Device Model Name = %s\n", modelName.c_str());
	string index;
	int tmp = 0;
    
    switch (GUID) {
        case GUID_WIFI:
            tmp = Default_24G_Index;
            break;
        case GUID_WIFI_5G:
            tmp = Default_5G_Index;
            break;
        default: //guest wifi
            tmp = DefaultIndex;
            break;
    }
    /*
	if(modelName == Device_NBG6716) {
		switch(GUID) {
            case GUID_WIFI:
                if(UUID == UUID_ssid) {
                    tmp = NBG6716_24G_Interface;
                } else if(UUID == UUID_presharekey) {
                    tmp = NBG6716_24G_Interface;
                } else if(UUID == UUID_security) {
                    tmp = NBG6716_24G_Interface;
                } else if(UUID == UUID_enable) {
                    tmp = NBG6716_24G_Interface;
                } else if(UUID == UUID_ssid_enable) {
                    tmp = NBG6716_24G_Interface;
                } else if(UUID == UUID_ssid_status) {
                    tmp = NBG6716_24G_Interface;
                } else if(UUID == UUID_radio_enable) {
                    tmp = NBG6716_24G_Interface;
                } else if(UUID == UUID_radio_status) {
                    tmp = NBG6716_24G_Interface;
                } else if(UUID == UUID_maxbitrate) {
                    tmp = NBG6716_24G_Interface;
                } else if(UUID == UUID_opearting_frequency_band) {
                    tmp = NBG6716_24G_Interface;
                } else if(UUID == UUID_radio_autochannel_enable) {
                    tmp = NBG6716_24G_Interface;
                } else if(UUID == UUID_opearting_standards) {
                    tmp = NBG6716_24G_Interface;
                } else if(UUID == UUID_channel) {
                    tmp = NBG6716_24G_Interface;
                } else if(UUID == UUID_transmit_power) {
                    tmp = NBG6716_24G_Interface;
                }
                break;
            case GUID_WIFI_5G:
                if(UUID == UUID_ssid) {
                    tmp = NBG6716_5G_Interface;
                } else if(UUID == UUID_presharekey) {
                    tmp = NBG6716_5G_Interface;
                } else if(UUID == UUID_security) {
                    tmp = NBG6716_5G_Interface;
                } else if(UUID == UUID_enable) {
                    tmp = NBG6716_5G_Interface;
                } else if(UUID == UUID_ssid_enable) {
                    tmp = NBG6716_5G_Interface;
                } else if(UUID == UUID_ssid_status) {
                    tmp = NBG6716_5G_Interface;
                } else if(UUID == UUID_radio_enable) {
                    tmp = NBG6716_5G_Interface;
                } else if(UUID == UUID_radio_status) {
                    tmp = NBG6716_5G_Interface;
                } else if(UUID == UUID_maxbitrate) {
                    tmp = NBG6716_5G_Interface;
                } else if(UUID == UUID_opearting_frequency_band) {
                    tmp = NBG6716_5G_Interface;
                } else if(UUID == UUID_radio_autochannel_enable) {
                    tmp = NBG6716_5G_Interface;
                } else if(UUID == UUID_opearting_standards) {
                    tmp = NBG6716_5G_Interface;
                } else if(UUID == UUID_channel) {
                    tmp = NBG6716_5G_Interface;
                } else if(UUID == UUID_transmit_power) {
                    tmp = NBG6716_5G_Interface;
                }
                break;
		}
	} else if(modelName == Device_AMG1312) {
		switch(GUID) {
            case GUID_WIFI:
                if(UUID == UUID_ssid) {
                    tmp = AMG1312_24G_Interface;
                } else if(UUID == UUID_presharekey) {
                    tmp = AMG1312_24G_Interface;
                } else if(UUID == UUID_security) {
                    tmp = AMG1312_24G_Interface;
                } else if(UUID == UUID_enable) {
                    tmp = AMG1312_24G_Interface;
                } else if(UUID == UUID_ssid_enable) {
                    tmp = AMG1312_24G_Interface;
                } else if(UUID == UUID_ssid_status) {
                    tmp = AMG1312_24G_Interface;
                } else if(UUID == UUID_radio_enable) {
                    tmp = AMG1312_24G_Interface;
                } else if(UUID == UUID_radio_status) {
                    tmp = AMG1312_24G_Interface;
                } else if(UUID == UUID_maxbitrate) {
                    tmp = AMG1312_24G_Interface;
                } else if(UUID == UUID_opearting_frequency_band) {
                    tmp = AMG1312_24G_Interface;
                } else if(UUID == UUID_radio_autochannel_enable) {
                    tmp = AMG1312_24G_Interface;
                } else if(UUID == UUID_opearting_standards) {
                    tmp = AMG1312_24G_Interface;
                } else if(UUID == UUID_channel) {
                    tmp = AMG1312_24G_Interface;
                } else if(UUID == UUID_transmit_power) {
                    tmp = AMG1312_24G_Interface;
                }
                break;
		}
	} else if(modelName == Device_AMG1202) {
		switch(GUID) {
            case GUID_WIFI:
                if(UUID == UUID_ssid) {
                    tmp = AMG1202_24G_Interface;
                } else if(UUID == UUID_presharekey) {
                    tmp = AMG1202_24G_Interface;
                } else if(UUID == UUID_security) {
                    tmp = AMG1202_24G_Interface;
                } else if(UUID == UUID_enable) {
                    tmp = AMG1202_24G_Interface;
                } else if(UUID == UUID_ssid_enable) {
                    tmp = AMG1202_24G_Interface;
                } else if(UUID == UUID_ssid_status) {
                    tmp = AMG1202_24G_Interface;
                } else if(UUID == UUID_radio_enable) {
                    tmp = AMG1202_24G_Interface;
                } else if(UUID == UUID_radio_status) {
                    tmp = AMG1202_24G_Interface;
                } else if(UUID == UUID_maxbitrate) {
                    tmp = AMG1202_24G_Interface;
                } else if(UUID == UUID_opearting_frequency_band) {
                    tmp = AMG1202_24G_Interface;
                } else if(UUID == UUID_radio_autochannel_enable) {
                    tmp = AMG1202_24G_Interface;
                } else if(UUID == UUID_opearting_standards) {
                    tmp = AMG1202_24G_Interface;
                } else if(UUID == UUID_channel) {
                    tmp = AMG1202_24G_Interface;
                } else if(UUID == UUID_transmit_power) {
                    tmp = AMG1202_24G_Interface;
                }
                break;
		}
	} else if(modelName == Device_WAH7130) {
		switch(GUID) {
            case GUID_WIFI:
                if(UUID == UUID_ssid) {
                    tmp = WAH7130_24G_Interface;
                } else if(UUID == UUID_presharekey) {
                    tmp = WAH7130_24G_Interface;
                } else if(UUID == UUID_security) {
                    tmp = WAH7130_24G_Interface;
                } else if(UUID == UUID_enable) {
                    tmp = WAH7130_24G_Interface;
                } else if(UUID == UUID_radio_enable) {
                    tmp = WAH7130_24G_Interface;
                } else if(UUID == UUID_radio_status) {
                    tmp = WAH7130_24G_Interface;
                } else if(UUID == UUID_maxbitrate) {
                    tmp = WAH7130_24G_Interface;
                } else if(UUID == UUID_opearting_frequency_band) {
                    tmp = WAH7130_24G_Interface;
                } else if(UUID == UUID_radio_autochannel_enable) {
                    tmp = WAH7130_24G_Interface;
                } else if(UUID == UUID_opearting_standards) {
                    tmp = WAH7130_24G_Interface;
                } else if(UUID == UUID_channel) {
                    tmp = WAH7130_24G_Interface;
                } else if(UUID == UUID_transmit_power) {
                    tmp = WAH7130_24G_Interface;
                }
                break;
		}
	} else{
        tmp = DefaultIndex;
        //throw ERR_CDATA_CANT_FIND_MODUL;
    }
    */
	std::stringstream ss;
	ss << tmp;
	index = "i" + ss.str();
	//LOGI("Find Interface Index  = %s\n", index.c_str());
	return index;
}

string CDataMessage::CreatParameterToString(int UUID, string value) {
	if (FormatType == JSON) {
		treeNode *node = NULL;
		switch(UUID) {
            case UUID_device_check_software_version:
                node = findNodeByKey(DM_CHECKSOFTWARE,
                                     findNodeByKey(DM_SOFTWAREUPDATEOTA,
                                                   findNodeByKey(DM_DEVICEINFO, ptreeNode)));
                break;
            case UUID_device_password:
                node = findNodeByKey(DM_CONFIGPASSWORD,
                                     findNodeByKey(DM_LANCONFIGSECURITY, ptreeNode));
                break;
            case UUID_apn:
                node = findNodeByKey(DM_APNAME,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_authentication:
                node = findNodeByKey(DM_PPPAUTH,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_cellular_username:
                node = findNodeByKey(DM_USERNAME,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_cellular_password:
                node = findNodeByKey(DM_PASSWORD,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_monthly_quota:
                node = findNodeByKey(DM_MONTHLYLIMIT,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_reset_day:
                node = findNodeByKey(DM_MONTHLYRESETDAY,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_auto_reset:
                node = findNodeByKey(DM_MONTHLYRESETENABLE,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_data_plan_enable:
                node = findNodeByKey(DM_ENABLE,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_wifi_autoconfig_op:
                node = findNodeByKey(DM_CTR_OP,
                                     findNodeByKey(DM_WIFI_AUTOCONFIG,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            
            case UUID_block:
                node = findNodeByKey(DM_HOSTMAC, findNodeByKey(DM_ENDDEVCTR_BLOCK,
                                                               findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_non_block:
                node = findNodeByKey(DM_HOSTMAC, findNodeByKey(DM_ENDDEVCTR_UNBLOCK,
                                                               findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_l2_reboot:
                node = findNodeByKey(DM_HOSTMAC, findNodeByKey(DM_L2CTR_REBOOT,
                                                               findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_l2_wifion:
                node = findNodeByKey(DM_HOSTMAC, findNodeByKey(DM_L2CTR_WIFION,
                                                               findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_l2_wifioff:
                node = findNodeByKey(DM_HOSTMAC, findNodeByKey(DM_L2CTR_WIFIOFF,
                                                               findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_device_reboot:
                node = findNodeByKey(DM_REBOOT,findNodeByKey(DM_ZYXELEXT, ptreeNode));
                break;
            case UUID_l2_speedtestinfo_url:
                node = findNodeByKey(DM_SP_URL,
                                     findNodeByKey(DM_SPEEDTESTINFO,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_l2_speedtestinfo_action_type:
                node = findNodeByKey(DM_SP_ACTION_TYPE,
                                     findNodeByKey(DM_SPEEDTESTINFO,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_c2g_speedtestinfo_enable:
                node = findNodeByKey(DM_GWSPEED_ENABLE,
                                     findNodeByKey(DM_GWSPEEDTESTINFO,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_wifi_wificonfigflag_op:
                node = findNodeByKey(DM_WIFI_WIFICONFIGFLAG,
                                     findNodeByKey(DM_WIFI_AUTOCONFIG,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_wifi_auto_config_approve_mac:
                node = findNodeByKey(DM_AUTO_CFG_APPROVE_MAC,
                                     findNodeByKey(DM_AUTO_CFG_APPROVE_DATA,
                                                   findNodeByKey(DM_ZYXELEXT,
                                                                 ptreeNode)));
                break;
            case UUID_wifi_auto_config_approve_option:
                node = findNodeByKey(DM_OPTION,
                                     findNodeByKey(DM_AUTO_CFG_APPROVE_DATA,
                                                   findNodeByKey(DM_ZYXELEXT,
                                                                 ptreeNode)));
                break;
            case UUID_wifi_auto_config_approve:
                node = findNodeByKey(DM_ENABLE,
                                     findNodeByKey(DM_AUTO_CFG_APPROVE,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
		}
		
		CJSON json(ptreeNode);
		string jstr = json.genJsonFromTreeWithPtr(node,value);
        
		return jstr;
	}else{
        cout << "Error!Didn't select FormatType!(in CreatParameterToString funtion)"<< endl;
        throw ERR_CDATA_CANT_FIND_DATATYPE;
        return "";
    }
}

string CDataMessage::CreatParameterToString(int UUID) {
	if (FormatType == JSON) {
		treeNode *node = NULL;
		switch(UUID) {
            case UUID_device_password:
                node = findNodeByKey(DM_CONFIGPASSWORD,
                                     findNodeByKey(DM_LANCONFIGSECURITY, ptreeNode));
                break;
            case UUID_device_software_version:
                node = findNodeByKey(DM_SOFTWAREVERSION,
                                     findNodeByKey(DM_DEVICEINFO, ptreeNode));
                break;
            case UUID_device_serialnumber:
                node = findNodeByKey(DM_SN,
                                     findNodeByKey(DM_DEVICEINFO, ptreeNode));
                break;
            case UUID_device_deviceinfo:
                node = findNodeByKey(DM_DEVICEINFO,
                                     findNodeByKey(DM_ZYXELEXT, ptreeNode));
                break;
            case UUID_device_waninfo:
                node = findNodeByKey(DM_WANINFO,
                                     findNodeByKey(DM_DEVICEINFO,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_device_software_update_ota:
                node = findNodeByKey(DM_SOFTWAREUPDATEOTA,
                                                   findNodeByKey(DM_DEVICEINFO, ptreeNode));
                break;
            case UUID_device_check_software_version:
                node = findNodeByKey(DM_CHECKSOFTWARE,
                                     findNodeByKey(DM_SOFTWAREUPDATEOTA,
                                                   findNodeByKey(DM_DEVICEINFO, ptreeNode)));
                break;
            case UUID_device_latest_software_version:
                node = findNodeByKey(DM_LATESTSOFTWARE,
                                     findNodeByKey(DM_SOFTWAREUPDATEOTA,
                                                   findNodeByKey(DM_DEVICEINFO, ptreeNode)));
                break;
            case UUID_device_update_software_status:
                node = findNodeByKey(DM_UPDATESOFTWARESTATUS,
                                     findNodeByKey(DM_SOFTWAREUPDATEOTA,
                                                   findNodeByKey(DM_DEVICEINFO, ptreeNode)));
                break;
            case UUID_device_fw_upgrade_status:
                node = findNodeByKey(DM_FWUPGRADESTATUS,
                                     findNodeByKey(DM_SOFTWAREUPDATEOTA,
                                                   findNodeByKey(DM_DEVICEINFO, ptreeNode)));
                break;
            case UUID_device_has_software_version:
                node = findNodeByKey(DM_HASNEWSOFTWARE,
                                     findNodeByKey(DM_SOFTWAREUPDATEOTA,
                                                   findNodeByKey(DM_DEVICEINFO, ptreeNode)));
                break;
            case UUID_device_latest_software_date:
                node = findNodeByKey(DM_LATESTSOFTWAREDATE,
                                     findNodeByKey(DM_SOFTWAREUPDATEOTA,
                                                   findNodeByKey(DM_DEVICEINFO, ptreeNode)));
                break;
            case UUID_apn:
                node = findNodeByKey(DM_APNAME,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_authentication:
                node = findNodeByKey(DM_PPPAUTH,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_cellular_username:
                node = findNodeByKey(DM_USERNAME,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_cellular_password:
                node = findNodeByKey(DM_PASSWORD,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_signal_strength:
                node = findNodeByKey(DM_SIGNALSTRENGTH,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_cellular_mode:
                node = findNodeByKey(DM_CELLULARMODE,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_service_provider:
                node = findNodeByKey(DM_SERVICEPROVIDER,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_auto_reset:
                node = findNodeByKey(DM_MONTHLYRESETENABLE,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_reset_count:
                node = findNodeByKey(DM_RESETCOUNTER,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_reset_day:
                node = findNodeByKey(DM_MONTHLYRESETDAY,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G,ptreeNode )));
                break;
            case UUID_last_reset_date:
                node = findNodeByKey(DM_LASTRESETDATE,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_monthly_quota:
                node = findNodeByKey(DM_MONTHLYLIMIT,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_current_usage:
                break;
            case UUID_bytes_send:
                node = findNodeByKey(DM_BYTESSENT,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_bytes_received:
                node = findNodeByKey(DM_BYTESRECEIVED,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_daily_usage_advice:
                break;
            case UUID_data_plan_enable:
                node = findNodeByKey(DM_ENABLE,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_device_battery_charge:
                node = findNodeByKey(DM_CHARGESTATUS,
                                     findNodeByKey(DM_BATTERYSTATUS,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_device_battery_total:
                node = findNodeByKey(DM_TOTALCAPACITY,
                                     findNodeByKey(DM_BATTERYSTATUS,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_device_battery:
                node = findNodeByKey(DM_REMAINCAPACITY,
                                     findNodeByKey(DM_BATTERYSTATUS,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_wifi_autoconfig_op:
                node = findNodeByKey(DM_CTR_OP,
                                     findNodeByKey(DM_WIFI_AUTOCONFIG,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_route_numberofipv4forwarding:
                node = findNodeByKey(DM_NUMOFIPV4,
                                     findNodeByKey(DM_ROUTER,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_tracerouter_numberofroutehops:
                node = findNodeByKey(DM_NUMOFROUTERHOPS,
                                     findNodeByKey(DM_TRACEROUTER,
                                                   findNodeByKey(DM_DIAGNOSTICS,
                                                                 findNodeByKey(DM_IP, ptreeNode))));
                break;
            case UUID_l2_speedtestinfo_dl_rate:
                node = findNodeByKey(DM_SP_DL_RATE,
                                     findNodeByKey(DM_SPEEDTESTINFO,
                                                   findNodeByKey(DM_ZYXELEXT,ptreeNode )));
                break;
            case UUID_l2_speedtestinfo_ul_rate:
                node = findNodeByKey(DM_SP_UL_RATE,
                                     findNodeByKey(DM_SPEEDTESTINFO,
                                                   findNodeByKey(DM_ZYXELEXT,ptreeNode )));
                break;
            case UUID_l2_speedtestinfo_end:
                node = findNodeByKey(DM_SP_END,
                                     findNodeByKey(DM_SPEEDTESTINFO,
                                                   findNodeByKey(DM_ZYXELEXT,ptreeNode )));
                break;
            case UUID_gateway_description:
                node = findNodeByKey(DM_GATEWAY_DESCRIPTION,
                                     findNodeByKey(DM_DESCRIPTION,
                                                   findNodeByKey(DM_ZYXELEXT,ptreeNode )));                
                break;
            case UUID_c2g_speedtestinfo_port:
                node = findNodeByKey(DM_GWSPEED_SERVER_PORT,
                                     findNodeByKey(DM_GWSPEEDTESTINFO,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                
                break;
             case UUID_gateway_info_hostname:
                node = findNodeByKey(DM_GATEWAY_HOSTNAME,
                                      findNodeByKey(DM_GATEWAYINFO,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));

                break;

             case UUID_gateway_info_hostmac:
                            node = findNodeByKey(DM_GATEWAY_HOSTMAC,
                                                  findNodeByKey(DM_GATEWAYINFO,
                                                               findNodeByKey(DM_ZYXELEXT, ptreeNode)));

                break;

             case UUID_gateway_info_autocfg:
                            node = findNodeByKey(DM_GATEWAY_AUTOCONFIG,
                                                  findNodeByKey(DM_GATEWAYINFO,
                                                               findNodeByKey(DM_ZYXELEXT, ptreeNode)));

                break;

            case UUID_gateway_gatewayinfo:
                node = findNodeByKey(DM_GATEWAYINFO,
                                     findNodeByKey(DM_ZYXELEXT, ptreeNode));
                break;
            case UUID_wifi_auto_config_approve:
                node = findNodeByKey(DM_ENABLE,
                                    findNodeByKey(DM_AUTO_CFG_APPROVE,
                                        findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
		}
        if (node == NULL) {
            cout<<"Can't find node by key(UUID:"<<UUID<<")"<<endl;
            throw ERR_CDATA_CANT_FIND_PARAMETER;
        }
		CJSON json(ptreeNode);
		string jstr = json.genJsonFromTree(node);
		return 	jstr;
	}else{
        cout << "Error!Didn't select FormatType!(in SetParameterToString funtion)"<< endl;
        throw ERR_CDATA_CANT_FIND_DATATYPE;
    }
}
string CDataMessage::CreatParameterToString(int UUID, int GUID) {
	if (FormatType == JSON) {
		treeNode *node = NULL;
        
		switch(UUID) {
            case UUID_ssid:
                if(GUID == GUID_WIFI) {
                    node = findNodeByKey(DM_SSID,
                                         findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_WIFI, ptreeNode)));
                } else if (GUID == GUID_GUEST_WIFI) {
                    node = findNodeByKey(DM_SSID,
                                         findNodeByKey(DM_GUESTAP, ptreeNode));
                }
                break;
            case UUID_security:
                if(GUID == GUID_WIFI) {
                    node = findNodeByKey(DM_MODEENABLED,
                                         findNodeByKey(GetInterfaceIndex(UUID, GUID),
                                                       findNodeByKey(DM_AP, ptreeNode)));
                } else if (GUID == GUID_GUEST_WIFI) {
                    node = findNodeByKey(DM_MODEENABLED,
                                         findNodeByKey(DM_SECURITY,
                                                       findNodeByKey(DM_GUESTAP, ptreeNode)));
                }
                break;
            case UUID_presharekey:
                if(GUID == GUID_WIFI) {
                    node = findNodeByKey(DM_PRESHAREKEY,
                                         findNodeByKey(GetInterfaceIndex(UUID, GUID),
                                                       findNodeByKey(DM_AP, ptreeNode)));
                } else if (GUID == GUID_GUEST_WIFI) {
                    node = findNodeByKey(DM_PRESHAREKEY,
                                         findNodeByKey(DM_GUESTAP, ptreeNode));
                }
                break;
            case UUID_enable:
                if(GUID == GUID_WIFI) {
                    node = findNodeByKey(DM_ENABLE,
                                         findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_WIFI, ptreeNode)));
                } else if (GUID == GUID_GUEST_WIFI) {
                    node = findNodeByKey(DM_ENABLE,
                                         findNodeByKey(DM_GUESTAP, ptreeNode));
                }
                break;
            case UUID_ssid_enable:
                node = findNodeByKey(DM_ENABLE,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_WIFI, ptreeNode)));
                break;
            case UUID_ssid_status:
                node = findNodeByKey(DM_STATUS,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_WIFI, ptreeNode)));
                break;
            case UUID_radio_num:
                node = findNodeByKey(DM_RADIONUM,
                                     findNodeByKey(DM_WIFI, ptreeNode));
                break;
            case UUID_radio_list:
                node = findNodeByKey(DM_RADIO,
                                     findNodeByKey(DM_WIFI, ptreeNode));
                break;
            case UUID_radio_enable:
                node = findNodeByKey(DM_ENABLE,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_RADIO, ptreeNode)));
                break;
            case UUID_radio_status:
                node = findNodeByKey(DM_STATUS,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_RADIO, ptreeNode)));
                break;
            case UUID_maxbitrate:
                node = findNodeByKey(DM_MAXBITRATE,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_RADIO, ptreeNode)));
                break;
            case UUID_opearting_frequency_band:
                node = findNodeByKey(DM_FREQUENCYBAND,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_RADIO, ptreeNode)));
                break;
            case UUID_opearting_standards:
                node = findNodeByKey(DM_STANDARDS,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_RADIO, ptreeNode)));
                break;
            case UUID_radio_autochannel_enable:
                node = findNodeByKey(DM_AUTOCHENABLE,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_RADIO, ptreeNode)));
                break;
            case UUID_channel:
                node = findNodeByKey(DM_CHANNEL,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_RADIO, ptreeNode)));
                break;
            case UUID_transmit_power:
                node = findNodeByKey(DM_TRANSMITPW,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_RADIO, ptreeNode)));
                break;
            case UUID_host_num:
                node = findNodeByKey(DM_HOSTNUM,
                                     findNodeByKey(DM_HOSTS, ptreeNode));
                break;
            case UUID_end_device_list:
                node = findNodeByKey(DM_HOST,
                                     findNodeByKey(DM_HOSTS, ptreeNode));
                break;
            case UUID_parentalcontrol_num_of_entries:
                node = findNodeByKey(DM_PARENTALNUM,
                                     findNodeByKey(DM_ZYXELEXT, ptreeNode));
                break;
            case UUID_all_profile:
                node = findNodeByKey(DM_PARENTALCONTROL,
                                     findNodeByKey(DM_ZYXELEXT, ptreeNode));
                break;
            case UUID_fw_num_of_entries:
                node = findNodeByKey(DM_FWTALNUM,
                                     findNodeByKey(DM_ZYXELEXT, ptreeNode));
                break;
            case UUID_timer:
                node = findNodeByKey(DM_TIMER,
                                     findNodeByKey(DM_GUESTAP, ptreeNode));
                break;
		}
        if (node == NULL) {
            cout<<"Can't find node by key(UUID:"<<UUID<<")"<<endl;
            throw ERR_CDATA_CANT_FIND_PARAMETER;
        }
		CJSON json(ptreeNode);
		string jstr = json.genJsonFromTree(node);
		return jstr;
	}else{
        cout << "Error!Didn't select FormatType!(in SetParameterToString funtion)"<< endl;
        throw ERR_CDATA_CANT_FIND_DATATYPE;
    }
}
string CDataMessage::CreatParameterToString(int UUID, int GUID, string value){
    if (FormatType == JSON) {
		treeNode *node = NULL;
		switch(UUID) {
            case UUID_ssid:
                if(GUID == GUID_WIFI) {
                    node = findNodeByKey(DM_SSID,
                                         findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_WIFI, ptreeNode)));
                } else if (GUID == GUID_GUEST_WIFI) {
                    node = findNodeByKey(DM_SSID,
                                         findNodeByKey(DM_GUESTAP, ptreeNode));
                }
                break;
            case UUID_security:
                if(GUID == GUID_WIFI) {
                    node = findNodeByKey(DM_MODEENABLED,
                                         findNodeByKey(GetInterfaceIndex(UUID, GUID),
                                                       findNodeByKey(DM_AP, ptreeNode)));
                } else if (GUID == GUID_GUEST_WIFI) {
                    node = findNodeByKey(DM_MODEENABLED, findNodeByKey(DM_SECURITY,
                                                                       findNodeByKey(DM_GUESTAP, ptreeNode)));
                }
                break;
            case UUID_presharekey:
                if(GUID == GUID_WIFI) {
                    node = findNodeByKey(DM_PRESHAREKEY,
                                         findNodeByKey(GetInterfaceIndex(UUID, GUID),
                                                       findNodeByKey(DM_AP, ptreeNode)));
                } else if (GUID == GUID_GUEST_WIFI) {
                    node = findNodeByKey(DM_PRESHAREKEY,
                                         findNodeByKey(DM_GUESTAP, ptreeNode));
                }
                break;
            case UUID_enable:
                if(GUID == GUID_WIFI) {
                    node = findNodeByKey(DM_ENABLE,
                                         findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_WIFI, ptreeNode)));
                } else if (GUID == GUID_GUEST_WIFI) {
                    node = findNodeByKey(DM_ENABLE,
                                         findNodeByKey(DM_GUESTAP, ptreeNode));
                }
                break;
            case UUID_ssid_enable:
                node = findNodeByKey(DM_ENABLE,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_WIFI, ptreeNode)));
                break;
            case UUID_ssid_status:
                node = findNodeByKey(DM_STATUS,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_WIFI, ptreeNode)));
                break;
            case UUID_radio_num:
                node = findNodeByKey(DM_RADIONUM,
                                     findNodeByKey(DM_WIFI, ptreeNode));
                break;
            case UUID_radio_enable:
                node = findNodeByKey(DM_ENABLE,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_RADIO, ptreeNode)));
                break;
            case UUID_radio_status:
                node = findNodeByKey(DM_STATUS,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_RADIO, ptreeNode)));
                break;
            case UUID_maxbitrate:
                node = findNodeByKey(DM_MAXBITRATE,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_RADIO, ptreeNode)));
                break;
            case UUID_opearting_frequency_band:
                node = findNodeByKey(DM_FREQUENCYBAND,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_RADIO, ptreeNode)));
                break;
            case UUID_opearting_standards:
                node = findNodeByKey(DM_STANDARDS,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_RADIO, ptreeNode)));
                break;
            case UUID_radio_autochannel_enable:
                node = findNodeByKey(DM_AUTOCHENABLE,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_RADIO, ptreeNode)));
                break;
            case UUID_channel:
                node = findNodeByKey(DM_CHANNEL,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_RADIO, ptreeNode)));
                break;
            case UUID_transmit_power:
                node = findNodeByKey(DM_TRANSMITPW,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_RADIO, ptreeNode)));
                break;
            case UUID_host_num:
                node = findNodeByKey(DM_HOSTNUM,
                                     findNodeByKey(DM_HOSTS, ptreeNode));
                break;
            case UUID_end_device_list:
                node = findNodeByKey(DM_HOST,
                                     findNodeByKey(DM_HOSTS, ptreeNode));
                break;
            case UUID_parentalcontrol_num_of_entries:
                node = findNodeByKey(DM_PARENTALNUM,
                                     findNodeByKey(DM_ZYXELEXT, ptreeNode));
                break;
            case UUID_all_profile:
                node = findNodeByKey(DM_PARENTALCONTROL,
                                     findNodeByKey(DM_ZYXELEXT, ptreeNode));
                break;
                
		}
        if (node == NULL) {
            cout<<"Can't find node by key(UUID:"<<UUID<<")"<<endl;
            throw ERR_CDATA_CANT_FIND_PARAMETER;
        }
        
		CJSON json(ptreeNode);
		string jstr = json.genJsonFromTreeWithPtr(node, value);
		return jstr;
	}else{
        cout << "Error!Didn't select FormatType!(in CreatParameterToString funtion)"<< endl;
        throw ERR_CDATA_CANT_FIND_DATATYPE;
        return "";
    }
}

string CDataMessage::CreatParameterToString(int UUID, int GUID, int index) {
	std::stringstream ss;
	ss << index;
	string indexStr = "i" + ss.str();
    
	if (FormatType == JSON) {
		treeNode *node = NULL;
        
		switch(UUID) {
                
            case UUID_all_profile:
                node = 	findNodeByKey(indexStr,
                                      findNodeByKey(DM_PARENTALCONTROL,
                                                    findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_end_device_list:
                node = 	findNodeByKey(indexStr,
                                      findNodeByKey(DM_HOST,
                                                    findNodeByKey(DM_HOSTS, ptreeNode)));
                break;
            case UUID_tracerouter_routehops_all:
                node = 	findNodeByKey(indexStr,
                                      findNodeByKey(DM_ROUTERHOPS,
                                                    findNodeByKey(DM_TRACEROUTER,
                                                                  findNodeByKey(DM_DIAGNOSTICS,
                                                                                findNodeByKey(DM_IP, ptreeNode)))));
                break;
            case UUID_route_ipv4forwarding_all:
                node = 	findNodeByKey(indexStr,
                                      findNodeByKey(DM_IPV4_FORWARDING,
                                                    findNodeByKey(DM_ROUTER,
                                                                  findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                break;
            case UUID_l2_fwupgrade_list:
                node = 	findNodeByKey(indexStr,
                                      findNodeByKey(DM_FWUPGRADE,
                                                    findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_radio_list:
                node =  findNodeByKey(indexStr,
                                      findNodeByKey(DM_RADIO,
                                                    findNodeByKey(DM_WIFI, ptreeNode)));
                break;
		}
        if (node == NULL) {
            cout<<"Can't find node by key(UUID:"<<UUID<<")"<<endl;
            throw ERR_CDATA_CANT_FIND_PARAMETER;
        }
		CJSON json(ptreeNode);
		string jstr = json.genJsonFromTree(node);
		return jstr;
	}else{
        cout << "Error!Didn't select FormatType!(in SetParameterToString funtion)"<< endl;
        throw ERR_CDATA_CANT_FIND_DATATYPE;
    }
}
bool CDataMessage::ParserData(string tmp) {
	int result = 0;
	if (FormatType == JSON) {
		CJSON json(ptreeNode);
		result = json.parseJsonToTree(tmp, ptreeNode);
        return result;
	}else{
        throw ERR_CDATA_CANT_FIND_DATATYPE;
    }
}

string CDataMessage::GetParameter(int UUID) {
	if (FormatType == JSON) {
		treeNode *node = NULL;
		switch(UUID) {
            case UUID_device_software_version:
                node = findNodeByKey(DM_SOFTWAREVERSION,
                                     findNodeByKey(DM_DEVICEINFO, ptreeNode));
                break;
            case UUID_device_check_software_version:
                node = findNodeByKey(DM_CHECKSOFTWARE,
                                     findNodeByKey(DM_SOFTWAREUPDATEOTA,
                                                   findNodeByKey(DM_DEVICEINFO, ptreeNode)));
                break;
            case UUID_device_latest_software_version:
                node = findNodeByKey(DM_LATESTSOFTWARE,
                                     findNodeByKey(DM_SOFTWAREUPDATEOTA,
                                                   findNodeByKey(DM_DEVICEINFO, ptreeNode)));
                break;
            case UUID_device_update_software_status:
                node = findNodeByKey(DM_UPDATESOFTWARESTATUS,
                                     findNodeByKey(DM_SOFTWAREUPDATEOTA,
                                                   findNodeByKey(DM_DEVICEINFO, ptreeNode)));
                break;
            case UUID_device_fw_upgrade_status:
                node = findNodeByKey(DM_FWUPGRADESTATUS,
                                     findNodeByKey(DM_SOFTWAREUPDATEOTA,
                                                   findNodeByKey(DM_DEVICEINFO, ptreeNode)));
                break;
            case UUID_device_has_software_version:
                node = findNodeByKey(DM_HASNEWSOFTWARE,
                                     findNodeByKey(DM_SOFTWAREUPDATEOTA,
                                                   findNodeByKey(DM_DEVICEINFO, ptreeNode)));
                break;
            case UUID_device_latest_software_date:
                node = findNodeByKey(DM_LATESTSOFTWAREDATE,
                                     findNodeByKey(DM_SOFTWAREUPDATEOTA,
                                                   findNodeByKey(DM_DEVICEINFO, ptreeNode)));
                break;
            case UUID_apn:
                node = findNodeByKey(DM_APNAME,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_authentication:
                node = findNodeByKey(DM_PPPAUTH,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_cellular_username:
                node = findNodeByKey(DM_USERNAME,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_cellular_password:
                node = findNodeByKey(DM_PASSWORD,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_signal_strength:
                node = findNodeByKey(DM_SIGNALSTRENGTH,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_cellular_mode:
                node = findNodeByKey(DM_CELLULARMODE,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_service_provider:
                node = findNodeByKey(DM_SERVICEPROVIDER,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_auto_reset:
                node = findNodeByKey(DM_MONTHLYRESETENABLE,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_reset_count:
                node = findNodeByKey(DM_RESETCOUNTER,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_reset_day:
                node = findNodeByKey(DM_MONTHLYRESETDAY,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_last_reset_date:
                node = findNodeByKey(DM_LASTRESETDATE,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_monthly_quota:
                node = findNodeByKey(DM_MONTHLYLIMIT,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_current_usage:
                break;
            case UUID_bytes_send:
                node = findNodeByKey(DM_BYTESSENT,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_bytes_received:
                node = findNodeByKey(DM_BYTESRECEIVED,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_daily_usage_advice:
                break;
            case UUID_data_plan_enable:
                node = findNodeByKey(DM_ENABLE,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_device_battery_charge:
                node = findNodeByKey(DM_CHARGESTATUS,
                                     findNodeByKey(DM_BATTERYSTATUS,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_device_battery_total:
                node = findNodeByKey(DM_TOTALCAPACITY,
                                     findNodeByKey(DM_BATTERYSTATUS,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_device_battery:
                node = findNodeByKey(DM_REMAINCAPACITY,
                                     findNodeByKey(DM_BATTERYSTATUS,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_wifi_autoconfig_op:
                node = findNodeByKey(DM_CTR_OP,
                                     findNodeByKey(DM_WIFI_AUTOCONFIG,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_tracerouter_numberofroutehops:
                node = findNodeByKey(DM_NUMOFROUTERHOPS,
                                     findNodeByKey(DM_TRACEROUTER,
                                                   findNodeByKey(DM_DIAGNOSTICS,
                                                                 findNodeByKey(DM_IP, ptreeNode))));
                break;
            case UUID_route_numberofipv4forwarding:
                node = findNodeByKey(DM_NUMOFIPV4,
                                     findNodeByKey(DM_ROUTER,
                                                   findNodeByKey(DM_ZYXELEXT,ptreeNode)));
                break;
            //Device.X_ZyXEL_Ext.DeviceInfo.WanInfo
            case UUID_waninfo_status:
                node = node = findNodeByKey(DM_WAN_STATUS,
                                            findNodeByKey(DM_WANINFO,
                                                          findNodeByKey(DM_DEVICEINFO,
                                                                        findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                break;
            case UUID_waninfo_ip:
                node = node = findNodeByKey(DM_WAN_IP,
                                            findNodeByKey(DM_WANINFO,
                                                          findNodeByKey(DM_DEVICEINFO,
                                                                        findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                break;
            case UUID_waninfo_phyrate:
                node = node = findNodeByKey(DM_WAN_PHYRATE,
                                            findNodeByKey(DM_WANINFO,
                                                          findNodeByKey(DM_DEVICEINFO,
                                                                        findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                break;
            case UUID_waninfo_mac:
                node = node = findNodeByKey(DM_WAN_MAC,
                                            findNodeByKey(DM_WANINFO,
                                                          findNodeByKey(DM_DEVICEINFO,
                                                                        findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                break;
            case UUID_waninfo_rx:
                node = node = findNodeByKey(DM_WAN_RX,
                                            findNodeByKey(DM_WANINFO,
                                                          findNodeByKey(DM_DEVICEINFO,
                                                                        findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                break;
            case UUID_waninfo_tx:
                node = node = findNodeByKey(DM_WAN_TX,
                                            findNodeByKey(DM_WANINFO,
                                                          findNodeByKey(DM_DEVICEINFO,
                                                                        findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                break;
            //Device.X_ZyXEL_Ext.DeviceInfo
            case UUID_deviceinfo_uptime:
                node = findNodeByKey(DM_UP_TIME,
                                     findNodeByKey(DM_DEVICEINFO,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_deviceinfo_systemtime:
                node = findNodeByKey(DM_SYSTEM_TIME,
                                     findNodeByKey(DM_DEVICEINFO,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            //Device.X_ZyXEL_Ext.DeviceInfo.ProcessStatus
            case UUID_deviceinfo_processstatus_cpuusage:
                node = findNodeByKey(DM_CPU_USAGE,
                                     findNodeByKey(DM_PROCESS_STATUS,
                                                   findNodeByKey(DM_DEVICEINFO,
                                                                 findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                break;
            case UUID_device_password:
                node = findNodeByKey(DM_CONFIGPASSWORD,
                                     findNodeByKey(DM_LANCONFIGSECURITY, ptreeNode));
                break;
            case UUID_l2_speedtestinfo_dl_rate:
                node = findNodeByKey(DM_SP_DL_RATE,
                                     findNodeByKey(DM_SPEEDTESTINFO,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_l2_speedtestinfo_ul_rate:
                node = findNodeByKey(DM_SP_UL_RATE,
                                     findNodeByKey(DM_SPEEDTESTINFO,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_l2_speedtestinfo_end:
                node = findNodeByKey(DM_SP_END,
                                     findNodeByKey(DM_SPEEDTESTINFO,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            //Device.X_ZyXEL_Ext.L2DevCtrl_FWCfg
            case UUID_l2ctrl_fwcfg_state:
                node = findNodeByKey(DM_CTR_FW_STATE,
                                     findNodeByKey(DM_L2DEV_CTR_FWCONFIG,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_l2ctrl_fwcfg_Success:
                node = findNodeByKey(DM_CTR_FW_SUCCESS,
                                     findNodeByKey(DM_L2DEV_CTR_FWCONFIG,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            //Device.X_ZyXEL_Ext.AppInfo
            case UUID_device_feature_app_version:
                node = findNodeByKey(DM_APPVERSION,
                                     findNodeByKey(DM_APPINFO,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_device_feature_deviceinfo:
                node = findNodeByKey(DM_DEVICEINFO,
                                     findNodeByKey(DM_FEATURELIST,
                                                   findNodeByKey(DM_APPINFO,
                                                                 findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                
                break;
            case UUID_device_feature_gatewayinfo:
                node = findNodeByKey(DM_GATEWAYINFO,
                                     findNodeByKey(DM_FEATURELIST,
                                                   findNodeByKey(DM_APPINFO,
                                                                 findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                
                break;
            case UUID_device_feature_flowinfo:
                node = findNodeByKey(DM_FLOWINFO,
                                     findNodeByKey(DM_FEATURELIST,
                                                   findNodeByKey(DM_APPINFO,
                                                                 findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                
                break;
            case UUID_device_feature_login:
                node = findNodeByKey(DM_LOGIN,
                                     findNodeByKey(DM_FEATURELIST,
                                                   findNodeByKey(DM_APPINFO,
                                                                 findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                
                break;
            case UUID_device_feature_nettopology:
                node = findNodeByKey(DM_NETTOPOLOGY,
                                     findNodeByKey(DM_FEATURELIST,
                                                   findNodeByKey(DM_APPINFO,
                                                                 findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                
                break;
            case UUID_device_feature_devivectrl:
                node = findNodeByKey(DM_DEVICECTRL,
                                     findNodeByKey(DM_FEATURELIST,
                                                   findNodeByKey(DM_APPINFO,
                                                                 findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                
                break;
            case UUID_device_feature_l2devivectrl:
                node = findNodeByKey(DM_L2DEVICECTRL,
                                     findNodeByKey(DM_FEATURELIST,
                                                   findNodeByKey(DM_APPINFO,
                                                                 findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                
                break;
            case UUID_device_feature_devicon:
                node = findNodeByKey(DM_DEVICON,
                                     findNodeByKey(DM_FEATURELIST,
                                                   findNodeByKey(DM_APPINFO,
                                                                 findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                
                break;
            case UUID_device_feature_guestwifi:
                node = findNodeByKey(DM_GUESTWIFI,
                                     findNodeByKey(DM_FEATURELIST,
                                                   findNodeByKey(DM_APPINFO,
                                                                 findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                
                break;
            case UUID_device_feature_wifiautocfg:
                node = findNodeByKey(DM_WIFI_AUTOCONFIG,
                                     findNodeByKey(DM_FEATURELIST,
                                                   findNodeByKey(DM_APPINFO,
                                                                 findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                
                break;
            case UUID_device_feature_wifionoff:
                node = findNodeByKey(DM_WIFIONOFF,
                                     findNodeByKey(DM_FEATURELIST,
                                                   findNodeByKey(DM_APPINFO,
                                                                 findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                
                break;
            case UUID_device_feature_speedtest:
                node = findNodeByKey(DM_SPEEDTEST,
                                     findNodeByKey(DM_FEATURELIST,
                                                   findNodeByKey(DM_APPINFO,
                                                                 findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                
                break;
            case UUID_device_feature_diagnostic:
                node = findNodeByKey(DM_DIAGONSTIC,
                                     findNodeByKey(DM_FEATURELIST,
                                                   findNodeByKey(DM_APPINFO,
                                                                 findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                
                break;
            case UUID_device_feature_cloudservice:
                node = findNodeByKey(DM_CLOUDSERVICE,
                                     findNodeByKey(DM_FEATURELIST,
                                                   findNodeByKey(DM_APPINFO,
                                                                 findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                
                break;
            case UUID_device_feature_fwupgrade:
                node = findNodeByKey(DM_APPINFO_FWUPGRADE,
                                     findNodeByKey(DM_FEATURELIST,
                                                   findNodeByKey(DM_APPINFO,
                                                                 findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                
                break;
            case UUID_device_feature_ledonoff:
                node = findNodeByKey(DM_LEDONOFF,
                                     findNodeByKey(DM_FEATURELIST,
                                                   findNodeByKey(DM_APPINFO,
                                                                 findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                
                break;

            case UUID_device_feature_guestgroup:
                node = findNodeByKey(DM_CONN_GUEST,
                                     findNodeByKey(DM_FEATURELIST,
                                                   findNodeByKey(DM_APPINFO,
                                                                 findNodeByKey(DM_ZYXELEXT, ptreeNode))));
            case UUID_gateway_description:
                node = findNodeByKey(DM_GATEWAY_DESCRIPTION,
                                     findNodeByKey(DM_DESCRIPTION,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_c2g_speedtestinfo_port:
                node = findNodeByKey(DM_GWSPEED_SERVER_PORT,
                                     findNodeByKey(DM_GWSPEEDTESTINFO,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;

            case UUID_g2c_rssi:
                node = findNodeByKey(DM_RSSI,
                                    findNodeByKey(DM_GATEWAY_RSSI,
                                                  findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;

             case UUID_gateway_info_hostname:
                            node = findNodeByKey(DM_GATEWAY_HOSTNAME,
                                                  findNodeByKey(DM_GATEWAYINFO,
                                                               findNodeByKey(DM_ZYXELEXT, ptreeNode)));

                break;

             case UUID_gateway_info_hostmac:
                            node = findNodeByKey(DM_GATEWAY_HOSTMAC,
                                                  findNodeByKey(DM_GATEWAYINFO,
                                                               findNodeByKey(DM_ZYXELEXT, ptreeNode)));

                break;

             case UUID_gateway_info_autocfg:
                            node = findNodeByKey(DM_GATEWAY_AUTOCONFIG,
                                                  findNodeByKey(DM_GATEWAYINFO,
                                                               findNodeByKey(DM_ZYXELEXT, ptreeNode)));

                break;

            case UUID_wifi_auto_config_approve:
                node = findNodeByKey(DM_ENABLE,
                                    findNodeByKey(DM_AUTO_CFG_APPROVE,
                                                    findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
		}
        if (node == NULL) {
            cout<<"Can't find node by key(UUID:"<<UUID<<")"<<endl;
            throw ERR_CDATA_CANT_FIND_PARAMETER;
        }
		if (node->data.isString()) {
			return node->data.asString();
		} else {
			string s;
			stringstream ss(s);
			ss << node->data.asInt();
			return ss.str();
		}
	}else{
        cout << "Error!Didn't select FormatType!(in GetParameter funtion)" << endl;
        throw ERR_CDATA_CANT_FIND_DATATYPE;
    }
}
string CDataMessage::GetParameter(int UUID, int GUID) {
    
	if (FormatType == JSON) {
		treeNode *node = NULL;
        
		switch(UUID) {
            case UUID_ssid:
                if(GUID == GUID_WIFI || GUID == GUID_WIFI_5G) {
                    node = findNodeByKey(DM_SSID,
                                         findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_WIFI, ptreeNode)));
                } else if (GUID == GUID_GUEST_WIFI) {
                    node = findNodeByKey(DM_SSID,
                                         findNodeByKey(DM_GUESTAP, ptreeNode));
                }
                break;
            case UUID_security:
                if(GUID == GUID_WIFI || GUID == GUID_WIFI_5G) {
                    node = findNodeByKey(DM_MODEENABLED,
                                         findNodeByKey(GetInterfaceIndex(UUID, GUID),
                                                       findNodeByKey(DM_AP, ptreeNode)));
                } else if (GUID == GUID_GUEST_WIFI) {
                    node = findNodeByKey(DM_MODEENABLED, findNodeByKey(DM_SECURITY,
                                                                       findNodeByKey(DM_GUESTAP, ptreeNode)));
                }
                break;
            case UUID_presharekey:
                if(GUID == GUID_WIFI || GUID == GUID_WIFI_5G) {
                    node = findNodeByKey(DM_PRESHAREKEY,
                                         findNodeByKey(GetInterfaceIndex(UUID, GUID),
                                                       findNodeByKey(DM_AP, ptreeNode)));
                } else if (GUID == GUID_GUEST_WIFI) {
                    node = findNodeByKey(DM_PRESHAREKEY,
                                         findNodeByKey(DM_GUESTAP, ptreeNode));
                }
                break;
            case UUID_enable:
                if(GUID == GUID_WIFI || GUID == GUID_WIFI_5G) {
                    node = findNodeByKey(DM_ENABLE,
                                         findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_WIFI, ptreeNode)));
                } else if (GUID == GUID_GUEST_WIFI) {
                    node = findNodeByKey(DM_ENABLE,
                                         findNodeByKey(DM_GUESTAP, ptreeNode));
                }
                break;
            case UUID_ssid_enable:
                node = findNodeByKey(DM_ENABLE,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_WIFI, ptreeNode)));
                break;
            case UUID_ssid_status:
                node = findNodeByKey(DM_STATUS,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_WIFI, ptreeNode)));
                break;
            case UUID_radio_num:
                node = findNodeByKey(DM_RADIONUM,
                                     findNodeByKey(DM_WIFI, ptreeNode));
                break;
            case UUID_radio_enable:
                node = findNodeByKey(DM_ENABLE,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_RADIO, ptreeNode)));
                break;
            case UUID_radio_status:
                //node = findNodeByKey(DM_STATUS,
                //                    findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_RADIO, ptreeNode)));
                if(GUID == GUID_WIFI) {
                node = findNodeByKey(DM_STATUS,
                                    findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_RADIO, ptreeNode)));
                } else if(GUID == GUID_WIFI_5G) {
                node = findNodeByKey(DM_STATUS,
                                    findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_RADIO, ptreeNode)));
                }
                break;
            case UUID_maxbitrate:
                node = findNodeByKey(DM_MAXBITRATE,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_RADIO, ptreeNode)));
                break;
            case UUID_opearting_frequency_band:
                node = findNodeByKey(DM_FREQUENCYBAND,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_RADIO, ptreeNode)));
                break;
            case UUID_opearting_standards:
                node = findNodeByKey(DM_STANDARDS,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_RADIO, ptreeNode)));
                break;
            case UUID_radio_autochannel_enable:
                node = findNodeByKey(DM_AUTOCHENABLE,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_RADIO, ptreeNode)));
                break;
            case UUID_channel:
                node = findNodeByKey(DM_CHANNEL,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_RADIO, ptreeNode)));
                break;
            case UUID_transmit_power:
                node = findNodeByKey(DM_TRANSMITPW,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_RADIO, ptreeNode)));
                break;
            case UUID_host_num:
                node = findNodeByKey(DM_HOSTNUM,
                                     findNodeByKey(DM_HOSTS, ptreeNode));
                break;
            case UUID_end_device_list:
                node = findNodeByKey(DM_HOST,
                                     findNodeByKey(DM_HOSTS, ptreeNode));
                break;
            case UUID_parentalcontrol_num_of_entries:
                node = findNodeByKey(DM_PARENTALNUM,
                                     findNodeByKey(DM_ZYXELEXT, ptreeNode));
                break;
            case UUID_fw_num_of_entries:
                node = findNodeByKey(DM_FWTALNUM,
                                     findNodeByKey(DM_ZYXELEXT, ptreeNode));
                break;
            case UUID_timer_hour:
                node = findNodeByKey(DM_TIMER_HOURS, findNodeByKey(DM_TIMER,
                                                                   findNodeByKey(DM_GUESTAP, ptreeNode)));
                break;
            case UUID_timer_minutes:
                node = findNodeByKey(DM_TIMER_MINUTES, findNodeByKey(DM_TIMER,
                                                                     findNodeByKey(DM_GUESTAP, ptreeNode)));
                break;
                
            default:
                break;
		}
        if (node == NULL) {
            cout<<"Can't find node by key(UUID:"<<UUID<<")"<<endl;
            throw ERR_CDATA_CANT_FIND_PARAMETER;
        }
		if (node->data.isString()) {
			return node->data.asString();
		} else {
			string s;
			stringstream ss(s);
			ss << node->data.asInt();
			return ss.str();
		}
	}else{
        cout << "Error!Didn't select FormatType!(in GetParameter funtion)" << endl;
        throw ERR_CDATA_CANT_FIND_DATATYPE;
    }
}

string CDataMessage::GetParameter(int UUID, int GUID, int index) {
	std::stringstream ss;
	ss << index;
	string indexStr = "i" + ss.str();
    
	if (FormatType == JSON) {
		treeNode *node = NULL;
		switch(UUID) {
            case UUID_host_name:
                node = findNodeByKey(DM_HOSTNAME,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_HOST,
                                                                 findNodeByKey(DM_HOSTS, ptreeNode))));
                break;
            case UUID_host_mac:
                node = findNodeByKey(DM_PHYSADDRESS,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_HOST,
                                                                 findNodeByKey(DM_HOSTS, ptreeNode))));
                break;
            case UUID_host_active:
                node = findNodeByKey(DM_ACTIVE,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_HOST,
                                                                 findNodeByKey(DM_HOSTS, ptreeNode))));
                break;
            case UUID_host_type:
                node = findNodeByKey(DM_HOSTTYPE,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_HOST,
                                                                 findNodeByKey(DM_HOSTS, ptreeNode))));
                break;
            case UUID_host_block:
                node = findNodeByKey(DM_BLOCKING,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_HOST,
                                                                 findNodeByKey(DM_HOSTS, ptreeNode))));
                break;
            case UUID_host_ip:
                node = findNodeByKey(DM_IPAddress,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_HOST,
                                                                 findNodeByKey(DM_HOSTS, ptreeNode))));
                break;
            case UUID_host_controlmask:
                node = findNodeByKey(DM_CONTROLLABLEMASK,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_HOST,
                                                                 findNodeByKey(DM_HOSTS, ptreeNode))));
                break;
            case UUID_WIFI_signalstrength:
                node = findNodeByKey(DM_WIFI_SIGNALSTRENGTH,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_HOST,
                                                                 findNodeByKey(DM_HOSTS, ptreeNode))));
                break;
            case UUID_l2_auto_config_status:
                node = findNodeByKey(DM_L2STA_AUTOCONFIG,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_HOST,
                                                                 findNodeByKey(DM_HOSTS, ptreeNode))));
                break;
            case UUID_l2_auto_config_enable:
                node = findNodeByKey(DM_L2ENA_AUTOCONFIG,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_HOST,
                                                                 findNodeByKey(DM_HOSTS, ptreeNode))));
                break;

            case UUID_l2_wifi_status:
                node = findNodeByKey(DM_L2STA_WIFI,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_HOST,
                                                                 findNodeByKey(DM_HOSTS, ptreeNode))));
                break;
            //Sharon add start
            case UUID_host_capability_type:
                node = findNodeByKey(DM_CAPABILITYTYPE,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_HOST,
                                                                 findNodeByKey(DM_HOSTS, ptreeNode))));
                break;
            case UUID_host_connection_type:
                node = findNodeByKey(DM_CONNECTIONTYPE,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_HOST,
                                                                 findNodeByKey(DM_HOSTS, ptreeNode))));
                break;
            case UUID_host_name_user_defined:
                node = findNodeByKey(DM_DEVICENAME_USER_DEFINED,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_HOST,
                                                                 findNodeByKey(DM_HOSTS, ptreeNode))));
                break;
            case UUID_host_phyrate:
                node = findNodeByKey(DM_RHYRATE,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_HOST,
                                                                 findNodeByKey(DM_HOSTS, ptreeNode))));
                break;
            case UUID_host_software_version:
                node = findNodeByKey(DM_SOFTWAREVERSION_HOST,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_HOST,
                                                                 findNodeByKey(DM_HOSTS, ptreeNode))));
                break;
            case UUID_host_neighbor:
                node = findNodeByKey(DM_NEIGHBOR,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_HOST,
                                                                 findNodeByKey(DM_HOSTS, ptreeNode))));
                break;
            case UUID_host_dl_rate:
                node = findNodeByKey(DM_DL_Rate,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_HOST,
                                                                 findNodeByKey(DM_HOSTS, ptreeNode))));
                break;
            case UUID_host_ul_rate:
                node = findNodeByKey(DM_UL_Rate,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_HOST,
                                                                 findNodeByKey(DM_HOSTS, ptreeNode))));
                break;
            case UUID_host_guest_wifi_group:
                node = findNodeByKey(DM_CONN_GUEST,
                                    findNodeByKey(indexStr,
                                                  findNodeByKey(DM_HOST,
                                                                findNodeByKey(DM_HOSTS, ptreeNode))));
                break;
            //Sharon add end
            case UUID_parentalcontrol_hostmac:
                node = findNodeByKey(DM_HOSTMAC,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_PARENTALCONTROL,
                                                                 findNodeByKey(DM_ZYXELEXT,
                                                                               ptreeNode))));
                break;
            case UUID_parentalcontrol_enable:
                node = findNodeByKey(DM_ENABLE,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_PARENTALCONTROL,
                                                                 findNodeByKey(DM_ZYXELEXT,
                                                                               ptreeNode))));
                break;
            case UUID_parentalcontrol_pcpname:
                node = findNodeByKey(DM_PCPNAME,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_PARENTALCONTROL,
                                                                 findNodeByKey(DM_ZYXELEXT,
                                                                               ptreeNode))));
                break;
            case UUID_parentalcontrol_schedule_days:
                node = findNodeByKey(DM_DAYS,
                                     findNodeByKey(DM_SCHEDULING,
                                                   findNodeByKey(indexStr,
                                                                 findNodeByKey(DM_PARENTALCONTROL,
                                                                               findNodeByKey(DM_ZYXELEXT,
                                                                                             ptreeNode)))));
                break;
            case UUID_parentalcontrol_schedule_time_start_hour:
                node = findNodeByKey(DM_STARTHOUR,
                                     findNodeByKey(DM_SCHEDULING,
                                                   findNodeByKey(indexStr,
                                                                 findNodeByKey(DM_PARENTALCONTROL,
                                                                               findNodeByKey(DM_ZYXELEXT,
                                                                                             ptreeNode)))));
                break;
            case UUID_parentalcontrol_schedule_time_start_min:
                node = findNodeByKey(DM_STARTMIN,
                                     findNodeByKey(DM_SCHEDULING,
                                                   findNodeByKey(indexStr,
                                                                 findNodeByKey(DM_PARENTALCONTROL,
                                                                               findNodeByKey(DM_ZYXELEXT,
                                                                                             ptreeNode)))));
                break;
            case UUID_parentalcontrol_schedule_time_stop_hour:
                node = findNodeByKey(DM_STOPHOUR,
                                     findNodeByKey(DM_SCHEDULING,
                                                   findNodeByKey(indexStr,
                                                                 findNodeByKey(DM_PARENTALCONTROL,
                                                                               findNodeByKey(DM_ZYXELEXT,
                                                                                             ptreeNode)))));
                break;
            case UUID_parentalcontrol_schedule_time_stop_min:
                node = findNodeByKey(DM_STOPMIN,
                                     findNodeByKey(DM_SCHEDULING,
                                                   findNodeByKey(indexStr,
                                                                 findNodeByKey(DM_PARENTALCONTROL,
                                                                               findNodeByKey(DM_ZYXELEXT,
                                                                                             ptreeNode)))));
                break;
            //Device.X_ZyXEL_Ext.Router.IPv4Forwarding.i
            case UUID_route_destipaddress:
                node = 	findNodeByKey(DM_DES_IP,
                                      findNodeByKey(indexStr,
                                                    findNodeByKey(DM_IPV4_FORWARDING,
                                                                  findNodeByKey(DM_ROUTER,
                                                                                findNodeByKey(DM_ZYXELEXT, ptreeNode)))));
                break;
            case UUID_route_destsubnetmask:
                node = 	findNodeByKey(DM_DES_SUBNETMASK,
                                      findNodeByKey(indexStr,
                                                    findNodeByKey(DM_IPV4_FORWARDING,
                                                                  findNodeByKey(DM_ROUTER,
                                                                                findNodeByKey(DM_ZYXELEXT, ptreeNode)))));
                break;
            case UUID_route_gatewayipaddress:
                node = 	findNodeByKey(DM_GATEWAY_IP,
                                      findNodeByKey(indexStr,
                                                    findNodeByKey(DM_IPV4_FORWARDING,
                                                                  findNodeByKey(DM_ROUTER,
                                                                                findNodeByKey(DM_ZYXELEXT, ptreeNode)))));
                break;
            case UUID_route_interface:
                node = 	findNodeByKey(DM_ZYXEL_INTERFACE,
                                      findNodeByKey(indexStr,
                                                    findNodeByKey(DM_IPV4_FORWARDING,
                                                                  findNodeByKey(DM_ROUTER,
                                                                                findNodeByKey(DM_ZYXELEXT, ptreeNode)))));
                break;
            //Device.IP.Diagnostics.TraceRoute.RouteHops.i
            case UUID_tracerouter_routehops_host:
                node = 	findNodeByKey(DM_HOST,
                                      findNodeByKey(indexStr,
                                                    findNodeByKey(DM_ROUTERHOPS,
                                                                  findNodeByKey(DM_TRACEROUTER,
                                                                                findNodeByKey(DM_DIAGNOSTICS,
                                                                                              findNodeByKey(DM_IP, ptreeNode))))));
                break;
            case UUID_tracerouter_routehops_hostaddress:
                node = 	findNodeByKey(DM_HOSTADDRESS,
                                      findNodeByKey(indexStr,
                                                    findNodeByKey(DM_ROUTERHOPS,
                                                                  findNodeByKey(DM_TRACEROUTER,
                                                                                findNodeByKey(DM_DIAGNOSTICS,
                                                                                              findNodeByKey(DM_IP, ptreeNode))))));
                break;
            case UUID_tracerouter_routehops_rtttime:
                node = 	findNodeByKey(DM_RTTIMES,
                                      findNodeByKey(indexStr,
                                                    findNodeByKey(DM_ROUTERHOPS,
                                                                  findNodeByKey(DM_TRACEROUTER,
                                                                                findNodeByKey(DM_DIAGNOSTICS,
                                                                                              findNodeByKey(DM_IP, ptreeNode))))));
                break;
            case UUID_l2_fwupgrade_modelname:
                node = findNodeByKey(DM_FWUPGRADE_MODELNAME,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_FWUPGRADE,
                                                                 findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                break;
            case UUID_l2_fwupgrade_mac:
                node = findNodeByKey(DM_FWUPGRADE_MAC,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_FWUPGRADE,
                                                                 findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                break;
            case UUID_l2_fwupgrade_newstate:
                node = findNodeByKey(DM_FWUPGRADE_NEWSTATE,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_FWUPGRADE,
                                                                 findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                break;
            case UUID_l2_fwupgrade_newversion:
                node = findNodeByKey(DM_FWUPGRADE_NEWVERSION,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_FWUPGRADE,
                                                                 findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                break;
            case UUID_l2_fwupgrade_newdate:
                node = findNodeByKey(DM_FWUPGRADE_NEWDATE,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_FWUPGRADE,
                                                                 findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                break;
            case UUID_host_autoconfig_approve:
                node = findNodeByKey(DM_WIFI_AUTO_CFG_APPROVE,
                                    findNodeByKey(indexStr,
                                                  findNodeByKey(DM_HOST,
                                                                findNodeByKey(DM_HOSTS, ptreeNode))));
                break;

            case UUID_host_manufacturer:
                node = findNodeByKey(DM_X_ZYXEL_MANUFACTURER,
                                    findNodeByKey(indexStr,
                                                  findNodeByKey(DM_HOST,
                                                                findNodeByKey(DM_HOSTS, ptreeNode))));
                break;

            case UUID_host_rssi:
                node = findNodeByKey(DM_X_ZYXEL_RSSI,
                                    findNodeByKey(indexStr,
                                                  findNodeByKey(DM_HOST,
                                                                findNodeByKey(DM_HOSTS, ptreeNode))));
                break;

            case UUID_host_band:
                node = findNodeByKey(DM_X_ZYXEL_BAND,
                                    findNodeByKey(indexStr,
                                                  findNodeByKey(DM_HOST,
                                                                findNodeByKey(DM_HOSTS, ptreeNode))));
                break;

            case UUID_host_linkrate24g:
                node = findNodeByKey(DM_X_ZYXEL_WIFILINKRATE24G,
                                    findNodeByKey(indexStr,
                                                  findNodeByKey(DM_HOST,
                                                                findNodeByKey(DM_HOSTS, ptreeNode))));
                break;

            case UUID_host_linkrate5g:
                node = findNodeByKey(DM_X_ZYXEL_WIFILINKRATE5G,
                                    findNodeByKey(indexStr,
                                                  findNodeByKey(DM_HOST,
                                                                findNodeByKey(DM_HOSTS, ptreeNode))));
                break;

            case UUID_host_channel24g:
                node = findNodeByKey(DM_X_ZYXEL_CHANNEL24G,
                                    findNodeByKey(indexStr,
                                                  findNodeByKey(DM_HOST,
                                                                findNodeByKey(DM_HOSTS, ptreeNode))));
                break;

             case UUID_host_channel5g:
                node = findNodeByKey(DM_X_ZYXEL_CHANNEL5G,
                                    findNodeByKey(indexStr,
                                                  findNodeByKey(DM_HOST,
                                                                findNodeByKey(DM_HOSTS, ptreeNode))));
                 break;
            case UUID_opearting_frequency_band:
                node = findNodeByKey(DM_FREQUENCYBAND,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_RADIO, ptreeNode)));
                break;
            case UUID_radio_status:
                node = findNodeByKey(DM_STATUS,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_RADIO, ptreeNode)));
                break;
            case UUID_channel:
                node = findNodeByKey(DM_CHANNEL,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_RADIO, ptreeNode)));
                break;
        }
        
        
		if (node == NULL) {
            cout<<"Can't find node by key(UUID:"<<UUID<<")"<<endl;
            throw ERR_CDATA_CANT_FIND_PARAMETER;
        }
		if (node->data.isString()) {
			return node->data.asString();
		} else {
			string s;
			stringstream ss(s);
			ss << node->data.asInt();
			return ss.str();
		}
	}else{
        cout << "Error!Didn't select FormatType!(in GetParameter funtion)" << endl;
        throw ERR_CDATA_CANT_FIND_DATATYPE;
    }
}

bool CDataMessage::SetParameterToBuffer(int UUID, string value, treeNode *bufferNode, int index) {
	std::stringstream ss;
	ss << index;
	string indexStr = "i" + ss.str();
	if (FormatType == JSON) {
		treeNode *node = NULL;
		switch(UUID) {
            case UUID_block:
                node = findNodeByKey(DM_BLOCKING,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_HOST,
                                                                 findNodeByKey(DM_HOSTS, ptreeNode))));
                break;
            case UUID_non_block:
                node = findNodeByKey(DM_BLOCKING,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_HOST,
                                                                 findNodeByKey(DM_HOSTS, ptreeNode))));
                break;
            case UUID_add_profile:
                node = findNodeByKey(DM_ADDRULE,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_HOST,
                                                                 findNodeByKey(DM_HOSTS, ptreeNode))));
                break;
            case UUID_modify_profile:
                node = findNodeByKey(DM_DELETE,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_PARENTALCONTROL,
                                                                 findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                break;
            case UUID_parentalcontrol_hostmac:
                node = findNodeByKey(DM_HOSTMAC,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_PARENTALCONTROL,
                                                                 findNodeByKey(DM_ZYXELEXT, ptreeNode))));
                break;
            case UUID_parentalcontrol_enable:
                node = findNodeByKey(DM_ENABLE,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_PARENTALCONTROL,
                                                                 findNodeByKey(DM_ZYXELEXT,
                                                                               ptreeNode))));
                break;
            case UUID_parentalcontrol_pcpname:
                node = findNodeByKey(DM_PCPNAME,
                                     findNodeByKey(indexStr,
                                                   findNodeByKey(DM_PARENTALCONTROL,
                                                                 findNodeByKey(DM_ZYXELEXT,
                                                                               ptreeNode))));
                break;
            case UUID_parentalcontrol_schedule_days:
                node = findNodeByKey(DM_DAYS,
                                     findNodeByKey(DM_SCHEDULING,
                                                   findNodeByKey(indexStr,
                                                                 findNodeByKey(DM_PARENTALCONTROL,
                                                                               findNodeByKey(DM_ZYXELEXT,
                                                                                             ptreeNode)))));
                break;
            case UUID_parentalcontrol_schedule_time_start_hour:
                node = findNodeByKey(DM_STARTHOUR,
                                     findNodeByKey(DM_SCHEDULING,
                                                   findNodeByKey(indexStr,
                                                                 findNodeByKey(DM_PARENTALCONTROL,
                                                                               findNodeByKey(DM_ZYXELEXT,
                                                                                             ptreeNode)))));
                break;
            case UUID_parentalcontrol_schedule_time_start_min:
                node = findNodeByKey(DM_STARTMIN,
                                     findNodeByKey(DM_SCHEDULING,
                                                   findNodeByKey(indexStr,
                                                                 findNodeByKey(DM_PARENTALCONTROL,
                                                                               findNodeByKey(DM_ZYXELEXT,
                                                                                             ptreeNode)))));
                break;
            case UUID_parentalcontrol_schedule_time_stop_hour:
                node = findNodeByKey(DM_STOPHOUR,
                                     findNodeByKey(DM_SCHEDULING,
                                                   findNodeByKey(indexStr,
                                                                 findNodeByKey(DM_PARENTALCONTROL,
                                                                               findNodeByKey(DM_ZYXELEXT,
                                                                                             ptreeNode)))));
                break;
            case UUID_parentalcontrol_schedule_time_stop_min:
                node = findNodeByKey(DM_STOPMIN,
                                     findNodeByKey(DM_SCHEDULING,
                                                   findNodeByKey(indexStr,
                                                                 findNodeByKey(DM_PARENTALCONTROL,
                                                                               findNodeByKey(DM_ZYXELEXT,
                                                                                             ptreeNode)))));
                break;
            case UUID_wifi_auto_config_approve_mac:
                node = findNodeByKey(DM_AUTO_CFG_APPROVE_MAC,
                                    findNodeByKey(DM_AUTO_CFG_APPROVE_DATA,
                                                   findNodeByKey(indexStr,
                                                                 findNodeByKey(DM_ZYXELEXT,
                                                                                            ptreeNode))));
                break;
            case UUID_wifi_auto_config_approve_option:
                node = findNodeByKey(DM_OPTION,
                                    findNodeByKey(DM_AUTO_CFG_APPROVE_DATA,
                                                   findNodeByKey(indexStr,
                                                                 findNodeByKey(DM_ZYXELEXT,
                                                                                            ptreeNode))));
                 break;
		}
        if (node == NULL) {
            cout<<"Can't find node by key(UUID:"<<UUID<<")"<<endl;
            throw ERR_CDATA_CANT_FIND_PARAMETER;
        }
		CJSON json(ptreeNode);
		string jstr = json.genJsonFromTreeWithPtr(node, value);
		json.creatTreeFromJSON(bufferNode, jstr);
        return true;
	}else{
        cout << "Error!Didn't select FormatType!(in SetParameterToBuffer funtion)" << endl;
        throw ERR_CDATA_CANT_FIND_DATATYPE;
    }
	
}

// Set Parameter To Buffer with UUID & GUID
bool CDataMessage::SetParameterToBuffer(int UUID, int GUID, string value, treeNode *bufferNode) {
	if (FormatType == JSON) {
		treeNode *node = NULL;
		string modelName = ZyXELDevice.ModelName;
		switch (UUID) {
		case UUID_enable:
			if(GUID == GUID_WIFI || GUID == GUID_WIFI_5G) {
				node = findNodeByKey(DM_ENABLE,
						findNodeByKey(GetInterfaceIndex(UUID, GUID),
								findNodeByKey(DM_SSID,
										findNodeByKey(DM_WIFI,ptreeNode))));
			} else if (GUID == GUID_GUEST_WIFI) {
				node = findNodeByKey(DM_ENABLE,
						findNodeByKey(DM_GUESTAP,ptreeNode));
			}
			break;
		case UUID_presharekey:
			if(GUID == GUID_WIFI || GUID == GUID_WIFI_5G) {
				node = findNodeByKey(DM_PRESHAREKEY,
						findNodeByKey(GetInterfaceIndex(UUID, GUID),
								findNodeByKey(DM_AP,ptreeNode)));
			} else if (GUID == GUID_GUEST_WIFI) {
				node = findNodeByKey(DM_PRESHAREKEY,
						findNodeByKey(DM_GUESTAP,ptreeNode));
			}
			break;
		case UUID_security:
			if(GUID == GUID_WIFI || GUID == GUID_WIFI_5G) {
				node = findNodeByKey(DM_MODEENABLED,
						findNodeByKey(GetInterfaceIndex(UUID, GUID),
								findNodeByKey(DM_AP,ptreeNode)));
			} else if (GUID == GUID_GUEST_WIFI) {
				node = findNodeByKey(DM_MODEENABLED, findNodeByKey(DM_SECURITY,
						findNodeByKey(DM_GUESTAP,ptreeNode)));
			}

			if(value == "none") {
	        	value = "none";
			} else if(modelName == Device_NBG6716 && value == "WPA2PSK") {
	        	value = "WPA2-Personal";
	        } else if(modelName == Device_AMG1202 && value == "WPA2PSK") {
	        	value = "WPA2PSK";
	        } else if(modelName == Device_AMG1312 && value == "WPA2PSK") {
	        	value = "WPA2PSK";
	        }

			break;
		case UUID_ssid:
			if(GUID == GUID_WIFI || GUID == GUID_WIFI_5G) {
				node = findNodeByKey(DM_SSID,
						findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_WIFI,ptreeNode)));
			} else if (GUID == GUID_GUEST_WIFI) {
				node = findNodeByKey(DM_SSID,
						findNodeByKey(DM_GUESTAP,ptreeNode));
			}
			break;

		case UUID_opearting_frequency_band:
		    if(GUID == GUID_WIFI) {
		        node = findNodeByKey(DM_FREQUENCYBAND,
            		    findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_WIFI,ptreeNode)));
		    } else if(GUID == GUID_WIFI_5G) {
		        node = findNodeByKey(DM_FREQUENCYBAND,
            			findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_WIFI,ptreeNode)));
		    }
		    break;

		case UUID_radio_status:
		    if(GUID == GUID_WIFI) {
		        node = findNodeByKey(DM_STATUS,
            		    findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_WIFI,ptreeNode)));
		    } else if(GUID == GUID_WIFI_5G) {
		        node = findNodeByKey(DM_STATUS,
            			findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_WIFI,ptreeNode)));
		    }
		    break;
        case UUID_timer_hour:
            node = findNodeByKey(DM_TIMER_HOURS, findNodeByKey(DM_TIMER,
                                                               findNodeByKey(DM_GUESTAP,ptreeNode)));

            
            break;
        case UUID_timer_minutes:
            node = findNodeByKey(DM_TIMER_MINUTES, findNodeByKey(DM_TIMER,
                                                               findNodeByKey(DM_GUESTAP,ptreeNode)));
            
            
            break;
        case UUID_ssid_freq_band:
            node = findNodeByKey(DM_WIFI_CHECK_BAND,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID), findNodeByKey(DM_WIFI,ptreeNode)));
            break;
        case UUID_presharekey_freq_band:
                node = findNodeByKey(DM_WIFI_CHECK_BAND,
                                     findNodeByKey(GetInterfaceIndex(UUID, GUID),
                                                   findNodeByKey(DM_AP,ptreeNode)));
            break;
         case UUID_enable_freq_band:
         node = findNodeByKey(DM_WIFI_CHECK_BAND,
                                      findNodeByKey(GetInterfaceIndex(UUID, GUID),
                                                   findNodeByKey(DM_WIFI,ptreeNode)));
            break;
         case UUID_security_freq_band:
            node = findNodeByKey(DM_WIFI_CHECK_BAND,
                                      findNodeByKey(GetInterfaceIndex(UUID, GUID),
                                                   findNodeByKey(DM_AP,ptreeNode)));
            break;

            case UUID_smart_device_power_plug_op:
                node = findNodeByKey(DM_SMARTDEV_CTR,
                                    findNodeByKey(DM_ZYXELEXT, ptreeNode));
                break;

            case UUID_smart_device_power_plug_protocol:
                node = findNodeByKey(DM_SMARTDEV_CTR_PROTOCOL,
                                    findNodeByKey(DM_SMARTDEV_CTR,
                                        findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;

            case UUID_smart_device_power_plug_id:
                node = findNodeByKey(DM_SMARTDEV_CTR_ID,
                                     findNodeByKey(DM_SMARTDEV_CTR,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;

            case UUID_smart_device_power_plug_action:
                node = findNodeByKey(DM_SMARTDEV_CTR_ACTION,
                                     findNodeByKey(DM_SMARTDEV_CTR,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
		}
        if (node == NULL) {
            cout<<"Can't find node by key(UUID:"<<UUID<<")"<<endl;
            throw ERR_CDATA_CANT_FIND_PARAMETER;
        }

		CJSON json(ptreeNode);
		string jstr = json.genJsonFromTreeWithPtr(node, value);
		json.creatTreeFromJSON(bufferNode, jstr);
        return true;
	}else{
        cout << "Error!Didn't select FormatType!(in SetParameterToBuffer funtion)" << endl;
        throw ERR_CDATA_CANT_FIND_DATATYPE;
    }
}

// Set Parameter To Buffer with UUID only
bool CDataMessage::SetParameterToBuffer(int UUID, string value, treeNode *bufferNode) {
	if (FormatType == JSON) {
		treeNode *node = NULL;
        nodesAddParent(ptreeNode);
		switch (UUID) {
            case UUID_radio_autochannel_enable:
                node = findNodeByKey(DM_AUTOCHENABLE,
                                     findNodeByKey(Default_Interface, findNodeByKey(DM_RADIO, ptreeNode)));
                break;
            case UUID_channel:
                node = findNodeByKey(DM_CHANNEL,
                                     findNodeByKey(Default_Interface, findNodeByKey(DM_RADIO, ptreeNode)));
                break;
            case UUID_apn:
                node = findNodeByKey(DM_APNAME,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_authentication:
                node = findNodeByKey(DM_PPPAUTH,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_cellular_username:
                node = findNodeByKey(DM_USERNAME,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_cellular_password:
                node = findNodeByKey(DM_PASSWORD,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_monthly_quota:
                node = findNodeByKey(DM_MONTHLYLIMIT,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_reset_day:
                node = findNodeByKey(DM_MONTHLYRESETDAY,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_auto_reset:
                node = findNodeByKey(DM_MONTHLYRESETENABLE,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_data_plan_enable:
                node = findNodeByKey(DM_ENABLE,
                                     findNodeByKey(Default_Interface,
                                                   findNodeByKey(DM_ZYXEL3G, ptreeNode)));
                break;
            case UUID_device_check_software_version:
                node = findNodeByKey(DM_CHECKSOFTWARE,
                                     findNodeByKey(DM_SOFTWAREUPDATEOTA,
                                                   findNodeByKey(DM_DEVICEINFO, ptreeNode)));
                break;
            case UUID_device_update_software:
                node = findNodeByKey(DM_UPDATESOFTWARE,
                                     findNodeByKey(DM_SOFTWAREUPDATEOTA,
                                                   findNodeByKey(DM_DEVICEINFO, ptreeNode)));
                break;
            case UUID_device_password:
                node = findNodeByKey(DM_CONFIGPASSWORD,
                                     findNodeByKey(DM_LANCONFIGSECURITY, ptreeNode));
                break;
            case UUID_transmit_power:
                node = findNodeByKey(DM_TRANSMITPW,
                                     findNodeByKey(Default_Interface, findNodeByKey(DM_RADIO, ptreeNode)));
                break;
            case UUID_block:
                node = findNodeByKey(DM_HOSTMAC, findNodeByKey(DM_ENDDEVCTR_BLOCK,
                                                               findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_non_block:
                node = findNodeByKey(DM_HOSTMAC, findNodeByKey(DM_ENDDEVCTR_UNBLOCK,
                                                               findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_l2_reboot:
                node = findNodeByKey(DM_HOSTMAC, findNodeByKey(DM_L2CTR_REBOOT,
                                                               findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_l2_wifion:
                node = findNodeByKey(DM_HOSTMAC, findNodeByKey(DM_L2CTR_WIFION,
                                                               findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_l2_wifioff:
                node = findNodeByKey(DM_HOSTMAC, findNodeByKey(DM_L2CTR_WIFIOFF,
                                                               findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_tracerouter_host:
                node = findNodeByKey(DM_HOST, findNodeByKey(DM_TRACEROUTER,
                                                               findNodeByKey(DM_IP, ptreeNode)));
                break;
            case UUID_tracerouter_state:
                node = findNodeByKey(DM_DIAGNOSTICS_STATE, findNodeByKey(DM_TRACEROUTER,
                                                            findNodeByKey(DM_IP, ptreeNode)));
                break;
            case UUID_tracerouter_numberoftries:
                node = findNodeByKey(DM_NUMOFTRIES, findNodeByKey(DM_TRACEROUTER,
                                                               findNodeByKey(DM_IP, ptreeNode)));
                break;
            case UUID_tracerouter_timeout:
                node = findNodeByKey(DM_TIMEOUT, findNodeByKey(DM_TRACEROUTER,
                                                               findNodeByKey(DM_IP, ptreeNode)));
                break;
            case UUID_tracerouter_maxhopcount:
                node = findNodeByKey(DM_MAXBITRATE, findNodeByKey(DM_TRACEROUTER,
                                                               findNodeByKey(DM_IP, ptreeNode)));
                break;
            case UUID_tracerouter_numberofroutehops:
                node = findNodeByKey(DM_NUMOFROUTERHOPS, findNodeByKey(DM_TRACEROUTER,
                                                                  findNodeByKey(DM_IP, ptreeNode)));
                break;
            case UUID_l2ctrl_autoconfig_op:
                node = findNodeByKey(DM_CTR_OP,
                                     findNodeByKey(DM_L2DEV_CTR_AUTOCONFIG,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_l2ctrl_autoconfig_host:
                node = findNodeByKey(DM_HOST_MAC,
                                     findNodeByKey(DM_L2DEV_CTR_AUTOCONFIG,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_l2ctrl_fwcfg_op:
                node = findNodeByKey(DM_CTR_OP,
                                     findNodeByKey(DM_L2DEV_CTR_FWCONFIG,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_l2ctrl_fwcfg_host_mac:
                node = findNodeByKey(DM_HOST_MAC,
                                     findNodeByKey(DM_L2DEV_CTR_FWCONFIG,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_l2ctrl_fwcfg_state:
                node = findNodeByKey(DM_CTR_FW_STATE,
                                     findNodeByKey(DM_L2DEV_CTR_FWCONFIG,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_l2ctrl_fwcfg_Success:
                node = findNodeByKey(DM_CTR_FW_SUCCESS,
                                     findNodeByKey(DM_L2DEV_CTR_FWCONFIG,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_host_changename_mac:
                node = findNodeByKey(DM_HOST_MAC,
                                     findNodeByKey(DM_CHANGE_DEV_NAME,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_host_changename_name:
                node = findNodeByKey(DM_NAME,
                                     findNodeByKey(DM_CHANGE_DEV_NAME,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_host_changehosttype_mac:
                node = findNodeByKey(DM_HOST_MAC,
                                     findNodeByKey(DM_CHANGE_HOST_TYPE,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_host_changehosttype_type:
                node = findNodeByKey(DM_HOST_TYPE,
                                     findNodeByKey(DM_CHANGE_HOST_TYPE,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_wifi_autoconfig_op:
                node = findNodeByKey(DM_CTR_OP,
                                     findNodeByKey(DM_WIFI_AUTOCONFIG,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_timer_hour:
                node = findNodeByKey(DM_TIMER_HOURS,
                                     findNodeByKey(DM_TIMER,
                                                   findNodeByKey(DM_GUESTAP, ptreeNode)));
                break;
            case UUID_timer_minutes:
                node = findNodeByKey(DM_TIMER_MINUTES,
                                     findNodeByKey(DM_TIMER,
                                                   findNodeByKey(DM_GUESTAP, ptreeNode)));
                break;
            case UUID_l2_speedtestinfo_url:
                node = findNodeByKey(DM_SP_URL,
                                     findNodeByKey(DM_SPEEDTESTINFO,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_l2_speedtestinfo_action_type:
                node = findNodeByKey(DM_SP_ACTION_TYPE,
                                     findNodeByKey(DM_SPEEDTESTINFO,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
            case UUID_l2_speedtestinfo_dl_rate:
                node = findNodeByKey(DM_SP_DL_RATE,
                                     findNodeByKey(DM_SPEEDTESTINFO,
                                                   findNodeByKey(DM_ZYXELEXT,ptreeNode )));
                break;
            case UUID_l2_speedtestinfo_ul_rate:
                node = findNodeByKey(DM_SP_UL_RATE,
                                     findNodeByKey(DM_SPEEDTESTINFO,
                                                   findNodeByKey(DM_ZYXELEXT,ptreeNode )));
                break;
            case UUID_l2_speedtestinfo_end:
                node = findNodeByKey(DM_SP_END,
                                     findNodeByKey(DM_SPEEDTESTINFO,
                                                   findNodeByKey(DM_ZYXELEXT,ptreeNode )));
                break;
            case UUID_device_latest_software_version:
                node = findNodeByKey(DM_LATESTSOFTWARE,
                                     findNodeByKey(DM_SOFTWAREUPDATEOTA,
                                                   findNodeByKey(DM_DEVICEINFO, ptreeNode)));
                break;
            case UUID_device_latest_software_date:
                node = findNodeByKey(DM_LATESTSOFTWAREDATE,
                                     findNodeByKey(DM_SOFTWAREUPDATEOTA,
                                                   findNodeByKey(DM_DEVICEINFO, ptreeNode)));
                break;
            case UUID_g2c_rssi:
                node = findNodeByKey(DM_RSSI,
                                    findNodeByKey(DM_GATEWAY_RSSI,
                                                  findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_g2c_host_mac:
                node = findNodeByKey(DM_HOST_MAC,
                                     findNodeByKey(DM_GATEWAY_RSSI,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_wifi_autoconfigtime_mode:
                node = findNodeByKey(DM_DEVICEMODE,
                                     findNodeByKey(DM_WIFI_AUTOCONFIGBYTIME,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_wifi_autoconfigtime_op:
                node = findNodeByKey(DM_CTR_OP,
                                     findNodeByKey(DM_WIFI_AUTOCONFIGBYTIME,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_wifi_autoconfigtime_time:
                node = findNodeByKey(DM_TIME,
                                     findNodeByKey(DM_WIFI_AUTOCONFIGBYTIME,
                                                   findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
            case UUID_smart_device_power_plug_protocol:
                 node = findNodeByKey(DM_SMARTDEV_CTR_PROTOCOL,
                                     findNodeByKey(DM_SMARTDEV_CTR,
                                                    findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                 break;

            case UUID_smart_device_power_plug_id:
                 node = findNodeByKey(DM_SMARTDEV_CTR_ID,
                                     findNodeByKey(DM_SMARTDEV_CTR,
                                                    findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                 break;

            case UUID_smart_device_power_plug_action:
                 node = findNodeByKey(DM_SMARTDEV_CTR_ACTION,
                                     findNodeByKey(DM_SMARTDEV_CTR,
                                                     findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                 break;

            case UUID_wifi_auto_config_approve_mac:
                node = findNodeByKey(DM_AUTO_CFG_APPROVE_MAC,
                                    findNodeByKey(DM_AUTO_CFG_APPROVE_DATA,
                                                                 findNodeByKey(DM_ZYXELEXT,
                                                                                            ptreeNode)));
                break;
            case UUID_wifi_auto_config_approve_option:
                node = findNodeByKey(DM_OPTION,
                                    findNodeByKey(DM_AUTO_CFG_APPROVE_DATA,
                                                                 findNodeByKey(DM_ZYXELEXT,
                                                                                            ptreeNode)));
                 break;
            case UUID_wifi_auto_config_approve:
                node = findNodeByKey(DM_ENABLE,
                                    findNodeByKey(DM_AUTO_CFG_APPROVE,
                                                    findNodeByKey(DM_ZYXELEXT, ptreeNode)));
                break;
		}
        if (node == NULL) {
            cout<<"Can't find node by key(UUID:"<<UUID<<")"<<endl;
            throw ERR_CDATA_CANT_FIND_PARAMETER;
        }
		CJSON json(ptreeNode);
		string jstr = json.genJsonFromTreeWithPtr(node, value);
		json.creatTreeFromJSON(bufferNode, jstr);
        return true;
	}else{
        cout << "Error!Didn't select FormatType!(in SetParameterToBuffer funtion)" << endl;
        throw ERR_CDATA_CANT_FIND_DATATYPE;
    }
	
}
bool CDataMessage::SendBuffer(treeNode *head){
    try {
        if (FormatType == JSON) {
            if (head->children.empty()) {
                cout<<"Error!No buffer to send!"<<endl;
                throw ERR_CDATA_NO_BUFFER_TO_SEND;
                return false;
            }
            CJSON json = *new CJSON(new tr181Tree);
            string temp = json.genJsonFromTreeWithData(head);
            ClearBuffer(head);
            //cout<<temp;
            sendAndRecvReq(temp,msgType_ParameterSetReq);

            return true;
        }else{
            cout<<"Can't find format type!"<<endl;
            return false;
        }
        
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}

string CDataMessage::SendBufferAndRecvReq(treeNode *head, EN_MSG_TYPE type){
    string recv;
    try {
        if (FormatType == JSON) {
            if (head->children.empty()) {
                cout<<"Error!No buffer to send!"<<endl;
                throw ERR_CDATA_NO_BUFFER_TO_SEND;
                return "";
            }
            CJSON json = *new CJSON(new tr181Tree);
            string temp = json.genJsonFromTreeWithData(head);
            ClearBuffer(head);
            //cout<<temp;
            switch (type) {
                case SET_REQ:
                    recv = sendAndRecvReq(temp,msgType_ParameterSetReq);
                    break;
                case GET_REQ:
                    recv = sendAndRecvReq(temp,msgType_ParameterGetReq);
                    break;
                default:
                    break;
            }
            
            return recv;
        }else{
            cout<<"Can't find format type!"<<endl;
            return "";
        }
        
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return recv;
    }
}

bool CDataMessage::ClearBuffer(treeNode *head){
    releaseNodes(head);
    head->key = "";
    head->children.clear();
    return true;
}

bool isSupportForOneConnect(string modelName , string supportList){
    vector<string> supportKeyWordList = split(supportList.c_str(), "|");
    for(int i=0 ; i<supportKeyWordList.size() ; i++){
        if(modelName.find(supportKeyWordList.at(i)) != std::string::npos || supportKeyWordList.at(i)=="ALL")
            return true;
    }
    return false;
}
vector<string> split(const string &s, const string &seperator){
    vector<string> result;
    typedef string::size_type string_size;
    string_size i = 0;
    
    while(i != s.size()){
        //找到字符串中首个不等于分隔符的字母；
        int flag = 0;
        while(i != s.size() && flag == 0){
            flag = 1;
            for(string_size x = 0; x < seperator.size(); ++x)
                if(s[i] == seperator[x]){
                    ++i;
                    flag = 0;
                    break;
                }
        }
        
        //找到又一个分隔符，将两个分隔符之间的字符串取出；
        flag = 0;
        string_size j = i;
        while(j != s.size() && flag == 0){
            for(string_size x = 0; x < seperator.size(); ++x)
                if(s[j] == seperator[x]){
                    flag = 1;
                    break;
                }
            if(flag == 0)
                ++j;
        }
        if(i != j){
            result.push_back(s.substr(i, j-i));
            i = j;
        }
    }
    return result;
}