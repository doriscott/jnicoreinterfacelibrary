/****************************************************************************/
/*
* Copyright (C) 2000-2013 ZyXEL Communications, Corp.
* All Rights Reserved.
*
* ZyXEL Confidential;
* Protected as an unpublished work, treated as confidential,
* and hold in trust and in strict confidence by receiving party.
* Only the employees who need to know such ZyXEL confidential information
* to carry out the purpose granted under NDA are allowed to access.
*
* The computer program listings, specifications and documentation
* herein are the property of ZyXEL Communications, Corp. and shall
* not be reproduced, copied, disclosed, or used in whole or in part
* for any reason without the prior express written permission of
* ZyXEL Communications, Corp.
*
*/
/****************************************************************************/
//
//  CSecurity.cpp
//
//  Created by Kevin on 13/10/7.
//  Copyright (c) 2013年 pan star. All rights reserved.
//
#include "../Lib/rc4/RC4Lib.h"
#include "CSecurity.h"

//string CSecurity::Key="1234";

CSecurity::CSecurity(){

}
CSecurity::CSecurity(string pwd){
    Key = pwd;
}
bool CSecurity::SetKey(string pwd){
    Key = pwd;
    return true;
}

string CSecurity::GetKey(){
    return Key;
}

void CSecurity::SetEncryptMethod(EncryptType option){
    EncryptMethod = option;
}

EncryptType CSecurity::GetEncryptMethod(){
    return EncryptMethod;
}

int CSecurity::EncrypData(int msgType, char *data , int dataLen){
    
//    if (EncryptMethod == RC4) {
//        const char *keyData = Key.c_str();
//        int keyLen=(int)strlen(Key.c_str());
//        
//        int result = msgEncode(msgType, data , dataLen ,(char *)keyData ,keyLen);
//        return result;
//    }
//    return false;
    
    if (EncryptMethod == RC4) {
        const char *keyData = Key.c_str();
        int keyLen=(int)strlen(Key.c_str());
//        int dataLen=strlen(data+4);
        
        int result = msgEncode(msgType, data ,(int)dataLen ,(char *)keyData ,keyLen);
        return result;
    }
    return false;
     
}

int CSecurity::DecrypData(int msgType, char *data, int dataLen){
    
//    if (EncryptMethod == RC4) {
//        const char *keyData = Key.c_str();
//        int keyLen=(int)strlen(Key.c_str());
//        //printf("key:%s keylen:%d type:%d \n",keyData,keyLen,msgType);
//        //printf("%s\n",data);
//        int result = msgEncode(msgType, data , dataLen ,(char *)keyData ,keyLen);
//        //printf("%s\n",data);
//        return result;
//    }
//    return false;
    
    if (EncryptMethod == RC4) {
        const char *keyData = Key.c_str();
        int keyLen=(int)strlen(Key.c_str());
//        int dataLen=0;
//        memcpy(&dataLen, data+2, 2);
//        dataLen+=4;
        int result = msgDecode(&msgType, data ,(int)dataLen ,(char *)keyData ,keyLen);
        return result;
    }
    return false;
    
    
}
