/**
 *  @brief		It's define the data struct of variable.
 *  @author
 - Kevin Wu--v2.0.0--2015.8.20
 - Sharon Yang--v2.0.1--2015.9.3
 *  @copyright
 .
 * Copyright (C) 2000-2015 ZyXEL Communications, Corp.\n
 * All Rights Reserved.\n
 *
 * ZyXEL Confidential;\n
 * Protected as an unpublished work, treated as confidential,\n
 * and hold in trust and in strict confidence by receiving party.\n
 * Only the employees who need to know such ZyXEL confidential information\n
 * to carry out the purpose granted under NDA are allowed to access.\n
 *\n
 * The computer program listings, specifications and documentation\n
 * herein are the property of ZyXEL Communications, Corp. and shall\n
 * not be reproduced, copied, disclosed, or used in whole or in part\n
 * for any reason without the prior express written permission of\n
 * ZyXEL Communications, Corp.\n
 * @file		DataType_Define.h
 */

#ifndef _DataType_Define_h
#define _DataType_Define_h
#include <iostream>
#include <vector>
using namespace std;

enum EN_DEVICE_TYPE{
    EN_DEVICE_TYPE_NOT_SUPPORT = -1,
    EN_DEVICE_TYPE_ROUTER = 0,
    EN_DEVICE_TYPE_AP,
    EN_DEVICE_TYPE_REPEATER
};
enum EN_LINK_WAY{
    WIFI = 0,
    ENTHERNET,
    L2_DEVICE
};
enum EN_WIFICONFIG_FLAG{
    EN_WIFICONFIG_FLAG_NOT_CHANGE = 0,
    EN_WIFICONFIG_FLAG_CHANGE
};

typedef struct {
    EN_LINK_WAY Way;
    string SystemName;
	string ModelName;
    string DeviceName;
	string IP;
	string MAC;
    string SoftwareVersion;
    string SerialNumber;
	int Port;
    
    //Agent for ONE Connect 2.3
    EN_DEVICE_TYPE DeviceMode;
    EN_WIFICONFIG_FLAG WIFIConfigFlag;
    bool GatewayAutoConfigEnable;
}ST_DEVICE;

typedef struct {
    string AppVersion;
    int DeviceInfo;
    int GatewayInfo;
    int FlowInfo;
    int Login;
    int NetTopology;
    int DeviceCtrl;
    int L2DeviceCtrl;
    int DevIcon;
    int GuestWiFi;
    int WiFiAutoCfg;
    int WiFiOnOff;
    int SpeedTest;
    int Diagnostic;
    int CloudService;
    int FwUpgrade;
    int LEDOnOff;
    int GuestWiFiGroup;
}ST_AppFeatureInfo;

#endif
