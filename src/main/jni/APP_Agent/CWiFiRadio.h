/****************************************************************************/
/*
* Copyright (C) 2000-2013 ZyXEL Communications, Corp.
* All Rights Reserved.
*
* ZyXEL Confidential;
* Protected as an unpublished work, treated as confidential,
* and hold in trust and in strict confidence by receiving party.
* Only the employees who need to know such ZyXEL confidential information
* to carry out the purpose granted under NDA are allowed to access.
*
* The computer program listings, specifications and documentation
* herein are the property of ZyXEL Communications, Corp. and shall
* not be reproduced, copied, disclosed, or used in whole or in part
* for any reason without the prior express written permission of
* ZyXEL Communications, Corp.
*
*/
/****************************************************************************/
//
//  CWiFiRadio.h
//
//  Created by Kevin on 13/10/8.
//  Copyright (c) 2013年 pan star. All rights reserved.
//

#ifndef __CWiFiRadio__
#define __CWiFiRadio__

#include <iostream>
#include "CWiFi.h"
#include "Property.h"

enum EN_CHANNEL{
	Auto =0,
	Ch1,
	Ch2,
	Ch3,
	Ch4,
	Ch5,
	Ch6,
	Ch7,
	Ch8,
	Ch9,
	Ch10,
	Ch11,
	Ch12,
	Ch13
};

enum EN_OUTPOWER{
	NEAR =0,
	FAR =1
};

enum EN_BANDSTATUS{
    EN_BANDSTATUS_NotPresent = 0,
    EN_BANDSTATUS_Up = 1
};

typedef struct {
    EN_BANDSTATUS _24gStatus;
    EN_BANDSTATUS _5gStatus;
    int _channel24G;
    int _channel5G;
    int _totalnumRadio;
}ST_WiFiRadio;

class CWiFiRadio : public CWiFi{
public:
    Property<EN_CHANNEL, CWiFiRadio, READ_WRITE> Channel;
    Property<EN_OUTPOWER, CWiFiRadio, READ_WRITE> TXPower;
    Property<bool, CWiFiRadio, READ_WRITE> AutoChannleEnable;
    Property<int, CWiFiRadio, READ_ONLY> TotalRadioNumber;
    Property<string, CWiFiRadio, READ_ONLY> MaxBitRate;
    Property<string, CWiFiRadio, READ_ONLY> OperatingFrequencyBand;
    Property<string, CWiFiRadio, READ_ONLY> OperatingStandards;
    Property<string, CWiFiRadio, READ_ONLY> RadioStatus;
    Property<bool, CWiFiRadio, READ_ONLY> RadioEnable;
    
	CWiFiRadio();
	//~CWiFiRadio();
	
	EN_CHANNEL GetChannel();	//[OUT]nchennel
	bool SetChannel(EN_CHANNEL channel);	//[OUT]nchennel
    bool SetChannelToBuffer(EN_CHANNEL channel);	//[OUT]nchennel
    
	EN_OUTPOWER GetTXPower();	//[OUT]npowermode enum ST_WIFIRADIO_POWER
	bool SetTXPower(EN_OUTPOWER npowermode);	//[IN]npowermode
    bool SetTXPowerToBuffer(EN_OUTPOWER npowermode);	//[IN]npowermode
    
    bool GetAutoChannelEnable();
	bool SetAutoChannleEnable(bool);
	bool SetAutoChannleEnableToBuffer(bool);
    
    int GetTotalRadioNumber();
	string GetMaxBitRate();
	string GetOperatingFrequencyBand();
	string GetOperatingStandards();
	string GetRadioStatus();
	bool GetRadioEnable();

    ST_WiFiRadio GetWiFiRadioStatus();
    bool SendBuffer();
private:
	EN_CHANNEL _Channel;
    EN_OUTPOWER _TXPower;
    bool _AutoChannleEnable;
    int _TotalRadioNumber;
    string _MaxBitRate;
    string _OperatingFrequencyBand;
    string _OperatingStandards;
    string _RadioStatus;
    bool _RadioEnable;
    ST_WiFiRadio _WiFiRadioStatus;
    int _totalnumRadio;
};

#endif /* defined(__CWiFiRadio__) */
