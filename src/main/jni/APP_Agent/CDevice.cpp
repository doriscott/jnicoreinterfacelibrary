/****************************************************************************/
/*
 * Copyright (C) 2000-2013 ZyXEL Communications, Corp.
 * All Rights Reserved.
 *
 * ZyXEL Confidential;
 * Protected as an unpublished work, treated as confidential,
 * and hold in trust and in strict confidence by receiving party.
 * Only the employees who need to know such ZyXEL confidential information
 * to carry out the purpose granted under NDA are allowed to access.
 *
 * The computer program listings, specifications and documentation
 * herein are the property of ZyXEL Communications, Corp. and shall
 * not be reproduced, copied, disclosed, or used in whole or in part
 * for any reason without the prior express written permission of
 * ZyXEL Communications, Corp.
 *
 */
/****************************************************************************/
//
//  CDevice.cpp
//
//  Created by Kevin on 13/10/7.
//  Copyright (c) 2013年 pan star. All rights reserved.
//
#include <sstream>
#include "CSecurity.h"
#include "CDevice.h"
#include "MessageType.h"
#include "UID_Define.h"
#include "CJSON.h"
#include "CDataMessage.h"
#include "ErrorCode.h"
#include "CErrorHandle.h"
#include "CTemporary.h"
#include "DevicePool.h"
//#include <android/log.h>

#define  LOG_TAG    "CDevice"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

CDevice::CDevice(int id){
	GUID = id;
    
    CurrentSoftwareVersion.Set_Class(this);
    CurrentSoftwareVersion.Set_Get(&CDevice::GetCurrentVersion);
	AvailableVersion.Set_Class(this);
    AvailableVersion.Set_Get(&CDevice::GetAvailableVersion);
	UpdateStatus.Set_Class(this);
    UpdateStatus.Set_Get(&CDevice::GetUpdateStatus);
    ConfigPassword.Set_Property_Control(this, &CDevice::GetConfigPassword, &CDevice::SetConfigPassword, &CDevice::SetConfigPasswordToBuffer);
}
CDevice::CDevice(){
    CurrentSoftwareVersion.Set_Class(this);
    CurrentSoftwareVersion.Set_Get(&CDevice::GetCurrentVersion);
	AvailableVersion.Set_Class(this);
    AvailableVersion.Set_Get(&CDevice::GetAvailableVersion);
	UpdateStatus.Set_Class(this);
    UpdateStatus.Set_Get(&CDevice::GetUpdateStatus);
    ConfigPassword.Set_Property_Control(this, &CDevice::GetConfigPassword, &CDevice::SetConfigPassword, &CDevice::SetConfigPasswordToBuffer);
}
int CDevice::GetGUID(){
	return GUID;
}

string CDevice::GetCurrentVersion(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_device_software_version);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_device_software_version);
        _CurrentSoftwareVersion = temp;
        
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
	return _CurrentSoftwareVersion;
}

bool CDevice::CheckVersion(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_device_check_software_version, "0");
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_device_check_software_version);
        if(atoi(temp.c_str()) == 1) {
            return true;
        } else {
            return false;
        }
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
	
	//	string temp;
	//	temp = iDataMessage->CreatParameterToString(UUID_device_check_software_version);
	//
	//    string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
	//
	//	iDataMessage->ParserData(receive);
	//	temp = iDataMessage->GetParameter(UUID_device_check_software_version);
	//	if(atoi(temp.c_str()) == 1) {
	//		return "TRUE";
	////		AvailableVersion = GetAvailableVersion();
	////		return AvailableVersion;
	//	} else {
	////		AvailableVersion = "No New Firmware";
	////		return AvailableVersion;
	//		return "FALSE";
	//	}
}

string CDevice::GetAvailableVersion(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_device_latest_software_version);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        _AvailableVersion = DM.GetParameter(UUID_device_latest_software_version);
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
	return _AvailableVersion;
}

bool CDevice::UpdateSoftware(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        //	temp = iDataMessage->SetParameterToString("Device_UpdateSoftware", "enable");
        temp = DM.SetParameterToBuffer(UUID_device_update_software, "1", CTemporary::BUFF);
        //sendAndRecvReq(temp, msgType_ParameterSetReq);
        CTemporary::SendBuffer();
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}

string CDevice::GetUpdateStatus(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_device_update_software_status);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_device_update_software_status);
        _UpdateStatus = temp.c_str();
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
	return _UpdateStatus;
}

int CDevice::GetFWUpdateState(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_device_fw_upgrade_status);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_device_fw_upgrade_status);
        _FWUpgradeState = atoi(temp.c_str());
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _FWUpgradeState;
}

bool CDevice::GetHasAvailableVersion(){
    try {
        _HasAvailableVersion = false;
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_device_has_software_version);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_device_has_software_version);
        _HasAvailableVersion = atoi(temp.c_str());
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _HasAvailableVersion;
}

string CDevice::GetFWReleaseDate(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_device_latest_software_date);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_device_latest_software_date);
        _AvailableVersionDate = temp.c_str();
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _AvailableVersionDate;
}

ST_DeviceFWInfo CDevice::GetDeviceFWInfo(){
    try {
        string temp, receive;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        string option_str = "";
        temp = DM.CreatParameterToString(UUID_device_software_update_ota);
        
        receive = sendAndRecvReq(temp,msgType_ParameterGetReq);        
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_device_has_software_version);
        _DeviceFWInfo.HasAvailableVersion = atoi(temp.c_str());
        temp = DM.GetParameter(UUID_device_latest_software_version);
        _DeviceFWInfo.NewFWVersion = temp;
        temp = DM.GetParameter(UUID_device_latest_software_date);
        _DeviceFWInfo.NewFWVersionDate = temp;
        
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _DeviceFWInfo;
}

string CDevice::GetConfigPassword(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_device_password);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        _ConfigPassword = DM.GetParameter(UUID_device_password);
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
	
	return _ConfigPassword;
}

bool CDevice::SetConfigPassword(string value){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_device_password, value);
        string receive = sendAndRecvReq(temp,msgType_ParameterSetReq);
        _ConfigPassword = value;
        if(receive != "") {
            //CSecurity::SetKey(value);
            CTemporary::SetKey(value);
        }
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}
bool CDevice::SetConfigPasswordToBuffer(string value){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.SetParameterToBuffer(UUID_device_password, value, CTemporary::BUFF);
        _ConfigPassword = value;
//        if(receive != "") {
//            //CSecurity::SetKey(value);
//            CTemporary::SetKey(value);
//        }
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
    
}
ST_DeviceInfo CDevice::GetDeviceInfo(){
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)) {
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_device_deviceinfo);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_deviceinfo_processstatus_cpuusage);
        _DeviceInfo.CPU_Usage = atoi(temp.c_str());
        temp = DM.GetParameter(UUID_deviceinfo_uptime);
        _DeviceInfo.UpTime = atoi(temp.c_str());
        _DeviceInfo.SystemTime = DM.GetParameter(UUID_deviceinfo_systemtime);
        
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _DeviceInfo;
}
ST_WanInfo CDevice::GetWanInfo(){
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_device_waninfo);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        _WanInfo.WanIP = DM.GetParameter(UUID_waninfo_ip);
        _WanInfo.WanMAC = DM.GetParameter(UUID_waninfo_mac);
        _WanInfo.WanStatus = DM.GetParameter(UUID_waninfo_status);
        temp = DM.GetParameter(UUID_waninfo_phyrate);
        _WanInfo.WanPhyRate = atoi(temp.c_str());
        temp = DM.GetParameter(UUID_waninfo_tx);
        _WanInfo.WanTX = atoi(temp.c_str());
        temp = DM.GetParameter(UUID_waninfo_rx);
        _WanInfo.WanRX = atoi(temp.c_str());
        
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _WanInfo;
}
vector<ST_RouteHop> CDevice::TraceRouter(){
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        string state = "Requested";
        string Host = "8.8.8.8";
        int NumberOfTries = 1;
        int Timeout = 10;
        int NumberOfRouteHops = 0;
//        int MaxHopCount = 0;
        
        string temp,receive;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        DM.SetParameterToBuffer(UUID_tracerouter_host, Host, CTemporary::BUFF);
        DM.SetParameterToBuffer(UUID_tracerouter_state, state, CTemporary::BUFF);
        std::stringstream ss;
        ss << NumberOfTries;
        DM.SetParameterToBuffer(UUID_tracerouter_numberoftries, ss.str(), CTemporary::BUFF);
        ss.clear();
        ss.str("");
        ss << Timeout;
        DM.SetParameterToBuffer(UUID_tracerouter_timeout, ss.str(), CTemporary::BUFF);
        ss.clear();
        ss.str("");
        ss << NumberOfRouteHops;
        DM.SetParameterToBuffer(UUID_tracerouter_numberofroutehops, ss.str(), CTemporary::BUFF);
        
//        ss.clear();
//        ss.str("");
//        ss << MaxHopCount;
//        DM.SetParameterToBuffer(UUID_tracerouter_maxhopcount, ss.str(), CTemporary::BUFF);
        
        
        receive = CTemporary::SendBufferAndRecvReq();
        DM.ParserData(receive);
//        temp = DM.CreatParameterToString(UUID_tracerouter_numberofroutehops);
//        receive = sendAndRecvReq(temp,UUID_tracerouter_numberofroutehops);
        
        temp = DM.GetParameter(UUID_tracerouter_numberofroutehops);
        int numOfRouteHops = (int)atoi(temp.c_str());
        
        for (int i = 1; i <= numOfRouteHops; i++) {
            temp = DM.CreatParameterToString(UUID_tracerouter_routehops_all, GUID_NONE,i);
            receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
            DM.ParserData(receive);
        }
        _RouterHops.clear();
        ST_RouteHop hop;
        for (int i = 1; i <= numOfRouteHops; i++) {
            hop.Host = DM.GetParameter(UUID_tracerouter_routehops_host, GUID_NONE, i);
            hop.HostAddress = DM.GetParameter(UUID_tracerouter_routehops_hostaddress, GUID_NONE, i);
            hop.RTTimes = DM.GetParameter(UUID_tracerouter_routehops_rtttime, GUID_NONE, i);
            _RouterHops.push_back(hop);
        }
        

    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _RouterHops;
}
vector<ST_IPV4Forwarding> CDevice::GetIPV4Forwarding(){
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp;
        string receive;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_route_numberofipv4forwarding);
        receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_route_numberofipv4forwarding);
        int numOfIPV4 = (int)atoi(temp.c_str());
        
        for (int i = 1; i <= numOfIPV4; i++) {
            temp = DM.CreatParameterToString(UUID_route_ipv4forwarding_all, GUID_NONE,i);
            receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
            DM.ParserData(receive);
        }
        _IPV4Forwardings.clear();
        ST_IPV4Forwarding forwarding;
        for (int i = 1; i <= numOfIPV4; i++) {
            forwarding.DestIPAddress = DM.GetParameter(UUID_route_destipaddress, GUID_NONE, i);
            forwarding.DestSubnetMask = DM.GetParameter(UUID_route_destsubnetmask, GUID_NONE, i);
            forwarding.GatewayIPAddress = DM.GetParameter(UUID_route_gatewayipaddress, GUID_NONE, i);
            forwarding.Interface = DM.GetParameter(UUID_route_interface, GUID_NONE, i);
            _IPV4Forwardings.push_back(forwarding);
        }
        
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _IPV4Forwardings;
}
bool CDevice::SetWiFiAutoConfig(bool option){
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp;
        string option_str;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        if (option) {
            option_str = "7";
        }else{
            option_str = "0";
        }
        temp = DM.CreatParameterToString(UUID_wifi_autoconfig_op, option_str);
        string receive = sendAndRecvReq(temp,msgType_ParameterSetReq);
        _WiFiAutoConfig = option;
        
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
    return _WiFiAutoConfig;
}
bool CDevice::SetWiFiAutoConfigToBuffer(bool option){
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp;
        string option_str;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        if (option) {
            option_str = "7";
        }else{
            option_str = "0";
        }
        DM.SetParameterToBuffer(UUID_wifi_autoconfig_op, option_str, CTemporary::BUFF);
        
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
    return true;
}
bool CDevice::GetWiFiAutoConfig(){
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_wifi_autoconfig_op);
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_wifi_autoconfig_op);
        
        int result = (bool)atoi(temp.c_str());
        if (result>0) {
            _WiFiAutoConfig = true;
        }else{
            _WiFiAutoConfig = false;
        }
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _WiFiAutoConfig;
}

bool CDevice::SetWiFiConfigFlag(bool option){
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp;
        string option_str;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        if (option) {
            option_str = "1";
        }else{
            option_str = "0";
        }
        temp = DM.CreatParameterToString(UUID_wifi_wificonfigflag_op, option_str);
        string receive = sendAndRecvReq(temp,msgType_ParameterSetReq);
        _WiFiAutoConfig = option;
        
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
    return _WiFiAutoConfig;
}
bool CDevice::SetAPWiFiAutoConfig(int sec){
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        string option_str;
        string mode_str;
        string sec_str;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        mode_str = "0";     //Register:0 Enrollee:1
        option_str = "7";
        
        stringstream ss(sec_str);
        ss << sec;
        sec_str = ss.str();
        
        DM.SetParameterToBuffer(UUID_wifi_autoconfigtime_mode, mode_str, CTemporary::BUFF);
        DM.SetParameterToBuffer(UUID_wifi_autoconfigtime_op, option_str, CTemporary::BUFF);
        DM.SetParameterToBuffer(UUID_wifi_autoconfigtime_time, sec_str, CTemporary::BUFF);
        string receive = CTemporary::SendBufferAndRecvReq();
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
    return true;
}
bool CDevice::SetL2DevCtrlAutoConfig(string HostMAC, bool option){
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp;
        string option_str;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        if (option) {
            option_str = "7";
        }else{
            option_str = "0";
        }
        DM.SetParameterToBuffer(UUID_l2ctrl_autoconfig_op, option_str, CTemporary::BUFF);
        DM.SetParameterToBuffer(UUID_l2ctrl_autoconfig_host, HostMAC, CTemporary::BUFF);
        
        string receive = CTemporary::SendBufferAndRecvReq();

        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}

bool CDevice::SetL2DevCtrlAutoConfigToBuffer(string HostMAC, bool option){
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp;
        string option_str;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        if (option) {
            option_str = "7";
        }else{
            option_str = "0";
        }
        DM.SetParameterToBuffer(UUID_l2ctrl_autoconfig_op, option_str, CTemporary::BUFF);
        DM.SetParameterToBuffer(UUID_l2ctrl_autoconfig_host, HostMAC, CTemporary::BUFF);
        
        //string receive = CTemporary::SendBufferAndRecvReq();
        
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}

bool CDevice::SetL2DevCtrlFWConfig(string HostMAC, bool option){
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp;
        string option_str;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        if (option) {
            option_str = "7";   //upgrade
        }else{
            option_str = "1";   //check
        }
        DM.SetParameterToBuffer(UUID_l2ctrl_fwcfg_host_mac, HostMAC, CTemporary::BUFF);
        DM.SetParameterToBuffer(UUID_l2ctrl_fwcfg_op, option_str, CTemporary::BUFF);
        
        string receive = CTemporary::SendBufferAndRecvReq();
        
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}

bool CDevice::SetL2DevCtrlFWConfigToBuffer(string HostMAC, bool option){
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp;
        string option_str;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        if (option) {
            option_str = "7";   //upgrade
        }else{
            option_str = "1";   //check
        }
        DM.SetParameterToBuffer(UUID_l2ctrl_fwcfg_host_mac, HostMAC, CTemporary::BUFF);
        DM.SetParameterToBuffer(UUID_l2ctrl_fwcfg_op, option_str, CTemporary::BUFF);
        
        //string receive = CTemporary::SendBufferAndRecvReq();
        
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}

int CDevice::GetL2DevCtrlFWState(string HostMAC){
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp, receive;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        string option_str = "0";
        DM.SetParameterToBuffer(UUID_l2ctrl_fwcfg_host_mac, HostMAC, CTemporary::BUFF);
        DM.SetParameterToBuffer(UUID_l2ctrl_fwcfg_state, option_str,  CTemporary::BUFF);
        receive = DM.SendBufferAndRecvReq(CTemporary::BUFF, GET_REQ);
        
        DM.ParserData(receive);
        
        temp = DM.GetParameter(UUID_l2ctrl_fwcfg_state);
        if(temp == "") {
            _FWUpgradeState = 0;
        }
        else _FWUpgradeState = atoi(temp.c_str());
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _FWUpgradeState;
}

bool CDevice::CheckL2DevCtrlFWUpgradeSuccess(string HostMAC){
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp, receive;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        string option_str = "0";
        DM.SetParameterToBuffer(UUID_l2ctrl_fwcfg_host_mac, HostMAC, CTemporary::BUFF);
        DM.SetParameterToBuffer(UUID_l2ctrl_fwcfg_Success, option_str,  CTemporary::BUFF);
        receive = DM.SendBufferAndRecvReq(CTemporary::BUFF, GET_REQ);
        
        DM.ParserData(receive);
        
        temp = DM.GetParameter(UUID_l2ctrl_fwcfg_Success);
        if(temp == "") {
            _FWUpgradeState = 0;
        }
        else _FWUpgradeState = atoi(temp.c_str());
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _FWUpgradeState;
}

bool CDevice::SetSpeedTest(string URL, bool option){
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp, receive;
        string option_str;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        if (option) {
            option_str = "Upload";   //1:Upload
        }else{
            option_str = "Download";   //0:Download
        }
//        DM.SetParameterToBuffer(UUID_l2_speedtestinfo_url, URL, CTemporary::BUFF);
//        DM.SetParameterToBuffer(UUID_l2_speedtestinfo_action_type, option_str, CTemporary::BUFF);
//        string receive = CTemporary::SendBufferAndRecvReq();
        
        temp = DM.CreatParameterToString(UUID_l2_speedtestinfo_url, URL);
        receive = sendAndRecvReq(temp, msgType_ParameterSetReq);
        temp = DM.CreatParameterToString(UUID_l2_speedtestinfo_action_type, option_str);
        receive = sendAndRecvReq(temp, msgType_ParameterSetReq);
        
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}

string CDevice::GetSpeedTestDownload(){
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp, receive;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);

        string option_str = "0";
        temp = DM.SetParameterToBuffer(UUID_l2_speedtestinfo_end, option_str ,  CTemporary::BUFF);
        option_str = "";
        DM.SetParameterToBuffer(UUID_l2_speedtestinfo_dl_rate, option_str,  CTemporary::BUFF);
        receive = DM.SendBufferAndRecvReq(CTemporary::BUFF, SET_REQ);
        
        DM.ParserData(receive);
        
        temp = DM.GetParameter(UUID_l2_speedtestinfo_dl_rate);
        if(temp == "") {
            _SpeedTestInfo.Dl_Rate = "0";
        }
        else _SpeedTestInfo.Dl_Rate = temp;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _SpeedTestInfo.Dl_Rate;
}
string CDevice::GetSpeedTestUpload(){
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp, receive;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        
        string option_str = "0";
        temp = DM.SetParameterToBuffer(UUID_l2_speedtestinfo_end, option_str ,  CTemporary::BUFF);
        option_str = "";
        DM.SetParameterToBuffer(UUID_l2_speedtestinfo_ul_rate, option_str,  CTemporary::BUFF);
        receive = DM.SendBufferAndRecvReq(CTemporary::BUFF, SET_REQ);
        
        DM.ParserData(receive);
        
        temp = DM.GetParameter(UUID_l2_speedtestinfo_ul_rate);
        if(temp == "") {
            _SpeedTestInfo.Ul_Rate = "0";
        }
        else _SpeedTestInfo.Ul_Rate = temp;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _SpeedTestInfo.Ul_Rate;
}
bool CDevice::GetSpeedTestEnd(){
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_l2_speedtestinfo_end);
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        DM.ParserData(receive);
        
        temp = DM.GetParameter(UUID_l2_speedtestinfo_end);
        if(atoi(temp.c_str()) == 1) {
            return true;
        } else {
            return false;
        }
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _SpeedTestInfo.End;
}

bool CDevice::Reboot(){
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_device_reboot,"1");
        
        string receive = sendAndRecvReq(temp, msgType_ParameterSetReq);
        
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}

string CDevice::getGatewayLANIP(){
    ST_DEVICE device = DevicePool::getCurrentDevice();
    return device.IP;
}

int CDevice::GetWifiDescription(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_gateway_description);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_gateway_description);
        _GatewayDescription = atoi(temp.c_str());
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _GatewayDescription;
}

int CDevice::GetGwSpeedTestPort() {
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_c2g_speedtestinfo_port);
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        DM.ParserData(receive);
        
        temp = DM.GetParameter(UUID_c2g_speedtestinfo_port);
        return (int)atoi(temp.c_str());
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return -1;
}

bool CDevice::SetGwSpeedTestEnable(bool option) {
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp, receive;
        string option_str;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        if (option) {
            option_str = "1";
        }else{
            option_str = "0";
        }
        temp = DM.CreatParameterToString(UUID_c2g_speedtestinfo_enable, option_str);
        receive = sendAndRecvReq(temp, msgType_ParameterSetReq);
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}

int CDevice::GetGateway2ClientRssi(string mobileMAC) {
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp, receive;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        string option_str = "0";
        DM.SetParameterToBuffer(UUID_g2c_host_mac, mobileMAC, CTemporary::BUFF);
        DM.SetParameterToBuffer(UUID_g2c_rssi, option_str,  CTemporary::BUFF);
        receive = DM.SendBufferAndRecvReq(CTemporary::BUFF, GET_REQ);

        DM.ParserData(receive);

        temp = DM.GetParameter(UUID_g2c_rssi);

        if(temp == "") {
            return 0;
        }
        else {
            return (int)atoi(temp.c_str());
        }
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return 0;
    }
}

string CDevice::GetGatewayHostName() {
try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_gateway_info_hostname);

        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);

        DM.ParserData(receive);
        _GatewayHostName = DM.GetParameter(UUID_gateway_info_hostname);
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }

	return _GatewayHostName;
}

string CDevice::GetGatewayHostMAC() {
try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_gateway_info_hostmac);

        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);

        DM.ParserData(receive);
        _GatewayHostMAC = DM.GetParameter(UUID_gateway_info_hostmac);
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _GatewayHostMAC;
}

int CDevice::GetGatewayAutoConfig() {
try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_gateway_info_autocfg);

        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);

        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_gateway_info_autocfg);
        _GatewayAutoConfig = atoi(temp.c_str());
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _GatewayAutoConfig;
}

ST_GatewayHostInfo CDevice::GetGatewayHostInfo() {
 try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)) {
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_gateway_gatewayinfo);

        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);

        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_gateway_info_hostname);
        _GatewayHostInfo.HostName = temp.c_str();
        temp = DM.GetParameter(UUID_gateway_info_hostmac);
        _GatewayHostInfo.HostMAC = temp.c_str();
        temp = DM.GetParameter(UUID_gateway_info_autocfg);
        _GatewayHostInfo.AutoConfig = atoi(temp.c_str());

    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _GatewayHostInfo;
}

bool CDevice::SetSmartDevCtrlPower(string protocol, string ID, bool option) {
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp;
        string option_str;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        if (option) {
            option_str = "on";
        }else{
            option_str = "off";
        }
        DM.SetParameterToBuffer(UUID_smart_device_power_plug_protocol, protocol, CTemporary::BUFF);
        DM.SetParameterToBuffer(UUID_smart_device_power_plug_id, ID, CTemporary::BUFF);
        DM.SetParameterToBuffer(UUID_smart_device_power_plug_action, option_str, CTemporary::BUFF);

        string receive = CTemporary::SendBufferAndRecvReq();

        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}

bool CDevice::SetSmartDevCtrlPowerToBuffer(string protocol, string ID, bool option) {
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp;
        string option_str;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        if (option) {
            option_str = "on";
        }else{
            option_str = "off";
        }
        DM.SetParameterToBuffer(UUID_smart_device_power_plug_protocol, protocol, CTemporary::BUFF);
        DM.SetParameterToBuffer(UUID_smart_device_power_plug_id, ID, CTemporary::BUFF);
        DM.SetParameterToBuffer(UUID_smart_device_power_plug_action, option_str, CTemporary::BUFF);

        //string receive = CTemporary::SendBufferAndRecvReq();

        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}

bool CDevice::SetSmartDevCtrlDaemon(string protocol){
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp;
        string option_str = "reload";
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        DM.SetParameterToBuffer(UUID_smart_device_power_plug_protocol, protocol, CTemporary::BUFF);
        DM.SetParameterToBuffer(UUID_smart_device_power_plug_action, option_str, CTemporary::BUFF);

        //string receive = CTemporary::SendBufferAndRecvReq();

        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}

bool CDevice::SetWiFiAutoConfigApproveProfileList(vector<ST_WiFiAutoConfigApproveInfo> profile) {
    int index = 1;
    treeNode *nodeTT = findNodeByKey(DM_AUTO_CFG_APPROVE_DATA,CTemporary::HEAD);
    nodeTT->addChild(oneConnectTree::CreateWiFiAutoConfigApproveDataWithIndex(index));
    treeNode *nodeT = findNodeByKey(DM_AUTO_CFG_APPROVE_DATA,CTemporary::HEAD);

	try {
        for(int i = 0; i < profile.size(); i++) {
            ST_WiFiAutoConfigApproveInfo tmp = profile.at(i);
            index = i + 1;
            CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
		    DM.SetParameterToBuffer(UUID_wifi_auto_config_approve_mac, tmp.HostMAC.c_str(),  CTemporary::BUFF);
            string option_str;
            if (tmp.AutoConfig) {
                option_str = "1";
            }else{
                option_str = "0";
            }
		    DM.SetParameterToBuffer(UUID_wifi_auto_config_approve_option, option_str, CTemporary::BUFF);
		    CTemporary::SendBuffer();
		}
	} catch (int errorCode) {
		CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
		errorHandle->handleErrorOccur(errorCode);
	}
    return true;
}

bool CDevice::SetWiFiAutoConfigApprove(bool option) {
try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp;
        string option_str;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        if (option) {
            option_str = "1";
        }else{
            option_str = "0";
        }
        //DM.SetParameterToBuffer(UUID_wifi_auto_config_approve, option_str, CTemporary::BUFF);
        temp = DM.CreatParameterToString(UUID_wifi_auto_config_approve, option_str);
        string receive = sendAndRecvReq(temp, msgType_ParameterSetReq);
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}

bool CDevice::GetWiFiAutoConfigApprove() {
 try {
         string temp;
         CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
         temp = DM.CreatParameterToString(UUID_wifi_auto_config_approve);

         string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);

         DM.ParserData(receive);
         temp = DM.GetParameter(UUID_wifi_auto_config_approve);
         _WiFiAutoConfigApproveStatus = atoi(temp.c_str());
     } catch (int errorCode) {
         CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
         errorHandle->handleErrorOccur(errorCode);
     }

 	return _WiFiAutoConfigApproveStatus;
}

bool CDevice::SendBuffer(){
    try {
        CTemporary::SendBuffer();
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return true;
}
