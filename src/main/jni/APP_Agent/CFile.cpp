//
//  CFile.cpp
//  ConnectAPP
//
//  Created by Kevin on 2014/2/19.
//  Copyright (c) 2014年 ZyXEL. All rights reserved.
//

#include "CFile.h"

CFile::CFile(){
    
}
CFile::~CFile(){
    
}

bool CFile::setPath(string path){
    filePath = path;
    return true;
}
string CFile::getPath(){
    return filePath;
}

bool CFile::saveNetworkDevices(vector<ST_EndDevice> currentDeviceList){
    
    vector<ST_EndDevice> oldDeviceList;
    
    ST_EndDevice device;
    fstream fio(filePath+CLIENT_LIST_FILE_NAME);
    if(!fio) {
        cerr <<"Can't load the file!" << endl;
        return false;
    }
    
    while(1) {
        fio.read(reinterpret_cast<char *> (&device),
                 sizeof(ST_EndDevice));
        if(!fio.eof()){
            //count++;
            oldDeviceList.push_back(device);
        }else
            break;
    }
    
    fio.clear();
    fio.close();
    
//    cout << "---------file device-------" <<endl;
//    for (int i=0; i < oldDeviceList.size(); i++) {
//        cout << oldDeviceList.at(i).MAC << "---"
//             << oldDeviceList.at(i).IPAddress << "----"
//             << oldDeviceList.at(i).Name << "---"
//             << oldDeviceList.at(i).ConnectInterface << "---"
//             << oldDeviceList.at(i).Block<< "---"
//             << oldDeviceList.at(i).OnlineStatus << endl;
//    }
//    cout << "---------file device-------" <<endl;
    
    for (int i = 0 ; i < oldDeviceList.size() ; i++) {
        bool find = false;
        for (int j = 0 ; j < currentDeviceList.size() ; j++) {
            if (oldDeviceList.at(i).MAC == currentDeviceList.at(j).MAC) {
                find = true;
            }
        }
        if (!find) {
            currentDeviceList.push_back(oldDeviceList.at(i));
        }
    }
    cout << "---------Synchronous devices and save-------" <<endl;
    for (int i=0; i < currentDeviceList.size(); i++) {
        cout << currentDeviceList.at(i).MAC << "---"
        << currentDeviceList.at(i).IPAddress << "----"
        << currentDeviceList.at(i).Name << "---"
        << currentDeviceList.at(i).ConnectInterface << "---"
        << currentDeviceList.at(i).Block<< "---"
        << currentDeviceList.at(i).OnlineStatus << endl;
    }
    cout << "--------------------------------------------" <<endl;
    
    ofstream fout(filePath+CLIENT_LIST_FILE_NAME);
    
    if(!fout) {
        cout << "Can't load the file!" << endl;
        return false;
    }

    
    for (int i=0; i < currentDeviceList.size(); i++) {
//        fout << deviceList.at(i).MAC << "\t" << deviceList.at(i).IPAddress << "\t" << deviceList.at(i).Name << "\t" << deviceList.at(i).ConnectInterface << "\t"
//             << deviceList.at(i).Block<< "\t" << deviceList.at(i).OnlineStatus << endl;
        fout.write(reinterpret_cast<const char*> (&currentDeviceList.at(i)),sizeof(ST_EndDevice));
    }
    fout.close();
    
    return true;
}
vector<ST_EndDevice> CFile::loadNetworkDevices(){
    vector<ST_EndDevice> oldDeviceList;

    ST_EndDevice device;
    fstream fio(filePath+CLIENT_LIST_FILE_NAME);
    if(!fio) {
        cerr <<"Can't load the file!" << endl;
        cerr <<"File not exist! creat file!" << endl;
        //return oldDeviceList;
        ofstream fout(filePath+CLIENT_LIST_FILE_NAME);
        fout.close();
    }else{
        while(1) {
            fio.read(reinterpret_cast<char *> (&device),
                     sizeof(ST_EndDevice));
            if(!fio.eof()){
                //count++;
                oldDeviceList.push_back(device);
            }else
                break;
        }
        
        fio.clear();
        fio.close();
        
        cout << "---------devices load from file-------" <<endl;
        for (int i=0; i < oldDeviceList.size(); i++) {
            cout << oldDeviceList.at(i).MAC << "---"
            << oldDeviceList.at(i).IPAddress << "----"
            << oldDeviceList.at(i).Name << "---"
            << oldDeviceList.at(i).ConnectInterface << "---"
            << oldDeviceList.at(i).Block<< "---"
            << oldDeviceList.at(i).OnlineStatus << endl;
        }
        cout << "--------------------------------------" <<endl;
    }
    return oldDeviceList;
}
vector<ST_EndDevice> CFile::loadNetworkDevices(vector<ST_EndDevice> currentDeviceList){
    vector<ST_EndDevice> oldDeviceList;
    
    ST_EndDevice device;
    fstream fio(filePath+CLIENT_LIST_FILE_NAME);
    if(!fio) {
        cerr <<"Can't load the file!" << endl;
        cerr <<"File not exist! creat file!" << endl;
        //return oldDeviceList;
        ofstream fout(filePath+CLIENT_LIST_FILE_NAME);
        fout.close();
    }else{
        while(1) {
            fio.read(reinterpret_cast<char *> (&device),
                     sizeof(ST_EndDevice));
            if(!fio.eof()){
                //count++;
                oldDeviceList.push_back(device);
            }else
                break;
        }
        
        fio.clear();
        fio.close();
        
        cout << "---------devices load from file-------" <<endl;
        for (int i=0; i < oldDeviceList.size(); i++) {
            cout << oldDeviceList.at(i).MAC << "---"
            << oldDeviceList.at(i).IPAddress << "----"
            << oldDeviceList.at(i).Name << "---"
            << oldDeviceList.at(i).ConnectInterface << "---"
            << oldDeviceList.at(i).Block<< "---"
            << oldDeviceList.at(i).OnlineStatus << endl;
        }
        cout << "---------------------------------------" <<endl;
    }

//    currentDeviceList.clear();
//    for (int i = 0 ; i < deviceList.size() ; i++){
//        currentDeviceList.push_back(deviceList.at(i));
//    }
    for (int i = 0 ; i < oldDeviceList.size() ; i++) {
        bool find = false;
        for (int j = 0 ; j < currentDeviceList.size() ; j++) {
            if (oldDeviceList.at(i).MAC == currentDeviceList.at(j).MAC) {
                currentDeviceList.at(j).UserDefineName = oldDeviceList.at(i).UserDefineName;
                find = true;
            }
        }
        if (!find) {
            oldDeviceList.at(i).OnlineStatus = false;
            currentDeviceList.push_back(oldDeviceList.at(i));
        }
    }
    
    cout << "---------devices synchronous with file-------" <<endl;
    for (int i=0; i < currentDeviceList.size(); i++) {
        cout << currentDeviceList.at(i).MAC << "---"
        << currentDeviceList.at(i).IPAddress << "----"
        << currentDeviceList.at(i).Name << "---"
        << currentDeviceList.at(i).ConnectInterface << "---"
        << currentDeviceList.at(i).Block<< "---"
        << currentDeviceList.at(i).OnlineStatus << endl;
    }
    cout << "--------------------------------------------" <<endl;
    
    return currentDeviceList;
}