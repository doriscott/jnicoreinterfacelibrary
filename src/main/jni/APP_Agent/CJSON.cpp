/****************************************************************************/
/*
* Copyright (C) 2000-2013 ZyXEL Communications, Corp.
* All Rights Reserved.
*
* ZyXEL Confidential;
* Protected as an unpublished work, treated as confidential,
* and hold in trust and in strict confidence by receiving party.
* Only the employees who need to know such ZyXEL confidential information
* to carry out the purpose granted under NDA are allowed to access.
*
* The computer program listings, specifications and documentation
* herein are the property of ZyXEL Communications, Corp. and shall
* not be reproduced, copied, disclosed, or used in whole or in part
* for any reason without the prior express written permission of
* ZyXEL Communications, Corp.
*
*/
/****************************************************************************/
//
//  CJSON.cpp
//
//  Created by Kevin on 13/10/8.
//  Copyright (c) 2013年 pan star. All rights reserved.
//

#include "CJSON.h"
#include "DevicePool.h"
#include "CDevice.h"
#include "CErrorHandle.h"
#include "CDataMessage.h"
//treeNode *CJSON::HEAD = new tr181Tree;
//treeNode *CJSON::BUFF = new treeNode;
#include <android/log.h>
#define  LOG_TAG    "CDataMessage"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

CJSON::CJSON(treeNode *tree) {
	_tree = tree;
}
bool CJSON::parseJsonToTree(string tmp,treeNode *node){
	//cout<<node->key<<endl;
	Json::Reader reader;
	Json::Value json_object;
    string test(tmp);
	if (!reader.parse(test, json_object)){
		std::cout<< "Parse jsonstr error!"<<endl;
        std::cout  << "Failed to parse configuration\n"<< reader.getFormatedErrorMessages();
		return false;
	}
	if(json_object.empty()|| !json_object.isObject()){
		//std::cout<<node->key + ":The jsonstr is empty or not object!"<<endl;
		return false;
	}
	else{
		Json::Value::Members member = json_object.getMemberNames();

		for(int i=0; i<member.size();i++)
		{
			//cout <<"-------"<< member.at(i).c_str() << endl;
			treeNode *temp = findNodeByKey((char *)member.at(i).c_str(), node);

			if (temp) {
				temp->data = json_object[member.at(i)];
				//cout<<temp->data.toStyledString();
				//-----------------------------------------Auto add children for Host
				if (temp->key == DM_HOSTNUM) {
				LOGI("create host with index");
					treeNode *nodeT = findNodeByKey(DM_HOST,findNodeByKey(DM_HOSTS, _tree));
                    ST_DEVICE device = DevicePool::getCurrentDevice();
					if (temp->data.asInt() != nodeT->children.size()) {
						nodeT->children.clear();
						for (int i = 0; i < temp->data.asInt() ; i++) {
                            if (IS_SUPPORT_ONE_CONNECT(device.ModelName)){
                                nodeT->addChild(oneConnectTree::CreatHostWithIndex(i+1));
                            }else{
                                nodeT->addChild(tr181Tree::CreatHostWithIndex(i+1));
                            }
						}
					}
				}

				if(temp->key == DM_RADIONUM) {
					treeNode *nodeT = findNodeByKey(DM_RADIO,findNodeByKey(DM_WIFI, _tree));
                    ST_DEVICE device = DevicePool::getCurrentDevice();
					if(temp->data.asInt() != nodeT->children.size()) {
						nodeT->children.clear();
						for(int i = 0; i < temp->data.asInt(); i++) {
                            if (IS_SUPPORT_ONE_CONNECT(device.ModelName)){
                                nodeT->addChild(oneConnectTree::CreatRadioWithIndex(i+1));
                            }else{
                                nodeT->addChild(tr181Tree::CreatRadioWithIndex(i+1));
                            }
						}
					}
				}

				if(temp->key == DM_PARENTALNUM) {
					//LOGI("Create Parental Control Profile Number!!!!!!!!!!!!!!!!!!!");
					treeNode *nodeT = findNodeByKey(DM_PARENTALCONTROL,findNodeByKey(DM_ZYXELEXT, _tree));
                    ST_DEVICE device = DevicePool::getCurrentDevice();
					if(temp->data.asInt() != nodeT->children.size()) {
						nodeT->children.clear();
						for(int i = 0; i < temp->data.asInt(); i++) {
                            if (IS_SUPPORT_ONE_CONNECT(device.ModelName)){
                                nodeT->addChild(oneConnectTree::CreatParentalControlWithIndex(i+1));
                            }else{
                                nodeT->addChild(tr181Tree::CreatParentalControlWithIndex(i+1));
                            }
						}
					}
				}
                //for one connect 2014.120
                if(temp->key == DM_NUMOFIPV4) {
            
                    treeNode *nodeT = findNodeByKey(DM_IPV4_FORWARDING,findNodeByKey(DM_ROUTER,findNodeByKey(DM_ZYXELEXT, _tree)));
                    ST_DEVICE device = DevicePool::getCurrentDevice();
                    if(temp->data.asInt() != nodeT->children.size()) {
                        nodeT->children.clear();
                        for(int i = 0; i < temp->data.asInt(); i++) {
                                nodeT->addChild(oneConnectTree::CreatIPv4ForwardingWithIndex(i+1));
                        }
                    }
                }
                if(temp->key == DM_NUMOFROUTERHOPS) {
                    
                    treeNode *nodeT = findNodeByKey(DM_ROUTERHOPS,findNodeByKey(DM_TRACEROUTER,findNodeByKey(DM_DIAGNOSTICS,findNodeByKey(DM_IP, _tree))));
                    //treeNode *nodeT = findNodeByKey(DM_ROUTERHOPS, _tree);
                    ST_DEVICE device = DevicePool::getCurrentDevice();
                    if(temp->data.asInt() != nodeT->children.size()) {
                        nodeT->children.clear();
                        for(int i = 0; i < temp->data.asInt(); i++) {
                                nodeT->addChild(oneConnectTree::CreatRouteHopsWithIndex(i+1));
                        }
                    }
                }
                if(temp->key == DM_FWTALNUM) {
                    treeNode *nodeT = findNodeByKey(DM_FWUPGRADE,findNodeByKey(DM_ZYXELEXT, _tree));
                    ST_DEVICE device = DevicePool::getCurrentDevice();
                    if(temp->data.asInt() != nodeT->children.size()) {
                        nodeT->children.clear();
                        for(int i = 0; i < temp->data.asInt(); i++) {
                            nodeT->addChild(oneConnectTree::CreatFWUpgradeWithIndex(i+1));
                        }
                    }
                }
				//-------------------------------------------------------------------
			}else{
				cout<<"----???"<<member.at(i)<<"??? can't paser!!!----"<<endl;
				//return;
			}

			// cout << json_object[(*iter)].toStyledString()<< endl;
			Json::Value aaa;
			if (reader.parse(json_object[member.at(i)].toStyledString(), aaa)) {
				parseJsonToTree((char*) aaa.toStyledString().c_str(), temp);
				//cout<<"+"<<i<<endl;
			}else
				return false;
			// cout<<"|||"<<i<<endl;
		}

	}
	return true;
	//reader.parse(json_object["Device"].toStyledString(), json_object);
	//reader.parse(json_object["X_ZyXEL_Ext"].toStyledString(), json_object);

}
Json::Value CJSON::genJsonFromNode(treeNode *node){
	Json::Value temp;
	if (node->hasChildren()) {
		for (int j = 0 ; j < node->children.size(); j++) {
			temp[node->children.at(j).key.c_str()] = genJsonFromNode(&node->children.at(j));
		}
	}else{
		if (node->type == isInt) {
			return 0;
		}else{
			return "";
		}
	}
	return temp;
}
Json::Value CJSON::genJsonFromNodeWithData(treeNode *node){
	Json::Value temp;
	if (node->hasChildren()) {
		for (int j = 0 ; j < node->children.size(); j++) {
			temp[node->children.at(j).key.c_str()] = genJsonFromNodeWithData(&node->children.at(j));
		}
	}else{
		return node->data;
	}
	return temp;
}
string CJSON::genJsonFromTree(treeNode *node){

	int max = node->deep()+1;

	Json::Value temp[10];
	treeNode *t_node = node;

	if (t_node->hasChildren()) {
		for (int j = 0 ; j < node->children.size(); j++) {
			switch (node->children.at(j).type) {
			case isInt:
				temp[max][node->children.at(j).key.c_str()] = 0;
				break;
			case isString:
				temp[max][node->children.at(j).key.c_str()] = "";
				//temp[max][node->children.at(j).key.c_str()] = node->children.at(j).data;
				break;
			default:
				//temp[max][node->children.at(j).key.c_str()] = "object";
				temp[max][node->children.at(j).key.c_str()] = genJsonFromNode(&node->children.at(j));

				break;
			}
		}
	}else
	{
		switch (node->type) {
		case isInt:
			temp[max] = 0;
			break;
		case isString:
			temp[max] = "";
			break;
		default:
			temp[max] = "object";
			break;
		}
		for (int i = max; i > 0; i--) {

			temp[i-1][t_node->key.c_str()] = temp[i];
			t_node = t_node->parent;
		}
		return temp[0].toStyledString();
		//return "Error!The node doesn't has children!";
	}

	for (int i = max; i > 0; i--) {

		temp[i-1][t_node->key.c_str()] = temp[i];
		t_node = t_node->parent;
	}
	//return temp[0];
	return temp[0].toStyledString();

}
string CJSON::genJsonFromTreeWithData(treeNode *node){

	int max = node->deep()+1;

	Json::Value temp[10];
	treeNode *t_node = node;

	if (t_node->hasChildren()) {
		for (int j = 0 ; j < node->children.size(); j++) {

			if (node->children.at(j).hasChildren()) {
				temp[max][node->children.at(j).key.c_str()] = genJsonFromNodeWithData(&node->children.at(j));
			}else{
				temp[max][node->children.at(j).key.c_str()] = node->children.at(j).data;
			}
		}
	}else
	{
		switch (node->type) {
		case isInt:
			temp[max] = 0;
			break;
		case isString:
			temp[max] = "";
			break;
		default:
			temp[max] = "object";
			break;
		}
		for (int i = max; i > 0; i--) {

			temp[i-1][t_node->key.c_str()] = temp[i];
			t_node = t_node->parent;
		}
		return temp[0].toStyledString();
		//return "Error!The node doesn't has children!";
	}

	for (int i = max; i > 0; i--) {

		temp[i-1][t_node->key.c_str()] = temp[i];
		t_node = t_node->parent;
	}
	//return temp[0];
	return temp[0].toStyledString();

}
string CJSON::genJsonFromTreeWithPtr(treeNode *node, string ptr){

	int max = node->deep()+1;

	Json::Value temp[10];
	treeNode *t_node = node;

	if (t_node->hasChildren()) {
		std::cout<<"The node shouldn't have child!"<<endl;
		return NULL;
	}else
	{
		switch (node->type) {
		case isInt:
			temp[max] = atoi(ptr.c_str());
			break;
		case isString:
			temp[max] = ptr;
			break;
		default:
			std::cout<<"The node type shouldn't be object!"<<endl;
			return NULL;
			break;
		}
		for (int i = max; i > 0; i--) {

			temp[i-1][t_node->key.c_str()] = temp[i];
			t_node = t_node->parent;
		}
		return temp[0].toStyledString();
		//return "Error!The node doesn't has children!";
	}

	for (int i = max; i > 0; i--) {

		temp[i-1][t_node->key.c_str()] = temp[i];
		t_node = t_node->parent;
	}
	//return temp[0];
	return temp[0].toStyledString();

}
bool CJSON::creatTreeFromJSON(treeNode *node, string json){

	Json::Reader reader;
	Json::Value json_object;

	if (!reader.parse(json.c_str(), json_object)){
		std::cout<< "Parse jsonstr error!"<<endl;
		return false;
	}
	if (!node->hasChildren()) {  //First time to paser json to creat tree.

		if(json_object.empty()|| !json_object.isObject()){
			//std::cout<<node->key + ":The jsonstr is empty or not object!"<<endl;
			return false;
		}
		Json::Value::Members member = json_object.getMemberNames();

		//node->key = member.at(0);
		for(int i=0; i<member.size();i++)
		{
			//treeNode *tmp = findNodeByKey((char *)member.at(i).c_str(),node);
			/*
             treeNode *temp = treeNodeWithKey(member.at(i).c_str(), isObject);  //<-----
             temp->key = member.at(i);
             node->addChild(temp);
			 */
			treeNode *temp;
			if (node->key=="") {
				node->key = member.at(i);
				temp = node;
			}
			else{
				node->addChild(treeNodeWithKey(member.at(i).c_str(), isObject));
				temp = findNodeByKey((char *)member.at(i).c_str(),node);
				temp->data = json_object[member.at(i)];
			}
			Json::Value aaa;
			if (reader.parse(json_object[member.at(i)].toStyledString(), aaa)) {
				creatTreeFromJSON(temp, aaa.toStyledString());
			}else
				return false;

		}
	}else{ //After the second to paser json
		if(json_object.empty()|| !json_object.isObject()){
			//std::cout<<node->key + ":The jsonstr is empty or not object!"<<endl;
			return false;
		}
		Json::Value::Members member = json_object.getMemberNames();

		//node->key = member.at(0);
		for(int i=0; i<member.size();i++)
		{

			treeNode *temp = NULL;
			treeNode *newNode;
			char *searchKey = (char *)member.at(i).c_str();
			temp = findNodeByKey(searchKey,node);
			if (temp) { //Find the key node from new tree.
				newNode = temp;
				//return false;
			}else{ //Add new node to new tree.
				temp = node;
				temp->addChild(treeNodeWithKey(searchKey, isObject));
				//temp->data = json_object[member.at(i)];
				//return false;
				findNodeByKey(searchKey,temp)->data = json_object[member.at(i)];
				newNode = findNodeByKey(searchKey,temp);
			}

			Json::Value aaa;
			if (reader.parse(json_object[member.at(i)].toStyledString(), aaa)) {
				creatTreeFromJSON(newNode, aaa.toStyledString());
			}else
				return false;

		}
	}
	return true;
}
bool CJSON::creatTreeFromNodeAndTree(treeNode *newTree, treeNode *node, treeNode *referenceTree){

	return true;
}
