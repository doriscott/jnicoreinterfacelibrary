/*! \mainpage About App Agent Client SDK
 
 * \section intro_sec Introduction
 * The App Agent Client SDK can let you connect to One Connect supported product of ZyXEL and configure various functionalities or check status/information of the product. It’s base on C++ language library, it supports many platforms such as iOS, Android, Windows, MAC OS … etc.
 
 * \section revision_his Revision History
 
 Date          | Release         | Author      | Descirption
 ------------- | --------------- | ----------- | -----------
              || 1.0.0           |  Kevin Wu   | The 1'st release version.
 2015/08/31    | 2.0.0           |  Kevin Wu   | Add feature list, fireware upgrade, speed test.
 2015/09/3     | 2.0.1           | Sharon Yang | Add fireware upgrade api, app feature list info

 
 * \section structure Structure
 * The SDK can be split into two categories:\n
 * Device Management
 * - The Data Controller let user can control device directly, it abstract device’s information and setting into classes, and it’s the most commonly used block.
 *  \image html images\DeviceManagement.png
 *
 * Data Controller
 * - The Data Controller let user can control device directly, it abstract device’s information and setting into classes, and it’s the most commonly used block.
 * Following shows the Data Controller and Device Management:
 *  \image html  images\DataController-1.png
 *  \image html images\DataController-2.png
 *
 * \section EventHandle Notification
 * The Error Handler is to deal with error event and message history, you can register callback function, when communication occur exception or error, the function will be triggered.
 *  \image html images\ErrortHandle.png

 * \section FeatureList Feature List
 * Based on the restriction for hardware on produce, the capability of product would be different for different product line. To address the issue, we used feature list to represent the capability of product. When we to send the discovery on WAN, the device would response with it’s feature list. Finally, according the feature we could determine the user interface layout by different device, it would be more flexible.
 *  \image html images\FeatureHandle.png
 
 Name               |Ver.   | Definition                                                                    | OneConnect Phase
 -------------------|-------|-------------------------------------------------------------------------------|------------------
 Device Information |101    |Support L2Device, Client capability type, Support WiFi, Other connection type  |1.0
                   ||102    |Support Pure_L2 capability type,Support PwrLine connection type                |2.0
 Gateway information|201    |WAN Information includes : Enable, Disable                                     |1.0
                   ||202    |WAN Information includes : Enable, Disable, LinkDown                           |2.0
 Flow Information   |301    |WAN flow information                                                           |1.0
                   ||302    |Per device flow information                                                    |2.0
 Login              |401    |Local Access                                                                   |1.0
                   ||402    |Remote Access                                                                  |2.0
 Network Topology   |501    |One layer in network topology                                                  |1.0
                   ||502    |Hierarchical layers in network topology                                        |2.0
 Network Device Control|601 |Support Internet access blocking                                               |1.0
                      ||602 |Support Internet access blocking, scheduling of Internet access blocking       |2.0
 L2 Device Control  |701    |Reboot, WiFi On/Off                                                            |1.0
 Network Icon       |801    |Phase one icon                                                                 |1.0
                   ||802    |Phase two icon                                                                 |2.0
 Guest WiFi         |901    |Guest WiFi Enable/SSID/Preshare key setting                                    |1.0
                   ||902    |Time limit of using guest WiFi                                                 |2.0
                   ||903    |Guest wifi updated                                                             |2.1
 Wi-Fi Auto-Configuration|1001  |L2Device support WiFi Auto-configuration                                   |1.0
 Speed Test         |1201   |Client to Internet                                                             |1.0
                   ||1202   |Gateway to Internet                                                            |2.0
                   ||1203   |Client to Gateway                                                              |2.1
 Diagnostic Detecting Tool|1301 |Ping, Traceroute, routing table                                            |1.0
                         ||1302 |Ping, Traceroute, routing table, topology list                             |2.1
 myZyXEL Cloud Service  |1401   |myZyXEL Cloud Service                                                      |1.0
 Firmware Upgrade   |1501   |Control L2 device and gateway to do online firmware upgrade                    |2.0
 Total LED on/off   |1601   |Turn On/Off L2 device and gateway LED                                          |2.1
 
 */
#ifndef Property_Header
#define Property_Header

#define READ_ONLY  1
#define WRITE_ONLY 2
#define READ_WRITE 3

template<typename Type, typename ClassHolder, int PropType>
class Property
{
private:
    
    Type (ClassHolder::*Get)();
//    void (ClassHolder::*Set)(Type Val);
    bool (ClassHolder::*Set)(Type Val);
    bool (ClassHolder::*SetToBuffer)(Type Val);
    ClassHolder * Class;
    
public:
    
//    Property() : Get(NULL), Set(NULL), SetToBuffer(NULL), Class(NULL) {}
//    Property(ClassHolder * ClassA,
//             Type (ClassHolder::*GetA)(),
//             bool (ClassHolder::*SetA)(Type Val)) :  Class(ClassA), Get(GetA), Set(SetA) ,SetToBuffer(SetA){}
    
    void Set_Property_Control(ClassHolder * ClassA, Type (ClassHolder::*GetA)(), bool (ClassHolder::*SetA)(Type Val), bool (ClassHolder::*SetToBufferA)(Type Val))
    {
        if(PropType == READ_WRITE){
            Class = ClassA;
            Get = GetA;
            Set = SetA;
            SetToBuffer = SetToBufferA;
        }else{
            //Class = NULL;
            Get = NULL;
            Set = NULL;
            SetToBuffer = NULL;
        }
    }
    
    void Set_Get(Type (ClassHolder::*GetA)())
    {
        if((PropType == READ_ONLY) || (PropType == READ_WRITE))
            Get = GetA;
        else
            Get = NULL;
    }
    
    void Set_Set(bool (ClassHolder::*SetA)(Type Val))
    {
        if((PropType == WRITE_ONLY) || (PropType == READ_WRITE))
            Set = SetA;
        else
            Set = NULL;
    }
    void Set_SetToBuffer(bool (ClassHolder::*SetToBufferA)(Type Val))
    {
        if((PropType == WRITE_ONLY) || (PropType == READ_WRITE))
            Set = SetToBufferA;
        else
            Set = NULL;
    }
    
    void Set_Class(ClassHolder * Holder)
    {
        Class = Holder;
    }
    
//    // set
//    Type operator = (const Type& In)
//    {
//        if ( Set ) (Class->*Set)(In);
//        return In;
//    }
//    
//    // set
//    Type operator = (Property& In)
//    {
//        if ( Set ) (Class->*Set)(In);
//        return In;
//    }
//    
//    // get
//    operator Type()
//    {
//        //return Get ? (Class->*Get)() : 0;
//        return (Class->*Get)();
//    }
    Type get(){
        return (Class->*Get)();
    }
    void set(const Type& In){
        if ( Set ){
            (Class->*Set)(In);
            //Class = In;
        }

    }
    void set(Property& In){
        if ( Set ){
            (Class->*Set)(In);
            //Class = In;

        }
    }
    void setToBuffer(const Type& In){
        if ( SetToBuffer ){
            (Class->*SetToBuffer)(In);
            //Class = In;
        }
        
    }
    void setToBuffer(Property& In){
        if ( SetToBuffer ){
            (Class->*SetToBuffer)(In);
            //Class = In;
            
        }
    }
    
};

#endif