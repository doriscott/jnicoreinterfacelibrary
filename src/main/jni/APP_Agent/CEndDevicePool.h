/**
 *  @brief		It's the data controller for end device.
 *  @details    Getting or setting the information/status of end device such as single strength for Wi-Fi, MAC, IP, and wheather the internet is blocking..etc.
 *  @author
 - Kevin Wu--v2.0.0--2015.8.20
 - Sharon Yang--v2.0.1--2015.9.3
 *  @copyright
 .
 * Copyright (C) 2000-2015 ZyXEL Communications, Corp.\n
 * All Rights Reserved.\n
 *
 * ZyXEL Confidential;\n
 * Protected as an unpublished work, treated as confidential,\n
 * and hold in trust and in strict confidence by receiving party.\n
 * Only the employees who need to know such ZyXEL confidential information\n
 * to carry out the purpose granted under NDA are allowed to access.\n
 *\n
 * The computer program listings, specifications and documentation\n
 * herein are the property of ZyXEL Communications, Corp. and shall\n
 * not be reproduced, copied, disclosed, or used in whole or in part\n
 * for any reason without the prior express written permission of\n
 * ZyXEL Communications, Corp.\n
 * @file		CEndDevicePool.h
 */

#ifndef __CEndDevicePool__
#define __CEndDevicePool__

#include <iostream>
#include <vector>
#include "CParentalControl.h"
using namespace std;

enum EN_HOST_TYPE{
    //DEFAULT = 0,
    DESKTOP=1,
    LATTOP,
    AP,
    NAS,
    POWERLINE,
    REPEATER,
    PHONE,
    CAMERA, 
    TABLET
};
typedef struct {
	string MAC;
	string Name;
    string UserDefineName;
	bool OnlineStatus;
	string ConnectInterface;
	bool Block;
    bool L2WifiStatus;
	string IPAddress;
	int ControlMask;
    int SignalStrength;
    int L2AutoConfigStatus;
    int L2AutoConfigEnable;
    int hostType;
    //Sharon add
    string CapabilityType;
    string ConnectionType;
    int PhyRate;
    string SoftwareVersion;
    string Neighbor;
    int DLRate;
    int ULRate;
	int GuestWiFiGroup;
	int AutoConfigApprove;
	string Manufacturer;
	int Rssi;
	int Band;
	int LinkRate24G;
	int LinkRate5G;
	int Channel24G;
	int Channel5G;
    
}ST_EndDevice;

typedef struct {
    string MAC;
    string ModelName;
    int FWUpgradeState;
    string NewFWUpgradeVersion;
    string NewFWUpgradeDate;
}ST_L2FWEndDevice;

 /// It's the data controller for end device.
class CEndDevicePool {
public:
    vector<ST_EndDevice> EndDeviceList;
	vector<ST_EndDevice> BlockList;
	vector<ST_EndDevice> NonBlockList;
	CParentalControl ParentalControl;
    
	CEndDevicePool();
	CEndDevicePool(int GUID);
	int GetGUID();

    /**
     *  Blocking the device.
     *
     *  @param mac The MAC of device.
     *
     *  @return <#return value description#>
     *  \note Available in Feature \"Network Device Control\" ver.601 and later.
     */
	bool SetBlock(string mac);
    /**
     *  Unblocking the device
     *
     *  @param mac The MAC of device.
     *
     *  @return <#return value description#>
     *  \note Available in Feature \"Network Device Control\" ver.602 and later.
     */
	bool SetNonBlock(string mac);
	bool SetL2DevReboot(string mac);
	bool SetL2DevWiFiOn(string mac);
	bool SetL2DevWiFiOff(string mac);
    
    bool SetBlockToBuffer(string mac);
    bool SetNonBlockToBuffer(string mac);
    bool SetL2DevRebootToBuffer(string mac);
    bool SetL2DevWiFiOnToBuffer(string mac);
    bool SetL2DevWiFiOffToBuffer(string mac);
    
    //2014-01-21
    bool SetName(string mac, string newName);
    bool SetType(string mac, int newType);
    bool SetNameToBuffer(string mac, string newName);
    bool SetTypeToBuffer(string mac, int newType);
    
    int GetTotalL2FWEndDevice();
    vector<ST_L2FWEndDevice> GetL2FWEndDeviceList();
	int GetTotalEndDevice();
	vector<ST_EndDevice> GetEndDeviceList();
	ST_EndDevice GetEndDevice(string mac);
	vector<ST_EndDevice> GetBlockList();
	vector<ST_EndDevice> GetNonBlockList();

	bool AddPcpRule(string mac);
	vector<ST_ParentalControl> GetParentalControlList(int index);

    bool SendBuffer();
private:
    static vector<ST_L2FWEndDevice> _L2FWEndDeviceList;
	static vector<ST_EndDevice> _EndDeviceList;
	vector<ST_EndDevice> _BlockList;
	vector<ST_EndDevice> _NonBlockList;
//	CParentalControl _myParentalControl;
protected:
	int GUID;
};

#endif /* defined(__CEndDevicePool__) */
