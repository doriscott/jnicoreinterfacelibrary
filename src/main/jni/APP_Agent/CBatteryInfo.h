/**
 *  @brief		It's the data controller for battery related of portable router.
 *  @details    Getting the information about the battery of portable router.
 *  @author
 - Kevin Wu--v2.0.0--2015.8.20
 *  @copyright
 .
 * Copyright (C) 2000-2015 ZyXEL Communications, Corp.\n
 * All Rights Reserved.\n
 *
 * ZyXEL Confidential;\n
 * Protected as an unpublished work, treated as confidential,\n
 * and hold in trust and in strict confidence by receiving party.\n
 * Only the employees who need to know such ZyXEL confidential information\n
 * to carry out the purpose granted under NDA are allowed to access.\n
 *\n
 * The computer program listings, specifications and documentation\n
 * herein are the property of ZyXEL Communications, Corp. and shall\n
 * not be reproduced, copied, disclosed, or used in whole or in part\n
 * for any reason without the prior express written permission of\n
 * ZyXEL Communications, Corp.\n
 * @file		CBatteryInfo.h
 */

#ifndef __CBatteryInfo__
#define __CBatteryInfo__

#include <iostream>
#include "CMobile.h"
#include "Property.h"

using namespace std;
class CBatteryInfo : public CMobile
{
    
public:
    Property<string, CBatteryInfo, READ_ONLY> ChargeState;
    Property<int, CBatteryInfo, READ_ONLY> TotalCapacity;
    Property<int, CBatteryInfo, READ_ONLY> RemainCapacity;
    
    CBatteryInfo();
    /**
     *  Getting the state of battery charge.
     *
     *  @return The state of battery charge.
     */
	string GetChargeState();
    /**
     *  Getting the total capacity of battery charge.
     *
     *  @return The total capacity.
     */
	int GetTotalCapacity();
    /**
     *  Getting how many remain capacity of battery.
     *
     *  @return The remain capacity.
     */
	int GetRemainCapacity();
    bool SendBuffer();
private:
    string _ChargeState;
	int _TotalCapacity;
	int _RemainCapacity;
};
#endif /* defined(__CBatteryInfo__) */
