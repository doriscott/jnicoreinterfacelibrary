/**
 *  @brief		It's the controller for event notify.
 *  @details    Handle the communicate message and error event.
 *  @author
 - Kevin Wu--v2.0.0--2015.8.20
 *  @copyright
 .
 * Copyright (C) 2000-2015 ZyXEL Communications, Corp.\n
 * All Rights Reserved.\n
 *
 * ZyXEL Confidential;\n
 * Protected as an unpublished work, treated as confidential,\n
 * and hold in trust and in strict confidence by receiving party.\n
 * Only the employees who need to know such ZyXEL confidential information\n
 * to carry out the purpose granted under NDA are allowed to access.\n
 *\n
 * The computer program listings, specifications and documentation\n
 * herein are the property of ZyXEL Communications, Corp. and shall\n
 * not be reproduced, copied, disclosed, or used in whole or in part\n
 * for any reason without the prior express written permission of\n
 * ZyXEL Communications, Corp.\n
 * @file		CErrorHandle.h
 */

/**
 *  \example Error&Message_Handler
 *  This is an example of how to use the event notify".\n
 *
 *  \code{.cpp}
 void errorHandler(int errorCode){
     switch (errorCode) {
         case ERR_AGENT_INVALID_STRING:
             //-------------------------
             //-------Do something------
             //-------------------------
             break;
         case ERR_AGENT_SET_ONLY:
             //-------------------------
             //-------Do something------
             //-------------------------
             break;
         default:
             //-------------------------
             //-------Do something------
             //-------------------------
             break;
     }
 }
 void handelMessage(string msg){
     //---------------------------------
     //--------Save the message---------
     //---------------------------------
 }
 int main(int argc, const char * argv[])
 {
     CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
     //register the error callback funtion
     errorHandle->setCallbackFun(errorHandler);
     //register the communicate message handel callback funtion
     errorHandle->setCallbackDebugFun(handelMessage);
     
     //---------------------------------------------------------
     //-------start connect to device and communication---------
     //---------------------------------------------------------
 }
 *  \endcode
 */
#ifndef __CErrorHandle__
#define __CErrorHandle__

#include <memory>
#include <iostream>
#include <string>
#include <vector>
#define ONE_CONNECT_SUPPORT_LIST "NBG|AMG|VMG|ALL"
//#define ONE_CONNECT_SUPPORT_LIST "NBG"

#define IS_SUPPORT_ONE_CONNECT(T) isSupportForOneConnect(T,ONE_CONNECT_SUPPORT_LIST)
using namespace std;
typedef void (* CALLBACK) (int);
typedef void (* CALLBACK_MSG) (string);

/// It's the controller for event notify.
class CErrorHandle{
    
public:
    CALLBACK callback;
    CALLBACK_MSG callbackMSG;
    void setCallbackFun(CALLBACK);
    void setCallbackDebugFun(CALLBACK_MSG);
    void handleErrorOccur(int);
    void handleDebugMessage(string message);
    void whatErrorOccur(int);
    
    static CErrorHandle * sharedInstance()
    {
        if( 0== _instance.get())
        {
            _instance.reset( new CErrorHandle);
        }
        return _instance.get();
    }
    //protected:
    CErrorHandle(void)
    {
        cout <<"Create CErrorHandle"<<endl;
        callback=nullptr;
        callbackMSG=nullptr;
    }
    virtual ~CErrorHandle(void)
    {
        cout << "Destroy CErrorHandle"<<endl;
    }
    friend class auto_ptr<CErrorHandle>;
    static auto_ptr<CErrorHandle> _instance;
};
void defaultErrorHandle(int);
std::string Format(const char* lpszFormat, ...);
vector<string> split(const string &s, const string &seperator);
bool isSupportForOneConnect(string modelName);
#endif /* defined(__CErrorHandle__) */