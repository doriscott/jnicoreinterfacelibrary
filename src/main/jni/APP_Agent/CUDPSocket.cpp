/****************************************************************************/
/*
* Copyright (C) 2000-2013 ZyXEL Communications, Corp.
* All Rights Reserved.
*
* ZyXEL Confidential;
* Protected as an unpublished work, treated as confidential,
* and hold in trust and in strict confidence by receiving party.
* Only the employees who need to know such ZyXEL confidential information
* to carry out the purpose granted under NDA are allowed to access.
*
* The computer program listings, specifications and documentation
* herein are the property of ZyXEL Communications, Corp. and shall
* not be reproduced, copied, disclosed, or used in whole or in part
* for any reason without the prior express written permission of
* ZyXEL Communications, Corp.
*
*/
/****************************************************************************/
/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// OpenGL ES 2.0 code
#include <string.h>
//#include <jni.h>
//#include <android/log.h>

#include <stdio.h>
#include <stdlib.h>

#include "CUDPSocket.h"
#include "DevicePool.h"

#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include "../Lib/CSocket/CSocket.h"
#include "CDataMessage.h"

#define TIMEOUT_RECV_UDP 4
#define  LOG_TAG    "CUDPSocket"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

int UDP_Broadcast() {
    DevicePool::clearDevice();
	int sockfd;
	struct sockaddr_in their_addr; // connector's address information
	struct hostent *he;
	int numbytes;
	int broadcast = 1;
	//char broadcast = '1'; // if that doesn't work, try this

	if ((he = gethostbyname("255.255.255.255")) == NULL) { // get the host infoif     perror("gethostbyname");        exit(1);
		printf("!!!!!!!");
	}

	if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
		perror("socket");
		//exit(1);
		return false;
	}

	// this call is what allows broadcast packets to be sent:
	if (setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &broadcast,
			sizeof broadcast) == -1) {
		printf("!!!!!!!");
		perror("setsockopt (SO_BROADCAST)");
		close(sockfd);
		//exit(1);
		return false;
	}
	//---------bind the port
	struct sockaddr_in sin;
	int sin_len;
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = htonl(INADDR_ANY);
	sin.sin_port = htons(0);
	sin_len = sizeof(sin);
	int bind_rc = ::bind(sockfd, (struct sockaddr *) &sin, sizeof(sin));
	if (bind_rc == -1) {
		perror("bind call failed 1");
		close(sockfd);
		//exit(errno);
		return false;
	}
	//---------
	their_addr.sin_family = AF_INET; // host byte order
	their_addr.sin_port = htons(263); // short, network byte order
	their_addr.sin_addr = *((struct in_addr *) he->h_addr);
	//memset(their_addr.sin_zero, '', sizeof their_addr.sin_zero);
	char packet_content[500] =
			"{\"Device\":{\"X_ZyXEL_Ext\":{\"AppInfo\":{\"MagicNum\":\"Z3704\",\"AppVersion\":1}}}}";
	char buffer[500];

	int version = 1;
	int type = 1;
	int len = (int)strlen(packet_content);
	memset(buffer, 0, sizeof(buffer));
	memcpy(buffer, &version, 1);
	memcpy(buffer + 1, &type, 1);
	memcpy(buffer + 2, &len, 2);
	memcpy(buffer + 4, packet_content, len);
	if ((numbytes = (int)sendto(sockfd, buffer, len + 4, 0,
			(struct sockaddr *) &their_addr, sizeof their_addr)) == -1) {
		perror("sendto");
		close(sockfd);
		//exit(1);
		return false;
	}
//	close(sockfd);
    
    int result;
    int close_rc;
    long recv_rc;
    char message[512];
    memset(message, 0, 512);
    struct timeval timeout;
    socklen_t tlen = sizeof(timeout);
    timeout.tv_sec = TIMEOUT_RECV_UDP;
    timeout.tv_usec = 0;
    result = setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO,
                        (char*) &timeout.tv_sec, tlen);
    if (result < 0) {
        perror("setsockopt:");
        //exit(1);
        return false;
    }
    //Loop forever (or until a termination message is received)
    // Received data through the socket and process it.The processing in this program is really simple --printing
    while (1) {
        
        recv_rc = recvfrom(sockfd, message, sizeof(message), 0,
                           (struct sockaddr *) &sin, (socklen_t *) &sin_len);
        if (recv_rc == -1) {
            perror("timeout");
            close(sockfd);
            break;
            //exit(errno);
        }
        printf("[IP:%s][Port:%d]", inet_ntoa(sin.sin_addr), ntohs(sin.sin_port));
        printf("Response from server : %s\n", message + 4);
        
        //amy_add
        ST_DEVICE device = CDataMessage::DeviceDiscoverResp(message + 4);
        
        device.IP = inet_ntoa(sin.sin_addr);
        printf("Response from server Model Name=%s\n", device.ModelName.c_str());
        
        //add 12/9 can't find device.
        bool isExist = false;
        vector<ST_DEVICE> deviceList;
        deviceList.clear();
        deviceList = DevicePool::getDeviceList();
        for (int i = 0; i < deviceList.size(); i++) {
            if (deviceList.at(i).SerialNumber == device.SerialNumber) {
                isExist = true;
            }
        }
        if (device.ModelName != "" && !isExist) {
            DevicePool::addDevice(device);
        }
    }
    close_rc = close(sockfd);
    if (close_rc == -1) {
        perror("close call failed");
        //exit(errno);
        return false;
    }
    
	return true;
}
int UDP_Server() {

    DevicePool::clearDevice();
	int port = 5622;
	int sin_len;
	char message[512];
	int socket_descriptor;
	struct sockaddr_in sin;

	int bind_rc, close_rc;
	ssize_t recv_rc;

	printf("Waiting for data from sender\n");

	// Initialize socket address structure for Internet Protocols
	bzero(&sin, sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = htonl(INADDR_ANY);
	sin.sin_port = htons(port);
	sin_len = sizeof(sin);

	//Create a UDP socket and bind it to the port
	socket_descriptor = socket(AF_INET, SOCK_DGRAM, 0);
	if (socket_descriptor == -1) {
		perror("socket call failed");
		//exit(errno);
		return false;
	}
//-----add timeout for receive

	int result;
	struct timeval timeout;
	socklen_t tlen = sizeof(timeout);
	timeout.tv_sec = TIMEOUT_RECV_UDP;
	timeout.tv_usec = 0;
	result = setsockopt(socket_descriptor, SOL_SOCKET, SO_RCVTIMEO,
			(char*) &timeout.tv_sec, tlen);
	if (result < 0) {
		perror("setsockopt:");
		//exit(1);
		return false;
	}

//-----
	bind_rc = ::bind(socket_descriptor, (struct sockaddr *) &sin, sizeof(sin));
	if (bind_rc == -1) {
		perror("bind call failed");
		close(socket_descriptor);
		//exit(errno);
		//return false;
	}

	//Loop forever (or until a termination message is received)
	// Received data through the socket and process it.The processing in this program is really simple --printing
    while (1) {

		recv_rc = recvfrom(socket_descriptor, message, sizeof(message), 0,
				(struct sockaddr *) &sin, (socklen_t *) &sin_len);
		if (recv_rc == -1) {
			perror("bind call failed");
            close(socket_descriptor);
			break;
			//exit(errno);
		}
		printf("[IP:%s][Port:%d]", inet_ntoa(sin.sin_addr), sin.sin_port);
		printf("Response from server : %s\n", message + 4);

		//amy_add
		ST_DEVICE device = CDataMessage::DeviceDiscoverResp(message + 4);
        
		device.IP = inet_ntoa(sin.sin_addr);
		 printf("Response from server Model Name=%s\n", device.ModelName.c_str());
        
		//add 12/9 can't find device.
        bool isExist = false;
        vector<ST_DEVICE> deviceList;
        deviceList.clear();
        deviceList = DevicePool::getDeviceList();
        for (int i = 0; i < deviceList.size(); i++) {
            if (deviceList.at(i).SerialNumber == device.SerialNumber) {
                isExist = true;
            }
        }
        if (device.ModelName != "" && !isExist) {
            DevicePool::addDevice(device);
        }
        //add end

//		if (strncmp(message, "stop", 4) == 0) {
//			printf("sender has told me to end the connection\n");
//			break;
//		}
	}
	close_rc = close(socket_descriptor);
	if (close_rc == -1) {
		perror("close call failed");
		//exit(errno);
		return false;
	}
	return true;

}


