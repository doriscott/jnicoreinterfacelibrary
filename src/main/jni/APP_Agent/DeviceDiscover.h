/**
 *  @brief		Discovery and login to ZyXEL product
 *  @details
 *  @author
 - Kevin Wu--v2.0.0--2015.8.20
 - Sharon Yang--v2.0.1--2015.9.3
 *  @copyright
 .
 * Copyright (C) 2000-2015 ZyXEL Communications, Corp.\n
 * All Rights Reserved.\n
 *
 * ZyXEL Confidential;\n
 * Protected as an unpublished work, treated as confidential,\n
 * and hold in trust and in strict confidence by receiving party.\n
 * Only the employees who need to know such ZyXEL confidential information\n
 * to carry out the purpose granted under NDA are allowed to access.\n
 *\n
 * The computer program listings, specifications and documentation\n
 * herein are the property of ZyXEL Communications, Corp. and shall\n
 * not be reproduced, copied, disclosed, or used in whole or in part\n
 * for any reason without the prior express written permission of\n
 * ZyXEL Communications, Corp.\n
 * @file		DeviceDiscover.h
 */

#ifndef __Device_Discover__
#define __Device_Discover__

#include <string>
#include "DataType_Define.h"
using namespace std;

/**
 *  Search the ZyXEL product on LAN.
 *
 *  @return
 */
int Broadcast();
/**
 *  Login to ZyXEL product.
 *
 *  @param index    The index of device list.
 *  @param password The admin password of product.
 *
 *  @return Login successfully or not.
 */
int Login(int index, std::string password);
/**
 *  Login to ZyXEL product.
 *
 *  @param device   The device.
 *  @param password The admin password of product.
 *
 *  @return Login successfully or not.
 */
int Login(ST_DEVICE device, std::string password);
/**
 *  Logout
 *
 *  @return
 */
int Logout();

/**
 *  \example Discover_&_Login
 *  This is an example of how to discover ZyXEL product and login.
 *
 *  Download test
 *  \code{.cpp}
 *      //Broad cast on LAN.
 *      Broadcast();
 *      //Getting the list of responsed devices.
 *      vector<ST_DEVICE> deviceList = DevicePool::getDeviceList();
 *      //Login to 1'st device
 *      Login(0, "1234");
 *      //you also can use below command to login, it's equl to above command.
 *      Login(deviceList.at(0), "1234");
 *  \endcode
 */

#endif /* defined(__Device_Discover__) */
