/****************************************************************************/
/*
* Copyright (C) 2000-2013 ZyXEL Communications, Corp.
* All Rights Reserved.
*
* ZyXEL Confidential;
* Protected as an unpublished work, treated as confidential,
* and hold in trust and in strict confidence by receiving party.
* Only the employees who need to know such ZyXEL confidential information
* to carry out the purpose granted under NDA are allowed to access.
*
* The computer program listings, specifications and documentation
* herein are the property of ZyXEL Communications, Corp. and shall
* not be reproduced, copied, disclosed, or used in whole or in part
* for any reason without the prior express written permission of
* ZyXEL Communications, Corp.
*
*/
/****************************************************************************/
//
//  CMobileMonitor.cpp
//
//  Created by Scott Tseng on 13/11/20.
//  Copyright (c) 2013年 pan star. All rights reserved.
//

#include "CBatteryInfo.h"
#include "MessageType.h"
#include <sstream>
#include "UID_Define.h"
#include "CJSON.h"
#include "CDataMessage.h"
#include "CErrorHandle.h"
#include "CTemporary.h"
CBatteryInfo::CBatteryInfo(){
    ChargeState.Set_Class(this);
    ChargeState.Set_Get(&CBatteryInfo::GetChargeState);
    TotalCapacity.Set_Class(this);
    TotalCapacity.Set_Get(&CBatteryInfo::GetTotalCapacity);
    RemainCapacity.Set_Class(this);
    RemainCapacity.Set_Get(&CBatteryInfo::GetRemainCapacity);
}
string CBatteryInfo::GetChargeState() {
	string temp;
    CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
	temp = DM.CreatParameterToString(UUID_device_battery_charge);

    string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
    
	DM.ParserData(receive);
	temp = DM.GetParameter(UUID_device_battery_charge);
	_ChargeState = temp;
	return _ChargeState;
}

int CBatteryInfo::GetTotalCapacity() {
    
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_device_battery_total);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_device_battery_total);
        _TotalCapacity = atoi(temp.c_str());
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
	return _TotalCapacity;
}

int CBatteryInfo::GetRemainCapacity() {
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_device_battery);
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_device_battery);
        _RemainCapacity = atoi(temp.c_str());
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
	return _RemainCapacity;
}
bool CBatteryInfo::SendBuffer(){
    try {
        CTemporary::SendBuffer();
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return true;
}