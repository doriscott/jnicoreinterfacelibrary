/****************************************************************************/
/*
* Copyright (C) 2000-2013 ZyXEL Communications, Corp.
* All Rights Reserved.
*
* ZyXEL Confidential;
* Protected as an unpublished work, treated as confidential,
* and hold in trust and in strict confidence by receiving party.
* Only the employees who need to know such ZyXEL confidential information
* to carry out the purpose granted under NDA are allowed to access.
*
* The computer program listings, specifications and documentation
* herein are the property of ZyXEL Communications, Corp. and shall
* not be reproduced, copied, disclosed, or used in whole or in part
* for any reason without the prior express written permission of
* ZyXEL Communications, Corp.
*
*/
/****************************************************************************/
//
//  CMobileMonitor.h
//
//  Created by Kevin on 13/10/8.
//  Copyright (c) 2013年 pan star. All rights reserved.
//

#ifndef __CMobileMonitor__
#define __CMobileMonitor__

#include <iostream>
#include "CMobile.h"
#include "Property.h"

using namespace std;
class CMobileMonitor : public CMobile
{
public:
    Property<bool, CMobileMonitor, READ_WRITE> AutoReset;
    Property<int, CMobileMonitor, READ_WRITE> CycleResetDate;
	Property<int, CMobileMonitor, READ_WRITE> Quota;
    Property<bool, CMobileMonitor, READ_WRITE> Enable;
    
	Property<int, CMobileMonitor, READ_ONLY> ResetCounter;
	Property<string, CMobileMonitor, READ_ONLY> LastResetDate;
	Property<float, CMobileMonitor, READ_ONLY> CurrentUsage;
	Property<float, CMobileMonitor, READ_ONLY> DataSend;
	Property<float, CMobileMonitor, READ_ONLY> DataReceived;
	Property<float, CMobileMonitor, READ_ONLY> DailyUsageAdvice;
    
	CMobileMonitor();
	//~CMobileMonitor();
	
	
	bool GetAutoReset();
	bool SetAutoReset(bool blAutoReset);
    bool SetAutoResetToBuffer(bool blAutoReset);
    
	int GetResetCounter();
	string GetLastResetDate();
    
	int GetCycleResetDate();
	bool SetCycleResetDate(int strcycleresetdate);
    bool SetCycleResetDateToBuffer(int strcycleresetdate);
    
	int GetQuota();
	bool SetQuota(int nquota);
    bool SetQuotaToBuffer(int nquota);
    
	float GetCurrentUsage();
	float GetDataSend();
	float GetDataReceived();
	float GetDailyUsageAdvice();
    
	bool IsEnable();
    bool SetEnable(bool);
    bool SetEnableToBuffer(bool);
    
    bool SendBuffer();
private:
	bool _AutoReset;
	int _ResetCounter;
	string _LastResetDate;
	int _CycleResetDate;
	int _Quota;
	float _CurrentUsage;
	float _DataSend;
	float _DataReceived;
	float _DailyUsageAdvice;
	bool _Enable;
};
int GetDaysInMonth(int year,int month);
#endif /* defined(__CMobileMonitor__) */
