/**
 *  @brief		It's the data controller for parental control.
 *  @details    Scheduleing the network access for client.
 *  @author
 - Kevin Wu--v2.0.0--2015.8.20
 *  @copyright
 .
 * Copyright (C) 2000-2015 ZyXEL Communications, Corp.\n
 * All Rights Reserved.\n
 *
 * ZyXEL Confidential;\n
 * Protected as an unpublished work, treated as confidential,\n
 * and hold in trust and in strict confidence by receiving party.\n
 * Only the employees who need to know such ZyXEL confidential information\n
 * to carry out the purpose granted under NDA are allowed to access.\n
 *\n
 * The computer program listings, specifications and documentation\n
 * herein are the property of ZyXEL Communications, Corp. and shall\n
 * not be reproduced, copied, disclosed, or used in whole or in part\n
 * for any reason without the prior express written permission of\n
 * ZyXEL Communications, Corp.\n
 * @file		CParentalControl.h
 */

#ifndef __CParentalControl__
#define __CParentalControl__

#include <iostream>
#include <vector>
using namespace std;

typedef struct {
	string ID;
	string EndDeviceMAC;
	string Enable;
	string ProfileName;
	string SchedulingDays;
	string SchedulingTimeStartHour;
	string SchedulingTimeStartMin;
	string SchedulingTimeStopHour;
	string SchedulingTimeStopMin;
}ST_ParentalControl;

 /// It's the data controller for parental control.
class CParentalControl {
public:
	CParentalControl();
	CParentalControl(int GUID);
	int GetGUID();

    /**
     *  Getting how many profiles has added.
     *
     *  @return The number of profiles.
     *  \note Available in Feature \"Network Device Control\" ver.602 and later.
     */
	int GetTotalProfile();
    /**
     *  Getting the profile list.
     *
     *  @return The profile list.
     *  \note Available in Feature \"Network Device Control\" ver.602 and later.
     */
	vector<ST_ParentalControl> GetProfileList();
	ST_ParentalControl GetProfile(int index);
    /**
     *  Setting edited profile to gateway.
     *
     *  @param pf edited profile.
     *  \note Available in Feature \"Network Device Control\" ver.602 and later.
     */
	void EditProfile(ST_ParentalControl pf);
    /**
     *  Adding new profiles.
     *
     *  @param pf_array The list of profiles.
     *  \note Available in Feature \"Network Device Control\" ver.602 and later.
     */
	//void AddProfiles(vector<ST_ParentalControl> pf_array);
    /**
     *  Adding new profile.
     *
     *  @param pf New profile.
     *  \note Available in Feature \"Network Device Control\" ver.602 and later.
     */
	void AddProfile(ST_ParentalControl pf);
    /**
     *  Delteing the list of profiles.
     *
     *  @param pf_array The list of profiles.
     *  \note Available in Feature \"Network Device Control\" ver.602 and later.
     */
	void DeleteProfiles(vector<ST_ParentalControl> pf_array);
    
    /**
     *  \example Network_Device_Control
     *  This is an example of how to use the feature "Network Device Control\".\n
     *
     *  Blocking and unblocking device.
     *  \code{.cpp}
     *  CEndDevicePool device;
     *  device.SetBlock("aaa");
     *  device.SetNonBlock()     
     *  \endcode
     *  \note Available in Feature \"Network Device Control\" ver.601 and later.
     
     *  Scheduling network access for device.
     * - Adding profiles.
     *  \code{.cpp}
     CEndDevicePool endDevicePool;
     //Getting the list of clients
     vector<ST_EndDevice> endDeviceList = endDevicePool.GetEndDeviceList();
     
     CParentalControl parentalControl;
     ST_ParentalControl profile;
     profile.Enable = "1"; //Enable:"1" Disable:"0"
     profile.ProfileName = "Schedule 123";
     profile.SchedulingDays = "Sun,Mon,Tue,Wed,Thu,Fri,Sat";
     //Setting the 1'st client as the Scheduling device.
     profile.EndDeviceMAC = endDeviceList.at(0).MAC;
     //Scheduling time 18:30~24:00
     profile.SchedulingTimeStartHour = "18";
     profile.SchedulingTimeStartMin = "30";
     profile.SchedulingTimeStopHour = "24";
     profile.SchedulingTimeStopMin = "00";
     
     endDevicePool.AddPcpRule(profile.EndDeviceMAC);
     endDevicePool.SendBuffer();
     parentalControl.AddProfile(profile);
     parentalControl.SendBuffer();
     parentalControl.GetProfileList();
     *  \endcode
     
     * - Editeing profiles.
     *  \code{.cpp}
     CParentalControl parentalControl;
     vector<ST_ParentalControl> profiles = parentalControl.GetProfileList();;
     ST_ParentalControl profile = profiles.at(0);
     //  -------------------
     //  --- edit profile---
     //  -------------------
     parentalControl.EditProfile(profile);
     parentalControl.SendBuffer();

     *  \endcode
     
     * - Deleting profiles.
     *  \code{.cpp}
     CParentalControl parentalControl;
     vector<ST_ParentalControl> profiles = parentalControl.GetProfileList();;
     vector<ST_ParentalControl> delProfiles;
     //Deleting the 3'st profile.
     delProfiles.push_back(profiles.at(2));
     parentalControl.DeleteProfiles(delProfiles);
     parentalControl.SendBuffer();
     parentalControl.GetProfileList();
     *  \endcode
     *  \note Available in Feature \"Network Device Control\" ver.602 and later.
     */
    bool SendBuffer();
    string SendBufferAndRecvReq();
public:
	static vector<ST_ParentalControl> ST_ParentalControlList;
	static int current_profile_size;
protected:
	int GUID;
};

#endif /* defined(__CParentalControl__) */
