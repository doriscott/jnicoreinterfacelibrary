/****************************************************************************/
/*
* Copyright (C) 2000-2013 ZyXEL Communications, Corp.
* All Rights Reserved.
*
* ZyXEL Confidential;
* Protected as an unpublished work, treated as confidential,
* and hold in trust and in strict confidence by receiving party.
* Only the employees who need to know such ZyXEL confidential information
* to carry out the purpose granted under NDA are allowed to access.
*
* The computer program listings, specifications and documentation
* herein are the property of ZyXEL Communications, Corp. and shall
* not be reproduced, copied, disclosed, or used in whole or in part
* for any reason without the prior express written permission of
* ZyXEL Communications, Corp.
*
*/
/****************************************************************************/
//
//  CJSON.h
//
//  Created by Kevin on 13/10/8.
//  Copyright (c) 2013年 pan star. All rights reserved.
//

#ifndef __CJSON__
#define __CJSON__

#include <iostream>
#include "../Lib/tr181/tr181Tree.h"
#include "../Lib/tr181/oneConnectTree.h"
class CJSON{
private:
    treeNode *_tree;
public:
    CJSON(treeNode *tree);
    Json::Value genJsonFromNode(treeNode *node);
    Json::Value genJsonFromNodeWithData(treeNode *node);
    string genJsonFromTree(treeNode *node);
    string genJsonFromTreeWithPtr(treeNode *node,string ptr);
    string genJsonFromTreeWithData(treeNode *node);
    bool parseJsonToTree(string tmp,treeNode *node);
    
    bool creatTreeFromJSON(treeNode *newTree, string json);
    bool creatTreeFromNodeAndTree(treeNode *oldTree, treeNode *node, treeNode *referenceTree);
};
#endif /* defined(__CJSON__) */
