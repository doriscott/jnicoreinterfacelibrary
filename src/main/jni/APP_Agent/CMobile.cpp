/****************************************************************************/
/*
* Copyright (C) 2000-2013 ZyXEL Communications, Corp.
* All Rights Reserved.
*
* ZyXEL Confidential;
* Protected as an unpublished work, treated as confidential,
* and hold in trust and in strict confidence by receiving party.
* Only the employees who need to know such ZyXEL confidential information
* to carry out the purpose granted under NDA are allowed to access.
*
* The computer program listings, specifications and documentation
* herein are the property of ZyXEL Communications, Corp. and shall
* not be reproduced, copied, disclosed, or used in whole or in part
* for any reason without the prior express written permission of
* ZyXEL Communications, Corp.
*
*/
/****************************************************************************/
//
//  CMobile.cpp
//
//  Created by Kevin on 13/10/8.
//  Copyright (c) 2013年 pan star. All rights reserved.
//

#include "CMobile.h"
#include "MessageType.h"
//#include <android/log.h>
#include "UID_Define.h"
#include "CDataMessage.h"
#include "ErrorCode.h"
#include "CErrorHandle.h"
#include "CTemporary.h"

#define  LOG_TAG    "CMobile"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

CMobile::CMobile(int id){
    GUID = id;
    
    APN.Set_Property_Control(this, &CMobile::GetAPN, &CMobile::SetAPN, &CMobile::SetAPNToBuffer);
	Authentication.Set_Property_Control(this, &CMobile::GetAuthenication, &CMobile::SetAuthenication, &CMobile::SetAuthenicationToBuffer);
	Username.Set_Property_Control(this, &CMobile::GetUsername, &CMobile::SetUsername, &CMobile::SetUsernameToBuffer);
	Password.Set_Property_Control(this, &CMobile::GetPassword, &CMobile::SetPassword, &CMobile::SetPasswordToBuffer);
    
    CellularMode.Set_Class(this);
	SignalStrength.Set_Class(this);
	ServiceProvider.Set_Class(this);
    CellularMode.Set_Get(&CMobile::GetCellularMode);
	SignalStrength.Set_Get(&CMobile::GetSignalStength);
	ServiceProvider.Set_Get(&CMobile::GetServiceProvider);
}
CMobile::CMobile(){
    APN.Set_Property_Control(this, &CMobile::GetAPN, &CMobile::SetAPN, &CMobile::SetAPNToBuffer);
	Authentication.Set_Property_Control(this, &CMobile::GetAuthenication, &CMobile::SetAuthenication, &CMobile::SetAuthenicationToBuffer);
	Username.Set_Property_Control(this, &CMobile::GetUsername, &CMobile::SetUsername, &CMobile::SetUsernameToBuffer);
	Password.Set_Property_Control(this, &CMobile::GetPassword, &CMobile::SetPassword, &CMobile::SetPasswordToBuffer);
    
    CellularMode.Set_Class(this);
	SignalStrength.Set_Class(this);
	ServiceProvider.Set_Class(this);
    CellularMode.Set_Get(&CMobile::GetCellularMode);
	SignalStrength.Set_Get(&CMobile::GetSignalStength);
	ServiceProvider.Set_Get(&CMobile::GetServiceProvider);
}
int CMobile::GetGUID(){
    return GUID;
}
string CMobile::GetCellularMode(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_cellular_mode);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_cellular_mode);
        _Mode = temp;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _Mode;
}
string CMobile::GetServiceProvider(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_service_provider);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_service_provider);
        _ServiceProvider = temp;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    
    return _ServiceProvider;
}
string CMobile::GetAPN(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_apn);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_apn);
        _APN = temp;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    
    return _APN;
}

EN_AUTH CMobile::GetAuthenication(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_authentication);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_authentication);
        if (temp=="0"){
            _Authentication =  _NONE;
        }
        else if (temp=="1")
            _Authentication =  PAP;
        else
            _Authentication =  CHAR;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    
    return _Authentication;
}

string CMobile::GetUsername(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_cellular_username);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_cellular_username);
        _Username =  temp;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _Username;
}

string CMobile::GetPassword(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_cellular_password);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_cellular_password);
        _Password = temp;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _Password;
}

EN_SIGNAL CMobile::GetSignalStength(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_signal_strength);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_signal_strength);
        //    SignalStrength = temp;
        if (temp=="1")
            return Lv1;
        else if (temp=="2")
            return Lv2;
        else if (temp=="3")
            return Lv3;
        else if (temp=="4")
            return Lv3;
        else
            return Lv0;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return Lv0;
    }
    
}
bool CMobile::SetAPN(string option){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_apn, option);
        string receive = sendAndRecvReq(temp,msgType_ParameterSetReq);
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}
bool CMobile::SetAuthenication(EN_AUTH option){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_authentication, option);
        string receive = sendAndRecvReq(temp,msgType_ParameterSetReq);
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}
bool CMobile::SetUsername(string option){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_cellular_username, option);
        string receive = sendAndRecvReq(temp,msgType_ParameterSetReq);
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}
bool CMobile::SetPassword(string option){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_cellular_password, option);
        string receive = sendAndRecvReq(temp,msgType_ParameterSetReq);
       return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}
bool CMobile::SetAPNToBuffer(string option){
    try {
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        DM.SetParameterToBuffer(UUID_apn, option,  CTemporary::BUFF);
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}
bool CMobile::SetAuthenicationToBuffer(EN_AUTH option){
    try {
        string temp;
        if (option==_NONE)
            temp = "0";
        else if (option==PAP)
            temp = "1";
        else
            temp = "2";
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        DM.SetParameterToBuffer(UUID_authentication, temp,  CTemporary::BUFF);
        
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
    
}
bool CMobile::SetUsernameToBuffer(string option){
    try {
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        DM.SetParameterToBuffer(UUID_cellular_username, option,  CTemporary::BUFF);
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
    
}
bool CMobile::SetPasswordToBuffer(string option){
    try {
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        DM.SetParameterToBuffer(UUID_cellular_password, option,  CTemporary::BUFF);
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}
bool CMobile::SendBuffer(){
    try {
        CTemporary::SendBuffer();
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return true;}