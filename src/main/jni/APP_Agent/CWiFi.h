/**
 *  @brief		It's the data controller for Wi-Fi related.
 *  @details    Setting or Getting Wi-Fi SSID, password, security type ...etc.
 *  @author
 - Kevin Wu--v2.0.0--2015.8.20
 *  @copyright
 .
 * Copyright (C) 2000-2015 ZyXEL Communications, Corp.\n
 * All Rights Reserved.\n
 *
 * ZyXEL Confidential;\n
 * Protected as an unpublished work, treated as confidential,\n
 * and hold in trust and in strict confidence by receiving party.\n
 * Only the employees who need to know such ZyXEL confidential information\n
 * to carry out the purpose granted under NDA are allowed to access.\n
 *\n
 * The computer program listings, specifications and documentation\n
 * herein are the property of ZyXEL Communications, Corp. and shall\n
 * not be reproduced, copied, disclosed, or used in whole or in part\n
 * for any reason without the prior express written permission of\n
 * ZyXEL Communications, Corp.\n
 * @file		CWiFi.h
 */

#ifndef __CWiFi__
#define __CWiFi__

#include <iostream>
#include <vector>
#include "Property.h"
#include "UID_Define.h"

using namespace std;

enum EN_WIFI_SECURITY {
	NONE = 0, WPA, WPA2, WPAPSK, WPA2PSK, WEP64, WEP128, NA
};
enum EN_WIFI_BAND{
    EN_WIFI_BAND_DUAL = 0,
    EN_WIFI_BAND_24G,
    EN_WIFI_BAND_5G
};
typedef struct {
	string PhysAddress;
	string Name;
	string Type;
    string IPAddress;
    bool isBlocking;
} ST_WIFI_CLIENT;
typedef struct {
    int Hours;
    int Minutes;
}ST_WIFI_TIMER;

/// It's the data controller for Wi-Fi related.
class CWiFi {
public:

    Property<ST_WIFI_TIMER, CWiFi, READ_WRITE>Timer;
    Property<string, CWiFi, READ_WRITE>NetworkName;
    Property<EN_WIFI_SECURITY, CWiFi, READ_WRITE>Security;
    Property<vector<ST_WIFI_CLIENT>, CWiFi, READ_ONLY >ClientList;
    Property<bool, CWiFi, READ_WRITE> Enable;
    Property<string, CWiFi, READ_ONLY> SSIDStatus;
    Property<bool, CWiFi, READ_ONLY> SSIDEnable;
    Property<string, CWiFi, READ_WRITE> Password;
    
    CWiFi();
	CWiFi(int GUID);
	//~CWiFi();

	int GetGUID();

    /**
     *  @brief Getting the timer of Guest Wi-Fi.
     *  @return the timer.
     *  \note Available in Feature \"Guest WiFi" ver.902 and later.
     */
    ST_WIFI_TIMER GetTimer();
    bool SetTimer(ST_WIFI_TIMER time);
    bool SetTimerToBuffer(ST_WIFI_TIMER time);
    
    /**
     *	@brief Getting the SSID of Wi-Fi.
     *  @return Getting success or failure.
     */
    string GetNetworkName();
    
    /**
     *	@brief Getting the SSID of Wi-Fi by selected band.
     *	@param band The Wi-Fi frequence band.
     *  @return Getting success or failure.
     */
    string GetNetworkName(enum EN_WIFI_BAND band);
    
    /**
     *	@brief Setting the Wi-Fi SSID.
     *	@param name The Wi-Fi SSID.
     *  @return Setting success or failure.
     */
	bool SetNetworkName(string name);
    
    /**
     *	@brief Setting the Wi-Fi SSID by selected band.
     *	@param band The Wi-Fi frequence band.
     *	@param name The Wi-Fi SSID.
     *  @return Setting success or failure.
     */
    bool SetNetworkName(enum EN_WIFI_BAND band, string name);
    
    /**
     *	@brief Setting the Wi-Fi security type to buffer.
     *	@param name The Wi-Fi SSID.
     *  @return Setting success or not.
     */
    bool SetNetworkNameToBuffer(string name);
    
    /**
     *	@brief Setting the Wi-Fi security type to buffer by selected band.
     *	@param band The Wi-Fi frequence band.
     *	@param name The Wi-Fi SSID.
     *  @return Setting success or not.
     */
    bool SetNetworkNameToBuffer(enum EN_WIFI_BAND band, string name);

    /**
     *	@brief Getting the Wi-Fi security type.
     *  @return Getting success or failure.
     */
	EN_WIFI_SECURITY GetSecurity();
    
    /**
     *	@brief Getting the Wi-Fi security type by selected band.
     *	@param band The Wi-Fi frequence band.
     *  @return Getting success or failure.
     */
    EN_WIFI_SECURITY GetSecurity(enum EN_WIFI_BAND band);

    /**
     *	@brief Setting the Wi-Fi security type.
     *	@param op The Wi-Fi security type.
     *  @return Setting success or not.
     */
	bool SetSecurity(EN_WIFI_SECURITY op);
    
//    /**
//     *	@brief Setting the Wi-Fi security type by selected band.
//     *	@param band The Wi-Fi frequence band.
//     *	@param op The Wi-Fi security type.
//     *  @return Setting success or not.
//     */
//    bool SetSecurity(enum EN_WIFI_BAND band, EN_WIFI_SECURITY op);
    
    /**
     *	@brief Setting the Wi-Fi security type to buffer.
     *	@param op The Wi-Fi security type.
     *  @return Setting success or failure.
     */
    bool SetSecurityToBuffer(EN_WIFI_SECURITY op);
    
//    /**
//     *	@brief Setting the Wi-Fi security type to buffer by selected band.
//     *	@param band The Wi-Fi frequence band.
//     *	@param op The Wi-Fi security type.
//     *  @return Setting success or failure.
//     */
//    bool SetSecurityToBuffer(enum EN_WIFI_BAND band, EN_WIFI_SECURITY op);

    /**
     *	@brief Getting Wi-Fi password.
     *  @return Getting success or failure.
     */
	string GetPassword();
    
    /**
     *	@brief Getting Wi-Fi password by selected band.
     *	@param band The Wi-Fi frequence band.
     *  @return Getting success or failure.
     */
    string GetPassword(enum EN_WIFI_BAND band);
    
    /**
     *	@brief Setting Wi-Fi password.
     *	@param pwd The password.
     *  @return Setting success or failure.
     */
	bool SetPassword(string pwd);
    
    /**
     *	@brief Setting Wi-Fi password by selected band.
     *	@param band The Wi-Fi frequence band.
     *	@param pwd The password.
     *  @return Setting success or failure.
     */
    bool SetPassword(enum EN_WIFI_BAND band, string pwd);
    
    /**
     *	@brief Setting Wi-Fi password to buffer.
     *	@param pwd The password.
     *  @return Setting success or failure.
     */
    bool SetPasswordToBuffer(string pwd);
    
    /**
     *	@brief Setting Wi-Fi password to buffer by selected band.
     *	@param band The Wi-Fi frequence band.
     *	@param pwd The password.
     *  @return Setting success or failure.
     */
    bool SetPasswordToBuffer(enum EN_WIFI_BAND band, string pwd);

    /**
     *	@brief Checking the Wi-Fi status is ON or not .
     *  @return The Wi-Fi status, enable or disable.
     */
	bool IsEnable();
    
    /**
     *	@brief Turn on or off the status of Wi-Fi .
     *	@param op Ture:ON False:OFF
     *  @return Setting success or failure.
     */
	bool SetEnable(bool op);
    
    /**
     *	@brief Turn on or off the status of Wi-Fi to buffer.
     *	@param op Ture:ON False:OFF
     *  @return Setting success or failure.
     */
    bool SetEnableToBuffer(bool op);

    /**
     *	@brief Turn on or off the status of Wi-Fi by selected band.
     *	@param band The Wi-Fi frequence band.
     *	@param op Ture:ON False:OFF
     *  @return Setting success or failure.
     */
    bool SetEnable(enum EN_WIFI_BAND band, bool op);

    /**
     *	@brief Turn on or off the status of Wi-Fi to buffer by selected band.
     *	@param band The Wi-Fi frequence band.
     *	@param op Ture:ON False:OFF
     *  @return Setting success or failure.
     */
    bool SetEnableToBuffer(enum EN_WIFI_BAND band, bool op);

    /**
     *  @brief Getting how many client connect to the device.
     *  @return The totoal clients.
     */
	int GetTotalClients();

    /**
     *  @brief Getting the information and status of client.
     *  @return The list of ST_WIFI_CLIENT
     */
	vector<ST_WIFI_CLIENT> GetClienList();
    
    /**
     *	@brief Getting the Wi-Fi is enable or not.
     *  @return Setting success or failure.
     */
	bool GetSSIDEnable();

    /**
     *	@brief Getting the Wi-Fi is enable or not by selected band.
     *	@param band The Wi-Fi frequence band.
     *  @return Getting success or failure.
     */
	bool GetSSIDEnable(enum EN_WIFI_BAND band);
    /**
     *	@brief Getting the status of Wi-Fi .
     *  @return The status of Wi-Fi.
     */
	string GetSSIDStatus();
    
//	string GetHostActive();
//	string GetHostBlocking();
//  bool SetHostBlocking(int index, bool option);
	bool SendBuffer();
    string SendBufferAndRecvReq();
	//bool ClearBuffer();

	//treeNode bufferNode;
    
    /**
     *  \example Guest_Wi-Fi_timer
     *  This is an example of how to use the Guest Wi-Fi timer.
     *
     *  Setting the timer of Guest Wi-Fi.
     *  \code{.cpp}
     *  ST_WIFI_TIMER guestwifitimer;
     *  guestwifitimer.Hours = 1;
     *  guestwifitimer.Minutes = 30;
     *  CWiFi myWifi = *new CWiFi(GUID_GUEST_WIFI);
     *  //set timer 1 hour 30 minutes.
     *  myWifi.SetTimer(guestwifitimer);
     *  guestwifitimer = myWifi.GetTimer();
     *  hh = guestwifitimer.Hours;
     *  mm = guestwifitimer.Minutes;
     *  \endcode
     
     
     *  Getting the timer of Guest Wi-Fi.
     *  \code{.cpp}
     *  ST_WIFI_TIMER guestwifitimer;
     *  CWiFi myWifi = *new CWiFi(GUID_GUEST_WIFI);
     *  guestwifitimer = myWifi.GetTimer();
     *  hh = guestwifitimer.Hours;
     *  mm = guestwifitimer.Minutes;
     *  \endcode
     *  \note Available in Feature \"Guest WiFi" ver.902 and later.
     */
    
    /**
     *  \example DataContriller_Get_Parameter
     *  This is an example of using the data controller to get parameter.
     *  Using controller::getParameter is equal to controller::Parameter.get();
     *  \code{.cpp}
     //Using CWiFi to get the SSID and Password.
     CWiFi wifi;
     //equl to wifi.GetNetworkName();
     wifi.NetworkName.get();
     //equl to wifi.GetPassword();
     wifi.Password.get();
     *  \endcode
     */
    
    /**
     *  \example DataContriller_Set_Parameter
     *  This is an example of using the data controller to get parameter.\n
     *  Using controller::SetParameter() is equal to controller::Parameter.set();
     *  \code{.cpp}
     string ssid = "my wifi";
     string pwd = "1234";
     
     //equl to wifi.SetNetworkName(ssid);
     wifi.NetworkName.set(ssid);
     //equl to wifi.SetPassword(pwd);
     wifi.Password.set(pwd);
     *  \endcode
     
     *  Some command will loss the connection with gateway, ex: change the setting for Wi-Fi related.\n
     *  Above case would fail to set the password, because the gateway Wi-Fi would reload after recive the first commad "change the SSID".\n
     *  In that casue you could using ther buffer to store the command, the example as below.
     *  \code{.cpp}
     string ssid = "my wifi";
     string pwd = "1234";
     
     //equl to wifi.SetNetworkNameToBuffer(ssid);
     wifi.NetworkName.setToBuffer(ssid);
     //equl to wifi.SetPasswordToBuffer(pwd);
     wifi.Password.setToBuffer(pwd);
     wifi.SendBuffer();

     *  \endcode
     */
    
protected:
	int GUID;
private:
    ST_WIFI_TIMER _Timer;
    string _NetworkName;
    EN_WIFI_SECURITY _Security;
    bool _Enable;
    bool _SSIDEnable;
    string _SSIDStatus;
    string _Password;
    vector<ST_WIFI_CLIENT> _ClientList;
};

#endif /* defined(__CWiFi__) */
