/****************************************************************************/
/*
* Copyright (C) 2000-2013 ZyXEL Communications, Corp.
* All Rights Reserved.
*
* ZyXEL Confidential;
* Protected as an unpublished work, treated as confidential,
* and hold in trust and in strict confidence by receiving party.
* Only the employees who need to know such ZyXEL confidential information
* to carry out the purpose granted under NDA are allowed to access.
*
* The computer program listings, specifications and documentation
* herein are the property of ZyXEL Communications, Corp. and shall
* not be reproduced, copied, disclosed, or used in whole or in part
* for any reason without the prior express written permission of
* ZyXEL Communications, Corp.
*
*/
/****************************************************************************/
//
//  ST_DEVICE.h
//  ZyXEL 4G Airspot
//
//  Created by Kevin on 13/10/7.
//  Copyright (c) 2013年 pan star. All rights reserved.
//

#ifndef __DevicePool__
#define __DevicePool__
#include <vector>
#include "DataType_Define.h"

#define Device_WAH7130			"WAH7130"
#define Device_NBG6716			"NBG6716"
#define Device_AMG1312			"VMG1312-B10B"
#define Device_AMG1202			"AMG1202-T10B"
#define Device_VMG8924			"VMG8924-B10A"

#define DEVICE_HOME_ROUTER_KEYWORD		"NBG"
#define DEVICE_CPE_ROUTER_KEYWORD		"AMG"

enum EN_DEVICE_MODEL_TYPE{
    DEVICE_HOME_ROUTER = 0,
    DEVICE_CPE_ROUTER
};

using namespace std;

class DevicePool{
private:
	static vector<ST_DEVICE> deviceList;
	static int currentDeviceIndex;
    static ST_DEVICE currentDevice;
    static ST_AppFeatureInfo appFeatureInfo;

public:
    static void setAppFeature(ST_AppFeatureInfo appFeature);
    static ST_AppFeatureInfo getAppFeature();
    static void setCurrentDevice(ST_DEVICE);
	static void clearDevice();
	static void addDevice(ST_DEVICE device);
	static vector<ST_DEVICE> getDeviceList();
	static ST_DEVICE getDevice(int i);
	static ST_DEVICE getCurrentDevice();
	static void setCurrentDeviceIndex(int idx);
    static int getCurrentDeviceIndex();
	static int getEncryptType();
    static EN_DEVICE_MODEL_TYPE checkDeviceModelType(string modelName);
    //add 12/9
    //static vector<ST_DEVICE> getNetworkDevices();

};
#endif /* defined(__DevicePool__) */
