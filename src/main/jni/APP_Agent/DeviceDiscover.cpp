/****************************************************************************/
/*
* Copyright (C) 2000-2013 ZyXEL Communications, Corp.
* All Rights Reserved.
*
* ZyXEL Confidential;
* Protected as an unpublished work, treated as confidential,
* and hold in trust and in strict confidence by receiving party.
* Only the employees who need to know such ZyXEL confidential information
* to carry out the purpose granted under NDA are allowed to access.
*
* The computer program listings, specifications and documentation
* herein are the property of ZyXEL Communications, Corp. and shall
* not be reproduced, copied, disclosed, or used in whole or in part
* for any reason without the prior express written permission of
* ZyXEL Communications, Corp.
*
*/
/****************************************************************************/
/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// OpenGL ES 2.0 code
#include <string.h>
//#include <android/log.h>

#include <stdio.h>
#include <stdlib.h>

#include "DeviceDiscover.h"
#include "DevicePool.h"
#include "CSecurity.h"
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include "../Lib/CSocket/CSocket.h"
#include "CUDPSocket.h"
#include "MessageType.h"
#include "CDataMessage.h"
#include "UID_Define.h"
#include "CErrorHandle.h"
#include "CTemporary.h"

#define  LOG_TAG    "DeviceDiscover"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

void setAppFeatureParameter(string receive);
string AgentVersion;
string MessageType;
string MagicNumber;

int Broadcast(){
    if (!UDP_Broadcast()) {
        return false;
    }
	return true;
}
int Login(int index,string password){

    string receive;
    CSocket *socket= CSocket::sharedSocket();
    try {
        socket->CloseSocket();
        ST_DEVICE device = DevicePool::getDevice(index);
        socket->ConnectToHostWithPort(device.IP, device.Port);
        
        if (IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            CTemporary::HEAD = new oneConnectTree;
        }
        //CSecurity::SetKey(password);
        CTemporary::SetKey(password);
        
        cout<<"Key:"<<password<<endl;
        
        string temp = "{\"Device\":{\"X_ZyXEL_Ext\":{\"AppInfo\":{\"MagicNum\":\"Z3704\",\"AppVersion\":1}}}}";
        receive = sendAndRecvReq(temp,msgType_SystemQueryReq);
        
        char *cstr = new char[receive.length() + 1];
        strcpy(cstr, receive.c_str());
        ST_AppFeatureInfo appFeature;
        appFeature = CDataMessage::DeviceSystemResp(cstr);
        DevicePool::setAppFeature(appFeature);
        //setAppFeatureParameter(receive);
        
        DevicePool::setCurrentDeviceIndex(index);
        return true;
        
    } catch (int errorCode) {
        socket->CloseSocket();
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return errorCode;
    } catch (exception){
        cout<<"[NO Device]"<<endl;
        return false;
    }
}
int Login(ST_DEVICE device, std::string password){
    string receive;
    CSocket *socket= CSocket::sharedSocket();
    try {
//        socket->CloseSocket();
        socket->ConnectToHostWithPort(device.IP, device.Port);
        
        if (IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            CTemporary::HEAD = new oneConnectTree;
        }
        //CSecurity::SetKey(password);
        CTemporary::SetKey(password);
        
        cout<<"Key:"<<password<<endl;
        
        string temp = "{\"Device\":{\"X_ZyXEL_Ext\":{\"AppInfo\":{\"MagicNum\":\"Z3704\",\"AppVersion\":1}}}}";
        receive = sendAndRecvReq(temp,msgType_SystemQueryReq);
        
        char *cstr = new char[receive.length() + 1];
        strcpy(cstr, receive.c_str());
        ST_AppFeatureInfo appFeature;
        appFeature = CDataMessage::DeviceSystemResp(cstr);
        DevicePool::setAppFeature(appFeature);
        //setAppFeatureParameter(receive);
        
        //DevicePool::setCurrentDeviceIndex(index);
        DevicePool::setCurrentDevice(device);
        return true;
        
    } catch (int errorCode) {
        socket->CloseSocket();
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return errorCode;
    } catch (exception){
        cout<<"NO Device"<<endl;
        return false;
    }
}

int Logout(){
    CSocket *socket= CSocket::sharedSocket();
    socket->CloseSocket();
    return true;
}

void setAppFeatureParameter(string receive){
    CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
    ST_AppFeatureInfo appFeature;
    string temp;
    DM.ParserData(receive);
    temp = DM.GetParameter(UUID_device_feature_app_version);
    appFeature.AppVersion = temp;
    temp = DM.GetParameter(UUID_device_feature_deviceinfo);
    appFeature.DeviceInfo = atoi(temp.c_str());
    temp = DM.GetParameter(UUID_device_feature_gatewayinfo);
    appFeature.GatewayInfo = atoi(temp.c_str());
    temp = DM.GetParameter(UUID_device_feature_flowinfo);
    appFeature.FlowInfo = atoi(temp.c_str());
    temp = DM.GetParameter(UUID_device_feature_login);
    appFeature.Login = atoi(temp.c_str());
    temp = DM.GetParameter(UUID_device_feature_nettopology);
    appFeature.NetTopology = atoi(temp.c_str());
    temp = DM.GetParameter(UUID_device_feature_devivectrl);
    appFeature.DeviceCtrl = atoi(temp.c_str());
    temp = DM.GetParameter(UUID_device_feature_l2devivectrl);
    appFeature.L2DeviceCtrl = atoi(temp.c_str());
    temp = DM.GetParameter(UUID_device_feature_devicon);
    appFeature.DevIcon = atoi(temp.c_str());
    temp = DM.GetParameter(UUID_device_feature_guestwifi);
    appFeature.GuestWiFi = atoi(temp.c_str());
    temp = DM.GetParameter(UUID_device_feature_wifiautocfg);
    appFeature.WiFiAutoCfg = atoi(temp.c_str());
    temp = DM.GetParameter(UUID_device_feature_wifionoff);
    appFeature.WiFiOnOff = atoi(temp.c_str());
    temp = DM.GetParameter(UUID_device_feature_speedtest);
    appFeature.SpeedTest = atoi(temp.c_str());
    temp = DM.GetParameter(UUID_device_feature_diagnostic);
    appFeature.Diagnostic = atoi(temp.c_str());
    temp = DM.GetParameter(UUID_device_feature_cloudservice);
    appFeature.CloudService = atoi(temp.c_str());
    temp = DM.GetParameter(UUID_device_feature_fwupgrade);
    appFeature.FwUpgrade = atoi(temp.c_str());
    temp = DM.GetParameter(UUID_device_feature_ledonoff);
    appFeature.LEDOnOff = atoi(temp.c_str());
    temp = DM.GetParameter(UUID_device_feature_guestgroup);
    appFeature.GuestWiFiGroup = atoi(temp.c_str());
    DevicePool::setAppFeature(appFeature);
}