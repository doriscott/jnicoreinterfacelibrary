/**
 *  @brief		It's to store some necessary instance class or variable.
 *  @author
 - Kevin Wu--v2.0.0--2015.8.20
 *  @copyright
 .
 * Copyright (C) 2000-2015 ZyXEL Communications, Corp.\n
 * All Rights Reserved.\n
 *
 * ZyXEL Confidential;\n
 * Protected as an unpublished work, treated as confidential,\n
 * and hold in trust and in strict confidence by receiving party.\n
 * Only the employees who need to know such ZyXEL confidential information\n
 * to carry out the purpose granted under NDA are allowed to access.\n
 *\n
 * The computer program listings, specifications and documentation\n
 * herein are the property of ZyXEL Communications, Corp. and shall\n
 * not be reproduced, copied, disclosed, or used in whole or in part\n
 * for any reason without the prior express written permission of\n
 * ZyXEL Communications, Corp.\n
 * @file		CTemporary.h
 */

#ifndef __CTemporary__
#define __CTemporary__

#include <iostream>
#include "../Lib/tr181/tr181Tree.h"
#include "../Lib/tr181/oneConnectTree.h"

/// It's to store some necessary instance class or variable.
class CTemporary{
public:
    static treeNode *HEAD;
    static treeNode *BUFF;
    static string Key;
    static bool SetKey(string pwd);
    static string GetKey();
    static bool ClearBuffer();
    static bool SendBuffer();
    static string SendBufferAndRecvReq();
    CTemporary();
    ~CTemporary();

};
#endif /* defined(__CTemporary__) */
