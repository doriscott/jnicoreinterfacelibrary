/**
 *  @brief		Control the Cipher type for message
 *  @details	Before deliver or receive data, the data need to decrypt or encrypt. This class manage Which cipher to be used.
 *  @author
 - Kevin Wu--v2.0.0--2015.8.20
 *  @copyright
 .
 * Copyright (C) 2000-2015 ZyXEL Communications, Corp.\n
 * All Rights Reserved.\n
 *
 * ZyXEL Confidential;\n
 * Protected as an unpublished work, treated as confidential,\n
 * and hold in trust and in strict confidence by receiving party.\n
 * Only the employees who need to know such ZyXEL confidential information\n
 * to carry out the purpose granted under NDA are allowed to access.\n
 *\n
 * The computer program listings, specifications and documentation\n
 * herein are the property of ZyXEL Communications, Corp. and shall\n
 * not be reproduced, copied, disclosed, or used in whole or in part\n
 * for any reason without the prior express written permission of\n
 * ZyXEL Communications, Corp.\n
 * @file		CSecurity.h
 */


#ifndef __CSecurity__
#define __CSecurity__

#include <iostream>

using namespace std;

/**
 *	@brief Cipher type, default is RC4.
 */
enum EncryptType{
    RC4=0
    //Add other cipher type ..
};

/// It's the security manager for Cipher type.
class CSecurity{
    //static char _msg[6000];
private:
    
    string Key;
    EncryptType EncryptMethod;
public:
    
    
    CSecurity();
    CSecurity(string pwd);
    //~CSecurity();
    bool SetKey(string pwd);
    string GetKey();
    void SetEncryptMethod(EncryptType option);
    EncryptType GetEncryptMethod();
    int EncrypData(int msgType, char *data, int dataLen);
    int DecrypData(int msgType, char *data, int dataLen);
};
#endif /* defined(__CSecurity__) */
