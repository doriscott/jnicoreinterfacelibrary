/****************************************************************************/
/*
 * Copyright (C) 2000-2013 ZyXEL Communications, Corp.
 * All Rights Reserved.
 *
 * ZyXEL Confidential;
 * Protected as an unpublished work, treated as confidential,
 * and hold in trust and in strict confidence by receiving party.
 * Only the employees who need to know such ZyXEL confidential information
 * to carry out the purpose granted under NDA are allowed to access.
 *
 * The computer program listings, specifications and documentation
 * herein are the property of ZyXEL Communications, Corp. and shall
 * not be reproduced, copied, disclosed, or used in whole or in part
 * for any reason without the prior express written permission of
 * ZyXEL Communications, Corp.
 *
 */
/****************************************************************************/
//
//  CEndDevicePool.cpp
//
//  Created by Scott Tseng on 13/11/20.
//  Copyright (c) 2013年 pan star. All rights reserved.
//
#include "CEndDevicePool.h"
#include <string>
#include <sstream>
#include "UID_Define.h"
#include "MessageType.h"
#include "CDataMessage.h"
#include "CJSON.h"
#include "ErrorCode.h"
#include "CErrorHandle.h"
#include "DevicePool.h"
#include "CTemporary.h"
//#include <android/log.h>

#define  LOG_TAG    "CEndDevicePool"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
//static TCPStream *stream;
vector<ST_EndDevice> CEndDevicePool:: _EndDeviceList;
vector<ST_L2FWEndDevice> CEndDevicePool:: _L2FWEndDeviceList;

CEndDevicePool::CEndDevicePool(int id){
	GUID = id;
}
CEndDevicePool::CEndDevicePool(){
//	_myParentalControl = *new CParentalControl();
	GUID = GUID_WIFI;
}
int CEndDevicePool::GetGUID(){
	return GUID;
}

int CEndDevicePool::GetTotalL2FWEndDevice(){
    try {
        string temp;
        string receive;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_fw_num_of_entries, GUID_NONE);
        receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_fw_num_of_entries, GUID_NONE);
        return (int)atoi(temp.c_str());
        
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return 0;
    }
}
vector<ST_L2FWEndDevice> CEndDevicePool::GetL2FWEndDeviceList(){
    try {
        string temp;
        int totall2fwClients;
        
        totall2fwClients = GetTotalL2FWEndDevice();
        if (totall2fwClients==0) {
            _L2FWEndDeviceList.clear();
            return _L2FWEndDeviceList;
        }
        _L2FWEndDeviceList.clear();
        printf("TotalL2FWEndDevice: %d\n", totall2fwClients);
        
        //------------Get client list one by one
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        ST_DEVICE device = DevicePool::getCurrentDevice();
        
        if (IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            for (int i = 1 ; i <= totall2fwClients; i++) {
                
                temp = DM.CreatParameterToString(UUID_l2_fwupgrade_list, GUID_NONE,i);
                
                oneConnectTree *tempT = new oneConnectTree;
                CJSON json = *new CJSON(CTemporary::HEAD);
                json.creatTreeFromJSON(tempT, temp);
                
                //nodesAddParent(tempT);
                //removeNodeByKey(DM_ADDRULE, tempT);
                
                //temp = json.CJSON::genJsonFromTreeWithData(tempT);
                //CJSON::genJsonFromNode(tempT);
                
                string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
                DM.ParserData(receive);
            }
            
            ST_L2FWEndDevice t1;
            for (int i = 1; i <= totall2fwClients; i++) {
                
                t1.ModelName = DM.GetParameter(UUID_l2_fwupgrade_modelname, GUID_NONE,i);
                t1.MAC = DM.GetParameter(UUID_l2_fwupgrade_mac, GUID_NONE,i);
                string strfwstate = DM.GetParameter(UUID_l2_fwupgrade_newstate, GUID_WIFI,i);
                t1.FWUpgradeState = atoi(strfwstate.c_str());
                t1.NewFWUpgradeVersion = DM.GetParameter(UUID_l2_fwupgrade_newversion, GUID_WIFI,i);
                t1.NewFWUpgradeDate = DM.GetParameter(UUID_l2_fwupgrade_newdate, GUID_WIFI,i);
                
                if (t1.MAC!="0") {
                    _L2FWEndDeviceList.push_back(t1);
                }
            }
        }
        
    } catch (int errorCode) {
        if (errorCode!=ERR_CDATA_CANT_FIND_PARAMETER) {
            CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
            errorHandle->handleErrorOccur(errorCode);
        }
    }
    return _L2FWEndDeviceList;
}

int CEndDevicePool::GetTotalEndDevice(){
	try {
		string temp;
		string receive;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
		temp = DM.CreatParameterToString(UUID_host_num, GUID_NONE);
		receive = sendAndRecvReq(temp,msgType_ParameterGetReq);

		DM.ParserData(receive);
		temp = DM.GetParameter(UUID_host_num, GUID_NONE);
		return (int)atoi(temp.c_str());

	} catch (int errorCode) {
		CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
		errorHandle->handleErrorOccur(errorCode);
		return 0;
	}

}
vector<ST_EndDevice> CEndDevicePool::GetEndDeviceList(){
    //CParentalControl _myParentalControl = *new CParentalControl();
    try {
        string temp;
        int totalClients;
        
        totalClients = GetTotalEndDevice();
        if (totalClients==0) {
            _EndDeviceList.clear();
            return _EndDeviceList;
        }
        _EndDeviceList.clear();
        printf("TotalEndDevice: %d\n",totalClients);
        
//------------Get client list one by one
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        ST_DEVICE device = DevicePool::getCurrentDevice();
        
        for (int i = 1 ; i <= totalClients; i++) {
            
            temp = DM.CreatParameterToString(UUID_end_device_list, GUID_NONE,i);
            
            treeNode *tempT = new treeNode;
            CJSON json = *new CJSON(CTemporary::HEAD);
            json.creatTreeFromJSON(tempT, temp);
            
            nodesAddParent(tempT);
            removeNodeByKey(DM_ADDRULE, tempT);
            
//            if (IS_SUPPORT_ONE_CONNECT(device.ModelName)) {
//                //            if (device.ModelName == Device_AMG1312 || device.ModelName == Device_AMG1202) {
//                //                removeNodeByKey(DM_L2STA_WIFI, tempT);
//                //                removeNodeByKey(DM_CONTROLLABLEMASK, tempT);
//                //            }
//                if (device.ModelName == Device_NBG6716) {   //Sharon add
//                    removeNodeByKey(DM_L2STA_AUTOCONFIG, tempT);
//                
//                    removeNodeByKey(DM_CAPABILITYTYPE, tempT);
//                    removeNodeByKey(DM_CONNECTIONTYPE, tempT);
//                    removeNodeByKey(DM_DEVICENAME_USER_DEFINED, tempT);
//                    removeNodeByKey(DM_RHYRATE, tempT);
//                    removeNodeByKey(DM_SOFTWAREVERSION_HOST, tempT);
//                    removeNodeByKey(DM_NEIGHBOR, tempT);
//                }
//            } else {
//                removeNodeByKey(DM_L2STA_WIFI, tempT);
//                removeNodeByKey(DM_CONTROLLABLEMASK, tempT);
//                removeNodeByKey(DM_L2STA_AUTOCONFIG, tempT);
//                removeNodeByKey(DM_WIFI_SIGNALSTRENGTH, tempT);
//            }
            
            
            temp = json.CJSON::genJsonFromTreeWithData(tempT);
            //CJSON::genJsonFromNode(tempT);
            
            
            string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
            
            DM.ParserData(receive);
        }
//-------------
        /*  Get all client lists
         CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
         temp = DM.CreatParameterToString(UUID_end_device_list, GUID_WIFI);
         
         treeNode *tempT = new treeNode;
         CJSON json = *new CJSON(CTemporary::HEAD);
         json.creatTreeFromJSON(tempT, temp);
         
         nodesAddParent(tempT);
         removeNodeByKey(DM_ADDRULE, tempT);
         
         ST_DEVICE device = DevicePool::getCurrentDevice();
         if (device.ModelName.find(DEVICE_HOME_ROUTER) != std::string::npos) {
         //            if (device.ModelName == Device_AMG1312 || device.ModelName == Device_AMG1202) {
         //                removeNodeByKey(DM_L2STA_WIFI, tempT);
         //                removeNodeByKey(DM_CONTROLLABLEMASK, tempT);
         //            }
            if (device.ModelName == Device_NBG6716) {   //Sharon add
            removeNodeByKey(DM_L2STA_AUTOCONFIG, tempT);
         
            removeNodeByKey(DM_CAPABILITYTYPE, tempT);
            removeNodeByKey(DM_CONNECTIONTYPE, tempT);
            removeNodeByKey(DM_DEVICENAME_USER_DEFINED, tempT);
            removeNodeByKey(DM_RHYRATE, tempT);
            removeNodeByKey(DM_SOFTWAREVERSION_HOST, tempT);
            removeNodeByKey(DM_NEIGHBOR, tempT);
            }
         } else {
         removeNodeByKey(DM_L2STA_WIFI, tempT);
         removeNodeByKey(DM_CONTROLLABLEMASK, tempT);
         removeNodeByKey(DM_L2STA_AUTOCONFIG, tempT);
         removeNodeByKey(DM_WIFI_SIGNALSTRENGTH, tempT);
         removeNodeByKey(DM_L2STA_WIFI, tempT);
         }
         
         
         temp = json.CJSON::genJsonFromTreeWithData(tempT);
         //CJSON::genJsonFromNode(tempT);
         
         
         string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
         
         DM.ParserData(receive);
         */
        ST_EndDevice t1;
        for (int i = 1; i <= totalClients; i++) {
            t1.Name = DM.GetParameter(UUID_host_name, GUID_NONE,i);
            t1.MAC = DM.GetParameter(UUID_host_mac, GUID_NONE,i);
            string OnlineStatus = DM.GetParameter(UUID_host_active, GUID_WIFI,i);
            if (OnlineStatus=="1") {
                t1.OnlineStatus = true;
            }else{
                t1.OnlineStatus = false;
            }
            
            string isBlocking = DM.GetParameter(UUID_host_block, GUID_WIFI,i);
            if (isBlocking=="Blocking") {
                t1.Block = true;
            }else{
                t1.Block = false;
            }
            
            t1.ConnectInterface = DM.GetParameter(UUID_host_type, GUID_WIFI,i);
            t1.IPAddress = DM.GetParameter(UUID_host_ip, GUID_WIFI,i);
            
            //if (IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            
            if (IS_SUPPORT_ONE_CONNECT(device.ModelName)){
                string l2WifiStatus = DM.GetParameter(UUID_l2_wifi_status, GUID_WIFI,i);
                if (l2WifiStatus=="1") {
                    t1.L2WifiStatus = true;
                }else{
                    t1.L2WifiStatus = false;
                }
                string controlMask = DM.GetParameter(UUID_host_controlmask, GUID_WIFI,i);
                t1.ControlMask = atoi(controlMask.c_str());
                t1.ConnectInterface = DM.GetParameter(UUID_host_type, GUID_WIFI,i);
                string signalStrength = DM.GetParameter(UUID_WIFI_signalstrength, GUID_WIFI,i);
                t1.SignalStrength = atoi(signalStrength.c_str());
                string l2AutoConfigStatus = DM.GetParameter(UUID_l2_auto_config_status, GUID_WIFI,i);
                t1.L2AutoConfigStatus = atoi(l2AutoConfigStatus.c_str());
                string l2AutoConfigEnable = DM.GetParameter(UUID_l2_auto_config_enable, GUID_WIFI,i);
                t1.L2AutoConfigEnable = atoi(l2AutoConfigEnable.c_str());

                //Sharon add
                t1.UserDefineName = DM.GetParameter(UUID_host_name_user_defined, GUID_WIFI,i);
                t1.CapabilityType = DM.GetParameter(UUID_host_capability_type, GUID_WIFI,i);
                t1.ConnectionType = DM.GetParameter(UUID_host_connection_type, GUID_WIFI,i);
                string strRhyRate = DM.GetParameter(UUID_host_phyrate, GUID_WIFI,i);
                t1.PhyRate = atoi(strRhyRate.c_str());
                t1.SoftwareVersion = DM.GetParameter(UUID_host_software_version, GUID_WIFI,i);
                t1.Neighbor = DM.GetParameter(UUID_host_neighbor, GUID_WIFI,i);
                
                string strDLRate = DM.GetParameter(UUID_host_dl_rate, GUID_WIFI,i);
                t1.DLRate = atoi(strDLRate.c_str());
                string strULRate = DM.GetParameter(UUID_host_ul_rate, GUID_WIFI,i);
                t1.ULRate = atoi(strULRate.c_str());

                string strGuestGroup = DM.GetParameter(UUID_host_guest_wifi_group, GUID_WIFI,i);
                t1.GuestWiFiGroup = atoi(strGuestGroup.c_str());

                string strAutoConfigApprove = DM.GetParameter(UUID_host_autoconfig_approve, GUID_WIFI, i);
                t1.AutoConfigApprove = atoi(strAutoConfigApprove.c_str());

                t1.Manufacturer = DM.GetParameter(UUID_host_manufacturer, GUID_WIFI, i);
                string strRssi = DM.GetParameter(UUID_host_rssi, GUID_WIFI, i);
                t1.Rssi = atoi(strRssi.c_str());

                string strBand = DM.GetParameter(UUID_host_band, GUID_WIFI, i);
                t1.Band = atoi(strBand.c_str());

                string strLinkRate24G = DM.GetParameter(UUID_host_linkrate24g, GUID_WIFI, i);
                t1.LinkRate24G = atoi(strLinkRate24G.c_str());

                string strLinkRate5G = DM.GetParameter(UUID_host_linkrate5g, GUID_WIFI, i);
                t1.LinkRate5G = atoi(strLinkRate5G.c_str());

                string strChannel24G = DM.GetParameter(UUID_host_channel24g, GUID_WIFI, i);
                t1.Channel24G = atoi(strChannel24G.c_str());

                string strChannel5G = DM.GetParameter(UUID_host_channel5g, GUID_WIFI, i);
                t1.Channel5G = atoi(strChannel5G.c_str());

                string tmp = DM.GetParameter(UUID_host_type, GUID_NONE,i);
                t1.hostType = atoi(tmp.c_str());
                if (t1.hostType <1) {
                    t1.hostType = 1;
                }
            }
            
            if (t1.MAC!="0") {
                _EndDeviceList.push_back(t1);
            }
        }
        
        for(int i = 0; i < _EndDeviceList.size(); i++) {
            if(_EndDeviceList.at(i).MAC != "Blocking"){
                _NonBlockList.push_back(_EndDeviceList.at(i));
            } else {
                _BlockList.push_back(_EndDeviceList.at(i));
            }
        }
    } catch (int errorCode) {
        if (errorCode!=ERR_CDATA_CANT_FIND_PARAMETER) {
            CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
            errorHandle->handleErrorOccur(errorCode);
        }
    }
    return _EndDeviceList;
}
ST_EndDevice CEndDevicePool::GetEndDevice(string mac){
	vector<ST_EndDevice> clientList;
	clientList = GetEndDeviceList();
	ST_EndDevice client;
	for(int i = 0; i < clientList.size(); i++) {
		if(clientList.at(i).MAC == mac) {
			client = clientList.at(i);
		}
	}
	return client;
}

vector<ST_EndDevice> CEndDevicePool::GetBlockList(){
	GetEndDeviceList();
	return _BlockList;
}

vector<ST_EndDevice> CEndDevicePool::GetNonBlockList(){
	GetEndDeviceList();
	return _NonBlockList;
}

bool CEndDevicePool::SetBlock(string mac){
	//	GetEndDeviceList();
	//string mac = EndDeviceList.at(index - 1).MAC;
	//LOGI("MAC = %s\n", mac.c_str());
    
    try {
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        string temp = DM.CreatParameterToString(UUID_block, mac);
        sendAndRecvReq(temp, msgType_ParameterSetReq);
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
    
}
bool CEndDevicePool::SetBlockToBuffer(string mac){
    //	GetEndDeviceList();
    //string mac = EndDeviceList.at(index - 1).MAC;
    //LOGI("MAC = %s\n", mac.c_str());
    try {
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        DM.SetParameterToBuffer(UUID_block, mac,  CTemporary::BUFF);
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
    

}
bool CEndDevicePool::SetNonBlock(string mac){
    //string mac = EndDeviceList.at(index - 1).MAC;
	//LOGI("MAC = %s\n", mac.c_str());
    try {
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        string temp = DM.CreatParameterToString(UUID_non_block, mac);
        sendAndRecvReq(temp, msgType_ParameterSetReq);
        return true;

    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}
bool CEndDevicePool::SetNonBlockToBuffer(string mac){
    try {
        //string mac = EndDeviceList.at(index - 1).MAC;
        //LOGI("MAC = %s\n", mac.c_str());
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        DM.SetParameterToBuffer(UUID_non_block, mac,  CTemporary::BUFF);
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
    
}
bool CEndDevicePool::SetL2DevReboot(string mac){
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        string temp = DM.CreatParameterToString(UUID_l2_reboot, mac);
        sendAndRecvReq(temp, msgType_ParameterSetReq);
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
    
}
bool CEndDevicePool::SetL2DevRebootToBuffer(string mac){
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        DM.SetParameterToBuffer(UUID_l2_reboot, mac,  CTemporary::BUFF);
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
    
}
bool CEndDevicePool::SetL2DevWiFiOn(string mac){
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        string temp = DM.CreatParameterToString(UUID_l2_reboot, mac);
        sendAndRecvReq(temp, UUID_l2_wifion);
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
    
}
bool CEndDevicePool::SetL2DevWiFiOnToBuffer(string mac){
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        DM.SetParameterToBuffer(UUID_l2_wifion, mac,  CTemporary::BUFF);
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
    
}
bool CEndDevicePool::SetL2DevWiFiOff(string mac){
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        string temp = DM.CreatParameterToString(UUID_l2_reboot, mac);
        sendAndRecvReq(temp, UUID_l2_wifioff);
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}
bool CEndDevicePool::SetL2DevWiFiOffToBuffer(string mac){
    try {
        ST_DEVICE device = DevicePool::getCurrentDevice();
        if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
            throw ERR_JOB_NO_SUPPORT;
        }
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        DM.SetParameterToBuffer(UUID_l2_wifioff, mac,  CTemporary::BUFF);
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
    
}

vector<ST_ParentalControl> CEndDevicePool::GetParentalControlList(int index) {
	CParentalControl _myParentalControl = *new CParentalControl();
    vector<ST_ParentalControl> TotalPCProfiles;
    vector<ST_ParentalControl> devicePCProfiles;
	try {
		TotalPCProfiles = _myParentalControl.GetProfileList();
		//	TotalPCProfiles = _myParentalControl.ST_ParentalControlList;
		string mac = _EndDeviceList.at(index - 1).MAC;
		string tmp_mac = mac;
		string mac_append = tmp_mac.append(",");
//		LOGI("mac = %s\n", mac.c_str());

		for(int i = 0; i < TotalPCProfiles.size(); i++){
			if(TotalPCProfiles.at(i).EndDeviceMAC == mac) {
				devicePCProfiles.push_back(TotalPCProfiles.at(i));
			} else if(TotalPCProfiles.at(i).EndDeviceMAC == mac_append) {
				devicePCProfiles.push_back(TotalPCProfiles.at(i));
			} else {
				char *delimitingMAC;
				const char *delimiter = ",";
				char *pcpMAC = new char[TotalPCProfiles.at(i).EndDeviceMAC.size() + 1];
				std::copy(TotalPCProfiles.at(i).EndDeviceMAC.begin(), TotalPCProfiles.at(i).EndDeviceMAC.end(), pcpMAC);
				pcpMAC[TotalPCProfiles.at(i).EndDeviceMAC.size()] = '\0';
				delimitingMAC = strtok(pcpMAC, delimiter);
				while(delimitingMAC != NULL) {
//					LOGI("%s\n", delimitingMAC);
					if(delimitingMAC == mac) {
						devicePCProfiles.push_back(TotalPCProfiles.at(i));
					}
					delimitingMAC = strtok(NULL, delimiter);
				}
				delete[] pcpMAC;
			}
		}
	} catch (int errorCode) {
		CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
		errorHandle->handleErrorOccur(errorCode);
	}
    return devicePCProfiles;
}

bool CEndDevicePool::AddPcpRule(string mac) {
	try {
		int index = 0;
		string index_str;
		//	vector<ST_EndDevice> device_list = EndDeviceList;
		for(int i = 0; i < _EndDeviceList.size(); i++) {
			//			LOGI("Device List MAC = %s\n", _EndDeviceList.at(i).MAC.c_str());
			if(_EndDeviceList.at(i).MAC == mac) {
				index = i+1;
				//				LOGI("Find MAC=%s\n", _EndDeviceList.at(i).MAC.c_str());
			}
		}
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
		DM.SetParameterToBuffer(UUID_add_profile, "1",  CTemporary::BUFF, index);
		return true;
	} catch (int errorCode) {
		CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
		errorHandle->handleErrorOccur(errorCode);
		return false;
	}
}
bool CEndDevicePool::SetName(string mac, string newName){
    ST_DEVICE device = DevicePool::getCurrentDevice();
    if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
        throw ERR_JOB_NO_SUPPORT;
    }
    try {
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        DM.SetParameterToBuffer(UUID_host_changename_mac, mac,  CTemporary::BUFF);
        DM.SetParameterToBuffer(UUID_host_changename_name, newName,  CTemporary::BUFF);
        DM.SendBuffer(CTemporary::BUFF);
        return true;
    }catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}
bool CEndDevicePool::SetNameToBuffer(string mac, string newName){
    ST_DEVICE device = DevicePool::getCurrentDevice();
    if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
        throw ERR_JOB_NO_SUPPORT;
    }
    try {
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        DM.SetParameterToBuffer(UUID_host_changename_mac, mac,  CTemporary::BUFF);
        DM.SetParameterToBuffer(UUID_host_changename_name, newName,  CTemporary::BUFF);
        return true;
    }catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}

bool CEndDevicePool::SetType(string mac, int newType){
    ST_DEVICE device = DevicePool::getCurrentDevice();
    if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
        throw ERR_JOB_NO_SUPPORT;
    }
    try {
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        DM.SetParameterToBuffer(UUID_host_changehosttype_mac, mac,  CTemporary::BUFF);
        std::stringstream ss;
        ss << newType;
        DM.SetParameterToBuffer(UUID_host_changehosttype_type, ss.str(),  CTemporary::BUFF);
        DM.SendBuffer(CTemporary::BUFF);
        return true;
    }catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}
bool CEndDevicePool::SetTypeToBuffer(string mac, int newType){
    ST_DEVICE device = DevicePool::getCurrentDevice();
    if (!IS_SUPPORT_ONE_CONNECT(device.ModelName)){
        throw ERR_JOB_NO_SUPPORT;
    }
    try {
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        DM.SetParameterToBuffer(UUID_host_changehosttype_mac, mac,  CTemporary::BUFF);
        std::stringstream ss;
        ss << newType;
        DM.SetParameterToBuffer(UUID_host_changehosttype_type, ss.str(),  CTemporary::BUFF);
        return true;
    }catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}

bool CEndDevicePool::SendBuffer(){
    try {
        CTemporary::SendBuffer();
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return true;
}
