/****************************************************************************/
/*
 * Copyright (C) 2000-2013 ZyXEL Communications, Corp.
 * All Rights Reserved.
 *
 * ZyXEL Confidential;
 * Protected as an unpublished work, treated as confidential,
 * and hold in trust and in strict confidence by receiving party.
 * Only the employees who need to know such ZyXEL confidential information
 * to carry out the purpose granted under NDA are allowed to access.
 *
 * The computer program listings, specifications and documentation
 * herein are the property of ZyXEL Communications, Corp. and shall
 * not be reproduced, copied, disclosed, or used in whole or in part
 * for any reason without the prior express written permission of
 * ZyXEL Communications, Corp.
 *
 */
/****************************************************************************/
//
//  CWiFi.cpp
//
//  Created by Kevin on 13/10/8.
//  Copyright (c) 2013年 pan star. All rights reserved.
//

#include "CWiFi.h"
#include <string>
#include <sstream>
#include "UID_Define.h"
#include "MessageType.h"
#include "CJSON.h"
#include "CDataMessage.h"
//#include <android/log.h>
#include "ErrorCode.h"
#include "CErrorHandle.h"
#include "CTemporary.h"

#define  LOG_TAG    "CWiFi"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

//static TCPStream *stream;

CWiFi::CWiFi(int id){
	GUID = id;
    
//    Timer.Set_Property_Control(this, &CWiFi::GetTimer, &CWiFi::SetTimer, &CWiFi::SetTimerToBuffer);
	NetworkName.Set_Property_Control(this, &CWiFi::GetNetworkName, &CWiFi::SetNetworkName, &CWiFi::SetNetworkNameToBuffer);
	Security.Set_Property_Control(this, &CWiFi::GetSecurity, &CWiFi::SetSecurity, &CWiFi::SetSecurityToBuffer);
	Enable.Set_Property_Control(this, &CWiFi::IsEnable, &CWiFi::SetEnable, &CWiFi::SetEnableToBuffer);
	SSIDStatus.Set_Class(this);
	SSIDStatus.Set_Get(&CWiFi::GetSSIDStatus);
	SSIDEnable.Set_Class(this);
	SSIDEnable.Set_Get(&CWiFi::GetSSIDEnable);
	Password.Set_Property_Control(this, &CWiFi::GetPassword, &CWiFi::SetPassword, &CWiFi::SetPasswordToBuffer);
	ClientList.Set_Class(this);
	ClientList.Set_Get(&CWiFi::GetClienList);
}
CWiFi::CWiFi(){
	GUID = GUID_WIFI;

//    Timer.Set_Property_Control(this, &CWiFi::GetTimer, &CWiFi::SetTimer, &CWiFi::SetTimerToBuffer);
	NetworkName.Set_Property_Control(this, &CWiFi::GetNetworkName, &CWiFi::SetNetworkName, &CWiFi::SetNetworkNameToBuffer);
	Security.Set_Property_Control(this, &CWiFi::GetSecurity, &CWiFi::SetSecurity, &CWiFi::SetSecurityToBuffer);
	Enable.Set_Property_Control(this, &CWiFi::IsEnable, &CWiFi::SetEnable, &CWiFi::SetEnableToBuffer);
	SSIDStatus.Set_Class(this);
	SSIDStatus.Set_Get(&CWiFi::GetSSIDStatus);
	SSIDEnable.Set_Class(this);
	SSIDEnable.Set_Get(&CWiFi::GetSSIDEnable);
	Password.Set_Property_Control(this, &CWiFi::GetPassword, &CWiFi::SetPassword, &CWiFi::SetPasswordToBuffer);
	ClientList.Set_Class(this);
	ClientList.Set_Get(&CWiFi::GetClienList);
}
int CWiFi::GetGUID(){
	return GUID;
}
ST_WIFI_TIMER CWiFi::GetTimer(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_timer, GUID);
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        DM.ParserData(receive);
        ST_WIFI_TIMER t1;
        string strhours = DM.GetParameter(UUID_timer_hour, GUID);
        t1.Hours = atoi(strhours.c_str());
        string strmins = DM.GetParameter(UUID_timer_minutes, GUID);
        t1.Minutes = atoi(strmins.c_str());
        _Timer = t1;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    
    return _Timer;
}
bool CWiFi::SetTimer(ST_WIFI_TIMER inStr){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        string option_str = "1";
        temp = DM.SetParameterToBuffer(UUID_enable, GUID_GUEST_WIFI, option_str ,  CTemporary::BUFF);
        std::stringstream hh, mm;
        hh << inStr.Hours;
        temp = hh.str();
        //temp=std::to_string(inStr.Hours);
        DM.SetParameterToBuffer(UUID_timer_hour, GUID_GUEST_WIFI, temp,  CTemporary::BUFF);
        mm << inStr.Minutes;
        temp = mm.str();
        //temp=std::to_string(inStr.Minutes);
        DM.SetParameterToBuffer(UUID_timer_minutes, GUID_GUEST_WIFI, temp,  CTemporary::BUFF);
        temp = DM.SendBuffer(CTemporary::BUFF);
        _Timer = inStr;
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}
bool CWiFi::SetTimerToBuffer(ST_WIFI_TIMER inStr){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        string option_str = "1";
        temp = DM.SetParameterToBuffer(UUID_enable, GUID_GUEST_WIFI, option_str ,  CTemporary::BUFF);
        std::stringstream hh, mm;
        hh << inStr.Hours;
        temp = hh.str();
        //temp=std::to_string(inStr.Hours);
        DM.SetParameterToBuffer(UUID_timer_hour, GUID_GUEST_WIFI, temp,  CTemporary::BUFF);
        mm << inStr.Minutes;
        temp = mm.str();
        //temp=std::to_string(inStr.Minutes);
        DM.SetParameterToBuffer(UUID_timer_minutes, GUID_GUEST_WIFI, temp,  CTemporary::BUFF);
        _Timer = inStr;
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}
string CWiFi::GetNetworkName(){
	try {
		string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
		temp = DM.CreatParameterToString(UUID_ssid, GUID);
		string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
		DM.ParserData(receive);
		temp = DM.GetParameter(UUID_ssid, GUID);
		_NetworkName = temp;
	} catch (int errorCode) {
		CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
		errorHandle->handleErrorOccur(errorCode);
	}

	return _NetworkName;
}
string CWiFi::GetNetworkName(enum EN_WIFI_BAND band){
    try {
        if (GUID == GUID_GUEST_WIFI) {
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        if (band == EN_WIFI_BAND_DUAL) {
            temp = DM.CreatParameterToString(UUID_ssid, GUID);
            string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
            DM.ParserData(receive);
            temp = DM.GetParameter(UUID_ssid, GUID);
        }else{
            
            if (band == EN_WIFI_BAND_24G) {
                DM.SetParameterToBuffer(UUID_ssid, GUID_WIFI, "",  CTemporary::BUFF);
                DM.SetParameterToBuffer(UUID_ssid_freq_band, GUID_WIFI, "24G", CTemporary::BUFF);
            }else{
                DM.SetParameterToBuffer(UUID_ssid, GUID_WIFI_5G, "",  CTemporary::BUFF);
                DM.SetParameterToBuffer(UUID_ssid_freq_band, GUID_WIFI_5G, "5G", CTemporary::BUFF);

            }
            string receive = DM.SendBufferAndRecvReq(CTemporary::BUFF, GET_REQ);
            DM.ParserData(receive);
            if (band == EN_WIFI_BAND_24G)
                temp = DM.GetParameter(UUID_ssid, GUID_WIFI);
            else
                temp = DM.GetParameter(UUID_ssid, GUID_WIFI_5G);
        }
        _NetworkName = temp;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    
    return _NetworkName;
}

bool CWiFi::SetNetworkName(string inStr){
	try {
		string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
		temp = DM.CreatParameterToString(UUID_ssid, GUID, inStr);
		string receive = sendAndRecvReq(temp,msgType_ParameterSetReq);
		_NetworkName = inStr;
		return true;
	} catch (int errorCode) {
		CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
		errorHandle->handleErrorOccur(errorCode);
		return false;
	}
}
bool CWiFi::SetNetworkName(enum EN_WIFI_BAND band, string inStr){
    try {
        if (GUID == GUID_GUEST_WIFI) {
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        if (band == EN_WIFI_BAND_DUAL) {
            temp = DM.CreatParameterToString(UUID_ssid, GUID, inStr);
            string receive = sendAndRecvReq(temp,msgType_ParameterSetReq);
            DM.ParserData(receive);
            temp = DM.GetParameter(UUID_ssid, GUID);
        }else{
            
            if (band == EN_WIFI_BAND_24G) {
                DM.SetParameterToBuffer(UUID_ssid, GUID_WIFI, inStr,  CTemporary::BUFF);
                DM.SetParameterToBuffer(UUID_ssid_freq_band, GUID_WIFI, "24G", CTemporary::BUFF);
            }else{
                DM.SetParameterToBuffer(UUID_ssid, GUID_WIFI_5G, inStr,  CTemporary::BUFF);
                DM.SetParameterToBuffer(UUID_ssid_freq_band, GUID_WIFI_5G, "5G", CTemporary::BUFF);
                
            }
            string receive = DM.SendBufferAndRecvReq(CTemporary::BUFF, SET_REQ);
            DM.ParserData(receive);
            temp = DM.GetParameter(UUID_ssid, GUID);
        }
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}

bool CWiFi::SetNetworkNameToBuffer(string inStr){
	try {
		string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
		temp = DM.SetParameterToBuffer(UUID_ssid, GUID, inStr,  CTemporary::BUFF);
		_NetworkName = inStr;
		return true;
	} catch (int errorCode) {
		CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
		errorHandle->handleErrorOccur(errorCode);
		return false;
	}
}
bool CWiFi::SetNetworkNameToBuffer(enum EN_WIFI_BAND band, string inStr){
    try {
        if (GUID == GUID_GUEST_WIFI) {
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        if (band == EN_WIFI_BAND_DUAL) {
            DM.SetParameterToBuffer(UUID_ssid, GUID_WIFI, inStr,  CTemporary::BUFF);
        }else{
            if (band == EN_WIFI_BAND_24G) {
                DM.SetParameterToBuffer(UUID_ssid, GUID_WIFI, inStr,  CTemporary::BUFF);
                DM.SetParameterToBuffer(UUID_ssid_freq_band, GUID_WIFI, "24G", CTemporary::BUFF);
            }else{
                DM.SetParameterToBuffer(UUID_ssid, GUID_WIFI_5G, inStr,  CTemporary::BUFF);
                DM.SetParameterToBuffer(UUID_ssid_freq_band, GUID_WIFI_5G, "5G", CTemporary::BUFF);
            }
        }
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}
EN_WIFI_SECURITY CWiFi::GetSecurity(){
	try {
		string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
		temp = DM.CreatParameterToString(UUID_security, GUID);
		string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
		DM.ParserData(receive);
		temp = DM.GetParameter(UUID_security, GUID);

        //return (EN_WIFI_SECURITY)atoi(temp.c_str());
        if (temp == "WPA2-Personal") {
        	return WPA2PSK;
        }else if(temp == "WPA-Personal"){
        	return WPAPSK;
        }else if(temp == "WPA"){
			return WPA;
		}else if(temp == "WPA2"){
			return WPA2;
		}else if(temp == "WPAPSK"){
			return WPAPSK;
		}else if(temp == "WPA2PSK"){
			return WPA2PSK;
		}else if(temp == "WPAPSKWPA2PSK"){
			return WPA2PSK;
		}else if(temp == "MIXPSKPSK2"){
			return WPA2PSK;
		}else if(temp == "WEP-64Bits"){
			return WEP64;
		}else if(temp == "WEP-128Bits"){
			return WEP128;
		}else if(temp == "N/A"){
			return NA;
		} else {
			return NONE;
		}
	} catch (int errorCode) {
		CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
		errorHandle->handleErrorOccur(errorCode);
		return NONE;
	}
}

EN_WIFI_SECURITY CWiFi::GetSecurity(enum EN_WIFI_BAND band) {
    try {
        if (GUID == GUID_GUEST_WIFI) {
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        if (band == EN_WIFI_BAND_DUAL) {
            temp = DM.CreatParameterToString(UUID_security, GUID);
            string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
            DM.ParserData(receive);
            temp = DM.GetParameter(UUID_security, GUID);

        }else{
            if (band == EN_WIFI_BAND_24G) {
                DM.SetParameterToBuffer(UUID_security, GUID_WIFI, "",  CTemporary::BUFF);
                DM.SetParameterToBuffer(UUID_security_freq_band, GUID_WIFI, "24G", CTemporary::BUFF);
            }else{
                DM.SetParameterToBuffer(UUID_security, GUID_WIFI_5G, "",  CTemporary::BUFF);
                DM.SetParameterToBuffer(UUID_security_freq_band, GUID_WIFI_5G, "5G", CTemporary::BUFF);
            }
            string receive = DM.SendBufferAndRecvReq(CTemporary::BUFF, GET_REQ);
            DM.ParserData(receive);
            if (band == EN_WIFI_BAND_24G)
                temp = DM.GetParameter(UUID_security, GUID_WIFI);
            else
                temp = DM.GetParameter(UUID_security, GUID_WIFI_5G);
        }
        if (temp == "WPA2-Personal") {
        	return WPA2PSK;
        }else if(temp == "WPA-Personal"){
        	return WPAPSK;
        }else if(temp == "WPA"){
			return WPA;
		}else if(temp == "WPA2"){
			return WPA2;
		}else if(temp == "WPAPSK"){
			return WPAPSK;
		}else if(temp == "WPA2PSK"){
			return WPA2PSK;
		}else if(temp == "WPAPSKWPA2PSK"){
			return WPA2PSK;
		}else if(temp == "MIXPSKPSK2"){
			return WPA2PSK;
		}else if(temp == "WEP-64Bits"){
			return WEP64;
		}else if(temp == "WEP-128Bits"){
			return WEP128;
		}else if(temp == "N/A"){
			return NA;
		} else {
			return NONE;
		}
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return NONE;
    }
}

bool CWiFi::SetSecurity(EN_WIFI_SECURITY option){
	try {
		string temp;
		string select;

		if (option == WPA2PSK) {
			select = "WPA2PSK";
			_Security = WPA2PSK;
		}else{
			select = "none";
			_Security = NONE;
		}
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
		temp = DM.CreatParameterToString(UUID_security, GUID, select);
		cout<<"Setting the WiFi security will disconnect with router, please connect to router later!"<<endl;
		string receive = sendAndRecvReq(temp,msgType_ParameterSetReq);
		return true;
	} catch (int errorCode) {
		CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
		errorHandle->handleErrorOccur(errorCode);
		return false;
	}
}

bool CWiFi::SetSecurityToBuffer(EN_WIFI_SECURITY option){
	try {
		string temp;
		string select;

		if (option == WPA2PSK) {
			select = "WPA2PSK";
			_Security = WPA2PSK;
		}else{
			select = "none";
			_Security = NONE;
		}
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
		temp = DM.SetParameterToBuffer(UUID_security, GUID, select,  CTemporary::BUFF);
		return true;
	} catch (int errorCode) {
		CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
		errorHandle->handleErrorOccur(errorCode);
		return false;
	}
}

string CWiFi::GetPassword(){
	try {
		string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
		temp = DM.CreatParameterToString(UUID_presharekey, GUID);
		string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
		DM.ParserData(receive);
		_Password = DM.GetParameter(UUID_presharekey, GUID);
	} catch (int errorCode) {
		CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
		errorHandle->handleErrorOccur(errorCode);
	}
	return _Password;
}
string CWiFi::GetPassword(enum EN_WIFI_BAND band){
    try {
        if (GUID == GUID_GUEST_WIFI) {
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        if (band == EN_WIFI_BAND_DUAL) {
            temp = DM.CreatParameterToString(UUID_presharekey, GUID);
            string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
            DM.ParserData(receive);
            temp = DM.GetParameter(UUID_presharekey, GUID);
        }else{
            if (band == EN_WIFI_BAND_24G) {
                DM.SetParameterToBuffer(UUID_presharekey, GUID_WIFI, "",  CTemporary::BUFF);
                DM.SetParameterToBuffer(UUID_presharekey_freq_band, GUID_WIFI, "24G", CTemporary::BUFF);
            }else{
                DM.SetParameterToBuffer(UUID_presharekey, GUID_WIFI_5G, "",  CTemporary::BUFF);
                DM.SetParameterToBuffer(UUID_presharekey_freq_band, GUID_WIFI_5G, "5G", CTemporary::BUFF);
            }
            string receive = DM.SendBufferAndRecvReq(CTemporary::BUFF, GET_REQ);
            DM.ParserData(receive);
            if (band == EN_WIFI_BAND_24G)
                temp = DM.GetParameter(UUID_presharekey, GUID_WIFI);
            else
                temp = DM.GetParameter(UUID_presharekey, GUID_WIFI_5G);
        }
        _Password = temp;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _Password;
}
bool CWiFi::SetPassword(string inStr){
	try {
		string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
		temp = DM.CreatParameterToString(UUID_presharekey, GUID, inStr);
		string receive = sendAndRecvReq(temp,msgType_ParameterSetReq);
		return true;
	} catch (int errorCode) {
		CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
		errorHandle->handleErrorOccur(errorCode);
		return false;
	}
}
bool CWiFi::SetPassword(enum EN_WIFI_BAND band, string inStr){
    try {
        if (GUID == GUID_GUEST_WIFI) {
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        if (band == EN_WIFI_BAND_DUAL) {
            temp = DM.CreatParameterToString(UUID_presharekey, GUID, inStr);
            string receive = sendAndRecvReq(temp,msgType_ParameterSetReq);
            DM.ParserData(receive);
            temp = DM.GetParameter(UUID_presharekey, GUID);
        }else{
            
            if (band == EN_WIFI_BAND_24G) {
                DM.SetParameterToBuffer(UUID_presharekey, GUID_WIFI, inStr,  CTemporary::BUFF);
                DM.SetParameterToBuffer(UUID_ssid_freq_band, GUID_WIFI, "24G", CTemporary::BUFF);
            }else{
                DM.SetParameterToBuffer(UUID_presharekey, GUID_WIFI_5G, inStr,  CTemporary::BUFF);
                DM.SetParameterToBuffer(UUID_presharekey_freq_band, GUID_WIFI_5G, "5G", CTemporary::BUFF);
                
            }
            string receive = DM.SendBufferAndRecvReq(CTemporary::BUFF, SET_REQ);
            DM.ParserData(receive);
            temp = DM.GetParameter(UUID_presharekey, GUID);
        }
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}

bool CWiFi::SetPasswordToBuffer(string inStr){
	try {
		string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
		temp = DM.SetParameterToBuffer(UUID_presharekey, GUID, inStr,  CTemporary::BUFF);
		return true;
	} catch (int errorCode) {
		CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
		errorHandle->handleErrorOccur(errorCode);
		return false;
	}
}
bool CWiFi::SetPasswordToBuffer(enum EN_WIFI_BAND band, string inStr){
    try {
        if (GUID == GUID_GUEST_WIFI) {
            throw ERR_JOB_NO_SUPPORT;
        }
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        if (band == EN_WIFI_BAND_DUAL) {
            DM.SetParameterToBuffer(UUID_presharekey, GUID_WIFI, inStr,  CTemporary::BUFF);
        }else{
            
            if (band == EN_WIFI_BAND_24G) {
                DM.SetParameterToBuffer(UUID_presharekey, GUID_WIFI, inStr,  CTemporary::BUFF);
                DM.SetParameterToBuffer(UUID_presharekey_freq_band, GUID_WIFI, "24G", CTemporary::BUFF);
            }else{
                DM.SetParameterToBuffer(UUID_presharekey, GUID_WIFI_5G, inStr,  CTemporary::BUFF);
                DM.SetParameterToBuffer(UUID_presharekey_freq_band, GUID_WIFI_5G, "5G", CTemporary::BUFF);
            }
        }
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}
bool CWiFi::IsEnable(){
	try {
		string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
		temp = DM.CreatParameterToString(UUID_enable, GUID);
		string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
		DM.ParserData(receive);
		temp = DM.GetParameter(UUID_enable, GUID);
		_Enable = (bool)atoi(temp.c_str());
	} catch (int errorCode) {
		CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
		errorHandle->handleErrorOccur(errorCode);
	}
	return _Enable;
}
bool CWiFi::SetEnableToBuffer(bool option){
	try {
		string temp;
		string option_str;
		if(option == 1) {
			option_str = "1";
		} else if(option == 0) {
			option_str = "0";
		}
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
		temp = DM.SetParameterToBuffer(UUID_enable, GUID, option_str ,  CTemporary::BUFF);

		return true;
	} catch (int errorCode) {
		CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
		errorHandle->handleErrorOccur(errorCode);
		return false;
	}
}
bool CWiFi::SetEnable(bool option){
	try {
		string temp;
		string option_str;
		if(option == 1) {
			option_str = "1";
		} else if(option == 0) {
			option_str = "0";
		}
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
		temp = DM.CreatParameterToString(UUID_enable, GUID, option_str);
		string receive = sendAndRecvReq(temp,msgType_ParameterSetReq);
		return true;
	} catch (int errorCode) {
		CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
		errorHandle->handleErrorOccur(errorCode);
		return false;
	}
}

bool CWiFi::SetEnable(enum EN_WIFI_BAND band, bool option){
    try {
		string temp;
		string option_str;
		if(option == 1) {
			option_str = "1";
		} else if(option == 0) {
			option_str = "0";
		}
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        if (band == EN_WIFI_BAND_DUAL) {
		    temp = DM.SetParameterToBuffer(UUID_enable, GUID, option_str ,  CTemporary::BUFF);
		    string receive = DM.SendBufferAndRecvReq(CTemporary::BUFF, SET_REQ);
            DM.ParserData(receive);
            temp = DM.GetParameter(UUID_enable, GUID);
        }else{
            if (band == EN_WIFI_BAND_24G) {
                DM.SetParameterToBuffer(UUID_enable, GUID_WIFI, option_str,  CTemporary::BUFF);
                DM.SetParameterToBuffer(UUID_enable_freq_band, GUID_WIFI, "24G", CTemporary::BUFF);
             }else{
                DM.SetParameterToBuffer(UUID_enable, GUID_WIFI_5G, option_str,  CTemporary::BUFF);
                DM.SetParameterToBuffer(UUID_enable_freq_band, GUID_WIFI_5G, "5G", CTemporary::BUFF);
            }
            string receive = DM.SendBufferAndRecvReq(CTemporary::BUFF, SET_REQ);
            DM.ParserData(receive);
            temp = DM.GetParameter(UUID_enable, GUID);
        }
		return true;
	} catch (int errorCode) {
		CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
		errorHandle->handleErrorOccur(errorCode);
		return false;
	}
}

bool CWiFi::SetEnableToBuffer(enum EN_WIFI_BAND band, bool option){
    try {
		string temp;
		string option_str;
		if(option == 1) {
			option_str = "1";
		} else if(option == 0) {
			option_str = "0";
		}
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        if (band == EN_WIFI_BAND_DUAL) {
		    temp = DM.SetParameterToBuffer(UUID_enable, GUID, option_str ,  CTemporary::BUFF);
        }else{
            if (band == EN_WIFI_BAND_24G) {
                DM.SetParameterToBuffer(UUID_enable, GUID_WIFI, option_str,  CTemporary::BUFF);
                DM.SetParameterToBuffer(UUID_enable_freq_band, GUID_WIFI, "24G", CTemporary::BUFF);
             }else{
                DM.SetParameterToBuffer(UUID_enable, GUID_WIFI_5G, option_str,  CTemporary::BUFF);
                DM.SetParameterToBuffer(UUID_enable_freq_band, GUID_WIFI_5G, "5G", CTemporary::BUFF);
            }
        }
		return true;
	} catch (int errorCode) {
		CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
		errorHandle->handleErrorOccur(errorCode);
		return false;
	}
}

int CWiFi::GetTotalClients(){
	try {
		if (GUID == GUID_GUEST_WIFI) { //GUID = GuestAP
			cout<<"The Guest AP doesn't access client list!"<<endl;
			return 0;
		}
		string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
		temp = DM.CreatParameterToString(UUID_host_num, GUID_WIFI);

		string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);

		DM.ParserData(receive);
		temp = DM.GetParameter(UUID_host_num, GUID_WIFI);
		return (int)atoi(temp.c_str());
	} catch (int errorCode) {
		CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
		errorHandle->handleErrorOccur(errorCode);
		return 0;
	}
}
vector<ST_WIFI_CLIENT> CWiFi::GetClienList(){
	try {
		string temp;
		int totalClients;

		if (GUID == GUID_GUEST_WIFI) { //GUID = GuestAP
			cout<<"The Guest AP doesn't access client list!"<<endl;
			return _ClientList;
		}

		totalClients = GetTotalClients();
		printf("TotalClients: %d\n",totalClients);

        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
		temp = DM.CreatParameterToString(UUID_end_device_list, GUID_WIFI);

		string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
		DM.ParserData(receive);

		ST_WIFI_CLIENT t1;
		for (int i = 1; i <= totalClients; i++) {
			//ST_WIFI_CLIENT t1;
			t1.Name = DM.GetParameter(UUID_host_name, GUID_WIFI,i);
			//cout<<"host:"<<DM.GetParameter("HostName",i)<<endl;
			t1.PhysAddress =DM.GetParameter(UUID_host_mac, GUID_WIFI,i);
			//cout<<"address:"<<DM.GetParameter("PhysAddress",i)<<endl;
			t1.Type = DM.GetParameter(UUID_host_type, GUID_WIFI,i);
			t1.IPAddress = DM.GetParameter(UUID_host_ip, GUID_WIFI,i);
			_ClientList.push_back(t1);
		}
	} catch (int errorCode) {
		CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
		errorHandle->handleErrorOccur(errorCode);
	}
	return _ClientList;
}

bool CWiFi::GetSSIDEnable(){
	try {
		string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
		temp = DM.CreatParameterToString(UUID_ssid_enable, GUID);
		string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
		DM.ParserData(receive);
		temp = DM.GetParameter(UUID_ssid_enable, GUID);
		_SSIDEnable = (bool) atoi(temp.c_str());
	} catch (int errorCode) {
		CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
		errorHandle->handleErrorOccur(errorCode);
	}
	return _SSIDEnable;
}

bool CWiFi::GetSSIDEnable(enum EN_WIFI_BAND band) {
    try {
        if (GUID == GUID_GUEST_WIFI) {
            throw ERR_JOB_NO_SUPPORT;
        }
		string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);

            if (band == EN_WIFI_BAND_DUAL) {
                temp = DM.CreatParameterToString(UUID_enable, GUID);
                string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
                DM.ParserData(receive);
                temp = DM.GetParameter(UUID_enable, GUID);
            }else{
                if (band == EN_WIFI_BAND_24G) {
                    DM.SetParameterToBuffer(UUID_enable, GUID_WIFI, "",  CTemporary::BUFF);
                    DM.SetParameterToBuffer(UUID_enable_freq_band, GUID_WIFI, "24G", CTemporary::BUFF);
                }else{
                    DM.SetParameterToBuffer(UUID_enable, GUID_WIFI_5G, "",  CTemporary::BUFF);
                    DM.SetParameterToBuffer(UUID_enable_freq_band, GUID_WIFI_5G, "5G", CTemporary::BUFF);
                }
                string receive = DM.SendBufferAndRecvReq(CTemporary::BUFF, GET_REQ);
                DM.ParserData(receive);
                if (band == EN_WIFI_BAND_24G)
                    temp = DM.GetParameter(UUID_enable, GUID_WIFI);
                else
                    temp = DM.GetParameter(UUID_enable, GUID_WIFI_5G);
            }
		if(temp == "") {
            _SSIDEnable = 0;
        }else {
            _SSIDEnable = (int)atoi(temp.c_str());
        }
	} catch (int errorCode) {
		CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
		errorHandle->handleErrorOccur(errorCode);
	}
    return _SSIDEnable;
}

string CWiFi::GetSSIDStatus(){
	try {
		string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
		temp = DM.CreatParameterToString(UUID_ssid_status, GUID);
		string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
		DM.ParserData(receive);
		temp = DM.GetParameter(UUID_ssid_status, GUID);
		_SSIDStatus = temp;
	} catch (int errorCode) {
		CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
		errorHandle->handleErrorOccur(errorCode);
	}
	return _SSIDStatus;
}
//
//string CWiFi::GetHostActive(){
	//	string temp;
	//	    temp = CDataMessage::CreatParameterToString("HostActive");
//
//	    const char *receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
//
//	    CDataMessage::ParserData((char*)receive);
//	    temp = CDataMessage::GetParameter("HostActive");
//
//	    return temp;
//}
//
//string CWiFi::GetHostBlocking(){
//	string temp;
//	    temp = CDataMessage::CreatParameterToString("HostBlocking");
//
//	    const char *receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
//
//	    CDataMessage::ParserData((char*)receive);
//	    temp = CDataMessage::GetParameter("HostBlocking");
//
//	    return temp;
//}
//bool CWiFi::SetHostBlocking(int index, bool option){
//	string temp;
//
//    if (option) {
//        CDataMessage::SetParameterToBuffer(UUID_block, "1",  CTemporary::BUFF, index);
//    }else{
//        CDataMessage::SetParameterToBuffer(UUID_block, "0",  CTemporary::BUFF, index);
//    }
//
//    return true;
//}

bool CWiFi::SendBuffer(){
    try {
        CTemporary::SendBuffer();
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return true;
}

string CWiFi::SendBufferAndRecvReq(){
    string temp = CTemporary::SendBufferAndRecvReq();
    return temp;
}

//bool CWiFi::ClearBuffer(){
//    try {
////         CTemporary::BUFF->key = "";
////         CTemporary::BUFF->children.clear();
//        releaseNodes( CTemporary::BUFF);
//        return true;
//    } catch (int errorCode) {
//        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
//        errorHandle->handleErrorOccur(errorCode);
//        return false;
//    }
//}
