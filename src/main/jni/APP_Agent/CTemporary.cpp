//
//  CTemporary.cpp
//  RouterAPP
//
//  Created by Kevin on 2014/5/27.
//  Copyright (c) 2014年 ZyXEL. All rights reserved.
//

#include "CTemporary.h"
#include "CDataMessage.h"
#include "ErrorCode.h"
#include "MessageType.h"
#include "CErrorHandle.h"

treeNode *CTemporary::HEAD = new tr181Tree;
treeNode *CTemporary::BUFF = new treeNode;
string CTemporary::Key = "1234";
CTemporary::CTemporary(){
}
bool CTemporary::SetKey(string pwd){
    Key = pwd;
    return true;
}

string CTemporary::GetKey(){
    return Key;
}
bool CTemporary::SendBuffer(){
    CDataMessage DM = *new CDataMessage(new tr181Tree,JSON);
    DM.SendBuffer(BUFF);
    return true;
}
bool CTemporary::ClearBuffer(){
    CDataMessage DM = *new CDataMessage(new tr181Tree,JSON);
    DM.ClearBuffer(BUFF);
    return true;
}
string CTemporary::SendBufferAndRecvReq(){
    CDataMessage DM = *new CDataMessage(new tr181Tree,JSON);
    string temp= DM.SendBufferAndRecvReq(BUFF, SET_REQ);
    return temp;
}
