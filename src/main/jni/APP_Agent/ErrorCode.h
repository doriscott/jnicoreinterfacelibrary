/**
 *  @brief		Representing the error code
 *  @details
 *  @author
 - Kevin Wu--v2.0.0--2015.8.20
 *  @copyright
 .
 * Copyright (C) 2000-2015 ZyXEL Communications, Corp.\n
 * All Rights Reserved.\n
 *
 * ZyXEL Confidential;\n
 * Protected as an unpublished work, treated as confidential,\n
 * and hold in trust and in strict confidence by receiving party.\n
 * Only the employees who need to know such ZyXEL confidential information\n
 * to carry out the purpose granted under NDA are allowed to access.\n
 *\n
 * The computer program listings, specifications and documentation\n
 * herein are the property of ZyXEL Communications, Corp. and shall\n
 * not be reproduced, copied, disclosed, or used in whole or in part\n
 * for any reason without the prior express written permission of\n
 * ZyXEL Communications, Corp.\n
 * @file		ErrorCode.h
 */



/*
 * \page tutorials CF CO Tutorials
 * A page containing Tutorial information.
 * \subpage tutorial1
 *aaaaa
 * \page tutorial1 Tutorial 1
 * A subpage covering Tutorial 4
 * \page test page
 * A page containing Tutorial information.
 */

#ifndef ConnectAPP_ErrorCode_h
#define ConnectAPP_ErrorCode_h

//ablout socket errors
#define ERR_SOCK_CANT_CREATE_SOCK               -1
#define ERR_SOCK_SETSOCKOPT                     -2
#define ERR_SOCK_CONNECT_TIMEOUT                -3
#define ERR_SOCK_SEND_TIMEOUT                   -4
#define ERR_SOCK_RECV_TIMEOUT                   -5

//ablout agent respond errors
#define ERR_AGENT_UNDEFINED_REQ_TYPE	        -101
#define ERR_AGENT_JSON_FORMAT	                -102
#define ERR_AGENT_APP_VER	                    -103
#define ERR_AGENT_MAGIC_NUM                     -104
#define ERR_AGENT_JOBJ_PARAMETER_NOT_FOUND      -105
#define ERR_AGENT_SOCKET_SYS_MGR	            -106
#define ERR_AGENT_WIFI_INVALID_ENCRYPT          -107
#define ERR_AGENT_HEADER_PROTOCOL_VER	        -108
#define ERR_AGENT_HEADER_PAYLOAD_LENGTH         -109
#define ERR_AGENT_HEADER_NOT_SUPPORT_TYPE       -110
#define ERR_AGENT_CELL_SOCKET_SET               -111
#define ERR_AGENT_UCI_INVALID_PACKAGE           -112
#define ERR_AGENT_UCI_INVALID_OPTION            -113
#define ERR_AGENT_UCI_APPLY_FAILED              -114
#define ERR_AGENT_UCI_COMMIT_FAILED             -115
#define ERR_AGENT_JOBJ_INVALID_INTERFACE        -116
#define ERR_AGENT_JOBJ_MAX_LEVEL_INTF           -117
#define ERR_AGENT_FILE_OPEN                     -118
#define ERR_AGENT_PROC_OPEN                     -119
#define ERR_AGENT_RC4KEY                        -120
#define ERR_AGENT_INVALID_STRING                -121
#define ERR_AGENT_READ_ONLY                     -122
#define ERR_AGENT_INTERNET_CONNECTION           -123
#define ERR_AGENT_NO_NEWER_FIRMWARE             -124
#define ERR_AGENT_INUMBER                       -125
#define ERR_AGENT_FAILURE                       -126
#define ERR_AGENT_SET_ONLY                   	-127

//ablout CData class errors (creat json, paser json..etc.)
#define ERR_CDATA_CANT_FIND_DATATYPE            -201
#define ERR_CDATA_CANT_FIND_PARAMETER           -202
#define ERR_CDATA_CANT_FIND_MODUL               -203
#define ERR_CDATA_NO_BUFFER_TO_SEND             -204
#define ERR_JOB_NO_SUPPORT                      -205

#endif
