/****************************************************************************/
/*
* Copyright (C) 2000-2013 ZyXEL Communications, Corp.
* All Rights Reserved.
*
* ZyXEL Confidential;
* Protected as an unpublished work, treated as confidential,
* and hold in trust and in strict confidence by receiving party.
* Only the employees who need to know such ZyXEL confidential information
* to carry out the purpose granted under NDA are allowed to access.
*
* The computer program listings, specifications and documentation
* herein are the property of ZyXEL Communications, Corp. and shall
* not be reproduced, copied, disclosed, or used in whole or in part
* for any reason without the prior express written permission of
* ZyXEL Communications, Corp.
*
*/
/****************************************************************************/
//
//  CWiFiRadio.cpp
//
//  Created by Kevin on 13/10/8.
//  Copyright (c) 2013年 pan star. All rights reserved.
//

#include "CWiFiRadio.h"
#include <string>
#include "UID_Define.h"
#include "MessageType.h"
#include "CDataMessage.h"
//#include <android/log.h>
#include "ErrorCode.h"
#include "CErrorHandle.h"
#include "CTemporary.h"

#define  LOG_TAG    "CWiFiRadio"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

CWiFiRadio::CWiFiRadio(){
    Channel.Set_Property_Control(this, &CWiFiRadio::GetChannel, &CWiFiRadio::SetChannel, &CWiFiRadio::SetChannelToBuffer);
    TXPower.Set_Property_Control(this, &CWiFiRadio::GetTXPower, &CWiFiRadio::SetTXPower, &CWiFiRadio::SetTXPowerToBuffer);
    AutoChannleEnable.Set_Property_Control(this, &CWiFiRadio::GetAutoChannelEnable, &CWiFiRadio::SetAutoChannleEnable, &CWiFiRadio::SetAutoChannleEnableToBuffer);
    TotalRadioNumber.Set_Class(this);
    TotalRadioNumber.Set_Get(&CWiFiRadio::GetTotalRadioNumber);
    MaxBitRate.Set_Class(this);
    MaxBitRate.Set_Get(&CWiFiRadio::GetMaxBitRate);
    OperatingFrequencyBand.Set_Class(this);
    OperatingFrequencyBand.Set_Get(&CWiFiRadio::GetOperatingFrequencyBand);
    OperatingStandards.Set_Class(this);
    OperatingStandards.Set_Get(&CWiFiRadio::GetOperatingStandards);
    RadioStatus.Set_Class(this);
    RadioStatus.Set_Get(&CWiFiRadio::GetRadioStatus);
    RadioEnable.Set_Class(this);
    RadioEnable.Set_Get(&CWiFiRadio::GetRadioEnable);
}
EN_CHANNEL CWiFiRadio::GetChannel(){
    EN_CHANNEL channel;
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_channel, GUID_WIFI);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_channel, GUID_WIFI);
        
        //return (EN_WIFI_SECURITY)atoi(temp.c_str());
        if (temp == "0") {
            channel = Auto;
        }else if(temp == "1"){
            channel = Ch1;
        }else if(temp == "2"){
            channel = Ch2;
        }else if(temp == "3"){
            channel = Ch3;
        }else if(temp == "4"){
            channel = Ch4;
        }else if(temp == "5"){
            channel = Ch5;
        }else if(temp == "6"){
            channel = Ch6;
        }else if(temp == "7"){
            channel = Ch7;
        }else if(temp == "8"){
            channel = Ch8;
        }else if(temp == "9"){
            channel = Ch9;
        }else if(temp == "10"){
            channel = Ch10;
        }else if(temp == "11"){
            channel = Ch11;
        }else if(temp == "12"){
            channel = Ch12;
        }else{
            channel = Ch13;
        }
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    _Channel = channel;
    return _Channel;
}
bool CWiFiRadio::SetChannel(EN_CHANNEL channel){
    try {
        string temp;
        string option_str;
        if (channel == Auto) {
            option_str = "0";
            _Channel = Auto;
        }else if (channel == Ch1){
            option_str = "1";
            _Channel = Ch1;
        }else if (channel == Ch2){
            option_str = "2";
            _Channel = Ch2;
        }else if (channel == Ch3){
            option_str = "3";
            _Channel = Ch3;
        }else if (channel == Ch4){
            option_str = "4";
            _Channel = Ch4;
        }else if (channel == Ch5){
            option_str = "5";
            _Channel = Ch5;
        }else if (channel == Ch6){
            option_str = "6";
            _Channel = Ch6;
        }else if (channel == Ch7){
            option_str = "7";
            _Channel = Ch7;
        }else if (channel == Ch8){
            option_str = "8";
            _Channel = Ch8;
        }else if (channel == Ch9){
            option_str = "9";
            _Channel = Ch9;
        }else if (channel == Ch10){
            option_str = "10";
            _Channel = Ch10;
        }else if (channel == Ch11){
            option_str = "11";
            _Channel = Ch11;
        }else if (channel == Ch12){
            option_str = "12";
            _Channel = Ch12;
        }else if (channel == Ch13){
            option_str = "13";
            _Channel = Ch13;
        }
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_channel, GUID_WIFI, option_str);
        string receive = sendAndRecvReq(temp,msgType_ParameterSetReq);
        
        return false;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}
bool CWiFiRadio::SetChannelToBuffer(EN_CHANNEL channel){
    try {
        string temp;
        if (channel == Auto) {
            temp = "0";
            _Channel = Auto;
        }else if (channel == Ch1){
            temp = "1";
            _Channel = Ch1;
        }else if (channel == Ch2){
            temp = "2";
            _Channel = Ch2;
        }else if (channel == Ch3){
            temp = "3";
            _Channel = Ch3;
        }else if (channel == Ch4){
            temp = "4";
            _Channel = Ch4;
        }else if (channel == Ch5){
            temp = "5";
            _Channel = Ch5;
        }else if (channel == Ch6){
            temp = "6";
            _Channel = Ch6;
        }else if (channel == Ch7){
            temp = "7";
            _Channel = Ch7;
        }else if (channel == Ch8){
            temp = "8";
            _Channel = Ch8;
        }else if (channel == Ch9){
            temp = "9";
            _Channel = Ch9;
        }else if (channel == Ch10){
            temp = "10";
            _Channel = Ch10;
        }else if (channel == Ch11){
            temp = "11";
            _Channel = Ch11;
        }else if (channel == Ch12){
            temp = "12";
            _Channel = Ch12;
        }else if (channel == Ch13){
            temp = "13";
            _Channel = Ch13;
        }
        //    temp = DM.SetParameterToString("Channel",temp);
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        DM.SetParameterToBuffer(UUID_channel, temp, CTemporary::BUFF);
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}
EN_OUTPOWER CWiFiRadio::GetTXPower(){
    EN_OUTPOWER power;
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_transmit_power, GUID_WIFI);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_transmit_power, GUID_WIFI);
        
        //return (EN_WIFI_SECURITY)atoi(temp.c_str());
        if (temp == "100") {
            power = FAR;
        }else{
            power = NEAR;
        }
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    _TXPower = power;
    return _TXPower;
}
bool CWiFiRadio::SetTXPower(EN_OUTPOWER power){
    try {
        string option_str;
        if (power == FAR) {
            option_str = "100";
            _TXPower = FAR;
        }else{
            option_str = "30";
            _TXPower = NEAR;
        }
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        DM.SetParameterToBuffer(UUID_transmit_power, option_str, CTemporary::BUFF);
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}
bool CWiFiRadio::SetTXPowerToBuffer(EN_OUTPOWER power){
    try {
        string temp;
        if (power == FAR) {
            temp = "100";
            _TXPower = FAR;
        }else{
            temp = "30";
            _TXPower = NEAR;
        }
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        DM.SetParameterToBuffer(UUID_transmit_power,temp, CTemporary::BUFF);
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}

int CWiFiRadio::GetTotalRadioNumber(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_radio_num, GUID);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_radio_num, GUID);
        _TotalRadioNumber = atoi(temp.c_str());
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
	return _TotalRadioNumber;
}
string CWiFiRadio::GetMaxBitRate(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_maxbitrate, GUID);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        _MaxBitRate = DM.GetParameter(UUID_maxbitrate, GUID);
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
	return _MaxBitRate;
}

string CWiFiRadio::GetOperatingFrequencyBand(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_opearting_frequency_band, GUID);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        _OperatingFrequencyBand = DM.GetParameter(UUID_opearting_frequency_band, GUID);
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
	return _OperatingFrequencyBand;
}

string CWiFiRadio::GetOperatingStandards(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_opearting_standards, GUID);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        _OperatingStandards = DM.GetParameter(UUID_opearting_standards, GUID);
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
	return _OperatingStandards;
}

string CWiFiRadio::GetRadioStatus(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_radio_status, GUID);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        _RadioStatus = DM.GetParameter(UUID_radio_status, GUID);
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
	return _RadioStatus;
}


bool CWiFiRadio::GetRadioEnable(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_radio_enable, GUID);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_radio_enable, GUID);
        _RadioEnable = atoi(temp.c_str());
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
	return _RadioEnable;
}
bool CWiFiRadio::GetAutoChannelEnable() {
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_radio_autochannel_enable, GUID);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_radio_autochannel_enable, GUID);
        _AutoChannleEnable = atoi(temp.c_str());
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
	return _AutoChannleEnable;
}
bool CWiFiRadio::SetAutoChannleEnable(bool option) {
    try {
        string temp;
        string select;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        
        if(option == 1){
            select = "1";
            _AutoChannleEnable = 1;
        }else{
            select = "0";
            _AutoChannleEnable = 0;
        }
        temp = DM.CreatParameterToString(UUID_radio_autochannel_enable, GUID, select);
        string receive = sendAndRecvReq(temp,msgType_ParameterSetReq);
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
    
}
bool CWiFiRadio::SetAutoChannleEnableToBuffer(bool option){
    try {
        string temp;
        string select;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        
        if(option == 1){
            select = "1";
            _AutoChannleEnable = 1;
        }else{
            select = "0";
            _AutoChannleEnable = 0;
        }
        if (option) {
            temp = DM.SetParameterToBuffer(UUID_radio_autochannel_enable, select, CTemporary::BUFF);
        }else{
            temp = DM.SetParameterToBuffer(UUID_radio_autochannel_enable, select, CTemporary::BUFF);
        }
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}

ST_WiFiRadio CWiFiRadio::GetWiFiRadioStatus() {
    try {
        _WiFiRadioStatus._24gStatus = EN_BANDSTATUS_NotPresent;
        _WiFiRadioStatus._5gStatus = EN_BANDSTATUS_NotPresent;
        _totalnumRadio = GetTotalRadioNumber();
        _WiFiRadioStatus._totalnumRadio = _totalnumRadio;
        string temp,receive;
        int ch = 0;
        temp = "";
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        for (int i = 1 ; i <= _totalnumRadio; i++) {
            temp = DM.CreatParameterToString(UUID_radio_list, GUID_NONE, i);
            oneConnectTree *tempT = new oneConnectTree;
            CJSON json = *new CJSON(CTemporary::HEAD);
            json.creatTreeFromJSON(tempT, temp);
            receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
            DM.ParserData(receive);
        }
        
        for (int i = 1; i <= _totalnumRadio; i++) {
            _OperatingFrequencyBand = DM.GetParameter(UUID_opearting_frequency_band, GUID_NONE, i);
            _RadioStatus = DM.GetParameter(UUID_radio_status, GUID_NONE, i);
            temp = DM.GetParameter(UUID_channel, GUID_NONE, i);
            ch = atoi(temp.c_str());
            if(_OperatingFrequencyBand == "2.4GHz") {
                if (_RadioStatus == "Up") {
                    _WiFiRadioStatus._24gStatus = EN_BANDSTATUS_Up;
                }
                _WiFiRadioStatus._channel24G = ch;
            }else if(_OperatingFrequencyBand == "5GHz") {
                _WiFiRadioStatus._24gStatus = EN_BANDSTATUS_NotPresent;
                if (_RadioStatus == "Up") {
                    _WiFiRadioStatus._5gStatus = EN_BANDSTATUS_Up;
                }
                _WiFiRadioStatus._channel5G = ch;
            }
        }
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _WiFiRadioStatus;
}

bool CWiFiRadio::SendBuffer(){
    try {
        CTemporary::SendBuffer();
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return true;
}
//SCOTT TSENG ADD END

