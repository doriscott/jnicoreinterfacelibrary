/**
 *  @brief		It's the data controller for ZyXEL device.
 *  @details    Getting or setting the information/status of device such as firmware version, router hop ...etc.
 *  @author
 - Kevin Wu--v2.0.0--2015.8.20
 - Sharon Yang--v2.0.1--2015.9.3
 *  @copyright
 .
 * Copyright (C) 2000-2015 ZyXEL Communications, Corp.\n
 * All Rights Reserved.\n
 *
 * ZyXEL Confidential;\n
 * Protected as an unpublished work, treated as confidential,\n
 * and hold in trust and in strict confidence by receiving party.\n
 * Only the employees who need to know such ZyXEL confidential information\n
 * to carry out the purpose granted under NDA are allowed to access.\n
 *\n
 * The computer program listings, specifications and documentation\n
 * herein are the property of ZyXEL Communications, Corp. and shall\n
 * not be reproduced, copied, disclosed, or used in whole or in part\n
 * for any reason without the prior express written permission of\n
 * ZyXEL Communications, Corp.\n
 * @file		CDevice.h
 */


#ifndef __CDevice__
#define __CDevice__

#include <iostream>
#include "Property.h"
#include <vector>

using namespace std;

typedef struct {
    int CPU_Usage;
    string SystemTime;
    int UpTime;
}ST_DeviceInfo;

typedef struct {
    bool HasAvailableVersion;
    string NewFWVersion;
    string NewFWVersionDate;
}ST_DeviceFWInfo;

typedef struct {
    string WanStatus;
    string WanIP;
    int WanPhyRate;
    string WanMAC;
    int WanTX;
    int WanRX;
}ST_WanInfo;

typedef struct {
    string Host;
    string HostAddress;
    string RTTimes;
}ST_RouteHop;

typedef struct {
    string DestIPAddress;
    string DestSubnetMask;
    string GatewayIPAddress;
    string Interface;
}ST_IPV4Forwarding;

typedef struct {
    string Dl_Rate;
    string Ul_Rate;
    bool End;
}ST_SpeedTestInfo;

typedef struct {
    string HostName;
    string HostMAC;
    bool AutoConfig;
}ST_GatewayHostInfo;

typedef struct {
    string HostMAC;
    bool AutoConfig;
}ST_WiFiAutoConfigApproveInfo;

/// It's the data controller for ZyXEL product.
class CDevice{
    
public:
    Property<string, CDevice, READ_ONLY> CurrentSoftwareVersion;
	Property<string, CDevice, READ_ONLY> AvailableVersion;
	Property<string, CDevice, READ_ONLY> UpdateStatus;
    Property<string, CDevice, READ_WRITE> ConfigPassword;
    
    
    CDevice();
    CDevice(int GUID);
	//~CDevice();
    int GetGUID();
    
    /**
     *  Getting the admin password of device.
     *
     *  @return The admin password.
     */
    string GetConfigPassword();
    /**
     *  Setting the admin password of device.
     *
     *  @param pwd The new password.
     *
     *  @return Setting successful or failture.
     */
    bool SetConfigPassword(string pwd);
    /**
     *  Setting the admin password of device to buffer.
     *
     *  @param pwd The new password.
     *
     *  @return Setting successful or failture.
     */
    bool SetConfigPasswordToBuffer(string pwd);
    /**
     *  Getting the firmware version of device.
     *
     *  @return The firmware version.
     */
	string GetCurrentVersion();
	bool CheckVersion();
    /**
     *  Getting the version of available firmware.
     *
     *  @return available firmware version.
     */
	string GetAvailableVersion();
    /**
     *  letting the gateway upgrade firmware immediately.
     *
     *  @return The string represent the status for upgrading firmware.
     *  \warning The operation of firmware upgrade may lose the connection with gateway.
     */
	bool UpdateSoftware();
    /**
     *  Getting the status for the upgrading firmware of gateway.
     *
     *  @return The string represent the status for upgrading firmware.\n
     *  "InProgress" It's mean still on the process of upgrading firmware.\n
     *  "Success"    It's mean Upgrading firmware successfully\n
     *  "Error"      It's mean fail to upgrading firmware fail\n
     *  \note It's supported for old model, not for One Connect.
     */
	string GetUpdateStatus();
    /**
     *  Getting the status for the upgrading firmware of gateway.
     *
     *  @return The int represent the status for upgrading firmware.\n
     *  0 : FW_FAILED\n
     *  1 : FW_SUCCESS\n
     *  2 : FW_DOWNLOAD_FW\n
     *  3 : FW_FINISH_DOWNLOAD_FW\n
     *  4 : FW_RUN_FW_UPGRADE\n
     *  5 : FW_FINISH_FW_UPGRADE\n
     *  \note Available in Feature \"Firmware Upgrade\" ver.1501 and later.
     */
    int GetFWUpdateState();
    /**
     *  Checking has available firmware for gateway.
     *
     *  @return return value description
     */
    bool GetHasAvailableVersion();
    /**
     *  Getting the release date of available firmware.
     *
     *  @return The release date.
     *  \note Available in Feature \"Firmware Upgrade\" ver.1501 and later.
     */
    string GetFWReleaseDate();
    /**
     *  Getting the information of available firmware.
     *
     *  @return The information of available firmware.
     *  \note Available in Feature \"Firmware Upgrade\" ver.1501 and later.
     */
    ST_DeviceFWInfo GetDeviceFWInfo();

//    string GetSerialNumber();
    /**
     *  Getting the IP of gateway for local area network.
     *
     *  @return The IP of gateway.
     */
    string getGatewayLANIP();
    /**
     *  Getting the information of gateway such as system time, CPU usage...etc.
     *
     *  @return The information of gateway.
     */
    ST_DeviceInfo GetDeviceInfo();
    /**
     *  Getting the information of local area network such as total TX/RX, status of internet...etc
     *
     *  @return The information of local area network.
     */
    ST_WanInfo GetWanInfo();
    /**
     *  Getting the result for tracing router.
     *
     *  @return The result for tracing router.
     */
    vector<ST_RouteHop> TraceRouter();
    /**
     *  Getting the result for IPV4 forwarding.
     *
     *  @return The IPV4 forwarding.
     *  \note Available in Feature \"Diagnostic Detecting Tool\" ver.1301 and later.
     */
    vector<ST_IPV4Forwarding> GetIPV4Forwarding();
    /**
     *  Enable or disable the Wi-Fi auto configuration.
     *
     *  @param option True:Enable, False:Disable
     *
     *  @return The setting is successful or not.
     *  \note Available in Feature \"Diagnostic Detecting Tool\" ver.1301 and later.
     */
    bool SetWiFiAutoConfig(bool option);
    /**
     *  Enable or disable the Wi-Fi auto configuration to buffer.
     *
     *  @param option True:Enable, False:Disable
     *
     *  @return The setting is successful or not.
     *  \note Available in Feature \"Wi-Fi Auto-Configuration\" ver.1001 and later.
     */
    bool SetWiFiAutoConfigToBuffer(bool option);
    /**
     *  Getting the status of  Wi-Fi auto configuration
     *
     *  @return True:Enable, False:Disable.
     *  \note Available in Feature \"Wi-Fi Auto-Configuration\" ver.1001 and later.
     */
    bool GetWiFiAutoConfig();
    /**
     *  Set the WIFIConfigFlag is enable or disable.
     *
     *  @param option True:Enable, False:Disable
     *
     *  @return  The setting is successful or not.
     */
    bool SetWiFiConfigFlag(bool option);
    /**
     *  Set AP mode(Register) to Wi-Fi auto configuration.
     *
     *  @param option sec The timer unit:seconds
     *
     *  @return The setting is successful or not.
     */
    bool SetAPWiFiAutoConfig(int sec);
    /**
     *  Enable or disable the Wi-Fi auto configuration for L2 device.
     *
     *  @param HostMAC The MAC of L2 device
     *  @param option  True:Enable, False:Disable
     *
     *  @return The setting is successful or not.
     *  \note Available in Feature \"Wi-Fi Auto-Configuration\" ver.1001 and later.
     */
    bool SetL2DevCtrlAutoConfig(string HostMAC, bool option);
    /**
     *   For L2 device, enable or disable the Wi-Fi auto configuration to buffer.
     *
     *  @param HostMAC The MAC of L2 device
     *  @param option  True:Enable, False:Disable
     *
     *  @return The setting is successful or not.
     *  \note Available in Feature \"Wi-Fi Auto-Configuration\" ver.1001 and later.
     */
    bool SetL2DevCtrlAutoConfigToBuffer(string HostMAC, bool option);
    /**
     *  letting the L2 device upgrade firmware immediately.
     *
     *  @param HostMAC The MAC of L2 device
     *  @param option  Check:0 Upgrade:1
     *
     *  @return Setting sucessful or not
     *  \note Available in Feature \"Firmware Upgrade\" ver.1501 and later.
     *  \warning The operation of firmware upgrade may lose the connection with L2 device(ex.AP extender, repeater...etc).
     */
    bool SetL2DevCtrlFWConfig(string HostMAC, bool option);             //check:0 upgrade:1
    /**
     *  letting the L2 device upgrade firmware immediately.
     *
     *  @param HostMAC The MAC of L2 device
     *  @param option  Check:0 Upgrade:1
     *
     *  @return Setting sucessful or not
     *  \note Available in Feature \"Firmware Upgrade\" ver.1501 and later.
     *  \warning The operation of firmware upgrade may lose the connection with L2 device(ex.AP extender, repeater...etc).
     */
    bool SetL2DevCtrlFWConfigToBuffer(string HostMAC, bool option);     //check:0 upgrade:1
    /**
     Getting the status for the upgrading firmware of L2 device.
     *
     *  @param HostMAC The MAC of L2 device.
     *
     *  @return The int represent the status for upgrading firmware.\n
     *  0 : FW_FAILED\n
     *  1 : FW_SUCCESS\n
     *  2 : FW_DOWNLOAD_FW\n
     *  3 : FW_FINISH_DOWNLOAD_FW\n
     *  4 : FW_RUN_FW_UPGRADE\n
     *  5 : FW_FINISH_FW_UPGRADE\n
     *  \note Available in Feature \"Firmware Upgrade\" ver.1501 and later.
     */
    int GetL2DevCtrlFWState(string HostMAC);
    /**
     Getting the status for the upgrading firmware of L2 device.
     *
     *  @param HostMAC The MAC of L2 device.
     *
     *  @return The int represent the status for upgrading firmware.\n
     *  0 : FW_FAILED\n
     *  1 : FW_SUCCESS\n     
     *  \note Available in Feature \"Firmware Upgrade\" ver.1501 and later.
     */
    bool CheckL2DevCtrlFWUpgradeSuccess(string HostMAC);
    /**
     *  Setting the URL of testing server, the test will stat immediately.
     *
     *  @param URL    The URL of testing server, ex:www.test.com
     *  @param option 1:Download, 0:Upload.
     *
     *  @return Setting successful or not
     *  \note Available in Feature \"Speed Test\" ver.1202 and later.
     */
    bool SetSpeedTest(string URL, bool option);
    /**
     *  Getting the download speed of testing
     *
     *  @return The download speed.
     *  \note Available in Feature \"Speed Test\" ver.1202 and later.
     */
    string GetSpeedTestDownload();
    /**
     *  Getting the upload speed of testing
     *
     *  @return The upload speed.
     *  \note Available in Feature \"Speed Test\" ver.1202 and later.
     */
    string GetSpeedTestUpload();
    /**
     *  Checking the test is finish or not.
     *
     *  @return True:still on process False: finish.
     *  \note Available in Feature \"Speed Test\" ver.1202 and later.
     */
    bool GetSpeedTestEnd();
    
    /**
     *  Reboot the gateway.
     *
     *  @return Setting sucessful or not.
     */
    bool Reboot();
    /**
     *  Getting the description for setting gateway Wi-Fi SSID.
     *
     *  @return The int represent the status for gateway model.\n
     *  0 : default(None)\n
     *  1 : channel\n
     *  2 : cpe\n
     */
    int GetWifiDescription();
    /**
     *  Getting the http server port number on gateway.
     *  @return the int represent the http server port number on gateway
     */
    int GetGwSpeedTestPort();
    /**
     *  Setting the http server enable or disable on gateway
     *  @param option 1:Enable, 0:Disable
     *  @return the staus of setting http server enable or disable
     */
    bool SetGwSpeedTestEnable(bool option);

    /**
     *  Getting the rssi value
     *  @param option mobile device MAC
     *  @return the rssi value
     */
    int GetGateway2ClientRssi(string mobileMAC);


    /**
     *  Getting the gateway host name.
     *
     *  @return host name of the gateway if it is ONE Connect supported gateway;
     *  This api is for AP (PLA5236) point of view to check if there is ONE Connect supported gateway exist
     *  Use this api from Gateway point of view will get "" string result
     */
    string GetGatewayHostName();

    /**
     *  Getting the gateway host mac address.
     *
     *  @return host mac address of the gateway if it is ONE Connect supported gateway;
     *  This api is for AP (PLA5236) point of view to check if there is ONE Connect supported gateway exist
     */
    string GetGatewayHostMAC();

    /**
     *  Getting the gateway auto configuration status.
     *
     *  @return auto configuration status of the gateway if it is ONE Connect supported gateway;
     *  This api is for AP (PLA5236) point of view to check if there is ONE Connect supported gateway exist
     */
    int GetGatewayAutoConfig();


    /**
     *  Getting the gateway host info.
     *
     *  @return ST_GatewayHostInfo in terms of host name, host mac and auto configuration status of 
     *  the gateway if it is ONE Connect supported gateway;
     *  This api is for AP (PLA5236) point of view to check if there is ONE Connect supported gateway exist
     */
    ST_GatewayHostInfo GetGatewayHostInfo();

    /**
     *  Enable or disable the power plug on/off for smart device.
     *
    *  @param protocol The protocol of the smart device
     *  @param ID The ID of smart device
     *  @param option  True:Enable, False:Disable
     *
     *  @return The setting is successful or not.
     */
    bool SetSmartDevCtrlPower(string protocol, string ID, bool option);
        
    /**
     *   For smart device, enable or disable the power plug on/off to buffer.
     *
     *  @param protocol The protocol of  the smart device
     *  @param HostMAC The ID of smart device
     *  @param option  True:Enable, False:Disable
     *
     *  @return The setting is successful or not.
     */
    bool SetSmartDevCtrlPowerToBuffer(string protocol, string ID, bool option);

    /**
     *   For smart device, restart IOT deamon.
     *
     *  @param protocol The protocol of the deamon
     *  @return The setting is successful or not.
     */    
    bool SetSmartDevCtrlDaemon(string protocol);

    /**
     *  Setting WiFi Auto Config Approve Profile List for L2 device.
     *
     *  @param vector of ST_WiFiAutoConfigApproveInfo
     *
     *  @return The setting is successful or not.
     */    
        bool SetWiFiAutoConfigApproveProfileList(vector<ST_WiFiAutoConfigApproveInfo> profile);

    /**
     *  Setting WiFi Auto Config Approve Confirm Message.
     *
     *  @param 1: Authorize or 0: UnAuthorize
     *
     *  @return The setting is successful or not.
     */    
        bool SetWiFiAutoConfigApprove(bool option);

    /**
     *  Getting WiFi Auto Config Approve Check Flag Status.
     *
     *  @param 1: Check or 0: UnCheck
     *
     *  @return The setting is successful or not.
     */    
        bool GetWiFiAutoConfigApprove();
    
    bool SendBuffer();
    
    /**
     *  \example firmware_Upgrade
     *  This is an example of how to use the firmware upgarde.
     *
     *  Upgrading the firmware of gateway
     *  \code{.cpp}
     *  CDevice myDevice = *new CDevice
     *  //Querying the gateway has available firmware or not.
     *  wHas = myDevice.GetHasAvailableVersion();
     *
     *  //exist new firmware or not
     *  if(fwHas) {
     *     //Getting the version of available firmware.
     *     fwversion = myDevice.GetAvailableVersion();
     *     //Getting the release date of available firmware.
     *     fwdate = myDevice.GetFWReleaseDate();
     *     //Upgrading the firmware of gateway.
     *     myDevice->UpdateSoftware();
     *  }
     *     //Tracing the status for the process of upgrading firmware.
     *     fwStat = myDevice.GetFWUpdateState();
     *  \endcode
     
     *  Upgrading the firmware of L2 device.
     *  \code{.cpp}
     *  CEndDevicePool ED = *new CEndDevicePool;
     *
     *  //Getting the information of availible new firmware for L2 devices.
     *  int l2fwNum = ED.GetTotalL2FWEndDevice();
     *  ST_L2FWEndDevice l2FWList = ED.GetL2FWEndDeviceList();
     *
     *  if(l2FWList.size()){
     *     CDevice myDevice = *new CDevice
     *     //Upgrading the firmware of 1st device.
     *     myDevice->SetL2DevCtrlFWConfig(l2FWList.at(0).MAC, 1);
     *  }
     *  \endcode
     If you already know the MAC of L2 device, you could directly upgrade the firmware.
     *  \code{.cpp}
     *  CDevice myDevice = *new CDevice
     *  //Querying the L2 Device has available firmware or not.
     *  wHas = myDevice.GetL2DevCtrlFWState(MAC);
     *
     *  //exist new firmware or not
     *  if(fwHas) {
     *     //Upgrading the firmware of L2 device.
     *     myDevice->SetL2DevCtrlFWConfig(MAC);
     *  }
     *  //Tracing the status for the process of upgrading firmware.
     *  fwStat = myDevice.GetL2DevCtrlFWState();
     *  \endcode
     *  \note Available in Feature \"Firmware Upgrade\" ver.1501 and later.
     *  \warning The operation of firmware upgrade may lose the connection with gateway.
     */
    
    /**
     *  \example Speed_Test_Gateway_to_Internet
     *  This is an example of how to use the Speed Test.
     *
     *  Download test
     *  \code{.cpp}
     *  CDevice myDevice;
     *  string strdl = myDevice.GetSpeedTestDownload();
     *  myDevice.SetSpeedTest("spthc1.seed.net.tw", 0);
     *  sleep(1);
     *  while (!myDevice.GetSpeedTestEnd()) {
     *      sleep(1);
     *      strdl = myDevice.GetSpeedTestDownload();
     *  }
     *  \endcode
     
     *  Upload test
     *  \code{.cpp}
     *  CDevice myDevice;
     *  string strul = myDevice.GetSpeedTestUpload();
     *  myDevice.SetSpeedTest("spthc1.seed.net.tw", 0);
     *  sleep(1);
     *  while (!myDevice.GetSpeedTestEnd()) {
     *      sleep(1);
     *      strul = myDevice.GetSpeedTestUpload();
     *  }
     *  \endcode
     */
protected:
    string _CurrentSoftwareVersion;
	string _AvailableVersion;
    string _AvailableVersionDate;
	string _UpdateStatus;
    string _ConfigPassword;
    string _SerialNumber;
    ST_DeviceInfo _DeviceInfo;
    ST_WanInfo _WanInfo;
    ST_DeviceFWInfo _DeviceFWInfo;
    ST_SpeedTestInfo _SpeedTestInfo;
    vector<ST_RouteHop> _RouterHops;
    vector<ST_IPV4Forwarding> _IPV4Forwardings;
    bool _WiFiAutoConfig;
    int _FWUpgradeState;
    bool _HasAvailableVersion;
    int GUID;
    int _GatewayDescription;
    string _GatewayHostName;
    string _GatewayHostMAC;
    int _GatewayAutoConfig;
    ST_GatewayHostInfo _GatewayHostInfo;
    int _WiFiAutoConfigApproveStatus;
};
#endif /* defined(__CDevice__) */
