/****************************************************************************/
/*
 * Copyright (C) 2000-2013 ZyXEL Communications, Corp.
 * All Rights Reserved.
 *
 * ZyXEL Confidential;
 * Protected as an unpublished work, treated as confidential,
 * and hold in trust and in strict confidence by receiving party.
 * Only the employees who need to know such ZyXEL confidential information
 * to carry out the purpose granted under NDA are allowed to access.
 *
 * The computer program listings, specifications and documentation
 * herein are the property of ZyXEL Communications, Corp. and shall
 * not be reproduced, copied, disclosed, or used in whole or in part
 * for any reason without the prior express written permission of
 * ZyXEL Communications, Corp.
 *
 */
/****************************************************************************/
#ifndef __UID_Define__
#define __UID_Define__

//Group ID
#define GUID_NONE                0x00
#define GUID_WIFI            	 0x01
#define GUID_WIFI_5G           	 0x02
#define GUID_GUEST_WIFI          0x03
#define GUID_3G                  0x04

//Item ID

//Wi-Fi & Guest Wi-Fi
#define UUID_ssid                0x01
#define UUID_presharekey         0x02
#define UUID_security            0x03
#define UUID_enable              0x04
#define UUID_transmit_power      0x05
#define UUID_channel             0x06
#define UUID_gateway_description 0x07
#define UUID_ssid_freq_band     0x08 //2.3
#define UUID_presharekey_freq_band 0x09
#define UUID_enable_freq_band     0x1C
#define UUID_security_freq_band     0x1D

#define UUID_maxbitrate			 0x13
#define UUID_opearting_frequency_band 0x14
#define UUID_opearting_standards 0x15

#define UUID_radio_enable 		 0x16
#define UUID_radio_status	     0x17
#define UUID_radio_num			 0x18
#define UUID_radio_autochannel_enable 0x19
#define UUID_ssid_enable 0x1A
#define UUID_ssid_status 0x1B
//#define UUID_hostnum = 0x1C
#define UUID_radio_list 0x1D

//3G
#define UUID_apn                 0x21
#define UUID_authentication      0x22
#define UUID_cellular_username   0x23
#define UUID_cellular_password   0x24
#define UUID_signal_strength     0x25
#define UUID_cellular_mode       0x26
#define UUID_service_provider    0x27
//3G monitor
#define UUID_auto_reset          0x28
#define UUID_reset_count         0x29
#define UUID_last_reset_date     0x2A
#define UUID_reset_day           0x2B
#define UUID_monthly_quota       0x2C
#define UUID_current_usage       0x2D
#define UUID_bytes_send          0x2E
#define UUID_bytes_received      0x2F
#define UUID_daily_usage_advice  0x30
#define UUID_data_plan_enable    0x31

//Others
#define UUID_device_password     	0x41
#define UUID_device_battery      	0x42
#define UUID_device_battery_charge 	0x43
#define UUID_device_battery_total 	0x44

// firmware version
#define UUID_device_software_version 		0x45
#define UUID_device_serialnumber     		0x40
#define UUID_device_check_software_version 	0x46
#define UUID_device_latest_software_version 0x47
#define UUID_device_update_software 		0x48
#define UUID_device_update_software_status 	0x49
#define UUID_device_deviceinfo              0x4A
#define UUID_device_waninfo                 0x4B
#define UUID_device_reboot                  0x4C
#define UUID_device_fw_upgrade_status       0x4D
#define UUID_device_has_software_version    0x4E
#define UUID_device_latest_software_date    0x4F

// parental control
#define UUID_parentalcontrol_hostmac 					0x50
#define UUID_parentalcontrol_enable 					0x51
#define UUID_parentalcontrol_pcpname 					0x52
#define UUID_parentalcontrol_schedule_days 				0x53
#define UUID_parentalcontrol_schedule_time_start_hour 	0x54
#define UUID_parentalcontrol_schedule_time_start_min 	0x55
#define UUID_parentalcontrol_schedule_time_stop_hour 	0x56
#define UUID_parentalcontrol_schedule_time_stop_min 	0x57
#define UUID_parentalcontrol_num_of_entries 			0x58
#define UUID_all_profile 								0x59
#define UUID_device_profile 							0x5A
#define UUID_modify_profile 							0x5B

#define UUID_add_profile		 						0x5C

// host
#define UUID_host_name 				0x60
#define UUID_host_mac 				0x61
#define UUID_host_active 			0x62
#define UUID_host_type 				0x63
#define UUID_host_block 			0x64
#define UUID_host_ip 				0x65
#define UUID_host_num 				0x66
#define UUID_host_controlmask 		0x67
#define UUID_client_list         	0x68
#define UUID_end_device_list 	 	0x69
#define UUID_non_block_list 	 	0x6A
#define UUID_block_list 	 	 	0x6B
#define UUID_block				 	0x6C
#define UUID_non_block			 	0x6D
#define UUID_client_list_remove		0x6E

#define UUID_WIFI_signalstrength        0x5D
#define UUID_host_changename_mac        0x5E
#define UUID_host_changename_name       0x5F

#define UUID_host_changehosttype_mac    0x7A
#define UUID_host_changehosttype_type   0x7B

// l2 device control
#define UUID_l2_reboot                  0x70
#define UUID_l2_wifion                  0x71
#define UUID_l2_wifioff                 0x72
#define UUID_l2_wifi_status             0x73
#define UUID_l2_auto_config_status      0x74
#define UUID_l2_auto_config_enable      0x75

//WAN info
#define UUID_waninfo                     0x80
#define UUID_waninfo_status              0x81
#define UUID_waninfo_ip                  0x82
#define UUID_waninfo_phyrate             0x83
#define UUID_waninfo_mac                 0x84
#define UUID_waninfo_tx                  0x85
#define UUID_waninfo_rx                  0x86

//Device info
#define UUID_deviceinfo                         0x87
#define UUID_deviceinfo_uptime                  0x88
#define UUID_deviceinfo_systemtime              0x89
#define UUID_deviceinfo_processstatus_cpuusage  0x8A
#define UUID_gateway_lan_ip                     0x8B
#define UUID_device_software_update_ota         0x8C

//Trace Router
#define UUID_tracerouter                        0xA0
#define UUID_tracerouter_state                  0xA1
#define UUID_tracerouter_host                   0xA2
#define UUID_tracerouter_numberoftries          0xA3
#define UUID_tracerouter_timeout                0xA4
#define UUID_tracerouter_maxhopcount            0xA5
#define UUID_tracerouter_numberofroutehops      0xA6

//routehops
#define UUID_tracerouter_routehops              0xA7
#define UUID_tracerouter_routehops_host         0xA8
#define UUID_tracerouter_routehops_hostaddress  0xA9
#define UUID_tracerouter_routehops_rtttime      0xAA
#define UUID_tracerouter_routehops_all          0xAB

//IPV4 forwarding
#define UUID_route_numberofipv4forwarding       0xB0
#define UUID_route_destipaddress                0xB1
#define UUID_route_destsubnetmask               0xB2
#define UUID_route_gatewayipaddress             0xB3
#define UUID_route_interface                    0xB4
#define UUID_route_ipv4forwarding_all           0xB5

//L2 device ctrl auto config
#define UUID_l2ctrl_autoconfig_host             0xB6
#define UUID_l2ctrl_autoconfig_op               0xB7

//Wi-Fi auto config
#define UUID_wifi_autoconfig_op                 0xB8
#define UUID_wifi_wificonfigflag_op             0xB9


// host
#define UUID_host_capability_type               0x91
#define UUID_host_connection_type               0x92
#define UUID_host_name_user_defined             0x93
#define UUID_host_phyrate                       0x94
#define UUID_host_software_version              0x95
#define UUID_host_neighbor                      0x96
#define UUID_host_dl_rate                       0x97
#define UUID_host_ul_rate                       0x98

#define UUID_host_guest_wifi_group              0x99

//data model 2.2 start
//L2 device ctrl fw config
#define UUID_l2ctrl_fwcfg_host_mac              0xB9
#define UUID_l2ctrl_fwcfg_op                    0xBA
#define UUID_l2ctrl_fwcfg_state                 0xBB
#define UUID_l2ctrl_fwcfg                       0xBC
#define UUID_l2ctrl_fwcfg_Success               0xBD

//L2 device firmware upgrade
#define UUID_fw_num_of_entries                  0xC0
#define UUID_l2_fwupgrade_list                  0xC1
#define UUID_l2_fwupgrade_modelname             0xC2
#define UUID_l2_fwupgrade_mac                   0xC3
#define UUID_l2_fwupgrade_newstate              0xC4
#define UUID_l2_fwupgrade_newversion            0xC5
#define UUID_l2_fwupgrade_newdate               0xC6

//Guest  Wi-Fi
#define UUID_timer                              0xC7
#define UUID_timer_hour                         0xC8
#define UUID_timer_minutes                      0xC9

//L2 SpeedTestInfo
#define UUID_l2_speedtestinfo                   0xD0
#define UUID_l2_speedtestinfo_url               0xD1
#define UUID_l2_speedtestinfo_action_type       0xD2
#define UUID_l2_speedtestinfo_end               0xD3
#define UUID_l2_speedtestinfo_ul_rate           0xD4
#define UUID_l2_speedtestinfo_dl_rate           0xD5
//data model 2.2 end

//App FeatureList
#define UUID_device_feature_app_version         0xE0
#define UUID_device_feature_deviceinfo          0xE1
#define UUID_device_feature_gatewayinfo         0xE2
#define UUID_device_feature_flowinfo            0xE3
#define UUID_device_feature_login               0xE4
#define UUID_device_feature_nettopology         0xE5
#define UUID_device_feature_devivectrl          0xE6
#define UUID_device_feature_l2devivectrl        0xE7
#define UUID_device_feature_devicon             0xE8
#define UUID_device_feature_guestwifi           0xE9
#define UUID_device_feature_wifiautocfg         0xEA
#define UUID_device_feature_wifionoff           0xEB
#define UUID_device_feature_speedtest           0xEC
#define UUID_device_feature_diagnostic          0xED
#define UUID_device_feature_cloudservice        0xEE
#define UUID_device_feature_fwupgrade           0xEF
#define UUID_device_feature_ledonoff            0xF0
#define UUID_device_feature_guestgroup          0xF1

//Client to Gateway GwSpeedTestInfo
#define UUID_c2g_speedtestinfo                  0xF2
#define UUID_c2g_speedtestinfo_port             0xF3
#define UUID_c2g_speedtestinfo_enable           0xF4
#define UUID_c2g_speedtestinfo_disable          0xF5

//Gateway to Client RSSI
#define UUID_g2c_rssi                           0xF6
#define UUID_g2c_host_mac                       0xF7

//data model 2.3
//AP auto config
#define UUID_wifi_autoconfigtime                0xF8
#define UUID_wifi_autoconfigtime_mode           0xF9
#define UUID_wifi_autoconfigtime_op             0xFA
#define UUID_wifi_autoconfigtime_time           0xFB

#define UUID_gateway_info_hostname              0xFC
#define UUID_gateway_info_hostmac               0xFD
#define UUID_gateway_info_autocfg               0xFE
#define UUID_gateway_gatewayinfo                0xFF


//Smart Home Device Power Plug control
#define UUID_smart_device_power_plug_op         0xF00
#define UUID_smart_device_power_plug_protocol   0xF01
#define UUID_smart_device_power_plug_id         0xF02
#define UUID_smart_device_power_plug_action     0xF03

// WiFi Auto Config Approve
#define UUID_host_autoconfig_approve            0xF04
#define UUID_wifi_auto_config_approve           0xF05
#define UUID_wifi_auto_config_approve_data      0xF06
#define UUID_wifi_auto_config_approve_option    0xF07
#define UUID_wifi_auto_config_approve_mac       0xF08

#define UUID_host_manufacturer            0xF09
#define UUID_host_rssi                    0xF0A
#define UUID_host_band                    0xF0B
#define UUID_host_linkrate24g             0xF0C
#define UUID_host_linkrate5g              0xF0D
#define UUID_host_channel24g              0xF0E
#define UUID_host_channel5g               0xF0F

#endif /* defined(__UID_Define__) */
