/****************************************************************************/
/*
* Copyright (C) 2000-2013 ZyXEL Communications, Corp.
* All Rights Reserved.
*
* ZyXEL Confidential;
* Protected as an unpublished work, treated as confidential,
* and hold in trust and in strict confidence by receiving party.
* Only the employees who need to know such ZyXEL confidential information
* to carry out the purpose granted under NDA are allowed to access.
*
* The computer program listings, specifications and documentation
* herein are the property of ZyXEL Communications, Corp. and shall
* not be reproduced, copied, disclosed, or used in whole or in part
* for any reason without the prior express written permission of
* ZyXEL Communications, Corp.
*
*/
/****************************************************************************/
//
//  CDataMessage.h
//
//  Created by Kevin on 13/10/7.
//  Copyright (c) 2013年 pan star. All rights reserved.
//

#ifndef __CDataMessage__
#define __CDataMessage__

#include <iostream>
#include <vector>
#include "../Lib/tr181/tr181Tree.h"
#include "CJSON.h"
#include "../Lib/CSocket/CSocket.h"
#include "DataType_Define.h"
//#include "TCPStream.h"

//#define PLATFORM 1
//#define test123(a);\
//{\
//   if(PLATFORM)\
//      printf("%s\n",a);\
//   else\
//      LOGI("%s\n",a);\
//}

#define Default_Interface			"i1"
#define DefaultIndex				1
#define WAH7130_24G_Interface		1
#define NBG6716_24G_Interface		1
#define NBG6716_5G_Interface		5
#define AMG1312_24G_Interface		1
#define AMG1312_5G_Interface		5
#define AMG1202_24G_Interface		1
#define AMG1202_5G_Interface		5
#define Default_24G_Index				1
#define Default_5G_Index				2

using namespace std;

enum EN_FORMAT{
	JSON = 0,
    NJSON
};

enum EN_MSG_TYPE{
    SET_REQ = 0,
    GET_REQ
};

class CDataMessage
{
public:
	//CDataMessage();
	CDataMessage(treeNode* tree, EN_FORMAT formatType);
	//~CDataMessage();
	
    static ST_DEVICE DeviceDiscoverResp(char* tmp);
    static ST_AppFeatureInfo DeviceSystemResp(char* tmp);
    
//    string CreatParameterToString(string tmp);
//    string SetParameterToString(string tmp, string option);
    string CreatParameterToString(int UUID, int GUID);
    string CreatParameterToString(int UUID, int GUID, string option);
    string CreatParameterToString(int UUID);
    string CreatParameterToString(int UUID, string option);
    string CreatParameterToString(int UUID, int GUID, int index);
    
    bool ParserData(string tmp);
    
    string GetParameter(int UUID);
    string GetParameter(int UUID, int GUID);
    string GetParameter(int UUID, int GUID, int index);
//    string GetParameter(int UUID, int index);
//    string GetParameter(string tmp,int index);
    //DATA_TYPE GetParameterType(string );

	bool SetParameterToBuffer(int UUID, string value, treeNode *node);
    bool SetParameterToBuffer(int UUID, int value, treeNode *node);
	bool SetParameterToBuffer(int UUID, int GUID, string value, treeNode *node);
	bool SetParameterToBuffer(int UUID, string value, treeNode *node, int index);
    string GetInterfaceIndex(int UUID, int GUID);
    bool ClearBuffer(treeNode *head);
    bool SendBuffer(treeNode *head);
    string SendBufferAndRecvReq(treeNode *head, EN_MSG_TYPE type);
    
private:
    
	int nInterface;
	int NumberofEntries;
	treeNode* ptreeNode;
    EN_FORMAT FormatType;
    string strAgentVersion;
	string strFormatType;
	static const char *data;
};
string sendAndRecvReq(string temp,int msgType);
vector<string> split(const string &s, const string &seperator);
bool isSupportForOneConnect(string modelName, string supportList);

//treeNode *CJSON::HEAD;
//EN_FORMAT CDataMessage::FormatType;
#endif /* defined(__CDataMessage__) */
