/****************************************************************************/
/*
* Copyright (C) 2000-2013 ZyXEL Communications, Corp.
* All Rights Reserved.
*
* ZyXEL Confidential;
* Protected as an unpublished work, treated as confidential,
* and hold in trust and in strict confidence by receiving party.
* Only the employees who need to know such ZyXEL confidential information
* to carry out the purpose granted under NDA are allowed to access.
*
* The computer program listings, specifications and documentation
* herein are the property of ZyXEL Communications, Corp. and shall
* not be reproduced, copied, disclosed, or used in whole or in part
* for any reason without the prior express written permission of
* ZyXEL Communications, Corp.
*
*/
/****************************************************************************/
//
//  CMobileMonitor.cpp
//
//  Created by Kevin on 13/10/8.
//  Copyright (c) 2013年 pan star. All rights reserved.
//

#include "CMobileMonitor.h"
#include "MessageType.h"
#include <sstream>
#include "UID_Define.h"
#include "CDataMessage.h"
#include "ErrorCode.h"
#include "CErrorHandle.h"
#include "CTemporary.h"

int GetDaysInMonth(int y,int m)
{
    int d;
    int day[]= {31,28,31,30,31,30,31,31,30,31,30,31};
    if (2==m)
    {
        d=((((0==y%4)&&(0!=y%100))||(0==y%400))?29:28);
        //d=(((0==y%4)&&(0!=y%100)||(0==y%400))?29:28);
    }
    else
    {
        d=day[m-1];
        
    }
    return d;
}

CMobileMonitor::CMobileMonitor(){
    AutoReset.Set_Property_Control(this, &CMobileMonitor::GetAutoReset, &CMobileMonitor::SetAutoReset, &CMobileMonitor::SetAutoResetToBuffer);
    CycleResetDate.Set_Property_Control(this, &CMobileMonitor::GetCycleResetDate, &CMobileMonitor::SetCycleResetDate, &CMobileMonitor::SetCycleResetDateToBuffer);
	Quota.Set_Property_Control(this, &CMobileMonitor::GetQuota, &CMobileMonitor::SetQuota, &CMobileMonitor::SetQuotaToBuffer);
    Enable.Set_Property_Control(this, &CMobileMonitor::IsEnable, &CMobileMonitor::SetEnable, &CMobileMonitor::SetEnableToBuffer);
    
	ResetCounter.Set_Class(this);
	LastResetDate.Set_Class(this);
	CurrentUsage.Set_Class(this);
	DataSend.Set_Class(this);
	DataReceived.Set_Class(this);
	DailyUsageAdvice.Set_Class(this);
    
    ResetCounter.Set_Get(&CMobileMonitor::GetResetCounter);
	LastResetDate.Set_Get(&CMobileMonitor::GetLastResetDate);
	CurrentUsage.Set_Get(&CMobileMonitor::GetCurrentUsage);
	DataSend.Set_Get(&CMobileMonitor::GetDataSend);
	DataReceived.Set_Get(&CMobileMonitor::GetDataReceived);
	DailyUsageAdvice.Set_Get(&CMobileMonitor::GetDailyUsageAdvice);
}
bool CMobileMonitor::GetAutoReset(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_auto_reset);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_auto_reset);
        
        if (temp=="disable"){
            _AutoReset = 0;
        }else{
            _AutoReset = 1;
        }
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _AutoReset;
}

int CMobileMonitor::GetResetCounter(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_reset_count);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_reset_count);
        _ResetCounter = atoi(temp.c_str());
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _ResetCounter;
}
string CMobileMonitor::GetLastResetDate(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_last_reset_date);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_last_reset_date);
        _LastResetDate = temp;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _LastResetDate;
}
int CMobileMonitor::GetCycleResetDate(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_reset_day);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_reset_day);
        _CycleResetDate = atoi(temp.c_str());
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _CycleResetDate;
}

int CMobileMonitor::GetQuota(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_monthly_quota);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_monthly_quota);
        _Quota = atoi(temp.c_str());
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _Quota;
}

float CMobileMonitor::GetCurrentUsage(){
    try {
        _CurrentUsage = GetDataReceived()+GetDataSend();
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _CurrentUsage;
}
float CMobileMonitor::GetDataSend(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_bytes_send);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_bytes_send);
        _DataSend = atoi(temp.c_str())/1024;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _DataSend;
    
}
float CMobileMonitor::GetDataReceived(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_bytes_received);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_bytes_received);
        _DataReceived = atoi(temp.c_str())/1024;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _DataReceived;
}
float CMobileMonitor::GetDailyUsageAdvice(){
    try {
        time_t t = time(0);
        char tmp[5];
        strftime( tmp, sizeof(tmp), "%d",localtime(&t) );//"%Y/%m/%d %X %A 本年第%j天 %z"
        int day = atoi(tmp);
        
        strftime( tmp, sizeof(tmp), "%Y",localtime(&t) );
        int year = atoi(tmp);
        strftime( tmp, sizeof(tmp), "%m",localtime(&t) );
        int month = atoi(tmp);
        int monthDays = GetDaysInMonth(year, month);
        
        int resetDay = GetCycleResetDate();
        int remainDay = 0;
        
        if(day>resetDay)
            remainDay = (monthDays-day)+resetDay;
        //remainDay = (30-day)+resetDay;
        else
            remainDay = resetDay - day;
        
        //printf("---------->y:%d,m:%d,md:%d\n",year,month,monthDays);
        //printf("---------->remainDay:%d monthDays:%d resetDay:%d\n",remainDay,monthDays,resetDay);
        _DailyUsageAdvice = (GetQuota() - GetCurrentUsage()) / remainDay;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _DailyUsageAdvice;
}
bool CMobileMonitor::IsEnable(){
    try {
        string temp;
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        temp = DM.CreatParameterToString(UUID_data_plan_enable);
        
        string receive = sendAndRecvReq(temp,msgType_ParameterGetReq);
        
        DM.ParserData(receive);
        temp = DM.GetParameter(UUID_data_plan_enable);
        
        if (temp=="disable"){
            _Enable = 0;
        }else{
            _Enable = 1;
        }
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return _Enable;
}
bool CMobileMonitor::SetAutoReset(bool option){
    try {
        string temp;
        if (option){
            temp = "enable";
            _Enable = 1;
        }else{
            temp = "disable";
            _Enable = 0;
        }
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        DM.CreatParameterToString(UUID_auto_reset, temp);
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}
bool CMobileMonitor::SetAutoResetToBuffer(bool option){
    try {
        string temp;
        if (option){
            temp = "enable";
            _Enable = 1;
        }else{
            temp = "disable";
            _Enable = 0;
        }
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        DM.SetParameterToBuffer(UUID_auto_reset, temp,  CTemporary::BUFF);
        
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}
bool CMobileMonitor::SetCycleResetDate(int option){
    try {
        string tmp;
        stringstream ss(tmp);
        ss << option;
        string temp = ss.str();
        
        //string temp = atoi(option);
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        DM.CreatParameterToString(UUID_reset_day, temp);
        _CycleResetDate = option;
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
    if (option>31 || option<1) {
        cout<<"The parameter of CycleResetDate is incorrect"<<endl;
        return false;
    }
}
bool CMobileMonitor::SetCycleResetDateToBuffer(int option){
    try {
        if (option>31 || option<1) {
            cout<<"The parameter of CycleResetDate is incorrect"<<endl;
            return false;
        }
        string tmp;
        stringstream ss(tmp);
        ss << option;
        string temp = ss.str();
        
        //string temp = atoi(option);
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        DM.SetParameterToBuffer(UUID_reset_day, temp,  CTemporary::BUFF);
        _CycleResetDate = option;
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}
bool CMobileMonitor::SetQuota(int option){
    try {
        string tmp;
        stringstream ss(tmp);
        ss << option;
        string temp = ss.str();
        
        //string temp = std::to_string(option);
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        DM.CreatParameterToString(UUID_monthly_quota, temp);
        _Quota = option;
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}
bool CMobileMonitor::SetQuotaToBuffer(int option){
    try {
        string tmp;
	    stringstream ss(tmp);
	    ss << option;
	    string temp = ss.str();
        
        //string temp = std::to_string(option);
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        DM.SetParameterToBuffer(UUID_monthly_quota, temp,  CTemporary::BUFF);
        _Quota = option;
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}
bool CMobileMonitor::SetEnable(bool option){
    try {
        string temp;
        if (option){
            temp = "enable";
            _Enable = 1;
        }else{
            temp = "disable";
            _Enable = 0;
        }
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        DM.CreatParameterToString(UUID_data_plan_enable, temp);
        
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}
bool CMobileMonitor::SetEnableToBuffer(bool option){
    try {
        string temp;
        if (option){
            temp = "enable";
            _Enable = 1;
        }else{
            temp = "disable";
            _Enable = 0;
        }
        CDataMessage DM = *new CDataMessage(CTemporary::HEAD,JSON);
        DM.SetParameterToBuffer(UUID_data_plan_enable, temp,  CTemporary::BUFF);
        
        return true;
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
        return false;
    }
}
bool CMobileMonitor::SendBuffer(){
    try {
        CTemporary::SendBuffer();
    } catch (int errorCode) {
        CErrorHandle *errorHandle = CErrorHandle::sharedInstance();
        errorHandle->handleErrorOccur(errorCode);
    }
    return true;}