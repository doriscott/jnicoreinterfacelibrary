/****************************************************************************/
/*
* Copyright (C) 2000-2013 ZyXEL Communications, Corp.
* All Rights Reserved.
*
* ZyXEL Confidential;
* Protected as an unpublished work, treated as confidential,
* and hold in trust and in strict confidence by receiving party.
* Only the employees who need to know such ZyXEL confidential information
* to carry out the purpose granted under NDA are allowed to access.
*
* The computer program listings, specifications and documentation
* herein are the property of ZyXEL Communications, Corp. and shall
* not be reproduced, copied, disclosed, or used in whole or in part
* for any reason without the prior express written permission of
* ZyXEL Communications, Corp.
*
*/
/****************************************************************************/
#ifndef __ZyXEL_MessageType__
#define __ZyXEL_MessageType__

#define msgType_DiscoverReq         0x1
#define msgType_DiscoverResp        0x2
#define msgType_SystemQueryReq      0x3
#define msgType_SystemQueryResp     0x4
#define msgType_ParameterGetReq     0x5
#define msgType_ParameterGetResp    0x6
#define msgType_ParameterSetReq     0x7
#define msgType_ParameterSetResp    0x8
#define msgType_DeviceNotifyReq     0x9
#define msgType_DeviceNotifyResp    0xa
#define msgType_ObjectAddReq		0xc
#define msgType_ObjectAddResp		0xd
#define msgType_ObjectDeleteReq		0xe
#define msgType_ObjectDeleteResp	0xf

#endif
