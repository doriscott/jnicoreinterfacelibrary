# Copyright (C) 2009 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE    := libgl2jni
#LOCAL_CFLAGS    := -Werror
LOCAL_CPPFLAGS += -fexceptions
LOCAL_CPPFLAGS += -std=c++11

#for enet
LOCAL_CFLAGS := -DHAS_SOCKLEN_T

LOCAL_SRC_FILES := android_interface.cpp \
                   JNIJava/Gateway.cpp \
                   JNIJava/Portable.cpp \
                   JNIJava/EndDevicePool.cpp \
                   JNIJava/GuestWiFi.cpp \
                   JNIJava/GatewayWiFi.cpp \
                   JNIJava/ParentalControl.cpp \
                   JNIJava/WiFiRadio.cpp \
                   ThirdParty/json/json_reader.cpp \
                   ThirdParty/json/json_value.cpp \
                   ThirdParty/json/json_writer.cpp \
                   APP_Agent/CDataMessage.cpp \
                   APP_Agent/DevicePool.cpp \
                   APP_Agent/CSecurity.cpp \
                   Lib/rc4/rc4fun.cpp \
                   Lib/rc4/RC4Lib.cpp \
                   Lib/tr181/tr181Tree.cpp \
                   Lib/tr181/treeNode.cpp \
                   Lib/tr181/oneConnectTree.cpp\
                   APP_Agent/CJSON.cpp \
                   APP_Agent/CWiFi.cpp \
                   Lib/CSocket/CSocket.cpp \
                   APP_Agent/CWiFiRadio.cpp \
   	               APP_Agent/CParentalControl.cpp \
   	               APP_Agent/CEndDevicePool.cpp \
                   APP_Agent/CMobile.cpp \
                   APP_Agent/CMobileMonitor.cpp \
                   APP_Agent/CBatteryInfo.cpp \
                   APP_Agent/DeviceDiscover.cpp \
                   APP_Agent/CUDPSocket.cpp \
                   APP_Agent/CDevice.cpp  \
                   APP_Agent/CErrorHandle.cpp \
                   APP_Agent/CTemporary.cpp \
                   Telnet/CTelnet.cpp

LOCAL_LDLIBS    := -llog -lz
include $(BUILD_SHARED_LIBRARY)


