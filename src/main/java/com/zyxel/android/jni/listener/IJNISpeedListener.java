package com.zyxel.android.jni.listener;

/**
 * An interface that define speed test listener, a class implement this listener will received speed test result from JNI
 */
public interface IJNISpeedListener {

	public void handleSpeed(double speed, boolean status);

}
