package com.zyxel.android.jni.listener;

import java.util.ArrayList;

/**
 * A class representing JNI speed handler
 */
public class JNISpeedHandler {
	private static JNISpeedHandler inst;
	private ArrayList<IJNISpeedListener> listeners = new ArrayList<IJNISpeedListener>();
	public double speed;
	public boolean status;

	public JNISpeedHandler() {

	}

	public static JNISpeedHandler getJNISpeedHandler() {
		if (inst == null) {
			inst = new JNISpeedHandler();
		}
		return inst;
	}

	public void speedFromJNI(double jnispeed, boolean jnistatus) {

//		Log.d("JNISpeedHandler", "JNILib Receive Real Time Speed = " + jnispeed);
		speed = jnispeed;
		status = jnistatus;
		JNISpeedHandler.getJNISpeedHandler().receivedNotify(speed, status);
	}

	public void addListener(IJNISpeedListener _listener) {
		listeners.add(_listener);
	}

	public void removeListener(IJNISpeedListener _listener) {
		listeners.remove(_listener);
	}

	public void receivedNotify(double _speed, boolean _status) {
		if (listeners.size() > 0) {
			for (IJNISpeedListener iel : listeners) {
				new Thread(new RunEvent(iel, _speed, _status)).start();
			}
		}
	}

	public class RunEvent implements Runnable {
		IJNISpeedListener myiel;

		public RunEvent(IJNISpeedListener iel, double _speed, boolean _status) {
			speed = _speed;
			status = _status;
			myiel = iel;
		}

		public void run() {
			if (myiel != null) {
				myiel.handleSpeed(speed, status);
			}
		}
	}
}
