package com.zyxel.android.jni.listener;

/**
 * The interface for error handler.
 * when error occur, this method will be called.
 * @author kevin
 *
 *
 *
 */

/**
 * An interface that define core library error code listener, a class implement the listener will receive error code from JNI
 */
public interface IJNIEventListener {

	public void handleEvent(int eventID);

}
