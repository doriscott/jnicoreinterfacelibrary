package com.zyxel.android.jni.listener;

import java.util.ArrayList;

import android.util.Log;

/**
 * A class representing JNI error code handler
 */
public class JNIEventHandler {
	private static JNIEventHandler inst;
	private ArrayList<IJNIEventListener> listeners = new ArrayList<IJNIEventListener>();
	public int errorCode;

	public JNIEventHandler() {

	}

	public static JNIEventHandler getJNIEventHandler() {
		if(inst == null) {
			inst = new JNIEventHandler();
		}
		return inst;
	}

	public void eventFromJNI(int errorID) {
		Log.d("JNIEventHandler","Receive Error Code = " + errorID);
		errorCode = errorID;
		JNIEventHandler.getJNIEventHandler().receivedNotify(errorCode);
	}

	public void addListener(IJNIEventListener _listener) {
		listeners.add(_listener);
	}

	public void removeListener(IJNIEventListener _listener) {
		listeners.remove(_listener);
	}

	public void receivedNotify(int _errorCode) {
		if(listeners.size() > 0) {
			for(IJNIEventListener iel : listeners) {
				new Thread(new RunEvent(iel, _errorCode)).start();
			}
		}
	}

	public class RunEvent implements Runnable {
		IJNIEventListener myiel;
		int errorID;
		public RunEvent(IJNIEventListener iel, int _errorCode) {
			errorCode = _errorCode;
			errorID = _errorCode;
			myiel = iel;
		}

		public void run() {
			if(myiel != null && errorCode < 0) {
				myiel.handleEvent(errorID);
			}
		}
	}
}
