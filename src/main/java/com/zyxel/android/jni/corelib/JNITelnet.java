package com.zyxel.android.jni.corelib;

import com.zyxel.android.jni.corelibinterface.JNITelnetLib;

/**
 * It's the JNI API.
 * If your call serial of XXXToBuffer(), it doesn't send to device, until the sendBuffer() be called.
 * @author kevin
 *
 */

/**
 * A implementation class of IJNIAPIs
 */
public class JNITelnet {

	public static final String TAG = JNITelnet.class.getSimpleName();


	public int loginWithTelent(String ip, int port, String username, String password) {
		return JNITelnetLib.getJNILib().loginWithTelnet(ip, port, username, password);
	}

	public int getModelNameWithTelent(String modelname) {
		return JNITelnetLib.getJNILib().getModelNameWithTelent(modelname);
	}

	public int setNewFirmwareDownloadWithTelnet() {
		return JNITelnetLib.getJNILib().setNewFirmwareDownloadWithTelnet();
	}

	public int getNewFirmwareDownloadStatusWithTelnet() {
		return JNITelnetLib.getJNILib().getNewFirmwareDownloadStatusWithTelnet();
	}

	public int setNewFirmwareUpgradeWithTelnet() {
		return JNITelnetLib.getJNILib().setNewFirmwareUpgradeWithTelnet();
	}
}
