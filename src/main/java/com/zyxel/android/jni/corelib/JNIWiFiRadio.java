package com.zyxel.android.jni.corelib;

import com.zyxel.android.jni.corelibinterface.JNIWiFiRadioLib;
import com.zyxel.android.jni.model.WiFiRadioProfile;

import java.util.ArrayList;

public class JNIWiFiRadio {

	public static final String TAG = JNIWiFiRadio.class.getSimpleName();

	public boolean getRadioEnable() {
		boolean temp;
		String temp_str = JNIWiFiRadioLib.getJNILib().getRadioEnable();
		if(temp_str.equalsIgnoreCase("true"))
			temp = true;
		else
			temp = false;
		return temp;
	}

	public String getRadioStatus() {
		return JNIWiFiRadioLib.getJNILib().getRadioStatus();
	}

	public String getMaxBitRate() {
		return JNIWiFiRadioLib.getJNILib().getMaxBitRate();
	}

	public String getOperatingFrequency() {
		return JNIWiFiRadioLib.getJNILib().getOperatingFrequency();
	}

	public String getOperatingStandard() {
		return JNIWiFiRadioLib.getJNILib().getOperatingStandard();
	}

	public boolean getAutoChannelEnable() {
		boolean temp;
		String temp_str = JNIWiFiRadioLib.getJNILib().getAutoChannelEnable();
		if(temp_str.equalsIgnoreCase("true"))
			temp = true;
		else
			temp = false;
		return temp;
	}

	public int getChannel() {
		int temp;
		String temp_str = JNIWiFiRadioLib.getJNILib().getChannel();
		temp = Integer.parseInt(temp_str);
		return temp;
	}

	public int getTransmitPower() {
		int temp;
		String temp_str = JNIWiFiRadioLib.getJNILib().getTransmitPower();
		temp = Integer.parseInt(temp_str);
		return temp;
	}

	public void addAutoChannelEnableToBuffer(boolean status) {
		String temp;
		if(status)
			temp = "TRUE";
		else
			temp = "FALSE";
		JNIWiFiRadioLib.getJNILib().addAutoChannelEnableToBuffer(temp);
	}

	public void addChannelToBuffer(int channel) {
		JNIWiFiRadioLib.getJNILib().addChannelToBuffer(String.valueOf(channel));
	}


	public int getTotalRadioNumber() {
		return JNIWiFiRadioLib.getJNILib().getTotalRadioNumber();
	}

	public WiFiRadioProfile getWiFiRadioProfile() {
		ArrayList<Object> wifiRadioProfile = JNIWiFiRadioLib.getJNILib().getWiFiRadioProfile();
		return (WiFiRadioProfile) wifiRadioProfile.get(0);
	}
}
