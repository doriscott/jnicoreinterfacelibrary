package com.zyxel.android.jni.corelib;

import com.zyxel.android.jni.corelibinterface.JNILib;

/**
 * It's the JNI API.
 * If your call serial of XXXToBuffer(), it doesn't send to device, until the sendBuffer() be called.
 * @author kevin
 *
 */

public class JNICall {

	public static final String TAG = JNICall.class.getSimpleName();
	private JNIGateway jniGateway;
	private JNIPortable jniPortable;
	private JNIGatewayWiFi jniGatewayWiFi;
	private JNIGuestWiFi jniGuestWiFi;
	private JNIEndDevicePool jniEndDevicePool;
	private JNIParentalControl jniParentalControl;
	private JNISmartHome jniSmartHome;
	private JNIWiFiRadio jniWiFiRadio;
	private JNITelnet jniTelnet;

	public JNICall() {
	}

	public JNIGateway getJniGateway() {
		if(jniGateway == null) {
			jniGateway = new JNIGateway();
		}
		return jniGateway;
	}


	public JNIPortable getJniPortable() {
		if(jniPortable == null) {
			jniPortable = new JNIPortable();
		}
		return jniPortable;
	}

	public JNIGatewayWiFi getJniGatewayWiFi() {
		if(jniGatewayWiFi == null) {
			jniGatewayWiFi = new JNIGatewayWiFi();
		}
		return jniGatewayWiFi;
	}

	public JNIGuestWiFi getJniGuestWiFi() {

		if(jniGuestWiFi == null) {
			jniGuestWiFi = new JNIGuestWiFi();
		}
		return jniGuestWiFi;
	}

	public JNIEndDevicePool getJniEndDevicePool() {
		if(jniEndDevicePool == null) {
			jniEndDevicePool = new JNIEndDevicePool();
		}
		return jniEndDevicePool;
	}

	public JNIParentalControl getJniParentalControl() {

		if(jniParentalControl == null) {
			jniParentalControl = new JNIParentalControl();
		}
		return jniParentalControl;
	}

	public JNISmartHome getJniSmartHome() {
		if(jniSmartHome == null) {
			jniSmartHome = new JNISmartHome();
		}
		return jniSmartHome;
	}

	public JNIWiFiRadio getJniWiFiRadio() {
		if(jniWiFiRadio == null) {
			jniWiFiRadio = new JNIWiFiRadio();
		}
		return jniWiFiRadio;
	}

	public JNITelnet getJniTelnet() {
		if(jniTelnet == null) {
			jniTelnet = new JNITelnet();
		}
		return jniTelnet;
	}


	public void init() {
		JNILib.getJNILib().init();
	}

	public void closeSocket() {
		JNILib.getJNILib().closeSocket();
	}

	/**
	 * Sending the buffer to device.
	 */
	public void sendBuffer() {
		JNILib.getJNILib().sendBuffer();
	}

}
