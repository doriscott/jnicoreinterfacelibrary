package com.zyxel.android.jni.corelib;

import android.util.Log;

import com.zyxel.android.jni.corelibinterface.JNIEndDevicePoolLib;
import com.zyxel.android.jni.corelibinterface.JNIGatewayLib;
import com.zyxel.android.jni.model.AppFeatureInfoProfile;
import com.zyxel.android.jni.model.DeviceInfoProfile;
import com.zyxel.android.jni.model.GatewayFWProfile;
import com.zyxel.android.jni.model.GatewayProfile;
import com.zyxel.android.jni.model.IPV4ForwardingProfile;
import com.zyxel.android.jni.model.ParentalControlProfile;
import com.zyxel.android.jni.model.RouteHopProfile;
import com.zyxel.android.jni.model.WanInfoProfile;
import com.zyxel.android.jni.model.WiFiAutoConfigApproveProfile;

import java.util.ArrayList;

public class JNIGateway {

	public static final String TAG = JNIGateway.class.getSimpleName();
	public JNIGateway() {
		registerErrorCodeBackFunction();
	}

	/*
     JNIGateway.cpp
        */

	/**Search all ZyXEL devices in LAN
	 *
	 */
	public void search() {
		Log.d(TAG, "response search method called !!!!!!!!!!!");
		JNIGatewayLib.getJNILib().search();
	}


	public String queryModelName() {
		return JNIGatewayLib.getJNILib().queryModelName();
	}
	/**Get search result
	 * @return device list
	 */
	public String[] queryDeviceList(String model) {
		return JNIGatewayLib.getJNILib().queryDeviceList(model);
	}


	public ArrayList<GatewayProfile> queryGatewayList() {
		return JNIGatewayLib.getJNILib().queryGatewayList();
	}

	/**
	 * Login with index of devicelist and password.
	 */
	public boolean loginDevice(int index, String password) {
		return JNIGatewayLib.getJNILib().loginDevice(index, password);
	}

	public AppFeatureInfoProfile getAppFeatureList() {
		ArrayList<Object> appFeatureInfoProfileArrayList = JNIGatewayLib.getJNILib().getAppFeatureList();
		return (AppFeatureInfoProfile) appFeatureInfoProfileArrayList.get(0);
	}

	/**
	 * Checking there is available firmware to update or not.
	 */
	public String isNewGatewayFirmwareAvailable() {
		return JNIGatewayLib.getJNILib().isNewGatewayFirmwareAvailable();
	}

	public String getNewGatewayFirmwareVersion() {
		return JNIGatewayLib.getJNILib().getNewGatewayFirmwareVersion();
	}

	public String getNewGatewayFirmwareReleaseDate() {
		return JNIGatewayLib.getJNILib().getNewGatewayFirmwareReleaseDate();
	}

	public GatewayFWProfile getNewGatewayFirmwareInfo() {
		Object temp = JNIGatewayLib.getJNILib().getNewGatewayFirmwareInfo();
		return (GatewayFWProfile) temp;
	}

	public int getNewGatewayFirmwareUpdateStatus() {
		int temp;
		String temp_str = JNIGatewayLib.getJNILib().getNewGatewayFirmwareUpdateStatus();
		temp = Integer.parseInt(temp_str);
		return temp;
	}


	/**
	 * @return The AutoConfig for WiFi is enable or not.
	 */
	public boolean getWiFiAutoConfigEnable() {
		boolean temp;
		String temp_str = JNIGatewayLib.getJNILib().getWiFiAutoConfigEnable();
		Log.d(TAG, "Auto Config = " + temp_str);
		if(temp_str.equalsIgnoreCase("true"))
			temp = true;
		else
			temp = false;
		return temp;
	}

	/**
	 * Getting the device of password.
	 * @see #addDevicePasswordToBuffer(String)
	 * @return The password for login to system of device.
	 */
	public String getDevicePassword() {
		return JNIGatewayLib.getJNILib().getDevicePassword();
	}


	/**
	 * Getting the IP of device(gateway) on LAN.
	 * @return The IP of device on LAN.
	 */
	public String getGatewayLanIP() {
		return JNIGatewayLib.getJNILib().getGatewayLanIP();
	}

	/**
	 * Getting the WAN information for device.
	 * @see WanInfoProfile
	 * @return The WAN information for device.
	 */
	public WanInfoProfile getWanInfo(){
		Object temp = JNIGatewayLib.getJNILib().getWanInfo();
		return (WanInfoProfile)temp;
	}

	/*
	******************** JNIGateway.cpp for Diagnostics APIs ************************
	*/
	/**
	 * Getting the system information for device.
	 * @see DeviceInfoProfile
	 * @return The system information for device.
	 */
	public DeviceInfoProfile getDeviceInfo(){
		Object temp = JNIGatewayLib.getJNILib().getDeviceInfo();
		return (DeviceInfoProfile)temp;
	}

	/**
	 * Getting the result for trace router.
	 * @see RouteHopProfile
	 * @return The result for trace router.
	 */
	public ArrayList<RouteHopProfile> getTraceRouterResult(){
		ArrayList<Object> routehop_profileList = JNIGatewayLib.getJNILib().getTraceRouterResult();
		ArrayList<RouteHopProfile> temp = new ArrayList<RouteHopProfile>();
		if(routehop_profileList.size() <= 0){
			return null;
		}else{
			for(int i = 0 ; i < routehop_profileList.size() ; i++){
				temp.add((RouteHopProfile)routehop_profileList.get(i));
			}
			return temp;
		}
	}

	/**
	 * Getting the IPV4 forwarding for gateway.
	 * @see IPV4ForwardingProfile
	 * @return The IPV4 forwarding for gateway.
	 */
	public ArrayList<IPV4ForwardingProfile> getIPV4Forwarding(){
		ArrayList<Object> ipv4forwarding_profileList = JNIGatewayLib.getJNILib().getIPV4Forwarding();
		ArrayList<IPV4ForwardingProfile> temp = new ArrayList<IPV4ForwardingProfile>();
		if(ipv4forwarding_profileList.size() <= 0){
			return null;
		}else{
			for(int i = 0 ; i < ipv4forwarding_profileList.size() ; i++){
				temp.add((IPV4ForwardingProfile)ipv4forwarding_profileList.get(i));
			}
			return temp;
		}
	}

	/*
	*********************** JNIGateway.cpp for PowerLineAP point of view to get Gateway Information APIs
	*/
	public String getGatewayHostName() {
		return JNIGatewayLib.getJNILib().getGatewayHostName();
	}

	public String getGatewayHostMAC() {
		return JNIGatewayLib.getJNILib().getGatewayHostMAC();
	}

	public int getGatewayAutoConfig() {
		return JNIGatewayLib.getJNILib().getGatewayAutoConfig();
	}


	/*
*********************** JNIGateway.cpp for Speed Test APIs
*/
	public String getGateway2InternetDownload() {
		return JNIGatewayLib.getJNILib().getGateway2InternetDownload();
	}

	public String getGateway2InternetUpload() {
		return JNIGatewayLib.getJNILib().getGateway2InternetUpload();
	}

	public String isGateway2InternetSpeedTestEnd() {
		return JNIGatewayLib.getJNILib().isGateway2InternetSpeedTestEnd();
	}


	public int getClient2GatewaySpeedTestPort() {
		return JNIGatewayLib.getJNILib().getClient2GatewaySpeedTestPort();
	}




	public void setGateway2InternetSpeedTest(String url, boolean option) {
		Log.d(TAG, "g2i server = " + url);
		if(option) {
			JNIGatewayLib.getJNILib().setGateway2InternetSpeedTest(url, 1);
		} else {
			JNIGatewayLib.getJNILib().setGateway2InternetSpeedTest(url, 0);
		}
	}


	public int getL2FWUpgradingStatus(String mac) {
		return JNIGatewayLib.getJNILib().getL2FWUpgradingStatus(mac);
	}

	public int getGateway2ClientRSSI(String mobileMAC) {
		return JNIGatewayLib.getJNILib().getGateway2ClientRSSI(mobileMAC);
	}

	public boolean getAutoConfigApprove() {
		return JNIGatewayLib.getJNILib().getAutoConfigApprove();
	}

	/*
	*******************************	Set APIs *******************************
	* ************************************************************** *
	*/


	public int setAPWiFiAutoConfig() {
		return JNIGatewayLib.getJNILib().setAPWiFiAutoConfig();
	}


	/**
	 * Reboot device.
	 */
	public void addRebootToBuffer() {
		JNIGatewayLib.getJNILib().addRebootToBuffer();
	}

	/**
	 * Setting the status of auto-configuration for WiFi.
	 */
	public void addWiFiAutoConfigEnableToBuffer(boolean status) {
		String temp;
		if(status)
			temp = "TRUE";
		else
			temp = "FALSE";
		JNIGatewayLib.getJNILib().addWiFiAutoConfigEnableToBuffer(temp);
	}

	/**
	 * Setting the password of WiFi.
	 */
	public void addDevicePasswordToBuffer(String devicepw) {
		JNIGatewayLib.getJNILib().addDevicePasswordToBuffer(devicepw);
	}

	/**
	 * Setting the status of auto-configuration for L2 device.
	 * @param mac device's MAC.
	 * @param op disable or enable.
	 */
	public void addL2WiFiAutoConfigEnableBuffer(String mac , boolean op) {
		String temp;
		if(op)
			temp = "TRUE";
		else
			temp = "FALSE";

		JNIGatewayLib.getJNILib().addL2WiFiAutoConfigEnableBuffer(mac, temp);
	}


	public void addFirmwareUpgradeToBuffer(boolean status) {
		String temp;
		if(status)
			temp = "TRUE";
		else
			temp = "FALSE";
		JNIGatewayLib.getJNILib().addFirmwareUpgradeToBuffer(temp);
	}

	public void setGatewayFirmwareUpgrade() {
		JNIGatewayLib.getJNILib().setGatewayFirmwareUpgrade();
	}



	public boolean setL2ControlFirmwareConfig(String mac, boolean option) {
		if(option) {
			JNIGatewayLib.getJNILib().setL2ControlFirmwareConfig(mac, 1);
		} else {
			JNIGatewayLib.getJNILib().setL2ControlFirmwareConfig(mac, 0);
		}
		return true;
	}

	public boolean addL2ControlFirmwareConfigToBuffer(String mac, boolean option) {
		if(option) {
			JNIGatewayLib.getJNILib().addL2ControlFirmwareConfigToBuffer(mac, 1);
		} else {
			JNIGatewayLib.getJNILib().addL2ControlFirmwareConfigToBuffer(mac, 0);
		}
		return true;
	}




	public int setClient2GatewaySpeedTestEnable(int option) {
		return JNIGatewayLib.getJNILib().setClient2GatewaySpeedTestEnable(option);
	}


	public void addAutoConfigApproveProfileListToBuffer(ArrayList<WiFiAutoConfigApproveProfile> profiles) {
		JNIGatewayLib.getJNILib().addAutoConfigApproveProfileListToBuffer(profiles);
	}

	public void setAutoConfigApproveToBuffer(boolean option) {
		JNIGatewayLib.getJNILib().setAutoConfigApproveToBuffer(option);
	}



		/*
	*********************** JNIGateway.cpp for Firmware Status APIs ***************************
	*/

	/**
	 * Getting the firmware version of device.
	 * @return The firmware version of device.
	 */
	public String getCurrentFirmwareVersion() {
		return JNIGatewayLib.getJNILib().getCurrentFirmwareVersion();
	}

	public String getLatestFirmwareVersion() {
		return JNIGatewayLib.getJNILib().getLatestFirmwareVersion();
	}

	/**
	 * Checking there is available firmware to update or not.
	 */
	public boolean checkNewFirmwareVersion() {
		boolean temp;
		String temp_str = JNIGatewayLib.getJNILib().checkNewFirmwareVersion();
		if(temp_str.equalsIgnoreCase("true"))
			temp = true;
		else
			temp = false;
		return temp;
	}

	public String getUpdateFirmwareStatus() {
		return JNIGatewayLib.getJNILib().getUpdateFirmwareStatus();
	}

	public void registerErrorCodeBackFunction() {
		JNIGatewayLib.getJNILib().registerErrorCodeBackFunction();
	}
}
