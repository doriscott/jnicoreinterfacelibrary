package com.zyxel.android.jni.corelib;

/**
 * It's the JNI API.
 * If your call serial of XXXToBuffer(), it doesn't send to device, until the sendBuffer() be called.
 * @author kevin
 *
 */

import com.zyxel.android.jni.corelibinterface.JNIGuestWiFiLib;
import com.zyxel.android.jni.model.GuestWiFiTimerProfile;

import java.util.ArrayList;

/**
 * A implementation class of IJNIAPIs
 */
public class JNIGuestWiFi {

	public static final String TAG = JNIGuestWiFi.class.getSimpleName();

	/**
	 * Getting the Guest WiFi is enable or not.
	 * @return true of false
	 */
	public boolean getGuestWiFiEnable(){
		boolean temp;
//		String temp_str = JNILib.getJNILib().parameterGetRequest(Define.GUID_GUEST_WIFI, Define.UUID_enable);
		String temp_str = JNIGuestWiFiLib.getJNILib().getGuestWiFiEnable();
		if(temp_str != null) {
			if(temp_str.equalsIgnoreCase("true"))
				temp = true;
			else
				temp = false;
			return temp;
		} else {
			return false;
		}
	}

	/**
	 * Getting the SSID of Guest WiFi.
	 * @see #addGuestWiFiSSIDToBuffer(String)
	 * @return The SSID of Guest WiFi.
	 */
	public String getGuestWiFiSSID() {
		String ssid = JNIGuestWiFiLib.getJNILib().getGuestWiFiSSID();
		if(ssid.equalsIgnoreCase("N/A")) {
			return "";
		} else {
			return ssid;
		}
	}
	/**
	 * Getting the Security of Guest WiFi.
	 * @see #addGuestWiFiSecurityToBuffer(String)
	 * @return The Security of Guest WiFi.
	 */
	public String getGuestWiFiSecurity() {
		String security = JNIGuestWiFiLib.getJNILib().getGuestWiFiSecurity();
		if(security.equalsIgnoreCase("N/A")) {
			return "";
		} else {
			return security;
		}
	}

	/**
	 * Getting the password of Guest WiFi.
	 * @see #addGuestWiFiPasswordToBuffer(String)
	 * @return The Password of Guest WiFi.
	 */
	public String getGuestWiFiPassword() {
		String password = JNIGuestWiFiLib.getJNILib().getGuestWiFiPassword();
		if(password.equalsIgnoreCase("N/A")) {
			return "";
		} else {
			return password;
		}
	}

	/**
	 * Setting the status of Guest WiFi.
	 */
	public void addGuestWiFiEnableToBuffer(boolean status) {
		String temp;
		if(status)
			temp = "TRUE";
		else
			temp = "FALSE";
		JNIGuestWiFiLib.getJNILib().addGuestWiFiEnableToBuffer(temp);
	}

	/**
	 * Setting the SSID of Guest WiFi.
	 */
	public void addGuestWiFiSSIDToBuffer(String ssid) {
		JNIGuestWiFiLib.getJNILib().addGuestWiFiSSIDToBuffer(ssid);
	}

	/**
	 * Setting the security of Guest WiFi.
	 */
	public void addGuestWiFiSecurityToBuffer(String security) {
		JNIGuestWiFiLib.getJNILib().addGuestWiFiSecurityToBuffer(security); // WPA2, NONE
	}

	/**
	 * Setting the password of Guest WiFi.
	 */
	public void addGuestWiFiPasswordToBuffer(String password) {
		JNIGuestWiFiLib.getJNILib().addGuestWiFiPasswordToBuffer(password);
	}

	public GuestWiFiTimerProfile getGuestWiFiTimer() {
		Object temp = JNIGuestWiFiLib.getJNILib().getGuestWiFiTimer();
		return (GuestWiFiTimerProfile) temp;
	}

//	public boolean setGuestWiFiTimer(ArrayList<GuestWiFiTimerProfile> guestWiFiTimerProfile) {
//		JNIGuestWiFiLib.getJNILib().setGuestWiFiTimer(guestWiFiTimerProfile);
//		return true;
//	}

	public boolean addGuestWiFiTimerToBuffer(ArrayList<GuestWiFiTimerProfile> guestWiFiTimerProfile) {
		JNIGuestWiFiLib.getJNILib().addGuestWiFiTimerToBuffer(guestWiFiTimerProfile);
		return true;
	}

}
