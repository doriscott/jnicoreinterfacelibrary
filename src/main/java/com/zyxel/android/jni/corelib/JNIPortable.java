package com.zyxel.android.jni.corelib;

import com.zyxel.android.jni.corelibinterface.JNIPortableLib;
import com.zyxel.android.jni.model.EndDeviceProfile;

public class JNIPortable {

	public static final String TAG = JNIPortable.class.getSimpleName();
	public JNIPortable() {

	}

	//	Get 3G APIs
	public String getAPN() {
		return JNIPortableLib.getJNILib().getAPN();
	}

	public String getAuthentication() {
		return JNIPortableLib.getJNILib().getAuthentication();
	}

	public String getUserName() {
		return JNIPortableLib.getJNILib().getUserName();
	}

	public String get3GPassword() {
		return JNIPortableLib.getJNILib().get3GPassword();
	}

	public int getSignalStrength() {
		int temp;
		String temp_str = JNIPortableLib.getJNILib().getSignalStrength();
		temp = Integer.parseInt(temp_str);
		return temp;
	}

	public String getCellularMode() {
		return JNIPortableLib.getJNILib().getCellularMode();
	}

	public String getServiceProvider() {
		return JNIPortableLib.getJNILib().getServiceProvider();
	}

	public boolean getAutoReset() {
		boolean temp;
		String temp_str = JNIPortableLib.getJNILib().getAutoReset();
		if(temp_str.equalsIgnoreCase("true"))
			temp = true;
		else
			temp = false;
		return temp;
	}

	public int getResetCount() {
		int temp;
		String temp_str = JNIPortableLib.getJNILib().getResetCount();
		temp = Integer.parseInt(temp_str);
		return temp;
	}

	public int getResetDay() {
		int temp;
		String temp_str = JNIPortableLib.getJNILib().getResetDay();
		temp = Integer.parseInt(temp_str);
		return temp;
	}

	public String getLastResetDate() {
		return JNIPortableLib.getJNILib().getLastResetDate();
	}

	public int getQuota() {
		int temp;
		String temp_str = JNIPortableLib.getJNILib().getQuota();
		temp = Integer.parseInt(temp_str);
		return temp;
	}

	public int getCurrentUsage() {
		int temp;
		String temp_str = JNIPortableLib.getJNILib().getCurrentUsage();
		temp = Integer.parseInt(temp_str);
		return temp;
	}
	public int getByteSent() {
		int temp;
		String temp_str = JNIPortableLib.getJNILib().getByteSent();
		temp = Integer.parseInt(temp_str);
		return temp;
	}

	public int getByteReceived() {
		int temp;
//		String temp_str = JNILib.getJNILib().parameterGetRequest(Define.GUID_3G, Define.UUID_bytes_received);
		String temp_str = JNIPortableLib.getJNILib().getByteReceived();
		temp = Integer.parseInt(temp_str);
		return temp;
	}

	public int getUsageAdvice() {
		int temp;
		String temp_str = JNIPortableLib.getJNILib().getUsageAdvice();
		temp = Integer.parseInt(temp_str);
		return temp;
	}

	public boolean getDataPlanManagementEnable() {
		boolean temp;
		String temp_str = JNIPortableLib.getJNILib().getDataPlanManagementEnable();
		if(temp_str.equalsIgnoreCase("true"))
			temp = true;
		else
			temp = false;
		return temp;
	}

	public int getBatteryRemainCapacity() {
		int temp;
		String temp_str = JNIPortableLib.getJNILib().getBatteryRemainCapacity();
		temp = Integer.parseInt(temp_str);
		return temp;
	}

	public int getBatteryTotalCapacity() {
		int temp;
		String temp_str = JNIPortableLib.getJNILib().getBatteryTotalCapacity();
		temp = Integer.parseInt(temp_str);
		return temp;
	}

	public String getBatteryChargeStatus() {
		return JNIPortableLib.getJNILib().getBatteryChargeStatus();
	}


	public void addAPNToBuffer(String apn) {
		JNIPortableLib.getJNILib().addAPNToBuffer(apn);
	}

	/**
	 * Setting the status of auto-configuration for L2 device.
	 * @see EndDeviceProfile#L2AutoConfigEnable
	 */
	public void addAuthenticationToBuffer(String authentication) {
		JNIPortableLib.getJNILib().addAuthenticationToBuffer(authentication);
	}

	public void addUserNameToBuffer(String username) {
		JNIPortableLib.getJNILib().addUserNameToBuffer(username);
	}

	public void add3GPasswordToBuffer(String password) {
		JNIPortableLib.getJNILib().add3GPasswordToBuffer(password);
	}

	public void addQuotaToBuffer(int quota) {
		JNIPortableLib.getJNILib().addQuotaToBuffer(String.valueOf(quota));
	}

	public void addResetDayToBuffer(int day) {
		JNIPortableLib.getJNILib().addResetDayToBuffer(String.valueOf(day));
	}

	public void addAutoResetToBuffer(boolean status) {
		String temp;
		if(status)
			temp = "TRUE";
		else
			temp = "FALSE";
		JNIPortableLib.getJNILib().addAutoResetToBuffer(temp);
	}

	public void addDataPlanManagementEnableToBuffer(boolean status) {
		String temp;
		if(status)
			temp = "TRUE";
		else
			temp = "FALSE";
		JNIPortableLib.getJNILib().addDataPlanManagementEnableToBuffer(temp);
	}
}
