package com.zyxel.android.jni.corelib;

import android.util.Log;

import com.zyxel.android.jni.corelibinterface.JNIParentalControlLib;
import com.zyxel.android.jni.model.ParentalControlProfile;

import java.util.ArrayList;

/**
 * It's the JNI API.
 * If your call serial of XXXToBuffer(), it doesn't send to device, until the sendBuffer() be called.
 * @author kevin
 *
 */

/**
 * A implementation class of IJNIAPIs
 */
public class JNIParentalControl {

	public static final String TAG = JNIParentalControl.class.getSimpleName();

	/**
	 * How many ParentalControlProfiles be set up.
	 */
//	public int getParentalControlProfileNumber() {
//		int temp;
//		String temp_str = JNIParentalControlLib.getJNILib().getParentalControlProfileNumber();
//		temp = Integer.parseInt(temp_str);
//		return temp;
//	}

	/**
	 * Getting the rules of parental control.
	 * @return all ParentalControlProfiles
	 */
	public ArrayList<ParentalControlProfile> getParentalControlProfile() {
		ArrayList<Object> parentalcontrol_profileList = JNIParentalControlLib.getJNILib().getParentalControlProfile();
//		Log.d("Router APP", "All Parental Control Profile List Size = " + parentalcontrol_profileList.size());
		ArrayList<ParentalControlProfile> allProfiles = new ArrayList<ParentalControlProfile>();
		if(parentalcontrol_profileList != null) {
			for(int i = 0; i < parentalcontrol_profileList.size(); i++) {
				ParentalControlProfile tmp = (ParentalControlProfile) parentalcontrol_profileList.get(i);
				allProfiles.add(tmp);
//			Log.d("Router APP", "PC Profile Index = " + i +  " --> " +
//					"PC Profile ID = " + tmp.ID + " --> " +
//					"PC Profile MAC = " + tmp.EndDeviceMAC + " --> " +
//					"PC Profile Enable = " + tmp.Enable + " --> " +
//					"PC Profile ProfileName = " + tmp.ProfileName + " --> " +
//					"PC Profile SchedulingDays = " + tmp.SchedulingDays + " --> " +
//					"PC Profile SchedulingTimeStartHour = " + tmp.SchedulingTimeStartHour + " --> " +
//					"PC Profile SchedulingTimeStartMin = " + tmp.SchedulingTimeStartMin  + " --> " +
//					"PC Profile SchedulingTimeStopHour = " + tmp.SchedulingTimeStopHour  + " --> " +
//					"PC Profile SchedulingTimeStopMin = " + tmp.SchedulingTimeStopMin);
			}
		}

		return allProfiles;
	}

	public ArrayList<ParentalControlProfile> getDeviceParentalControlProfile(int index) {
//		ArrayList profiles = null;
		ArrayList<Object> parentalcontrol_profileList = JNIParentalControlLib.getJNILib().getDeviceParentalControlProfile(index);
		if(parentalcontrol_profileList != null) {
//		Log.d("Router APP", "Device Parental Control Profile List Size = " + parentalcontrol_profileList.size());
			ArrayList<ParentalControlProfile> deviceProfiles = new ArrayList<ParentalControlProfile>();
			for(int i = 0; i < parentalcontrol_profileList.size(); i++) {
				ParentalControlProfile tmp = (ParentalControlProfile) parentalcontrol_profileList.get(i);
				deviceProfiles.add(tmp);
//			Log.d("Router APP", "PC Profile Index = " + i +  " --> " +
//					"PC Profile ID = " + tmp.ID + " --> " +
//					"PC Profile MAC = " + tmp.EndDeviceMAC + " --> " +
//					"PC Profile Enable = " + tmp.Enable + " --> " +
//					"PC Profile ProfileName = " + tmp.ProfileName + " --> " +
//					"PC Profile SchedulingDays = " + tmp.SchedulingDays + " --> " +
//					"PC Profile SchedulingTimeStartHour = " + tmp.SchedulingTimeStartHour + " --> " +
//					"PC Profile SchedulingTimeStartMin = " + tmp.SchedulingTimeStartMin  + " --> " +
//					"PC Profile SchedulingTimeStopHour = " + tmp.SchedulingTimeStopHour  + " --> " +
//					"PC Profile SchedulingTimeStopMin = " + tmp.SchedulingTimeStopMin);
			}
			return deviceProfiles;
		}else {
			return null;
		}
	}
	public void addNewParentalControlRuleToBuffer(ArrayList<ParentalControlProfile> profiles) {
		JNIParentalControlLib.getJNILib().addNewParentalControlRuleToBuffer(profiles);
	}

	/**
	 * Deleting the rules of parental control profile.
	 * @see ParentalControlProfile
	 */
	public void addEliminateParentalControlRuleToBuffer(ArrayList<ParentalControlProfile> profiles) {
		JNIParentalControlLib.getJNILib().addEliminateParentalControlRuleToBuffer(profiles);
	}

	/**
	 * To Modify the rules of parental control profile.
	 * @see ParentalControlProfile
	 */
	public void addModifyParentalControlRuleToBuffer(ArrayList<ParentalControlProfile> profiles) {
		Log.d(TAG, "pcp id = " + profiles.get(0).getID());
		Log.d(TAG, "pcp name = " + profiles.get(0).getProfileName());
		Log.d(TAG, "pcp status = " + profiles.get(0).getStatus());
		Log.d(TAG, "pcp s h = " + profiles.get(0).getSchedulingTimeStartHour());
		Log.d(TAG, "pcp s m = " + profiles.get(0).getSchedulingTimeStartMin());
		Log.d(TAG, "pcp e h = " + profiles.get(0).getSchedulingTimeStopHour());
		Log.d(TAG, "pcp e m = " + profiles.get(0).getSchedulingTimeStopMin());
		Log.d(TAG, "pcp day = " + profiles.get(0).getSchedulingDays());

		JNIParentalControlLib.getJNILib().addModifyParentalControlRuleToBuffer(profiles);
	}
}
