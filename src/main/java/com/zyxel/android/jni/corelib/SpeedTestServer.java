package com.zyxel.android.jni.corelib;

/**
 * A data class representing Ookla server information
 */
public class SpeedTestServer {
	public String index;
	public String url;
	public String latitude;
	public String longitude;
	public String name;
	public String country;
	public String country_code;
	public String sponsor;

	public SpeedTestServer(String id, String url_par, String lat, String lon,
			String name_par, String country_par, String cc, String sponsor_par) {
		index = id;
		url = url_par;
		latitude = lat;
		longitude = lon;
		name = name_par;
		country = country_par;
		country_code = cc;
		sponsor = sponsor_par;
	}

	public SpeedTestServer() {

	}

	/**
	 * Get Ookla server index
	 * @return index of the ookla server
	 */
	public String getIndex() {
		return index;
	}

	/**
	 * Get Ookla server url
	 * @return url of the ookla server
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Get Ookla server latitude
	 * @return latitude of the ookla server
	 */
	public String getLatitude() {
		return latitude;
	}

	/**
	 * Get Ookla server longitude
	 * @return longitude of the ookla server
	 */
	public String getLongitude() {
		return longitude;
	}
	/**
	 * Get Ookla server name
	 * @return name of the ookla server
	 */
	public String getName() {
		return name;
	}
	/**
	 * Get Ookla server country
	 * @return country of the ookla server
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * Get Ookla server country code
	 * @return country code of the ookla server
	 */
	public String getCountryCode() {
		return country_code;
	}
	/**
	 * Get Ookla server sponsor
	 * @return sponsor of the ookla server
	 */
	public String getSponsor() {
		return sponsor;
	}

	public void setIndex(String id) {
		index = id;
	}

	public void setUrl(String url_par) {
		url = url_par;
	}

	public void setLatitude(String lat_par) {
		latitude = lat_par;
	}

	public void setLongitude(String lon_par) {
		longitude = lon_par;
	}

	public void setName(String name_par) {
		name = name_par;
	}

	public void setCountry(String country_par) {
		country = country_par;
	}

	public void setCountryCode(String cc) {
		country_code = cc;
	}

	public void setSponsor(String sponsor_par) {
		sponsor = sponsor_par;
	}
}
