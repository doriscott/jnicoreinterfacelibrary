package com.zyxel.android.jni.corelib;

import com.zyxel.android.jni.corelibinterface.JNIGatewayLib;

public class JNISmartHome {

	public static final String TAG = JNISmartHome.class.getSimpleName();

	/**
	 * Setting the status of switch on/off for zwave/zigbee power plug device.
	 * @param id device's ID.
	 * @param op disable or enable.
	 */
	public void addSmartHomePowerPlugBuffer(String protocol, String id, boolean op) {
		String temp;
		if(op)
			temp = "TRUE";
		else
			temp = "FALSE";

		JNIGatewayLib.getJNILib().addSmartHomePowerPlugBuffer(protocol, id, temp);
	}

	public void reloadSmartHomeDaemon(String protocol) {
		JNIGatewayLib.getJNILib().reloadSmartHomeDaemon(protocol);
	}


}
