package com.zyxel.android.jni.corelib;

import com.zyxel.android.jni.corelibinterface.JNIGatewayWiFiLib;

/**
 * It's the JNI API.
 * If your call serial of XXXToBuffer(), it doesn't send to device, until the sendBuffer() be called.
 * @author kevin
 *
 */

/**
 * A implementation class of IJNIAPIs
 */
public class JNIGatewayWiFi {

	public static final String TAG = JNIGatewayWiFi.class.getSimpleName();


	/**
	 * Getting the SSID of WiFi.
	 * @see #addWiFiSSIDToBuffer(String)
	 * @return The SSID of WiFi.
	 */
	public String getWiFiSSID() {
//		return JNILib.getJNILib().parameterGetRequest(Define.GUID_WIFI, Define.UUID_ssid);
		return JNIGatewayWiFiLib.getJNILib().getWiFiSSID();
	}

	/**
	 * Getting the Security of WiFi.
	 * @see #addWiFiSecurityToBuffer(String)
	 * @return The Security of WiFi.
	 */
	public String getWiFiSecurity() {
		String security = JNIGatewayWiFiLib.getJNILib().getWiFiSecurity();
		if(security.equalsIgnoreCase("N/A")) {
			return "";
		} else  {
			return security;
		}
	}

	/**
	 * Getting the password of WiFi.
	 * @see #addWiFiPasswordToBuffer(String)
	 * @return The Password of WiFi.
	 */
	public String getWiFiPassword() {
		String password = JNIGatewayWiFiLib.getJNILib().getWiFiPassword();
		if(password.equalsIgnoreCase("N/A")) {
			return "";
		} else {
			return password;
		}
	}

	/**
	 * Getting the status of WiFi.
	 * @see #addWiFiEnableToBuffer(boolean)
	 * @return The WiFi is enable or not.
	 */
	public boolean getWiFiEnable() {
		boolean temp;
		String temp_str = JNIGatewayWiFiLib.getJNILib().getWiFiEnable();
		if(temp_str.equalsIgnoreCase("true"))
			temp = true;
		else
			temp = false;
		return temp;
	}

	public boolean getWiFiSSIDEnable() {
		boolean temp;
		String temp_str = JNIGatewayWiFiLib.getJNILib().getWiFiSSIDEnable();
		if(temp_str.equalsIgnoreCase("true"))
			temp = true;
		else
			temp = false;
		return temp;
	}

	public String getWiFiSSIDStatus() {
		return JNIGatewayWiFiLib.getJNILib().getWiFiSSIDStatus();
	}

	public boolean getWiFiEnableByBand(int WiFiBand) {
		boolean temp;
		String temp_str = JNIGatewayWiFiLib.getJNILib().getWiFiEnableByBand(WiFiBand);
		if(temp_str.equalsIgnoreCase("true"))
			temp = true;
		else
			temp = false;
		return temp;
	}
	public String getWiFiSSIDByBand(int WiFiBand) {
		return JNIGatewayWiFiLib.getJNILib().getWiFiSSIDByBand(WiFiBand);
	}
	public String getWiFiSecurityByBand(int WiFiBand) {
		String security = JNIGatewayWiFiLib.getJNILib().getWiFiSecurityByBand(WiFiBand);
		if(security.equalsIgnoreCase("N/A")) {
			return "";
		} else  {
			return security;
		}
	}
	public String getWiFiPasswordByBand(int WiFiBand) {
		String password = JNIGatewayWiFiLib.getJNILib().getWiFiPasswordByBand(WiFiBand);
		if(password.equalsIgnoreCase("N/A")) {
			return "";
		} else {
			return password;
		}
	}

	/**
	 * Setting the status of WiFi.
	 */
	public void addWiFiEnableToBuffer(boolean status) {
		String temp;
		if(status)
			temp = "TRUE";
		else
			temp = "FALSE";
		JNIGatewayWiFiLib.getJNILib().addWiFiEnableToBuffer(temp);
	}

	/**
	 * Setting the SSID of WiFi.
	 */
	public void addWiFiSSIDToBuffer(String ssid) {
		JNIGatewayWiFiLib.getJNILib().addWiFiSSIDToBuffer(ssid);
	}

	/**
	 * Setting the security of WiFi.
	 */
	public void addWiFiSecurityToBuffer(String security) {
		JNIGatewayWiFiLib.getJNILib().addWiFiSecurityToBuffer(security); // WPA2, NONE
	}

	/**
	 * Setting the password of WiFi.
	 */
	public void addWiFiPasswordToBuffer(String password) {
		JNIGatewayWiFiLib.getJNILib().addWiFiPasswordToBuffer(password);
	}

	public void addWiFiEnableByBandToBuffer(int WiFiBand, boolean status){
		String temp;
		if(status)
			temp = "TRUE";
		else
			temp = "FALSE";
		JNIGatewayWiFiLib.getJNILib().addWiFiEnableByBandToBuffer(WiFiBand, temp);
	}

	public void addWiFiSSIDByBandToBuffer(int WiFiBand, String ssid) {
		JNIGatewayWiFiLib.getJNILib().addWiFiSSIDByBandToBuffer(WiFiBand, ssid);
	}

	public void addWiFiSecurityByBandToBuffer(int WiFiBand, String security) {
		JNIGatewayWiFiLib.getJNILib().addWiFiSecurityByBandToBuffer(WiFiBand, security);
	}

	public void addWiFiPasswordByBandToBuffer(int WiFiBand, String password) {
		JNIGatewayWiFiLib.getJNILib().addWiFiPasswordByBandToBuffer(WiFiBand, password);
	}
}
