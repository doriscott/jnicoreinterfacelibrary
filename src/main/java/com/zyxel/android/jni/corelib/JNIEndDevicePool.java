package com.zyxel.android.jni.corelib;

import android.util.Log;

import com.zyxel.android.jni.corelibinterface.JNIEndDevicePoolLib;
import com.zyxel.android.jni.model.EndDeviceProfile;
import com.zyxel.android.jni.model.L2FWEndDeviceProfile;

import java.util.ArrayList;

public class JNIEndDevicePool {

	public static final String TAG = JNIEndDevicePool.class.getSimpleName();

	public JNIEndDevicePool() {
	}
	/**
	 * Getting the information of clients (or L2Device).
	 * @see EndDeviceProfile
	 */
	public ArrayList<EndDeviceProfile> getClientList() {
		ArrayList<Object> clientList = JNIEndDevicePoolLib.getJNILib().getTotalEndDevice();
		if(clientList != null) {
//			Log.d("Router APP", "End Device List Size = " + clientList.size());

			ArrayList<EndDeviceProfile> clientlist = new ArrayList<EndDeviceProfile>();
			for(int i = 0; i < clientList.size(); i++) {
				EndDeviceProfile tmp = (EndDeviceProfile) clientList.get(i);
				clientlist.add(tmp);
					Log.d(TAG, "End Device Index = " + i + " --> " +
							"End Device Name = " + tmp.Name + " --> " +
							"End Device MAC = " + tmp.MAC + " --> " +
							"End Device HostType = " + tmp.HostType + " --> " +
							"End Device Active = " + tmp.Active + " --> " +
							"End Device Blocking = " + tmp.Blocking + " --> " +
							"End Device IPAddress = " + tmp.IPAddress + " --> " +
							"End Device RX = " + tmp.getDLRate() + " --> " +
							"End Device TX = " + tmp.getULRate() + " --> " +
							"End Device Band = " + tmp.getBand() + " --> " +
							"End Device 2.4G Channel = " + tmp.getChannel24G() + " --> " +
							"End Device 5G Channel = " + tmp.getChannel5G() + " --> " +
							"End Device Guest Group = " + tmp.GuestGroup);
			}
			return clientlist;
		}else {
			ArrayList<EndDeviceProfile> clientlist = new ArrayList<EndDeviceProfile>();
			return clientlist;
		}
	}

	public ArrayList<L2FWEndDeviceProfile> getL2DeviceList() {
		Object temp = JNIEndDevicePoolLib.getJNILib().getL2DeviceList();
		return (ArrayList<L2FWEndDeviceProfile>) temp;
	}

	/**
	 * Reboot L2 device by MAC.
	 * @param mac device's MAC.
	 */
	public void addL2ControlRebootToBuffer(String mac) {
		JNIEndDevicePoolLib.getJNILib().addL2ControlRebootToBuffer(mac);
	}

	/**
	 * Turn on WiFi for L2 device by MAC
	 * @param mac device's MAC.
	 */
	public void addL2ControlWiFiOnToBuffer(String mac) {
		JNIEndDevicePoolLib.getJNILib().addL2ControlWiFiOnToBuffer(mac);
	}

	/**
	 * Turn off WiFi for L2 device by MAC
	 * @param mac device's MAC.
	 */
	public void addL2ControlWiFiOffToBuffer(String mac) {
		JNIEndDevicePoolLib.getJNILib().addL2ControlWiFiOffToBuffer(mac);
	}



	/**
	 * Setting the name for client.
	 * @see EndDeviceProfile#Name
	 */
	public void addNewDeviceNameToBuffer(String mac, String newName) {
		JNIEndDevicePoolLib.getJNILib().addNewDeviceNameToBuffer(mac, newName);
	}

	/**
	 * Setting the host type for client.
	 * @see EndDeviceProfile#HostType
	 */
	public void addNewDeviceTypeToBuffer(String mac, int newType) {
		JNIEndDevicePoolLib.getJNILib().addNewDeviceTypeToBuffer(mac, newType);
	}

//	public void addBlockDeviceToBuffer(int deviceID) {
//		JNIEndDevicePoolLib.getJNILib().addBlockDeviceToBuffer("TRUE", deviceID);
//	}

	/**
	 * Blocking client by MAC.
	 * @see EndDeviceProfile#Blocking
	 * @param mac client's MAC
	 */
	public void addBlockDeviceToBuffer(String mac) {
		JNIEndDevicePoolLib.getJNILib().addBlockDeviceToBuffer(mac);
	}

//	public void addNonBlockDeviceToBuffer(int deviceID) {
//		JNIEndDevicePoolLib.getJNILib().addNonBlockDeviceToBuffer("FALSE", deviceID);
//	}

	/**
	 * Unblocking client by MAC.
	 * @see EndDeviceProfile#Blocking
	 * @param mac client's MAC
	 */
	public void addNonBlockDeviceToBuffer(String mac) {
		JNIEndDevicePoolLib.getJNILib().addNonBlockDeviceToBuffer(mac);
	}

}
