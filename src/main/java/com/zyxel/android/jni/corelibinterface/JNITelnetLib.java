/****************************************************************************/
/*
 * Copyright (C) 2000-2013 ZyXEL Communications, Corp.
 * All Rights Reserved.
 *
 * ZyXEL Confidential; 
 * Protected as an unpublished work, treated as confidential, 
 * and hold in trust and in strict confidence by receiving party. 
 * Only the employees who need to know such ZyXEL confidential information 
 * to carry out the purpose granted under NDA are allowed to access. 
 * 
 * The computer program listings, specifications and documentation 
 * herein are the property of ZyXEL Communications, Corp. and shall 
 * not be reproduced, copied, disclosed, or used in whole or in part 
 * for any reason without the prior express written permission of 
 * ZyXEL Communications, Corp.
 * 
 */
/****************************************************************************/
package com.zyxel.android.jni.corelibinterface;

import android.util.Log;

public class JNITelnetLib {
	private static JNITelnetLib inst;

	public static JNITelnetLib getJNILib() {
		if (inst == null) {
			inst = new JNITelnetLib();
		}
		return inst;
	}

	private final static String TAG = JNITelnetLib.class.getSimpleName();

	static {
		try {
			System.loadLibrary("gl2jni");
		} catch (UnsatisfiedLinkError e) {
			Log.d(TAG, e.toString());
		}
	}

	// Telnet.cpp
	public native int loginWithTelnet(String ip, int port, String username, String password);
	public native void logoutWithTelnet();
	public native int getModelNameWithTelent(String modelname);
	public native int setNewFirmwareDownloadWithTelnet();
	public native int getNewFirmwareDownloadStatusWithTelnet();
	public native int setNewFirmwareUpgradeWithTelnet();




}
