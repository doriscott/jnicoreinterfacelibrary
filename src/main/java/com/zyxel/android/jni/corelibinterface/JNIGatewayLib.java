/****************************************************************************/
/*
 * Copyright (C) 2000-2013 ZyXEL Communications, Corp.
 * All Rights Reserved.
 *
 * ZyXEL Confidential; 
 * Protected as an unpublished work, treated as confidential, 
 * and hold in trust and in strict confidence by receiving party. 
 * Only the employees who need to know such ZyXEL confidential information 
 * to carry out the purpose granted under NDA are allowed to access. 
 * 
 * The computer program listings, specifications and documentation 
 * herein are the property of ZyXEL Communications, Corp. and shall 
 * not be reproduced, copied, disclosed, or used in whole or in part 
 * for any reason without the prior express written permission of 
 * ZyXEL Communications, Corp.
 * 
 */
/****************************************************************************/
package com.zyxel.android.jni.corelibinterface;

import android.util.Log;

import com.zyxel.android.jni.model.GatewayProfile;
import com.zyxel.android.jni.model.ParentalControlProfile;
import com.zyxel.android.jni.model.WiFiAutoConfigApproveProfile;

import java.util.ArrayList;

// Wrapper for native library

/**
 * A class representing APIs between JAVA and JNI
 */
public class JNIGatewayLib {
	private static JNIGatewayLib inst;

	public static JNIGatewayLib getJNILib() {
		if (inst == null) {
			inst = new JNIGatewayLib();
		}
		return inst;
	}

	private final static String TAG = JNIGatewayLib.class.getSimpleName();

	static {
		try {
			System.loadLibrary("gl2jni");
		} catch (UnsatisfiedLinkError e) {
			Log.d(TAG, e.toString());
		}
	}

	//	JNIGateway.cpp
	public native void search();
	public native String queryModelName();
	public native String[] queryDeviceList(String model);
	public native ArrayList<GatewayProfile> queryGatewayList();

	public native boolean loginDevice(int index, String password);
	public native ArrayList<Object> getAppFeatureList();

	public native String isNewGatewayFirmwareAvailable();
	public native String getNewGatewayFirmwareVersion();
	public native String getNewGatewayFirmwareReleaseDate();
	public native Object getNewGatewayFirmwareInfo();
	public native String getNewGatewayFirmwareUpdateStatus();

	public native String getWiFiAutoConfigEnable();
	public native String getDevicePassword();
	public native Object getDeviceInfo();
	public native Object getWanInfo();
	public native String getGatewayLanIP();
	public native ArrayList<Object> getTraceRouterResult();
	public native ArrayList<Object> getIPV4Forwarding();

	public native String getCurrentFirmwareVersion();
	public native String getLatestFirmwareVersion();
	public native String checkNewFirmwareVersion();
	public native String getUpdateFirmwareStatus();

	public native String getGatewayHostName();
	public native String getGatewayHostMAC();
	public native int getGatewayAutoConfig();
	public native boolean getAutoConfigApprove();

	public native String getGateway2InternetDownload();
	public native String getGateway2InternetUpload();
	public native String isGateway2InternetSpeedTestEnd();
	public native int getClient2GatewaySpeedTestPort();

	public native int getL2FWUpgradingStatus(String mac);
	public native int getGateway2ClientRSSI(String mobileMAC);

	public native int setAPWiFiAutoConfig();
	public native void addRebootToBuffer();
	public native void addWiFiAutoConfigEnableToBuffer(String status);
	public native void addDevicePasswordToBuffer(String password);
	public native void addFirmwareUpgradeToBuffer(String status);
	public native void setGatewayFirmwareUpgrade();
	public native int setClient2GatewaySpeedTestEnable(int option);
	public native void setGateway2InternetSpeedTest(String url, int option);
	public native void addL2WiFiAutoConfigEnableBuffer(String mac, String status);
	public native void setL2ControlFirmwareConfig(String mac, int option);
	public native void addL2ControlFirmwareConfigToBuffer(String mac, int option);

	public native void addSmartHomePowerPlugBuffer(String protocol, String id, String op);
	public native void reloadSmartHomeDaemon(String protocol);

	public native void addAutoConfigApproveProfileListToBuffer(ArrayList<WiFiAutoConfigApproveProfile> profiles);
	public native void setAutoConfigApproveToBuffer(boolean option);

	public native void registerErrorCodeBackFunction();


}
