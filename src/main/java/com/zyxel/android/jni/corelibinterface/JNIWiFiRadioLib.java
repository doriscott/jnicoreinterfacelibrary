/****************************************************************************/
/*
 * Copyright (C) 2000-2013 ZyXEL Communications, Corp.
 * All Rights Reserved.
 *
 * ZyXEL Confidential; 
 * Protected as an unpublished work, treated as confidential, 
 * and hold in trust and in strict confidence by receiving party. 
 * Only the employees who need to know such ZyXEL confidential information 
 * to carry out the purpose granted under NDA are allowed to access. 
 * 
 * The computer program listings, specifications and documentation 
 * herein are the property of ZyXEL Communications, Corp. and shall 
 * not be reproduced, copied, disclosed, or used in whole or in part 
 * for any reason without the prior express written permission of 
 * ZyXEL Communications, Corp.
 * 
 */
/****************************************************************************/
package com.zyxel.android.jni.corelibinterface;

import android.util.Log;

import com.zyxel.android.jni.model.WiFiRadioProfile;

import java.util.ArrayList;

// Wrapper for native library

/**
 * A class representing APIs between JAVA and JNI
 */
public class JNIWiFiRadioLib {
	private static JNIWiFiRadioLib inst;

	public static JNIWiFiRadioLib getJNILib() {
		if (inst == null) {
			inst = new JNIWiFiRadioLib();
		}
		return inst;
	}

	private final static String TAG = JNIWiFiRadioLib.class.getSimpleName();

	static {
		try {
			System.loadLibrary("gl2jni");
		} catch (UnsatisfiedLinkError e) {
			Log.d(TAG, e.toString());
		}
	}

	//	JNIWiFiRadio.cpp
	public native String getRadioEnable();
	public native String getRadioStatus();
	public native String getMaxBitRate();
	public native String getOperatingFrequency();
	public native String getOperatingStandard();
	public native String getAutoChannelEnable();
	public native String getChannel();
	public native String getTransmitPower();
	public native void addAutoChannelEnableToBuffer(String status);
	public native void addChannelToBuffer(String channel);

	public native int getTotalRadioNumber();
	public native ArrayList<Object> getWiFiRadioProfile();

}
