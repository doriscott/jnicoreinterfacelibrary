/****************************************************************************/
/*
 * Copyright (C) 2000-2013 ZyXEL Communications, Corp.
 * All Rights Reserved.
 *
 * ZyXEL Confidential; 
 * Protected as an unpublished work, treated as confidential, 
 * and hold in trust and in strict confidence by receiving party. 
 * Only the employees who need to know such ZyXEL confidential information 
 * to carry out the purpose granted under NDA are allowed to access. 
 * 
 * The computer program listings, specifications and documentation 
 * herein are the property of ZyXEL Communications, Corp. and shall 
 * not be reproduced, copied, disclosed, or used in whole or in part 
 * for any reason without the prior express written permission of 
 * ZyXEL Communications, Corp.
 * 
 */
/****************************************************************************/
package com.zyxel.android.jni.corelibinterface;

import android.util.Log;

// Wrapper for native library

/**
 * A class representing APIs between JAVA and JNI
 */
public class JNIGatewayWiFiLib {
	private static JNIGatewayWiFiLib inst;

	public static JNIGatewayWiFiLib getJNILib() {
		if (inst == null) {
			inst = new JNIGatewayWiFiLib();
		}
		return inst;
	}

	private final static String TAG = JNIGatewayWiFiLib.class.getSimpleName();

	static {
		try {
			System.loadLibrary("gl2jni");
		} catch (UnsatisfiedLinkError e) {
			Log.d(TAG, e.toString());
		}
	}

	//	JNIGatewayWiFi.cpp
	public native String getWiFiEnable();
	public native String getWiFiSSIDEnable();
	public native String getWiFiSSIDStatus();
	public native String getWiFiSSID();
	public native String getWiFiSecurity();
	public native String getWiFiPassword();
	public native String getWiFiEnableByBand(int WiFiBand);
	public native String getWiFiSSIDByBand(int WiFiBand);
	public native String getWiFiSecurityByBand(int WiFiBand);
	public native String getWiFiPasswordByBand(int WiFiBand);

	public native void addWiFiEnableToBuffer(String status);
	public native void addWiFiSSIDToBuffer(String ssid);
	public native void addWiFiSecurityToBuffer(String security);
	public native void addWiFiPasswordToBuffer(String password);
	public native void addWiFiEnableByBandToBuffer(int WiFiBand, String status);
	public native void addWiFiSSIDByBandToBuffer(int WiFiBand, String ssid);
	public native void addWiFiSecurityByBandToBuffer(int WiFiBand, String security);
	public native void addWiFiPasswordByBandToBuffer(int WiFiBand, String password);

}
