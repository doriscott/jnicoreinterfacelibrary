/****************************************************************************/
/*
 * Copyright (C) 2000-2013 ZyXEL Communications, Corp.
 * All Rights Reserved.
 *
 * ZyXEL Confidential; 
 * Protected as an unpublished work, treated as confidential, 
 * and hold in trust and in strict confidence by receiving party. 
 * Only the employees who need to know such ZyXEL confidential information 
 * to carry out the purpose granted under NDA are allowed to access. 
 * 
 * The computer program listings, specifications and documentation 
 * herein are the property of ZyXEL Communications, Corp. and shall 
 * not be reproduced, copied, disclosed, or used in whole or in part 
 * for any reason without the prior express written permission of 
 * ZyXEL Communications, Corp.
 * 
 */
/****************************************************************************/
package com.zyxel.android.jni.corelibinterface;

import android.util.Log;

// Wrapper for native library

/**
 * A class representing APIs between JAVA and JNI
 */
public class JNILib {
	private static JNILib inst;

	public static JNILib getJNILib() {
		if (inst == null) {
			inst = new JNILib();
		}
		return inst;
	}

	private final static String TAG = JNILib.class.getSimpleName();

	static {
		try {
			System.loadLibrary("gl2jni");
		} catch (UnsatisfiedLinkError e) {
			Log.d(TAG, e.toString());
		}
	}

	// android_interface.cpp
	public native void init();
	public native void closeSocket();
	public native void sendBuffer();

}
