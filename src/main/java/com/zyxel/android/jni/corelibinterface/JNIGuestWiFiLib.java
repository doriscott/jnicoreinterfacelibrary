/****************************************************************************/
/*
 * Copyright (C) 2000-2013 ZyXEL Communications, Corp.
 * All Rights Reserved.
 *
 * ZyXEL Confidential; 
 * Protected as an unpublished work, treated as confidential, 
 * and hold in trust and in strict confidence by receiving party. 
 * Only the employees who need to know such ZyXEL confidential information 
 * to carry out the purpose granted under NDA are allowed to access. 
 * 
 * The computer program listings, specifications and documentation 
 * herein are the property of ZyXEL Communications, Corp. and shall 
 * not be reproduced, copied, disclosed, or used in whole or in part 
 * for any reason without the prior express written permission of 
 * ZyXEL Communications, Corp.
 * 
 */
/****************************************************************************/
package com.zyxel.android.jni.corelibinterface;

import android.util.Log;

import com.zyxel.android.jni.model.GuestWiFiTimerProfile;

import java.util.ArrayList;

// Wrapper for native library

/**
 * A class representing APIs between JAVA and JNI
 */
public class JNIGuestWiFiLib {
	private static JNIGuestWiFiLib inst;

	public static JNIGuestWiFiLib getJNILib() {
		if (inst == null) {
			inst = new JNIGuestWiFiLib();
		}
		return inst;
	}

	private final static String TAG = JNIGuestWiFiLib.class.getSimpleName();

	static {
		try {
			System.loadLibrary("gl2jni");
		} catch (UnsatisfiedLinkError e) {
			Log.d(TAG, e.toString());
		}
	}

	//	JNIGuestWiFi.cpp
	public native String getGuestWiFiEnable();
	public native String getGuestWiFiSSID();
	public native String getGuestWiFiSecurity();
	public native String getGuestWiFiPassword();
	public native Object getGuestWiFiTimer();

	public native void addGuestWiFiEnableToBuffer(String status);
	public native void addGuestWiFiSSIDToBuffer(String ssid);
	public native void addGuestWiFiSecurityToBuffer(String security);
	public native void addGuestWiFiPasswordToBuffer(String password);
//	public native void setGuestWiFiTimer(ArrayList<GuestWiFiTimerProfile> guestWiFiTimerProfile);
	public native void addGuestWiFiTimerToBuffer(ArrayList<GuestWiFiTimerProfile> guestWiFiTimerProfile);

}
