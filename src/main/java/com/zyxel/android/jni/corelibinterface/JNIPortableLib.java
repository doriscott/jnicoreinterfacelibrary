/****************************************************************************/
/*
 * Copyright (C) 2000-2013 ZyXEL Communications, Corp.
 * All Rights Reserved.
 *
 * ZyXEL Confidential; 
 * Protected as an unpublished work, treated as confidential, 
 * and hold in trust and in strict confidence by receiving party. 
 * Only the employees who need to know such ZyXEL confidential information 
 * to carry out the purpose granted under NDA are allowed to access. 
 * 
 * The computer program listings, specifications and documentation 
 * herein are the property of ZyXEL Communications, Corp. and shall 
 * not be reproduced, copied, disclosed, or used in whole or in part 
 * for any reason without the prior express written permission of 
 * ZyXEL Communications, Corp.
 * 
 */
/****************************************************************************/
package com.zyxel.android.jni.corelibinterface;

import android.util.Log;

// Wrapper for native library

/**
 * A class representing APIs between JAVA and JNI
 */
public class JNIPortableLib {
	private static JNIPortableLib inst;

	public static JNIPortableLib getJNILib() {
		if (inst == null) {
			inst = new JNIPortableLib();
		}
		return inst;
	}

	private final static String TAG = JNIPortableLib.class.getSimpleName();

	static {
		try {
			System.loadLibrary("gl2jni");
		} catch (UnsatisfiedLinkError e) {
			Log.d(TAG, e.toString());
		}
	}


	// JNIPortable.cpp
	public native String getAPN();
	public native String getAuthentication();
	public native String getUserName();
	public native String get3GPassword();
	public native String getSignalStrength();
	public native String getCellularMode();
	public native String getServiceProvider();
	public native String getAutoReset();
	public native String getResetCount();
	public native String getResetDay();
	public native String getLastResetDate();
	public native String getQuota();
	public native String getCurrentUsage();
	public native String getByteSent();
	public native String getByteReceived();
	public native String getUsageAdvice();
	public native String getDataPlanManagementEnable();
	public native String getBatteryRemainCapacity();
	public native String getBatteryTotalCapacity();
	public native String getBatteryChargeStatus();

	public native void addAPNToBuffer(String apn);
	public native void addAuthenticationToBuffer(String authentication);
	public native void addUserNameToBuffer(String username);
	public native void add3GPasswordToBuffer(String password);
	public native void addQuotaToBuffer(String quota);
	public native void addResetDayToBuffer(String day);
	public native void addAutoResetToBuffer(String status);
	public native void addDataPlanManagementEnableToBuffer(String status);

}
