/****************************************************************************/
/*
 * Copyright (C) 2000-2013 ZyXEL Communications, Corp.
 * All Rights Reserved.
 *
 * ZyXEL Confidential; 
 * Protected as an unpublished work, treated as confidential, 
 * and hold in trust and in strict confidence by receiving party. 
 * Only the employees who need to know such ZyXEL confidential information 
 * to carry out the purpose granted under NDA are allowed to access. 
 * 
 * The computer program listings, specifications and documentation 
 * herein are the property of ZyXEL Communications, Corp. and shall 
 * not be reproduced, copied, disclosed, or used in whole or in part 
 * for any reason without the prior express written permission of 
 * ZyXEL Communications, Corp.
 * 
 */
/****************************************************************************/
package com.zyxel.android.jni.corelibinterface;

import android.util.Log;

import com.zyxel.android.jni.model.ParentalControlProfile;

import java.util.ArrayList;

// Wrapper for native library

/**
 * A class representing APIs between JAVA and JNI
 */
public class JNIParentalControlLib {
	private static JNIParentalControlLib inst;

	public static JNIParentalControlLib getJNILib() {
		if (inst == null) {
			inst = new JNIParentalControlLib();
		}
		return inst;
	}

	private final static String TAG = JNIParentalControlLib.class.getSimpleName();

	static {
		try {
			System.loadLibrary("gl2jni");
		} catch (UnsatisfiedLinkError e) {
			Log.d(TAG, e.toString());
		}
	}

	//	JNIParentalControl.cpp
	public native ArrayList<Object> getParentalControlProfile();
	public native ArrayList<Object> getDeviceParentalControlProfile(int index); // does not use on one connect app
//	public native String getParentalControlProfileNumber();

	public native void addNewParentalControlRuleToBuffer(ArrayList<ParentalControlProfile> profiles);
	public native void addEliminateParentalControlRuleToBuffer(ArrayList<ParentalControlProfile> profiles);
	public native void addModifyParentalControlRuleToBuffer(ArrayList<ParentalControlProfile> profiles);

}
