package com.zyxel.android.jni.model;

import com.zyxel.android.jni.corelib.JNICall;

/**
 * A data class representing ipv4 forwarding information
 * @see	JNICall#getIPV4Forwarding()
 */
public class IPV4ForwardingProfile {
	String DestIPAddress;
    String DestSubnetMask;
    String GatewayIPAddress;
    String Interface;
    
    public IPV4ForwardingProfile(){
    	
    }
    public IPV4ForwardingProfile(String destipaddress_arg, String destsubnetmask_arg, String gatewayipaddress_arg, String interface_arg){
    	this.DestIPAddress = destipaddress_arg;
    	this.DestSubnetMask = destsubnetmask_arg;
    	this.GatewayIPAddress = gatewayipaddress_arg;
    	this.Interface = interface_arg;
    }

	/**
	 * Get ipv4 forwarding destination ip address
	 * @return destination ip address
	 */
    public String getDestIPAddress(){
		return this.DestIPAddress;
	}

	/**
	 * Get ipv4 forwarding destination subnet mask
	 * @return destination subnet mask
	 */
    public String getDestSubnetMask(){
		return this.DestSubnetMask;
	}

	/**
	 * Get ipv4 forwarding gateway ip address
	 * @return gateway ip address
	 */
    public String getGatewayIPAddress(){
		return this.GatewayIPAddress;
	}
    public String getInterface(){
		return this.Interface;
	}
	
	public void setDestIPAddress(String destipaddress_arg){
		this.DestIPAddress = destipaddress_arg;
	}
	public void setDestSubnetMask(String destsubnetmask_arg){
		this.DestSubnetMask = destsubnetmask_arg;
	}
	public void setGatewayIPAddress(String gatewayipaddress_arg){
		this.GatewayIPAddress = gatewayipaddress_arg;
	}
	public void setInterface(String interface_arg){
		this.Interface = interface_arg;
	}
	
}
