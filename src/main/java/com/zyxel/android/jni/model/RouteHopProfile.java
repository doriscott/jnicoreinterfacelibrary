package com.zyxel.android.jni.model;

import com.zyxel.android.jni.corelib.JNICall;

/**
 * A class representing trace router result
 * @see JNICall#getTraceRouterResult()
 */
public class RouteHopProfile {
	String Host;
    String HostAddress;
    String RTTimes;
    
    public RouteHopProfile(){
    	
    }
    public RouteHopProfile(String host_arg, String hostaddress_arg, String rtttime_arg){
    	this.Host = host_arg;
    	this.HostAddress = hostaddress_arg;
    	this.RTTimes = rtttime_arg;
    }
    
    public String getHost(){
		return this.Host;
	}
    public String getHostAddress(){
		return this.HostAddress;
	}
    public String getRTTimes(){
		return this.RTTimes;
	}
    
    
	public void setHost(String host_arg){
		this.Host = host_arg;
	}
	public void setHostAddress(String hostaddress_arg){
		this.HostAddress = hostaddress_arg;
	}
	public void setRTTimes(String rtttime_arg){
		this.RTTimes = rtttime_arg;
	}
}
