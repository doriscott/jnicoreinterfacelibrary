/****************************************************************************/
/*
 * Copyright (C) 2000-2013 ZyXEL Communications, Corp.
 * All Rights Reserved.
 *
 * ZyXEL Confidential; 
 * Protected as an unpublished work, treated as confidential, 
 * and hold in trust and in strict confidence by receiving party. 
 * Only the employees who need to know such ZyXEL confidential information 
 * to carry out the purpose granted under NDA are allowed to access. 
 * 
 * The computer program listings, specifications and documentation 
 * herein are the property of ZyXEL Communications, Corp. and shall 
 * not be reproduced, copied, disclosed, or used in whole or in part 
 * for any reason without the prior express written permission of 
 * ZyXEL Communications, Corp.
 * 
 */
/****************************************************************************/
package com.zyxel.android.jni.model;

import com.zyxel.android.jni.corelib.JNICall;

import java.io.Serializable;

/**
 * A data class representing end device information in terms of mac, name, user define name, blocking, ip address, connection type,
 * capability type, software version, host type, l2 auto configuration enable, l2 wifi status, l2 wifi sync status, signal strength
 * @see JNICall#getClientList()
 */
public class EndDeviceProfile implements Serializable {

	/**
	 * It's the MAC of client.
	 */
	public String MAC;

	/**
	 * It's the name of client.
	 */
	public String Name;

	/**
	 * It's let user to re-define name for client.
	 * The default value as name.
	 * @see JNICall#addNewDeviceNameToBuffer(String, String)
	 */
	public String UserDefineName;

	public String Active;

	/**
	 * Whether the client is be blocked.
	 * The value can be excepted as "Blocking" or "Non-Blocking".
	 * @see JNICall#addBlockDeviceToBuffer(String)
	 * @see JNICall#addNonBlockDeviceToBuffer(String)
	 */
	public String Blocking;

	/**
	 * It's the IP address of client.
	 */
	public String IPAddress;

	/**
	 * Which way the client connect to gateway.
	 * The value can be excepted as "WiFi", "WiFi_2", "WiFi_5" and "Other"
	 */
	public String ConnectionType;

	/**
	 * What capability type is the client represented.
	 * The value can be excepted as "Client" or "L2Device".
	 */
	public String CapabilityType;

	/**
	 * The software version of client.
	 */
	public String SoftwareVersion;

	/**
	 * What host type is the client represented.<br/> 
	 * 1:Desktop <br/> 
	 * 2:Notebook <br/> 
	 * 3:Router <br/> 
	 * 4:NAS <br/> 
	 * 5:Power line <br/> 
	 * 6:Repeater <br/> 
	 * 7:Smart Phone <br/> 
	 * 8:Camera <br/>
	 * 9:Tablet <br/>
	 * @see JNICall#addNewDeviceTypeToBuffer(String, int)
	 */
	public int HostType;

	public int L2AutoConfigEnable;
	public int L2WifiStatus;
	public int L2WifiSyncStatus;

	/**
	 * It's the WiFi signal strength, range 0~5
	 */
	public int SignalStrength;

	/**
	 * The physical rate of client.
	 */
	public int PhyRate;
	public int L2ControllableMask;

	public String Neighbor;
	public int DLRate;

	public int getULRate() {
		return ULRate;
	}

	public void setULRate(int ULRate) {
		this.ULRate = ULRate;
	}

	public String getNeighbor() {
		return Neighbor;
	}

	public void setNeighbor(String neighbor) {
		Neighbor = neighbor;
	}

	public int getDLRate() {
		return DLRate;
	}

	public void setDLRate(int DLRate) {
		this.DLRate = DLRate;
	}

	public String getConnectionType() {
		return ConnectionType;
	}

	public void setConnectionType(String connectionType) {
		ConnectionType = connectionType;
	}

	public String getCapabilityType() {
		return CapabilityType;
	}

	public void setCapabilityType(String capabilityType) {
		CapabilityType = capabilityType;
	}

	public int ULRate;

	public int getGuestGroup() {
		return GuestGroup;
	}

	public void setGuestGroup(int guestGroup) {
		this.GuestGroup = guestGroup;
	}

	public int GuestGroup;

	public int getWiFiAutoConfigApprove() {
		return WiFiAutoConfigApprove;
	}

	public void setWiFiAutoConfigApprove(int wiFiAutoConfigApprove) {
		WiFiAutoConfigApprove = wiFiAutoConfigApprove;
	}

	public int WiFiAutoConfigApprove;

	public EndDeviceProfile(String mac, String name, String active,
							int hosttype, String blocking, String ipaddress,
							int controlmask) {
		MAC = mac;
		Name = name;
		Active = active;
		HostType = hosttype;
		Blocking = blocking;
		IPAddress = ipaddress;
		L2ControllableMask = controlmask;
	}

	public String getManufacturer() {
		return Manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		Manufacturer = manufacturer;
	}

	public int getRssi() {
		return Rssi;
	}

	public void setRssi(int rssi) {
		Rssi = rssi;
	}

	public int getBand() {
		return Band;
	}

	public void setBand(int band) {
		Band = band;
	}

	public int getLinkRate24G() {
		return LinkRate24G;
	}

	public void setLinkRate24G(int linkRate24G) {
		LinkRate24G = linkRate24G;
	}

	public int getLinkRate5G() {
		return LinkRate5G;
	}

	public void setLinkRate5G(int linkRate5G) {
		LinkRate5G = linkRate5G;
	}

	public int getChannel24G() {
		return Channel24G;
	}

	public void setChannel24G(int channel24G) {
		Channel24G = channel24G;
	}

	public int getChannel5G() {
		return Channel5G;
	}

	public void setChannel5G(int channel5G) {
		Channel5G = channel5G;
	}

	public String Manufacturer;
	public int Rssi;
	public int Band;
	public int LinkRate24G;
	public int LinkRate5G;
	public int Channel24G;
	public int Channel5G;
	public EndDeviceProfile() {

	}

	public void setMAC(String mac_arg) {
		MAC = mac_arg;
	}

	public void setName(String name_arg) {
		Name = name_arg;
	}
	public void setUserDefineName(String userdefinename_arg) {
		Name = userdefinename_arg;
	}
	public void setActive(String active_arg) {
		Active = active_arg;
	}

	public void setHostType(int hosttype_arg) {
		HostType = hosttype_arg;
	}

	public void setBlocking(String blocking_arg) {
		Blocking = blocking_arg;
	}

	public void setIPAddress(String ip_arg) {
		IPAddress = ip_arg;
	}
	public void SetConnectionType(String connection_arg){
		ConnectionType = connection_arg;
	}
	public void SetCapabilityType(String capability_arg){
		CapabilityType = capability_arg;
	}
	public void setSoftwareVersion(String version_arg){
		SoftwareVersion = version_arg;
	}
	public void setL2ControllableMask(int cm_arg) {
		L2ControllableMask = cm_arg;
	}
	public void setL2AutoConfigEnable(int L2AutoConfigEnable_arg){
		L2AutoConfigEnable =L2AutoConfigEnable_arg;
	}
	public void setL2WifiStatus(int L2WifiStatus_arg){
		L2WifiStatus = L2WifiStatus_arg;
	}
	public void setL2WifiSyncStatus(int L2WifiSyncStatus_arg){
		L2WifiSyncStatus = L2WifiSyncStatus_arg;
	}
	public void setSignalStrength(int SignalStrength_arg){
		SignalStrength = SignalStrength_arg;
	}
	public void setPhyRate(int PhyRate_arg){
		PhyRate = PhyRate_arg;
	}
	public String getMAC() {
		return MAC;
	}
	public String getName() {
		return Name;
	}
	public String getUserDefineName() {
		return UserDefineName;
	}
	public String getActive() {
		return Active;
	}

	public int getHostType() {
		return HostType;
	}

	public String getBlocking() {
		return Blocking;
	}

	public String getIPAddress() {
		return IPAddress;
	}
	public String GetConnectionType(){
		return ConnectionType;
	}
	public String GetCapabilityType(){
		return CapabilityType;
	}
	public String getSoftwareVersion(){
		return SoftwareVersion;
	}
	public int getL2ControllableMask() {
		return L2ControllableMask;
	}
	public int getL2AutoConfigEnable() {
		return L2AutoConfigEnable;
	}
	public int getL2WifiStatus() {
		return L2WifiStatus;
	}
	public int getL2WifiSyncStatus() {
		return L2WifiSyncStatus;
	}
	public int getSignalStrength() {
		return SignalStrength;
	}
	public int getPhyRate() {
		return PhyRate;
	}
}
