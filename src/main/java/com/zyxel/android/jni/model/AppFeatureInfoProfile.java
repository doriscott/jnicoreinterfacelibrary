package com.zyxel.android.jni.model;

import com.zyxel.android.jni.corelib.JNICall;

/**
 * A data class representing a gateway's features item and version
 * @see JNICall#getAppFeatureList()
 */
public class AppFeatureInfoProfile {

    String AppVersion;
    int DeviceInfo;
    int GatewayInfo;
    int FlowInfo;
    int Login;
    int NetTopology;
    int DeviceCtrl;
    int L2DeviceCtrl;
    int DevIcon;
    int GuestWiFi;
    int WiFiAutoCfg;
    int WiFiOnOff;
    int SpeedTest;
    int Diagnostic;
    int CloudService;
    int FwUpgrade;
    int LEDOnOff;
    int GuestWiFiGroup;

    public AppFeatureInfoProfile() {

    }

    /**
     * Get the app agent version
     * @return String
     */
    public String getAppVersion() {
        return AppVersion;
    }

    public void setAppVersion(String appVersion) {
        AppVersion = appVersion;
    }

    /**
     * Get device information feature version
     * @return 101:1.0, 102:2.0
     */
    public int getDeviceInfo() {
        return DeviceInfo;
    }

    public void setDeviceInfo(int deviceInfo) {
        DeviceInfo = deviceInfo;
    }

    /**
     * Get gateway information feature version
     * @return 201:1.0, 202:2.0
     */
    public int getGatewayInfo() {
        return GatewayInfo;
    }

    public void setGatewayInfo(int gatewayInfo) {
        GatewayInfo = gatewayInfo;
    }

    /**
     * Get flow information feature version
     * @return 301:1.0 wan flow information, 302:2.0 per device flow information
     */
    public int getFlowInfo() {
        return FlowInfo;
    }

    public void setFlowInfo(int flowInfo) {
        FlowInfo = flowInfo;
    }

    /**
     * Get login feature version
     * @return 401:1.0 local access, 402:2.0 remote access
     */
    public int getLogin() {
        return Login;
    }

    public void setLogin(int login) {
        Login = login;
    }

    /**
     * Get network topology feature version
     * @return 501:1.0 only layer in network topology, 502:2.0 hierarchical layers network topology
     */
    public int getNetTopology() {
        return NetTopology;
    }

    public void setNetTopology(int netTopology) {
        NetTopology = netTopology;
    }

    /**
     * Get network device control feature version
     * @return 601:1.0 support internet access blocking, 602:2.0 support internet access blocking, scheduling of internet access blocking
     */
    public int getDeviceCtrl() {
        return DeviceCtrl;
    }

    public void setDeviceCtrl(int deviceCtrl) {
        DeviceCtrl = deviceCtrl;
    }

    /**
     * Get L2 device control feature version
     * @return 701:1.0 reboot, Wi-Fi ON/OFF
     */
    public int getL2DeviceCtrl() {
        return L2DeviceCtrl;
    }

    public void setL2DeviceCtrl(int l2DeviceCtrl) {
        L2DeviceCtrl = l2DeviceCtrl;
    }

    /**
     * Get network icon feature version
     * @return 801:1.0, 802:2.0
     */
    public int getDevIcon() {
        return DevIcon;
    }

    public void setDevIcon(int devIcon) {
        DevIcon = devIcon;
    }

    /**
     * Get guest wifi feature version
     * @return 901:1.0 Guest Wi-Fi Enable/SSID/Preshare key setting, 902:2.0 Time limit of using guest Wi-Fi, 903:2.1 Guest Wi-Fi updated
     */
    public int getGuestWiFi() {
        return GuestWiFi;
    }

    public void setGuestWiFi(int guestWiFi) {
        GuestWiFi = guestWiFi;
    }

    /**
     * Get Wi-Fi Auto-Configuration feature version
     * @return 1001:1.0 L2 device support Wi-Fi Auto-Configuration
     */
    public int getWiFiAutoCfg() {
        return WiFiAutoCfg;
    }

    public void setWiFiAutoCfg(int wiFiAutoCfg) {
        WiFiAutoCfg = wiFiAutoCfg;
    }

    /**
     *
     * @return
     */
    public int getWiFiOnOff() {
        return WiFiOnOff;
    }

    public void setWiFiOnOff(int wiFiOnOff) {
        WiFiOnOff = wiFiOnOff;
    }

    /**
     * Get Speed Test feature version
     * @return 1201:1.0 client to internet, 1202:2.0 gateway to internet, 1203:2.1 client to gateway
     */
    public int getSpeedTest() {
        return SpeedTest;
    }

    public void setSpeedTest(int speedTest) {
        SpeedTest = speedTest;
    }

    /**
     * Get diagnostic detecting tool feature version
     * @return 1301:1.0 ping, traceroute, routing table, 1302:2.1 ping, traceroute, routing table, topology list
     */
    public int getDiagnostic() {
        return Diagnostic;
    }

    public void setDiagnostic(int diagnostic) {
        Diagnostic = diagnostic;
    }

    public int getCloudService() {
        return CloudService;
    }

    public void setCloudService(int cloudService) {
        CloudService = cloudService;
    }

    /**
     * Get firmware upgrade feature version
     * @return 1501:2.0 control l2 device and gateway to do firmware upgrade
     */
    public int getFwUpgrade() {
        return FwUpgrade;
    }

    public void setFwUpgrade(int fwUpgrade) {
        FwUpgrade = fwUpgrade;
    }

    /**
     * Get total LED on/off feature version
     * @return 1601:2.1 turn on/off l2 device and gateway LED
     */
    public int getLEDOnOff() {
        return LEDOnOff;
    }

    public void setLEDOnOff(int LEDOnOff) {
        this.LEDOnOff = LEDOnOff;
    }


    public int getGuestWiFiGroup() {
        return GuestWiFiGroup;
    }

    public void setGuestWiFiGroup(int GuestWiFiGroup) {
        this.GuestWiFiGroup = GuestWiFiGroup;
    }
}
