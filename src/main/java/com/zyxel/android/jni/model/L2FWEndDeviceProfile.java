package com.zyxel.android.jni.model;

import com.zyxel.android.jni.corelib.JNICall;

/**
 * A data class representing upgradable L2 device information in terms of mac, model name, new firmware version, new firmware release date, firmware upgrade status
 * @see JNICall#getL2DeviceList()
 */
public class L2FWEndDeviceProfile {

    public String MAC;
    public String ModelName;
    public String NewFWVersion;
    public String NewFWReleaseDate;
    public int FWUpgradeState;

    /**
     * Get upgradable L2 device mac address
     * @return mac address
     */
    public String getMAC() {
        return MAC;
    }

    public void setMAC(String MAC) {
        this.MAC = MAC;
    }

    /**
     * Get upgradable L2 device model name
     * @return model name
     */
    public String getModelName() {
        return ModelName;
    }

    public void setModelName(String modelName) {
        ModelName = modelName;
    }

    /**
     * Get upgradable L2 device new firmware version
     * @return new firmware version
     */
    public String getNewFWVersion() {
        return NewFWVersion;
    }

    public void setNewFWVersion(String newFWVersion) {
        NewFWVersion = newFWVersion;
    }

    /**
     * Get upgradable L2 device new firmware release date
     * @return new firmware release date
     */
    public String getNewFWReleaseDate() {
        return NewFWReleaseDate;
    }

    public void setNewFWReleaseDate(String newFWReleaseDate) {
        NewFWReleaseDate = newFWReleaseDate;
    }

    /**
     * Get upgradable L2 device new firmware upgrade status
     * @return new firmware upgrade status
     */
    public int getFWUpgradeState() {
        return FWUpgradeState;
    }

    public void setFWUpgradeState(int FWUpgradeState) {
        this.FWUpgradeState = FWUpgradeState;
    }

}
