package com.zyxel.android.jni.model;

import com.zyxel.android.jni.corelib.JNICall;

/**
 * A data class representing gateway's Wi-Fi radio information
 */
public class WiFiRadioProfile {
    public int get_24gStatus() {
        return _24gStatus;
    }

    public void set_24gStatus(int _24gStatus) {
        this._24gStatus = _24gStatus;
    }

    public int get_5gStatus() {
        return _5gStatus;
    }

    public void set_5gStatus(int _5gStatus) {
        this._5gStatus = _5gStatus;
    }

    public int _24gStatus;
    public int _5gStatus;

    public int get_24gChannel() {
        return _24gChannel;
    }

    public void set_24gChannel(int _24gChannel) {
        this._24gChannel = _24gChannel;
    }

    public int get_5gChannel() {
        return _5gChannel;
    }

    public void set_5gChannel(int _5gChannel) {
        this._5gChannel = _5gChannel;
    }

    public int _24gChannel;
    public int _5gChannel;

}
