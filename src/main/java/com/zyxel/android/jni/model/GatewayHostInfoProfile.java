package com.zyxel.android.jni.model;

import com.zyxel.android.jni.corelib.JNICall;

/**
 * A data class representing gateway's information in terms of model name, ip address, firmware version and serial number
 * @see JNICall#queryGatewayList()
 */
public class GatewayHostInfoProfile {
    public String hostName;
    public String hostMAC;
    public int autoConfigEnable;

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getHostMAC() {
        return hostMAC;
    }

    public void setHostMAC(String hostMAC) {
        this.hostMAC = hostMAC;
    }

    public int isAutoConfigEnable() {
        return autoConfigEnable;
    }

    public void setAutoConfigEnable(int autoConfigEnable) {
        this.autoConfigEnable = autoConfigEnable;
    }
}
