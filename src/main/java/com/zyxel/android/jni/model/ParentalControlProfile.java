/****************************************************************************/
/*
 * Copyright (C) 2000-2013 ZyXEL Communications, Corp.
 * All Rights Reserved.
 *
 * ZyXEL Confidential; 
 * Protected as an unpublished work, treated as confidential, 
 * and hold in trust and in strict confidence by receiving party. 
 * Only the employees who need to know such ZyXEL confidential information 
 * to carry out the purpose granted under NDA are allowed to access. 
 * 
 * The computer program listings, specifications and documentation 
 * herein are the property of ZyXEL Communications, Corp. and shall 
 * not be reproduced, copied, disclosed, or used in whole or in part 
 * for any reason without the prior express written permission of 
 * ZyXEL Communications, Corp.
 * 
 */
/****************************************************************************/
package com.zyxel.android.jni.model;

import com.zyxel.android.jni.corelib.JNICall;

import java.io.Serializable;

/**
 * A data class representing a gateway's parental control rule information
 * @see JNICall#getParentalControlProfile()
 */
public class ParentalControlProfile implements Serializable {
	public String ID;
	public String EndDeviceMAC;
	public String Enable;
	public String ProfileName;
	public String SchedulingDays;
	public String SchedulingTimeStartHour;
	public String SchedulingTimeStartMin;
	public String SchedulingTimeStopHour;
	public String SchedulingTimeStopMin;

	public ParentalControlProfile(String id, String mac, String enable,
			String profileName, String schedulingDays,
			String schedulingTimeStartHour, String schedulingTimeStartMin,
			String schedulingTimeStopHour, String schedulingTimeStopMin) {
		ID = id;
		EndDeviceMAC = mac;
		Enable = enable;
		ProfileName = profileName;
		SchedulingDays = schedulingDays;
		SchedulingTimeStartHour = schedulingTimeStartHour;
		SchedulingTimeStartMin = schedulingTimeStartMin;
		SchedulingTimeStopHour = schedulingTimeStopHour;
		SchedulingTimeStopMin = schedulingTimeStopMin;
	}

	public ParentalControlProfile() {

	}

	/**
	 * Get parental control rule index
	 * @return index of the parental control rule
	 */
	public String getID() {
		return ID;
	}

	/**
	 * Get parental control rule mac address
	 * @return mac address of the parental control rule
	 */
	public String getMAC() {
		return EndDeviceMAC;
	}

	/**
	 * Get parental control rule status
	 * @return
	 */
	public String getStatus() {
		return Enable;
	}

	/**
	 * Get parental control rule name
	 * @return name of the parental control rule
	 */
	public String getProfileName() {
		return ProfileName;
	}

	/**
	 * Get parental control rule repeat days
	 * @return repeat days of the parental control rule
	 */
	public String getSchedulingDays() {
		return SchedulingDays;
	}

	/**
	 * Get parental control rule start hour
	 * @return start hour of the parental control rule
	 */
	public String getSchedulingTimeStartHour() {
		return SchedulingTimeStartHour;
	}
	/**
	 * Get parental control rule start minute
	 * @return start minute of the parental control rule
	 */
	public String getSchedulingTimeStartMin() {
		return SchedulingTimeStartMin;
	}
	/**
	 * Get parental control rule stop hour
	 * @return stop hour of the parental control rule
	 */
	public String getSchedulingTimeStopHour() {
		return SchedulingTimeStopHour;
	}
	/**
	 * Get parental control rule stop minute
	 * @return stop minute of the parental control rule
	 */
	public String getSchedulingTimeStopMin() {
		return SchedulingTimeStopMin;
	}

	public void setID(String id_arg) {
		ID = id_arg;
	}

	public void setMAC(String mac_arg) {
		EndDeviceMAC = mac_arg;
	}

	public void setStatus(String status_arg) {
		Enable = status_arg;
	}

	public void setProfileName(String profileName_arg) {
		ProfileName = profileName_arg;
	}

	public void setSchedulingDays(String schedulingDays_arg) {
		SchedulingDays = schedulingDays_arg;
	}

	public void setSchedulingTimeStartMin(String startMin_arg) {
		SchedulingTimeStartMin = startMin_arg;
	}

	public void setSchedulingTimeStartHour(String startHour_arg) {
		SchedulingTimeStartHour = startHour_arg;
	}

	public void setSchedulingTimeStopMin(String stopMin_arg) {
		SchedulingTimeStopMin = stopMin_arg;
	}

	public void setSchedulingTimeStopHour(String stopHour_arg) {
		SchedulingTimeStopHour = stopHour_arg;
	}

}
