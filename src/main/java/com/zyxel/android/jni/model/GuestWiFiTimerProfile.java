package com.zyxel.android.jni.model;

import com.zyxel.android.jni.corelib.JNICall;

/**
 * A data class representing guest wifi timer information
 * @see JNICall#getGuestWiFiTimer()
 */
public class GuestWiFiTimerProfile {

    public String hours;
    public String minutes;

    public GuestWiFiTimerProfile() {

    }

    /**
     * Constructor of GuestWiFiTimerProfile
     * @param hours
     * @param minutes
     */
    public GuestWiFiTimerProfile(String hours, String minutes) {
        this.hours = hours;
        this.minutes = minutes;
    }

    /**
     * Get guest wifi timer in terms of hours
     * @return hours of guest wifi timer
     */
    public String getHours() {
        return hours;
    }

    /**
     * Set guest wifi timer in terms of hours
     * @param hours hours of guest wifi timer
     */
    public void setHours(String hours) {
        this.hours = hours;
    }

    /**
     * Get guest wifi timer in terms of minutes
     * @return minutes of guest wifi timer
     */
    public String getMinutes() {
        return minutes;
    }

    /**
     * Set guest wifi timer in terms of minutes
     * @param minutes minutes of guest wifi timer
     */
    public void setMinutes(String minutes) {
        this.minutes = minutes;
    }


}
