package com.zyxel.android.jni.model;

import com.zyxel.android.jni.corelib.JNICall;

/**
 * A data class representing gateway's information in terms of model name, ip address, firmware version and serial number
 * @see JNICall#queryGatewayList()
 */
public class GatewayProfile {
    public String modelName;
    public String IP;
    public String firmwareVersion;
    public String serial;
    public String password;
    public int type;
    public int autoConfigEnable;

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }

    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    public void setFirmwareVersion(String firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getAutoConfigEnable() {
        return autoConfigEnable;
    }

    public void setAutoConfigEnable(int autoConfigEnable) {
        this.autoConfigEnable = autoConfigEnable;
    }

}
