package com.zyxel.android.jni.model;

/**
 * Created by scott on 7/22/16.
 */

public class WiFiAutoConfigApproveProfile {
    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    String mac;
    String action;
}
