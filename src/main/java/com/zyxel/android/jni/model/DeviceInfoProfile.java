package com.zyxel.android.jni.model;

/**
 * A data class representing a gateway's system information in terms of cpu usage, system time, up time.
 */
public class DeviceInfoProfile {
	public int CPU_Usage;
	public String SystemTime;
	public int UpTime;
	
	public DeviceInfoProfile(){
		
	}
	public DeviceInfoProfile(int cpuusage_arg, String systemtime_arg, int uptime_arg){
		this.CPU_Usage = cpuusage_arg;
		this.SystemTime = systemtime_arg;
		this.UpTime = uptime_arg;
	}

	/**
	 * Get gateway's cpu usage in term of percentage
	 * @return cpu usage
	 */
	public int getCPUUsage(){
		return this.CPU_Usage;
	}

	/**
	 * Get gateway's system time in terms of Tue Nov 3 14:23:05 GMT 2015 format
	 * @return system time
	 */
	public String getSystemTime(){
		return this.SystemTime;
	}

	/**
	 * Get gateway's up time in terms of million seconds
	 * @return up time
	 */
	public int getUpTime(){
		return this.UpTime;
	}
	
	public void setCPUUsage(int cpuusage_arg){
		this.CPU_Usage = cpuusage_arg;
	}
	public void setSystemTime(String systemtime_arg){
		this.SystemTime = systemtime_arg;
	}
	public void setUpTime(int uptime_arg){
		this.UpTime = uptime_arg;
	}
}
