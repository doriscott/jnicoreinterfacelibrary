package com.zyxel.android.jni.model;

import com.zyxel.android.jni.corelib.JNICall;

/**
 * A data class representing gateway's wan information
 * @see JNICall#getWanInfo()
 */
public class WanInfoProfile {
	String WanStatus;
    String WanIP;
    int WanPhyRate;
    String WanMAC;
    int WanTX;
    int WanRX;
    
    public WanInfoProfile(){
    	
    }

    /**
     * WanInfoProfile constructor
     * @param wanstatus_arg
     * @param wanip_arg
     * @param wanphyrate_arg
     * @param wanmac_arg
     * @param wantx_arg
     * @param wanrx_arg
     */
    public WanInfoProfile(String wanstatus_arg, String wanip_arg, int wanphyrate_arg, String wanmac_arg, int wantx_arg, int wanrx_arg){
    	this.WanStatus = wanstatus_arg;
    	this.WanIP = wanip_arg;
    	this.WanPhyRate = wanphyrate_arg;
    	this.WanMAC = wanmac_arg;
    	this.WanTX = wantx_arg;
    	this.WanRX = wanrx_arg;
    }

    /**
     * Get gateway's wan status
     * @return status in terms of "Disable", "LinkDown", "Down", ""
     */
    public String getWanStatus(){
    	return this.WanStatus;
    }

    /**
     * Get gateway's wan ip address
     * @return wan ip address of the gateway
     */
    public String getWanIP(){
    	return this.WanIP;
    }


    /**
     * Get gateway's wan physical rate
     * @return wan physical rate of the gateway
     */
    public int getWanPhyRate(){
    	return this.WanPhyRate;
    }

    /**
     * Get gateway's wan mac address
     * @return wan mac address of the gateway
     */
    public String getWanMAC(){
    	return this.WanMAC;
    }

    /**
     * Get gateway's wan tx rate
     * @return wan tx rate of the gateway
     */
    public int getWanTX(){
    	return this.WanTX;
    }

    /**
     * Get gateway's wan rx rate
     * @return wan rx rate of the gateway
     */
    public int getWanRX(){
    	return this.WanRX;
    }
    
    public void setWanStatus(String wanstatus_arg){
    	this.WanStatus = wanstatus_arg;
    }
    public void setWanIP(String wanip_arg){
    	this.WanIP = wanip_arg;
    }
    public void setWanPhyRate(int wanphyrate_arg){
    	this.WanPhyRate = wanphyrate_arg;
    }
    public void setWanMAC(String wanmac_arg){
    	this.WanMAC = wanmac_arg;
    }
    public void setWanTX(int wantx_arg){
    	this.WanTX = wantx_arg;
    }
    public void setWanRX(int wanrx_arg){
    	this.WanRX = wanrx_arg;
    }
}
