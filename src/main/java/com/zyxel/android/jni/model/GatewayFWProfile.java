package com.zyxel.android.jni.model;

import com.zyxel.android.jni.corelib.JNICall;

/**
 * A data class representing a gateway's new firmware information in terms of version, release date and status
 * @see JNICall#getNewGatewayFirmwareInfo()
 */
public class GatewayFWProfile {

    public int hasNewFirmware;
    public String newFirmwareVersion;
    public String newFirmwareReleaseDate;

    public GatewayFWProfile() {

    }

    public GatewayFWProfile(int hasnewfirmware, String newfirmwareversion, String newfirmwarereleasedate) {
        this.hasNewFirmware = hasnewfirmware;
        this.newFirmwareVersion = newfirmwareversion;
        this.newFirmwareReleaseDate = newfirmwarereleasedate;
    }

    /**
     * Get new firmware status
     * @return 0:no new firmware, 1:has new firmware
     */
    public int isHasNewFirmware() {
        return hasNewFirmware;
    }

    public void setHasNewFirmware(int hasNewFirmware) {
        this.hasNewFirmware = hasNewFirmware;
    }

    /**
     * Get new firmware version
     * @return firmware version
     */
    public String getNewFirmwareVersion() {
        return newFirmwareVersion;
    }

    public void setNewFirmwareVersion(String newFirmwareVersion) {
        this.newFirmwareVersion = newFirmwareVersion;
    }

    /**
     * Get new firmware release date
     * @return release date
     */
    public String getNewFirmwareReleaseDate() {
        return newFirmwareReleaseDate;
    }

    public void setNewFirmwareReleaseDate(String newFirmwareReleaseDate) {
        this.newFirmwareReleaseDate = newFirmwareReleaseDate;
    }
}
